﻿const exec = require('child_process').exec;
const http = require('http')
const forward = require('http-forward')

// Start the front-end code
exec('serve -s -l 8081', function(err, stdout, stderr) {
    if (err) {
        console.error(stderr || err);
        process.exit(-1);
    }
});

// Start the grpcwebproxy
exec(`./go/grpcwebproxy --backend_addr=${process.env.GRPC_IP}:8051 --run_tls_server=false --allow_all_origins=true --server_http_debug_port=8082`, function(err, stdout, stderr) {
    if (err) {
        console.error(stderr || err);
        process.exit(1);
    }
});

const server = http.createServer(function (req, res) {
    const { url } = req;
    const config = { target: 'http://127.0.0.1:8081', changeOrigin: true };
    if (url.startsWith('/grpcfacade.ExecutorService')) config.target = 'http://127.0.0.1:8082';
    if (url.startsWith('/graphql')) config.target = process.env.GRAPHQL_URL;
    if (url.startsWith('/proxy/')) {
        config.ignorePath = true;
        config.target = `http://${url.substring(7)}`;
    }
    req.forward = config;
    forward(req, res);
});
const PORT = process.env.PORT || 3000;
server.listen(PORT);
console.log('Server Listen at ' + PORT);
console.log('GraphQL URL: '+ process.env.GRAPHQL_URL);
console.log('GRPC Server IP: '+ process.env.GRPC_IP);