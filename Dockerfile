# build environment
FROM node:10 as builder

ARG PORT=80
ARG BRANCH_NAME
RUN echo "Branch is $BRANCH_NAME"

RUN mkdir /usr/src/app
WORKDIR /usr/src/app
ENV PATH /usr/src/app/node_modules/.bin:$PATH
COPY package.json /usr/src/app/
COPY package-lock.json /usr/src/app/

RUN apt-get update
RUN apt-get install build-essential

RUN PATH=/user/local/bin:$PATH npm install --scripts-prepend-node-path=true

COPY . /usr/src/app
RUN if [ "$BRANCH_NAME" = "dev" ] ; then NODE_PATH=. npm run-script build --scripts-prepend-node-path=true; fi
RUN if [ "$BRANCH_NAME" = "staging" ] ; then NODE_PATH=. npm run-script build-opt --scripts-prepend-node-path=true; fi
RUN if [ "$BRANCH_NAME" = "prod" ] ; then NODE_PATH=. npm run-script build-opt --scripts-prepend-node-path=true; fi


# build grpcwebproxy
from golang:alpine as grpcwebproxy

RUN apk --no-cache add git curl
RUN curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh && \
    mkdir -p $GOPATH/src/github.com/improbable-eng/grpc-web && \
    cd $GOPATH/src/github.com/improbable-eng && \
    git clone https://github.com/improbable-eng/grpc-web && \
    cd grpc-web && \
    dep ensure
RUN go get -u github.com/improbable-eng/grpc-web/go/grpcwebproxy

# deploy
FROM node:alpine
ARG GRAPHQL_URL
ARG GRPC_IP
RUN echo "GraphQl URL is $GRAPHQL_URL"
RUN echo "GRPC URL is $GRPC_IP"
ENV PORT=80
ENV GRAPHQL_URL=${GRAPHQL_URL}
ENV GRPC_IP=${GRPC_IP}
RUN npm install -g serve
RUN mkdir -p /usr/src/app/go
WORKDIR /usr/src/app
COPY --from=grpcwebproxy /go/bin/grpcwebproxy ./go
COPY --from=builder /usr/src/app/dist/browser .
COPY ./server server
RUN cd /usr/src/app/server && npm install
EXPOSE 80
CMD ["node","server/server.js"]