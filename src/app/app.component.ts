import { Component, OnInit } from '@angular/core';
import { GaService } from 'src/services/ga.service';
import { AuthService } from 'src/services/auth.service';
import { PromptUpdateService } from './prompt-update-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
  title = 'Equeum';
  profile;
  showNav = () => !!localStorage.getItem('access_token');
  constructor(public auth: AuthService, private ga: GaService, private updates: PromptUpdateService) {
    this.updates.checkForUpdates();
  }
  async ngOnInit() {
    this.auth.localAuthSetup();
    this.auth.userProfile$.subscribe(profile => {
        if (!profile) return;
        this.profile = profile;
        this.ga.init(profile.sub);
      }
    );
  }
}
