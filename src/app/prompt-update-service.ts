import { Injectable } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { interval } from 'rxjs';

@Injectable()
export class PromptUpdateService {

    constructor(public updates: SwUpdate) {
        if (updates.isEnabled) {
            interval(1000 * 60 * 2).subscribe(() => updates.checkForUpdate()
                .then(() => console.log('Checking for Updates')));
        }
    }

    public checkForUpdates(): void {
        this.updates.available.subscribe(event => this.promptUser());
    }

    private promptUser(): void {
        if (document.hidden) {
            setTimeout(() => this.promptUser(), 1000);
            return;
        }
        if (window.confirm('New version available, Try it?')) {
            this.updates.activateUpdate().then(() => document.location.reload());
        }
    }
}
