import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminGuard } from 'src/guards/admin.guard';
import { AuthGuard } from 'src/guards/auth.guard';

const routes: Routes = [
  {
    path: 'callback',
    loadChildren: () => import('src/auth.callback/auth.callback.module').then(m => m.AuthCallbackModule)
  },
  { path: 'admin', loadChildren: () => import('src/admin/admin.module').then(m => m.AdminModule), canActivate: [AdminGuard] },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'signUp',
    redirectTo: '/home',
    pathMatch: 'full',
    canActivate: [AuthGuard],
  },
  {
    path: '',
    loadChildren: () => import('src/applications/root.apps/root.apps.module').then(m => m.RootAppsAppModule),
    canActivate: [AuthGuard],
  },
  {
    path: '',
    loadChildren: () => import('src/applications/root.apps/root.apps.module').then(m => m.RootAppsAppModule),
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
