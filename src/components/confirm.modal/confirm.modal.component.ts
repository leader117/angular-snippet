import { Component, Input, Output, EventEmitter } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

/**
 * @title App main search box
 */
@Component({
  selector: 'app-confirm-modal',
  templateUrl: 'confirm.modal.component.html',
  styleUrls: ['confirm.modal.component.css'],
})
export class ConfirmModalComponent {
  @Input() message = 'Do you want to confirm?';
  @Input() confirmButtonText = 'Yes';
  @Input() cancelButtonText = 'No';
  @Input() isYesNo = true;
  @Output() confirmed = new EventEmitter();
  @Output() cancelled = new EventEmitter();
  constructor(public bsModalRef: BsModalRef) {
  }
  confirm(): void {
    this.confirmed.emit();
    this.bsModalRef.hide();
  }
  decline(): void {
    this.cancelled.emit();
    this.bsModalRef.hide();
  }
}
