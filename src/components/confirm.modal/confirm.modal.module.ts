import { NgModule } from '@angular/core';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ConfirmModalComponent } from './confirm.modal.component';
import { ConfirmModalService } from './confirm.modal.service';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ConfirmModalComponent
  ],
  imports: [CommonModule, FormsModule, ModalModule],
  providers: [ConfirmModalService],
  bootstrap: [],
  exports: [ConfirmModalComponent],
  entryComponents: [ConfirmModalComponent],
})
export class ConfirmModalModule { }
