import { Injectable } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ConfirmModalComponent } from './confirm.modal.component';

@Injectable()
export class ConfirmModalService {
    constructor(private modalService: BsModalService) { }

    show(state, onConfirmed?: () => void, onCancelled?: () => void) {
        const bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState: state });
        let $1;
        let $2;
        $1 = bsModalRef.content.confirmed.subscribe(() => {
            if (onConfirmed) onConfirmed();
            $1.unsubscribe();
            $2.unsubscribe();
        });
        $2 = bsModalRef.content.cancelled.subscribe(() => {
            if (onCancelled) onCancelled();
            $1.unsubscribe();
            $2.unsubscribe();
        });
    }
    showMessage(message: string) {
        this.modalService.show(ConfirmModalComponent, { initialState: { message, isYesNo: false } });
    }
}
