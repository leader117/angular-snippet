import { AppAgGridComponent } from './grid.component';
import { ZsGridColumn } from './grid.column';
import { ZsGridCellValueUnit } from './ZsGridCellValueUnit';
import * as numeral from 'numeral';
import * as moment from 'moment-timezone';
import { generateSvgOfPoints } from './chart.svg';

export const UnitFormatter = {
  1: { symbol: 'K', value: 1000 },
  2: { symbol: 'M', value: 1000000 },
  3: { symbol: 'B', value: 1000000000 },
};

// Base class for all grid renders
export class GridBaseRender {
  static instance = new GridBaseRender();
  render(grid: AppAgGridComponent, row: any, column: ZsGridColumn, initHtml?: string) {
    return this.default(row, column);
  }
  default(row: any, column: ZsGridColumn) {
    return row[column.code];
  }
}

// Render the masked data by "UnLock" link
export class GridMaskDataRender extends GridBaseRender {
  static instance = new GridMaskDataRender();
  render(grid: AppAgGridComponent, row: any, column: ZsGridColumn, initHtml?: string) {
    return '<div class="zs-grid-cell-link">Unlock</div>';
  }
}

// Render the clicked cells
export class GridLinkRender extends GridBaseRender {
  static instance = new GridLinkRender();
  render(grid: AppAgGridComponent, row: any, column: ZsGridColumn, initHtml?: string) {
    const val = initHtml || this.default(row, column);
    const { screen, params } = column.onPress.angularRoute;
    const queryParams = { ...params };
    const url = Object.keys(queryParams).map(k => `${k}=${this.processParam(queryParams[k], row)}`).join('&');
    row.navigateUrl = `${screen || location.pathname}?${url}`;
    return `<a href="${row.navigateUrl}" onclick="return !!event.ctrlKey" class="zs-grid-cell-link">${val}</a>`;
  }
  private processParam(param, row) {
    if (!param.startsWith(':')) return param;
    return encodeURIComponent(row[param.substring(1)]);
  }
}


// Render the numbers where the grid uses the units (Millions, Thousands, ...)
export class GridNumberUnitRender extends GridBaseRender {
  static instance = new GridNumberUnitRender();
  static isSuitable(grid: AppAgGridComponent, row: any, column: ZsGridColumn) {
    const { formatValue } = column.format;
    const hasFraction = formatValue && (formatValue.indexOf(',') > 0 || parseInt(formatValue, 10));
    const supportUnitFormat = hasFraction && grid.cellValueUnit !== ZsGridCellValueUnit.Actual;
    return supportUnitFormat;
  }
  render(grid: AppAgGridComponent, row: any, column: ZsGridColumn, initHtml?: string) {
    if (!GridNumberUnitRender.isSuitable(grid, row, column)) return initHtml || this.default(row, column);
    return this.formatNumberTo(row[column.code], grid.cellValueUnit);
  }
  private formatNumberTo(num: any, unitType: ZsGridCellValueUnit) {
    const si = UnitFormatter[unitType] || { symbol: '', value: 1 };
    const num2 = numeral(num).value() / si.value;
    if (Math.abs(num2) < 0.001 && unitType > 1) return this.formatNumberTo(num, unitType - 1);
    const ff = numeral(num2).format('0,0.00').replace('.00', '');
    return ff && ff !== '0' ? ff + si.symbol : num;
  }
}


// Render the numeral cells
export class GridNumeralRender extends GridBaseRender {
  static instance = new GridNumeralRender();
  render(grid: AppAgGridComponent, row: any, column: ZsGridColumn, initHtml?: string) {
    const { formatValue } = column.format;
    return this.numeralFormat(row[column.code], formatValue);
  }
  private numeralFormat(val: any, formatValue: string) {
    if (val == null || val === '' || val === undefined) { return ''; }
    return numeral(val).format(formatValue);
  }
}

// Render the Numbers cells
export class GridNumberRender extends GridBaseRender {
  static instance = new GridNumberRender();
  render(grid: AppAgGridComponent, row: any, column: ZsGridColumn, initHtml?: string) {
    const { formatValue, cellsFormat } = column.format;
    return this.numberFormat(row[column.code], cellsFormat, Number(formatValue));
  }
  private numberFormat(val: any, cellsFormat: string = null, decimals: number = null) {
    if (val == null || val === '' || val === undefined) { return ''; }
    val = numeral(val);
    decimals = Number(decimals) || 0;
    const x = decimals ? `0.${'0'.repeat(decimals)}` : '0';
    if (cellsFormat === 'd') { return val.format(x); }
    if (cellsFormat === 'p') { return `${val.format(x)} %`; }
    return val.format();
  }
}


// Render the dates cells
export class GridDateRender extends GridBaseRender {
  static instance = new GridDateRender();
  render(grid: AppAgGridComponent, row: any, column: ZsGridColumn, initHtml?: string) {
    return moment(row[column.code]).format('MM/DD/YYYY');
  }
}

// Render the svg chart cells
export class GridSvgChartRender extends GridBaseRender {
  static instance = new GridSvgChartRender();
  render(grid: AppAgGridComponent, row: any, column: ZsGridColumn, initHtml?: string) {
    return generateSvgOfPoints(row[column.code], column);
  }
}

// Render the symbol_value cells
export class GridSymbolValueRender extends GridBaseRender {
  static instance = new GridSymbolValueRender();
  render(grid: AppAgGridComponent, row: any, column: ZsGridColumn, initHtml?: string) {
    const freq = row.price_frequency;
    const link = GridLinkRender.instance.render(grid, row, column);
    if (!freq) return link;
    // return `${link} <sub class="frequency-type-label" title="${freq}">${freq[0]}</sub>`;
    return `${link} <small class="frequency-type-label" title="${freq}">${freq[0]}</small>`;
  }
}
