export class ZsGridColumn {
    code: string;
    name: string;
    label?: string;
    enableSort = true;
    greenRedWhite = false;
    canAccess = true;
    width?: number;
    fixed = false;
    colorized = false;
    styles?: { textAlign: string, color?: string } = { textAlign: 'left' };
    format?: { type: string, cellsFormat?: string, formatValue?: string } = { type: 'text' };
    onPress?: { angularRoute: { emitMessage?: boolean, screen: string, params?: any } } = undefined;
    constructor(params?: any) {
        if (!params) return;
        this.code = params.code;
        this.name = params.name;
        if (params.label) this.label = params.label;
        this.enableSort = params.enableSort;
        if (params.width) this.width = params.width;
        this.fixed = params.fixed;
        if (params.colorized) this.colorized = params.colorized;
        if (params.styles) this.styles = { ...this.styles, ...params.styles };
        if (params.format) this.format = { ...this.format, ...params.format };
        if (params.onPress) this.onPress = { ...this.onPress, ...params.onPress };
        this.greenRedWhite = params.green_red_white;
        this.canAccess = params.can_access_desktop;
    }
}
