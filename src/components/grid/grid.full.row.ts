
export interface GridFullRowRenderer {
    fullWidthCellRenderer(params: any): void;
    getRowHeight(params: any): number;
    isFullWidthCell(params: any): boolean;
}
