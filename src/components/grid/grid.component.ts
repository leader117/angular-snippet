import { Component, Input, OnInit, OnChanges, SimpleChanges, Output, EventEmitter, OnDestroy } from '@angular/core';
import { ApiService } from 'src/services/api';
import { ZsGridResult } from './grid.result';
import { AutoUnsubscribeComponent } from '../auto-unsubscribe';
import { AppRouteService } from 'src/services/app.route';
import { GridFullRowRenderer } from './grid.full.row';
import { compareNumber } from './compares/number';
import { ZsGridColumn } from './grid.column';
import { adjustElementRemainWindowHeight } from 'src/scripts/utilities';
import { ZsGridCellValueUnit } from './ZsGridCellValueUnit';
import {
  GridMaskDataRender, GridLinkRender, GridNumberUnitRender,
  GridBaseRender, GridNumeralRender, GridNumberRender,
  GridDateRender, GridSvgChartRender, GridSymbolValueRender
} from './grid-renders';
import { ISocketContainer, PricesSocketService } from 'src/services/prices.socket';

export class AppGridRowModel extends AutoUnsubscribeComponent implements ISocketContainer {
  rowModelType: string;
  isVir: boolean;
  isPricesSocketEnabled: boolean;
  pageData: ZsGridResult;
  url: string;
  urlParams: any = {};
  grid: AppAgGridComponent;
  rowCount: number; // behave as infinite scroll
  constructor(grid: AppAgGridComponent, isVir: boolean, private pricesSocket: PricesSocketService) {
    super();
    this.grid = grid;
    this.isVir = isVir;
    this.rowModelType = isVir ? 'serverSide' : null;
  }
  async getRows(params) {
    const req = params.request;
    const sort = req.sortModel[0];
    const sortField = sort && sort.colId && this.grid.agGrid.getColumnDef(sort.colId).field;
    const sortDirection = sort && sort.sort;
    const newSort = (sortField && this.pageData.sortField !== sortField) ||
      (sortDirection && this.pageData.sortDirection !== sortDirection);
    if (newSort || req.startRow > 0) {
      const myParams = {
        ...this.urlParams,
        recordstartindex: req.startRow,
        recordendindex: req.endRow,
        sortField,
        sortDirection,
      };
      this.pageData = await this.grid.api.getGridData(this.url, myParams);
      if (this.pageData.error) return params.failCallback();
    }
    const diff = (req.endRow - req.startRow) - this.pageData.rows.length;
    const lastRow = diff <= 0 ? -1 : req.endRow - diff;
    if (this.isPricesSocketEnabled) this.createSymbolChannel(this.pageData.rows);
    params.successCallback(this.pageData.rows, lastRow);
  }
  setDataSource(url: string, urlParams: any = {}, firstPage: ZsGridResult) {
    this.url = url;
    this.urlParams = urlParams;
    this.pageData = firstPage;
  }
  startPricesSocket() {
    console.log('Opening Socket for this grid');
    this.isPricesSocketEnabled = true;
    this.createSymbolChannel(this.grid.rowData || []);
  }
  private createSymbolChannel(rows) {
    const symbolIds = rows.map(s => s.symbol_id).map(s => s);
    if (!symbolIds.length) return;
    this.pricesSocket.createSymbolChannel(this, symbolIds.join());
  }
  private updateRowValue(rowNode, newData: any[]) {
    const { data } = rowNode;
    if (!data) return;
    const prices = newData.find(r => r.symbol_id === data.symbol_id);
    if (!prices) return;
    this.grid.columnDefs.forEach((c) => {
      if (prices[c.field] === undefined || prices[c.field] === data[c.field]) return;
      rowNode.setDataValue(c.field, prices[c.field]);
    });
  }
  async onSocketMessage(name: string, msg: any) {
    if (!this.grid || !this.grid.agGrid) return;
    if (name === 'prices' && msg && msg.length) {
      this.grid.agGrid.forEachNode((rowNode, index) => {
        this.updateRowValue(rowNode, msg);
      });
    }
  }
  async onSocketReConnected() {
  }
}
/**
 * @title App main search box
 */
@Component({
  selector: 'app-grid',
  templateUrl: 'grid.component.html',
  styleUrls: ['grid.component.css'],
  animations: []
})
export class AppAgGridComponent extends AutoUnsubscribeComponent implements OnInit, OnChanges, OnDestroy {
  agGrid = undefined;
  @Input() domLayout = 'autoHeight';
  cellValueUnit: ZsGridCellValueUnit = ZsGridCellValueUnit.Actual;
  @Input() columnDefs: any[] = [{ headerName: '', field: '' }];
  @Input() rowData = [];
  @Input() virtualEnabled = false;
  @Input() fullRowRenderer: GridFullRowRenderer;
  @Input() hideHeader = false;
  model: AppGridRowModel = new AppGridRowModel(this, false, this.pricesSocket);
  @Output() routerMessage = new EventEmitter<{ screen: string, params: any } | any>();
  @Output() cellClicked = new EventEmitter<any>();
  gridOptions: any = {
    // rowBuffer: 100,
    suppressColumnVirtualisation: true,
    groupUseEntireRow: true,
    blockLoadDebounceMillis: 500,
  };
  private urlWithParams: string;
  @Input() isLoading = false;
  @Input() gridData: ZsGridResult;
  columns: Array<ZsGridColumn>;
  constructor(public api: ApiService, private appRouter: AppRouteService, private pricesSocket: PricesSocketService) {
    super();
  }
  ngOnInit() {
    if (this.hideHeader) {
      this.gridOptions.headerHeight = 0;
    }
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes.virtualEnabled) {
      this.model = new AppGridRowModel(this, this.virtualEnabled, this.pricesSocket);
      if (this.virtualEnabled) this.domLayout = 'normal';
    }
    if (changes.fullRowRenderer && this.fullRowRenderer) {
      this.gridOptions.isFullWidthCell = p => this.fullRowRenderer.isFullWidthCell(p);
      this.gridOptions.getRowHeight = p => this.fullRowRenderer.getRowHeight(p);
      this.gridOptions.fullWidthCellRenderer = p => this.fullRowRenderer.fullWidthCellRenderer(p);
      this.gridOptions.headerHeight = 0;
    }
    if (changes.gridData && this.gridData) {
      this.setGridColumns(this.gridData);
      this.rowData = this.gridData.rows;
    }
  }
  ngOnDestroy(): void {
    super.ngOnDestroy();
    if (this.agGrid) this.agGrid.destroy();
    this.model.grid = null;
    this.model = null;
    this.loadGridData = null;
    this.setGridColumns = null;
  }
  setCellValueUnit(cellValueUnit: ZsGridCellValueUnit) {
    this.cellValueUnit = cellValueUnit;
    if (this.agGrid) { this.agGrid.redrawRows({ force: true }); }
  }
  onGridReady(params) {
    this.agGrid = params.api;
    this.adjustRemainWindowHeight();
  }
  loadGridData = async (url: string, params: any = {}, cancelIfSame?: boolean) => {
    while (!this.agGrid) {
      await new Promise(resolve => setTimeout(resolve, 100));
    }
    const newUrlParams = `${url},${JSON.stringify(params)}`;
    if (cancelIfSame && this.urlWithParams === newUrlParams) return;
    this.agGrid.showLoadingOverlay();
    this.isLoading = true;
    const data = await this.api.getGridData(url, params);
    this.isLoading = false;
    this.agGrid.hideOverlay();
    if (!this.setGridColumns) return;
    this.setGridColumns(data);
    if (this.virtualEnabled) {
      this.model.setDataSource(url, params, data);
      this.agGrid.setServerSideDatasource(this.model);
    }
    else this.rowData = data.rows;
    if (data.supportPricesSocket) this.model.startPricesSocket();
    this.urlWithParams = newUrlParams;
  }
  setGridColumns = (data: ZsGridResult) => {
    this.columns = data.columns;
    this.columnDefs = data.columns.map((c, index) => {
      const col: any = {
        headerName: c.name,
        field: c.code,
        width: c.width,
        pinned: c.fixed || index === 0 ? 'left' : '',
        sortable: c.enableSort,
        resizable: true,
        sort: data.sortField === c.code ? data.sortDirection : null,
        cellClass: [],
        headerClass: [],
        cellRenderer: p => p.data && this.pickRender(p.data, c),
        // suppressColumnMoveAnimation: true,
        enableCellChangeFlash: true,
      };
      if (c.format.type === 'number' || c.format.type === 'numeral') {
        col.comparator = compareNumber;
      }
      if (c.greenRedWhite) {
        col.cellClassRules = {
          'zs-grid-cell-positive': 'x > 0',
          'zs-grid-cell-negative': 'x < 0',
        };
      }
      if (c.format.type === 'direction') {
        col.cellClassRules = {
          'zs-grid-cell-positive': p => p.value === 'UP',
          'zs-grid-cell-negative': p => p.value === 'DOWN',
        };
      }
      const align = c.styles && c.styles.textAlign;
      if (!c.canAccess) col.cellClass.push('zs-grid-cell-center');
      else if (align === 'right') col.cellClass.push('zs-grid-cell-right');
      if (align === 'right') col.headerClass.push('zs-grid-header-cell-right');
      return col;
    });
    if (data.groupBy) {
      this.gridOptions.groupUseEntireRow = true;
      this.gridOptions.groupDefaultExpanded = -1;
      this.columnDefs.push({ headerName: data.groupBy, field: data.groupBy, width: 1, rowGroup: true, hide: true });
    }
    if (this.agGrid) this.agGrid.setColumnDefs(this.columnDefs);
  }
  private pickRender(row: any, column: ZsGridColumn) {
    let render: GridBaseRender;
    const { type: formatType } = column.format;

    if (!column.canAccess) render = GridMaskDataRender.instance;
    else if (column.code === 'symbol_value') render = GridSymbolValueRender.instance;
    else if (column.onPress && column.onPress.angularRoute) render = GridLinkRender.instance;
    else if (GridNumberUnitRender.isSuitable(this, row, column)) render = GridNumberUnitRender.instance;
    else if (formatType === 'numeral') render = GridNumeralRender.instance;
    else if (formatType === 'number') render = GridNumberRender.instance;
    else if (formatType === 'date') render = GridDateRender.instance;
    else if (formatType === 'chart') render = GridSvgChartRender.instance;
    else render = GridBaseRender.instance;
    return render.render(this, row, column);
  }
  onCellClicked(clickParams) {
    const { event, colDef, data: row } = clickParams;
    if (event.ctrlKey) return;
    if (this.columns) {
      const href = event.srcElement.getAttribute('href');
      const column = this.columns.find(c => c.code === colDef.field);
      if (column.onPress) {
        const { angularRoute } = column.onPress;
        if (angularRoute.emitMessage) {
          this.routerMessage.emit({ ...angularRoute, row, column });
          return;
        }
      }
      // if (href) this.appRouter.navigateByUrl(href);
    }
    this.cellClicked.emit(clickParams);
  }
  adjustRemainWindowHeight() {
    const g: any = document.getElementsByClassName('zs-ag-grid-normal')[0];
    // adjustElementRemainWindowHeight(g, 300);
  }
}
