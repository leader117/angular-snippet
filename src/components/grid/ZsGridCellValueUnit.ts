export enum ZsGridCellValueUnit {
  Actual,
  Thousands,
  Millions,
  Billions
}
