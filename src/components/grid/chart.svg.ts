import * as moment from 'moment-timezone';
import { ZsGridColumn } from './grid.column';
import { AppConstants } from 'src/services/AppConstants';
const QUICK_CHART_HORIZONTAL_MARGIN = 0.2;
const ROW_HEIGHT = 30;

function convertQuickChartDataToQuickChartProps(pointsArray, config: { width: number }) {
    const validPoints = [];
    let minValue: number;
    let maxValue: number;
    for (const p of pointsArray) {
        const date = moment(p.date);
        const value = parseFloat(p.value);
        if (date.isValid() && !Number.isNaN(value)) {
            if (minValue > value || minValue === undefined) {
                minValue = value;
            }
            if (maxValue < value || maxValue === undefined) {
                maxValue = value;
            }
            validPoints.push({ date, value });
        }
    }
    if (validPoints.length < 2) {
        return null;
    }
    const heightAmplitude = maxValue - minValue;
    const heightProportion = heightAmplitude !== 0
        ? ROW_HEIGHT * (1 - 2 * QUICK_CHART_HORIZONTAL_MARGIN) / heightAmplitude
        : 0;
    const widthProportion
        = config.width / getInterval(validPoints[0].date, validPoints[validPoints.length - 1].date);
    const coordinatePairs = validPoints.map((point) => {
        const x = getInterval(validPoints[0].date, point.date) * widthProportion;
        const y = heightAmplitude !== 0
            ? ROW_HEIGHT * (1 - QUICK_CHART_HORIZONTAL_MARGIN)
            - (point.value - minValue) * heightProportion
            : ROW_HEIGHT / 2;
        return `${x},${y}`;
    });
    return {
        points: coordinatePairs.join(' '),
        color: getQuickChartColor(validPoints),
        width: config.width - 20,
        height: ROW_HEIGHT,
    };
}

const getQuickChartColor = (pointsArray) => {
    const arrayLength = pointsArray.length;
    return pointsArray[arrayLength - 1].value >= pointsArray[arrayLength - 2].value ? AppConstants.GREEN_COLOR : AppConstants.RED_COLOR;
};

function getInterval(start, end) {
    return moment.duration(end.diff(start)).asHours();
}



export function generateSvgOfPoints(val: any, column: ZsGridColumn) {
    if (!Array.isArray(val)) { return ''; }
    const props = convertQuickChartDataToQuickChartProps(val, { width: (column.width || 120) });
    if (!props) return '';
    return `<svg width=${props.width} height=${props.height}>
                  <polyline
                      points="${props.points}"
                      fill="none"
                      stroke="${props.color}"
                      strokeWidth="1"
                  />
            </svg>`;
}