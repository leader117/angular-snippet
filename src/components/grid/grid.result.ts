import { ZsGridColumn } from './grid.column';

export class ZsGridResult {
    columns: Array<ZsGridColumn>;
    rows: Array<any>;
    groupBy?: string;
    totalRecords?: number;
    totalPages?: number;
    pageSize?: number;
    pageNum?: number;
    sortField?: string;
    sortDirection?: string;
    error?: any;
    supportPricesSocket?: boolean;
    constructor(params?: ZsGridResult) {
        if (!params) return;
        this.columns = params.columns;
        this.rows = params.rows;
        this.groupBy = params.groupBy;
        this.totalRecords = params.totalRecords;
        this.totalPages = params.totalPages;
        this.pageSize = params.pageSize;
        this.pageNum = params.pageNum;
        this.sortField = params.sortField;
        this.sortDirection = params.sortDirection;
        this.supportPricesSocket = params.supportPricesSocket;
        this.error = params.error;
    }
}
