import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import { SpinnerFlagModule } from 'src/components/spinner/spinner.module';
import { ApiService } from 'src/services/api';

import { AgGridModule } from 'ag-grid-angular';
import { AppAgGridComponent } from './grid.component';
import { PricesSocketService } from 'src/services/prices.socket';
import { LicenseManager } from 'ag-grid-enterprise';
LicenseManager.setLicenseKey('140.32_LLC_dba_Equeum_Equeum_2Devs1_SaaS_15_May_2019__MTU1Nzg3NDgwMDAwMA==d38e91b91ef09dd542011a11f9dd21c9');

@NgModule({
  declarations: [
    AppAgGridComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    SpinnerFlagModule,
    AgGridModule.withComponents([])
  ],
  providers: [ApiService, PricesSocketService],
  bootstrap: [],
  exports: [AppAgGridComponent],
})
export class AppAgGridModule { }
