import * as numeral from 'numeral';

export function compareNumber(n1: any, n2: any) {
    const x = numeral(n1).value();
    const y = numeral(n2).value();
    if (x === y) return 0;
    if (x > y) return 1;
    if (x < y) return -1;
}
