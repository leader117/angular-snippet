import { NgModule } from '@angular/core';
import { SpinnerFlagComponent } from './spinner.component';

@NgModule({
  declarations: [
    SpinnerFlagComponent
  ],
  imports: [],
  providers: [],
  bootstrap: [],
  exports: [SpinnerFlagComponent],
})
export class SpinnerFlagModule { }
