import { Component, Input } from '@angular/core';

/**
 * @title App main search box
 */
@Component({
  selector: 'spinner-flag',
  templateUrl: 'spinner.component.html',
  styleUrls: ['spinner.component.css'],
})
export class SpinnerFlagComponent {
  @Input() message: string = 'Loading...';
  constructor() {
  }
  async ngOnInit() {
  }
}