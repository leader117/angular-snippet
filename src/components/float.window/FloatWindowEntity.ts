export const DEFAULT_ZINDEX = 1000;
export class FloatWindowSettings {
    static InstanceCount = 0;
    isMinimized = false;
    width = 150;
    height = 150;
    minWidth = 50;
    minHeight = 20;
    isClosed = false;
    beforeMinimizationHeight = 150;
    x = 10;
    y = 50;
    zIndex = DEFAULT_ZINDEX + (FloatWindowSettings.InstanceCount++);
}
export class FloatWindowEntity {
    settings = new FloatWindowSettings();
    title: string;
    data: any = {};
    public adjustPosition(clientWidth: number, clientHeight: number) {
        const { settings } = this;
        settings.width = Math.min(clientWidth * 0.5, 500);
        settings.height = Math.min(clientHeight * 0.5, 500);
        settings.x = (clientWidth - settings.width) / 2;
        settings.y = clientHeight - settings.height + 20;
    }
}
