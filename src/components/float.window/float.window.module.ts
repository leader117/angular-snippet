import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import { FloatWindowComponent } from './float.window.component';
import { AppLoaderModule } from 'src/applications/base/app.loader/app.loader.module';
import { AngularDraggableModule } from 'angular2-draggable';

@NgModule({
  declarations: [
    FloatWindowComponent
  ],
  imports: [CommonModule, FormsModule, AppLoaderModule, AngularDraggableModule],
  providers: [],
  bootstrap: [],
  exports: [FloatWindowComponent],
})
export class FloatWindowModule { }
