import { Component, Input, Output, EventEmitter, OnChanges, SimpleChanges, OnInit, ElementRef } from '@angular/core';
import { UpdateObj } from 'src/scripts/utilities';
import { FloatWindowEntity, FloatWindowSettings, DEFAULT_ZINDEX } from './FloatWindowEntity';

@Component({
  selector: 'app-float-window',
  templateUrl: './float.window.component.html',
  styleUrls: ['./float.window.component.less'],
})
export class FloatWindowComponent {
  @Input() public model = new FloatWindowEntity();
  @Output()
  public change = new EventEmitter<{ action: string }>();

  public onMoveEnd({ x, y }) {
    UpdateObj(this.model.settings, {
      alreadyMoved: true,
      x,
      y,
    });
    this.change.emit({ action: 'onMoveEnd' });
  }

  public close() {
    this.model.settings.isClosed = true;
    this.change.emit({ action: 'close' });
  }

  public onResizeStop(element: HTMLElement) {
    UpdateObj(this.model.settings, {
      width: element.clientWidth,
      height: element.clientHeight,
    });
    this.change.emit({ action: 'onResizeStop' });
  }

  public selectWidget(element: HTMLElement) {
    this.model.settings.zIndex = ++FloatWindowSettings.InstanceCount + DEFAULT_ZINDEX;
    element.style.zIndex = `${this.model.settings.zIndex}`;
    this.change.emit({ action: 'selectWidget' });
  }

  public minimizeItem(element: HTMLElement) {
    const { settings } = this.model;
    settings.isMinimized = !settings.isMinimized;

    if (settings.isMinimized) {
      settings.width = settings.width ? settings.width : element.clientWidth;
      settings.beforeMinimizationHeight = element.clientHeight;
    }

    settings.height = settings.isMinimized ? 26 : settings.beforeMinimizationHeight || 150;
    this.change.emit({ action: 'minimizeItem' });
  }
}
