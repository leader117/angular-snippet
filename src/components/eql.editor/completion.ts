
export const CompletionItemProvider = (functions) => ({
    provideCompletionItems(model, position) {
        // console.log(model, position);
        return {
            suggestions: JSON.parse(JSON.stringify(functions)),
        };
    },

    // resolveCompletionItem(model, position, item, token) {
    //   return item;
    // },
}) as any;

export const SignatureHelpProvider = (functions) => ({
    signatureHelpTriggerCharacters: ['(', ','],
    provideSignatureHelp(model, position, token): monaco.languages.SignatureHelpResult {
        // console.log(position, token);
        const textUntilPosition = model.getValueInRange({
            startLineNumber: position.lineNumber,
            startColumn: 1,
            endLineNumber: position.lineNumber,
            endColumn: position.column,
        });
        const matches = textUntilPosition.includes('(') && textUntilPosition.match(/[a-zA-Z_]+\(/g);
        // TODO: need upgrade
        const functionName = matches && matches.pop().split('(')[0];
        const startIndex = textUntilPosition.lastIndexOf(functionName);
        const activeParameter = textUntilPosition.substr(startIndex, position.column).split(',').length - 1;

        return {
            value: {
                activeSignature: 0,
                activeParameter,
                signatures: functions.filter(f => {
                    // console.log(f.label, functionName);
                    return f.label.startsWith(functionName);
                }),
            },
            dispose() { }
        };
    },
}) as monaco.languages.SignatureHelpProvider;
