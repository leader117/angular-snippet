export const EQL_THEME: any = {
    base: 'vs-dark',
    inherit: true,
    rules: [
        { token: '', foreground: '687794' },
        { token: 'delimiter', foreground: '687794' },
        { token: 'comment', foreground: '00B476' },
        { token: 'keyword', foreground: 'f5a623' },
        { token: 'identifier', foreground: '6E96FC' },
        { token: 'variable', foreground: 'ff4837' },
        { token: 'number', foreground: '00f19e' },
    ],
    colors: {
        'editor.foreground': '#687794',
        'editor.background': '#181D29',
        'editorLineNumber.foreground': '#3A425A',
        'editorLineNumber.activeForeground': '#3A425A',
        'editorCursor.foreground': '#687794',

        'editor.lineHighlightBackground': '#3A425A',
        'editor.lineHighlightBorder': '#3A425A',

        'editorSuggestWidget.background': '#232737',
        'editorSuggestWidget.border': '#353C4E',
        'editorSuggestWidget.foreground': '#687794',
        'editorSuggestWidget.highlightForeground': '#00B476',
        'editorSuggestWidget.selectedBackground': '#3A425A',

        'list.hoverBackground': '#3A425A',

        'editorWidget.background': '#232737',
        'editorWidget.border': '#353C4E',

        'input.background': '#232737',
        'input.border': '#232737',
    },
};
