export const EQL_CONFIG: monaco.languages.LanguageConfiguration = {
    wordPattern: /(-?\d*\.\d\w*)|([^\`\~\!\@\#\%\^\&\*\(\)\-\=\+\[\{\]\}\\\|\;\:\'\"\,\.\<\>\/\?\s]+)/g,
    comments: {
        lineComment: '//',
        blockComment: ['/*', '*/'],
    },
    brackets: [
        ['[', ']'],
        ['(', ')'],
    ],
    autoClosingPairs: [
        { open: '[', close: ']' },
        { open: '(', close: ')' },
        { open: '"', close: '"' },
        { open: '\'', close: '\'' },
    ],
    surroundingPairs: [
        { open: '[', close: ']' },
        { open: '(', close: ')' },
        { open: '"', close: '"' },
        { open: '\'', close: '\'' },
        { open: '<', close: '>' },
    ]


};

export const EQL_LANGUAGE: any = {
    defaultToken: '',
    tokenPostfix: '.eql',
    keywords: ['as', 'open', 'low', 'high', 'close', 'Volume', 'with', 'where', 'UNIVERSE', 'start_date', 'end_date', 'names'],
    snippets: {
        // tslint:disable-next-line: max-line-length
        with: 'with start_date=${1:YYYY}.${2:MM}.${3:DD}, end_date=${4:YYYY}.${5:MM}.${6:DD}, rebalance_period=${7:2w}, bar_size=${8:BS_1MIN}'
    },
    operators: ['->', '='],
    symbols: /[=>-]+/,
    tokenizer: {
        root: [
            [/[ \t\r\n]+/, 'white'],
            [/\/\/.*$/, 'comment'],
            [/[\(\)\[\]]/, '@brackets'],
            [
                /\[a-zA-Z\sas]\w*/,
                {
                    cases: {
                        '@keywords': 'keyword',
                        '@default': 'variable',
                    },
                },
            ],
            [
                /[a-zA-Z]\w*/,
                {
                    cases: {
                        '@keywords': 'keyword',
                        '@default': '',
                    },
                },
            ],
            [
                /@symbols/,
                {
                    cases: {
                        '@operators': 'keyword',
                    },
                },
            ],
            [/\d*\.\d+([eE][\-+]?\d+)?/, 'number.float'],
            [/0[xX][0-9a-fA-F]+/, 'number.hex'],
            [/\d+/, 'number'],
        ],
    },
};
export function getEqlKeywords() {
    return EQL_LANGUAGE.keywords.map(k => ({
        label: k,
        kind: monaco.languages.CompletionItemKind.Keyword,
        documentation: '',
        insertText: k
    }));
}
export function getEqlSnippets() {
    return Object.keys(EQL_LANGUAGE.snippets).map(k => ({
        label: k,
        kind: monaco.languages.CompletionItemKind.Snippet,
        insertTextRules: monaco.languages.CompletionItemInsertTextRule.InsertAsSnippet,
        detail: EQL_LANGUAGE.snippets[k],
        insertText: EQL_LANGUAGE.snippets[k]
    }));
}
