import { Component, Input, Output, EventEmitter, OnInit, OnChanges, OnDestroy, ViewChild } from '@angular/core';
import { matchAll } from 'src/scripts/utilities';
import { ExtendEqlEditorModel } from './eql.editor.model.extended';

/**
 * @title App main search box
 */
@Component({
  selector: 'app-eql-editor',
  templateUrl: 'eql.editor.component.html',
  styleUrls: ['eql.editor.component.less'],
})
export class EqlEditorComponent implements OnInit, OnChanges, OnDestroy {
  @Input() content = 'x as MSFT.close';
  @Input() disabled = false;
  @Input() config = {
    theme: 'vs-dark', // 'equeum-dark',
    language: 'eql',
    automaticLayout: true,
    glyphMargin: true,
    fontFamily: 'Menlo Regular',
    /*fontSize: '14px',
    lineHeight: '30px',
    cursorBlinking: 'smooth',
    wrappingIndent: 'same',
    scrollBeyondLastLine: false,*/
    minimap: {
      enabled: false,
    },
    scrollbar: {
      useShadows: false,
    }
  };
  @Output() contentChange = new EventEmitter();
  @Output() changeCursor = new EventEmitter<{ line: number, column: number }>();
  @Output() clickDecoration = new EventEmitter<{ position: monaco.Position, classNames: string[] }>();
  editor: monaco.editor.ICodeEditor;
  model: monaco.editor.ITextModel;
  deltaDecorations = [];
  constructor() {
  }
  async ngOnInit() {
  }
  ngOnDestroy() {
  }
  ngOnChanges(params) {
  }
  onInit(editor) {
    // window['editor11'] = editor;
    this.editor = editor;
    this.model = editor.getModel();
    const extended =  ExtendEqlEditorModel(editor.getModel());
    editor.onDidChangeCursorPosition(ev => {
      const { position } = ev;
      this.changeCursor.emit({ line: position.lineNumber, column: position.column });
      const range = new monaco.Range(position.lineNumber, 1, position.lineNumber, 1);
      const options = {
        isWholeLine: true,
        glyphMarginClassName: 'loupeGlyphDecoration',
        glyphMarginHoverMessage: { value: 'Run backtest for current line' }
      };
      extended.createDecorations([{ range, options }], 'onDidChangeCursorPosition');
    });
    editor.onMouseDown(e => {
      const arr = e.target.element.classList.value.split(' ').filter(s => s.includes('Decoration'));
      if (arr.length) {
        this.clickDecoration.emit({ position: e.target.position, classNames: arr });
      }
    });
  }
  public onValueChange(value: string): void {
    this.content = value;
    this.contentChange.emit(value);
    ExtendEqlEditorModel(this.model).highlightAllVariables();
  }
}
