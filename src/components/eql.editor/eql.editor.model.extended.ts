import { matchAll } from 'src/scripts/utilities';
import { PlotDpoDragDropDragHandleOptions } from 'highcharts';

export function ExtendEqlEditorModel(model: monaco.editor.IModel) {
    // tslint:disable-next-line: no-string-literal
    const buffer = model['ExtendEqlEditorModel'] || {};
    // tslint:disable-next-line: no-string-literal
    model['ExtendEqlEditorModel'] = buffer;
    if (!buffer.deltaDecorations) buffer.deltaDecorations = {};
    const funcs = {
        createDecorations(arr: { range: monaco.Range, options: monaco.editor.IModelDecorationOptions }[], key: string) {
            buffer.deltaDecorations[key] = model.deltaDecorations(buffer.deltaDecorations[key], arr);
        },
        clearDecorations(key: string) {
            buffer.deltaDecorations[key] = model.deltaDecorations(buffer.deltaDecorations[key], []);
        },
        getCodeInRange(startLine: number, endLine: number) {
            const partOfTheText = model.getValueInRange({
                startLineNumber: startLine,
                startColumn: 0,
                endLineNumber: endLine,
                endColumn: 10,
            });
            return partOfTheText;
        },
        /**
         * Parse the code and return all variables
         * Variable is something like x as MSFT.close
         */
        getAllVariables() {
            const mm = matchAll(model.getValue(), '\\b[a-zA-Z_]* +as ').map(m => {
                const pos = model.getPositionAt(m.index);
                const name = model.getWordAtPosition(pos).word;
                return {
                    pos,
                    name,
                    range: new monaco.Range(pos.lineNumber, pos.column, pos.lineNumber, pos.column + name.length)
                };
            });
            return mm;
        },
        highlightAllVariables() {
            const options: monaco.editor.IModelDecorationOptions = {
                inlineClassName: 'eqlVariableDecoration',
                hoverMessage: { value: 'Click to fetch variable\'s value' }
            };
            const arr = funcs.getAllVariables().map(v => (
                {
                    range: v.range,
                    options
                }
            ));
            funcs.createDecorations(arr, 'highlightAllVariables');
        }
    };
    return funcs;
}
