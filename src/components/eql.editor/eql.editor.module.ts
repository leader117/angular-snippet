import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MonacoEditorModule, NgxMonacoEditorConfig } from 'ngx-monaco-editor';
import { EqlEditorComponent } from './eql.editor.component';
import { FormsModule } from '@angular/forms';
import { EQL_LANGUAGE, EQL_CONFIG, getEqlKeywords, getEqlSnippets } from './eql.language';
import { EQL_THEME } from './eql.theme';
import { CompletionItemProvider, SignatureHelpProvider } from './completion';
import { GRPCService } from 'src/grpc/grpc.service';
import { ExtendEqlEditorModel } from './eql.editor.model.extended';
const ENGINE_FUNCTIONS = [];
let EngineWaiter;

const monacoConfig: NgxMonacoEditorConfig = {
  baseUrl: './assets',
  defaultOptions: { scrollBeyondLastLine: false },
  onMonacoLoad: editorWillMount
};
@NgModule({
  declarations: [
    EqlEditorComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    MonacoEditorModule.forRoot(monacoConfig)
  ],
  exports: [
    EqlEditorComponent
  ],
  providers: [GRPCService]
})
export class EqlEditorModule {
  constructor(private grpc: GRPCService) {
    EngineWaiter = this.grpc.getLanguageInfo().then(funcs => {
      EngineWaiter = null;
      funcs.forEach(f => {
        ENGINE_FUNCTIONS.push({
          label: f.name,
          // kind: monaco.languages.CompletionItemKind.Function,
          documentation: f.help,
          insertText: f.name,
          parameters: f.parameters.map(p => ({ label: p.name, documentation: p.help }))
        });
      });
    });
  }
}


export async function editorWillMount() {
  if (EngineWaiter) await EngineWaiter;
  if (!monaco.languages.getLanguages().some(({ id }) => id === 'eql')) {
    monaco.languages.register({ id: 'eql' });
    monaco.languages.setMonarchTokensProvider('eql', EQL_LANGUAGE);
    monaco.languages.setLanguageConfiguration('eql', EQL_CONFIG);
    monaco.editor.defineTheme('equeum-dark', EQL_THEME);
    const signs = ENGINE_FUNCTIONS.map(f => ({
      ...f,
      label: `${f.label}(${f.parameters.map(p => p.label).join(' , ')})`
    }));
    const arr = [...getEqlKeywords(), ...getEqlSnippets(), ...ENGINE_FUNCTIONS];
    monaco.languages.registerCompletionItemProvider('eql', CompletionItemProvider(arr));
    monaco.languages.registerSignatureHelpProvider('eql', SignatureHelpProvider(signs));

    monaco.languages.registerHoverProvider('eql', {
      provideHover: (model, position) => {
        return;
        console.log(model, model.getWordAtPosition(position));
        return {
          range: new monaco.Range(1, 1, model.getLineCount(), model.getLineMaxColumn(model.getLineCount())),
          contents: [
            { value: '**bold** _italics_ regular' }
          ]
        };
      }
    });
  }
}
