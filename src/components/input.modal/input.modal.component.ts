import { Component, Input, Output, EventEmitter } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

/**
 * @title App main search box
 */
@Component({
  selector: 'app-input-modal',
  templateUrl: 'input.modal.component.html',
  styleUrls: ['input.modal.component.css'],
})
export class InputModalComponent {
  @Output() confirmed = new EventEmitter<string>();
  @Output() cancelled = new EventEmitter();
  @Input() title = '';
  @Input() value = '';
  constructor(public bsModalRef: BsModalRef) {
  }
  confirm(): void {
    this.confirmed.emit(this.value);
    this.bsModalRef.hide();
  }
  decline(): void {
    this.cancelled.emit();
    this.bsModalRef.hide();
  }
}
