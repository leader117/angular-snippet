import { Injectable, EventEmitter } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { InputModalComponent } from './input.modal.component';

@Injectable()
export class InputModalService {
    constructor(private modalService: BsModalService) {}

    show(title: string, value: string, onConfirmed?: (value: string) => void) {
        const bsModalRef = this.modalService.show(InputModalComponent, { initialState: { title, value } });
        const $1 = bsModalRef.content.confirmed.subscribe((value1: string) => {
            if (onConfirmed) onConfirmed(value1);
            $1.unsubscribe();
        });
    }
}
