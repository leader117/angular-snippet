import { NgModule } from '@angular/core';
import { InputModalComponent } from './input.modal.component';
import { InputModalService } from './input.modal.service';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';

@NgModule({
  declarations: [
    InputModalComponent
  ],
  imports: [CommonModule, FormsModule, ModalModule],
  providers: [InputModalService],
  bootstrap: [],
  exports: [InputModalComponent],
  entryComponents: [InputModalComponent],
})
export class InputModalModule { }
