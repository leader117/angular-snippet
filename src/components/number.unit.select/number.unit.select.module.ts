import { NgModule } from '@angular/core';
import { NumberUnitSelectComponent } from './number.unit.select.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    NumberUnitSelectComponent
  ],
  imports: [
    CommonModule, FormsModule,
  ],
  providers: [],
  bootstrap: [],
  exports: [NumberUnitSelectComponent],
})
export class NumberUnitSelectModule { }
