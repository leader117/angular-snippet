import { Component, ElementRef, ViewChild, EventEmitter, Output, Input } from '@angular/core';
import { ZsGridCellValueUnit } from "../grid/ZsGridCellValueUnit";
import { AppAgGridComponent } from '../grid/grid.component';

const defaultValues = [
  { key: 'Actual', value: ZsGridCellValueUnit.Actual },
  { key: 'Thousands', value: ZsGridCellValueUnit.Thousands },
  { key: 'Millions', value: ZsGridCellValueUnit.Millions },
  { key: 'Billions', value: ZsGridCellValueUnit.Billions }
];
/**
 * @title App main search box
 */
@Component({
  selector: 'app-number-unit-select',
  templateUrl: 'number.unit.select.component.html',
  styleUrls: ['number.unit.select.component.css'],
})
export class NumberUnitSelectComponent {
  @Input() selected: ZsGridCellValueUnit = ZsGridCellValueUnit.Actual;
  @Input() items: Array<{ key: string, value: ZsGridCellValueUnit }> = [...defaultValues];
  @Output() select = new EventEmitter<ZsGridCellValueUnit>();
  @Input() grid: AppAgGridComponent;
  constructor() {
  }
  onSelect = () => {
    this.select.emit(this.selected);
    if (this.grid) this.grid.setCellValueUnit(this.selected);
  }
}
