import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ModelBuilderRequestProgressComponent } from './model.builder.request.progress.component';

@NgModule({
  declarations: [
    ModelBuilderRequestProgressComponent
  ],
  imports: [
    CommonModule, FormsModule
  ],
  providers: [],
  bootstrap: [],
  exports: [ModelBuilderRequestProgressComponent],
})
export class ModelBuilderRequestProgressModule { }
