import { Component, ElementRef, ViewChild, EventEmitter, Output, Input, OnChanges, OnDestroy } from '@angular/core';
import { SubmitUIRequestWithHandle } from 'src/grpc/grpc.service';

/**
 * @title App main search box
 */
@Component({
  selector: 'app-model-builder-request-progress',
  templateUrl: 'model.builder.request.progress.component.html',
  styleUrls: ['model.builder.request.progress.component.css'],
})
export class ModelBuilderRequestProgressComponent implements OnChanges, OnDestroy {
  @Input() uiReq: SubmitUIRequestWithHandle;
  progressText = '';
  constructor() {
  }
  ngOnChanges(params) {
    if (params.uiReq && this.uiReq) {
      this.progressText = '';
      this.uiReq.onProgress = (p) => {
        this.progressText = p;
      };
    }
  }
  ngOnDestroy() {
    if (this.uiReq) this.uiReq.onProgress = null;
  }
}
