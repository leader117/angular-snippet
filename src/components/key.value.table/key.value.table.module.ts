import { NgModule } from '@angular/core';
import { KeyValueTableComponent } from './key.value.table.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    KeyValueTableComponent
  ],
  imports: [CommonModule, FormsModule],
  providers: [],
  bootstrap: [],
  exports: [KeyValueTableComponent],
})
export class KeyValueTableModule { }
