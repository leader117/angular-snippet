import { Component, Input } from '@angular/core';
import { ArrayGroupByAsArray, ArrayGroupItem } from 'src/scripts/utilities';


export class KeyValueRow {
  public key: string;
  public value: string;
  public group?: string;
  public cssClass?: string;
}

/**
 * @title App main search box
 */
@Component({
  selector: 'key-value-table',
  templateUrl: 'key.value.table.component.html',
  styleUrls: ['key.value.table.component.css'],
})
export class KeyValueTableComponent {
  @Input() rows: Array<KeyValueRow>;
  groups: Array<ArrayGroupItem> = [];
  constructor() {
  }
  async ngOnChanges(changes) {
    if (changes.rows && this.rows) {
      this.groups = ArrayGroupByAsArray(this.rows, 'group');
    }
  }
}