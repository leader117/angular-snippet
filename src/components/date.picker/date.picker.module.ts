import { NgModule } from '@angular/core';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { DatePickerComponent } from './date.picker.component';

@NgModule({
  declarations: [
    DatePickerComponent
  ],
  imports: [BsDatepickerModule.forRoot()],
  providers: [],
  bootstrap: [],
  exports: [BsDatepickerModule, DatePickerComponent],
})
export class DatePickerModule { }
