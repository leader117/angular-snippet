import { Component, Input, Output, EventEmitter, OnInit, OnChanges, OnDestroy } from '@angular/core';
import * as moment from 'moment-timezone';
import { AutoUnsubscribeComponent } from '../auto-unsubscribe';

/**
 * @title App main search box
 */
@Component({
  selector: 'app-date-picker',
  templateUrl: 'date.picker.component.html',
  styleUrls: ['date.picker.component.css'],
})
export class DatePickerComponent extends AutoUnsubscribeComponent implements OnInit {
  @Input() date: Date;
  @Output() dateChange = new EventEmitter<Date>();
  constructor() {
    super();
  }
  async ngOnInit() {
  }
  onDateChange(date: Date) {
    this.date = date;
    this.dateChange.emit(date);
  }
}
