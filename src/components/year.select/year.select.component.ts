import { Component, ElementRef, ViewChild, EventEmitter, Output, Input } from '@angular/core';

/**
 * @title App main search box
 */
@Component({
  selector: 'year-select',
  templateUrl: 'year.select.component.html',
  styleUrls: ['year.select.component.css'],
})
export class YearSelectComponent {
  @Input() selected: number;
  @Input() years: Array<number> = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  @Output() select = new EventEmitter<number>();
  constructor() {
  }
  onSelect = () => {
    this.select.emit(this.selected);
  };
}