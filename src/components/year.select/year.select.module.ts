import { NgModule } from '@angular/core';
import { YearSelectComponent } from './year.select.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    YearSelectComponent
  ],
  imports: [
    CommonModule, FormsModule
  ],
  providers: [],
  bootstrap: [],
  exports: [YearSelectComponent],
})
export class YearSelectModule { }
