import { Component, ElementRef, ViewChild, EventEmitter, Output, Input } from '@angular/core';

/**
 * @title App main search box
 */
@Component({
  selector: 'app-items-select',
  templateUrl: 'items.select.component.html',
  styleUrls: ['items.select.component.css'],
})
export class ItemsSelectComponent {
  @Input() selected: string;
  @Input() emptyText: string;
  @Input() items: Array<{ code: string, name: string }>;
  @Output() select = new EventEmitter<string>();
  constructor() {
  }
  onSelect = () => {
    this.select.emit(this.selected);
  }
}
