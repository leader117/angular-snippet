import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule} from '@angular/common';
import { ItemsSelectComponent } from './items.select.component';

@NgModule({
  declarations: [
    ItemsSelectComponent
  ],
  imports: [FormsModule, CommonModule, FormsModule],
  providers: [],
  bootstrap: [],
  exports: [ItemsSelectComponent],
})
export class ItemsSelectModule { }
