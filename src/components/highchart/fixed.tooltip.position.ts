export function FixedTooltipPosition(width: number, height: number, point) {
    const chart = this.chart;
    let position;

    if (point.isHeader) {
        position = {
            x: Math.max(
                // Left side limit
                chart.plotLeft,
                Math.min(
                    point.plotX + chart.plotLeft - width / 2,
                    // Right side limit
                    chart.chartWidth - width - chart.marginRight
                )
            ),
            y: point.plotY - 30
        };
    } else {
        position = {
            x: point.series.chart.plotLeft,
            y: point.series.yAxis.top - chart.plotTop
        };
    }

    return position;
}
