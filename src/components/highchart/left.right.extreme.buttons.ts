import { HighchartComponent } from './highchart.component';
declare var require: any;
const Highstock = require('highcharts/highstock');
import * as moment from 'moment-timezone';
const holidays = require('@date/holidays-us')

const width = 16;
const height = 12;
const color = 'black';
const cursor = 'pointer';
export class HighstockLeftRightExtremeButtons {
    linkedChartCom: HighchartComponent;
    constructor(private chartCom: HighchartComponent) {
    }
    private getRangeType() {
        return this.chartCom.getSelectedRange();
    }
    apply() {
        const { chart } = this.chartCom;

        // Draw buttons after chart redraw
        Highstock.addEvent(chart, 'redraw', () => this.drawButtons());
    }
    // Draw the left and right buttons
    drawButtons() {
        const { chart } = this.chartCom;
        const x = chart.chartWidth - 280; // chart.chartWidth * 0.5;
        const y = 15; // chart.chartHeight - (chart.options.scrollbar.enabled ? 75 : 12);
        if (chart.xAxisPlusButton) chart.xAxisPlusButton.destroy();
        if (chart.xAxisMinusButton) chart.xAxisMinusButton.destroy();
        delete chart.xAxisPlusButton;
        delete chart.xAxisMinusButton;
        chart.xAxisMinusButton = this.drawZoomButton(x, y, false, () => this.forward(false));
        chart.xAxisPlusButton = this.drawZoomButton(x + width + 240, y, true, () => this.forward(true));
        this.chartCom.reflow();
    }
    private forward(isForward = true, startRange?: number, recCount = 10) {
        const range = this.getRange(isForward, startRange);
        if (!range) return;
        this.getSeriesXAxis().setExtremes(range[0], range[1], true, false);
        // if (recCount > 0 && this.isEmptyRange(range[0], range[1])) this.forward(isForward, undefined, recCount - 1);
    }
    private getRange(isForward = true, startRange: number) {
        const { type, count } = this.getRangeType() || { type: 'all', count: undefined };
        const { dataMax, dataMin } = this.getNavXAxis().getExtremes();
        const { min, max } = this.getSeriesXAxis().getExtremes();
        const fac = isForward ? 1 : -1;
        if (type === 'all' || type === 'ytd') {
            this.chartCom.selectRangeButton('1m');
            return;
        }
        if (!startRange) {
            let dd = moment.utc(isForward ? max : min).add(fac * count, type as any).valueOf();
            while (type === 'day' && this.isHoliday(moment.utc(dd))) dd = moment.utc(dd).add(fac, 'day').valueOf();
            startRange = dd;
        }
        let start = moment.utc(startRange).startOf(type as any).add(4, 'hour').valueOf();
        let end = moment.utc(start).endOf(type as any).valueOf();
        const startDiff = moment(start).diff(dataMin, 'days');
        const endDiff = moment(end).diff(dataMax, 'days');

        if (!isForward && startDiff <= 0) start = dataMin;
        if (isForward && endDiff >= 0) end = dataMax;
        if (start < dataMin || end > dataMax || start > end) return;
        return [start, end];
    }
    isEmptyRange(startRange, endRange) {
        const { chart } = this.chartCom;
        return chart.series.every((series) => !this.chartCom.hasValuesInRange(series, startRange, endRange, 1));
    }
    isHoliday(m: moment.Moment) {
        if (m.format('ddd') === 'Sat' || m.format('ddd') === 'Sun') return true;
        return holidays.isHoliday(m.toDate());
    }
    drawZoomButton(x, y, plus, onclick) {
        const src = plus ? 'signia-368.svg' : 'signia-365.svg';
        const elm = this.drawImage(src, x, y);
        if (onclick) {
            //   elm1.on('click', onclick);
            elm.on('click', onclick);
        }
        return elm;
    }
    drawImage(src, x, y) {
        const { chart } = this.chartCom;
        const element = chart.renderer.image('/assets/images/' + src, x, y, width, height)
            .attr({})
            .css({ cursor })
            .add();
        return element;
    }
    private getNavXAxis() {
        const { chart } = this.linkedChartCom || this.chartCom;
        return chart && chart.xAxis && (chart.xAxis.find(x => x.options.id === 'navigator-x-axis') || chart.xAxis[0]);
    }
    private getSeriesXAxis() {
        const { chart } = this.linkedChartCom || this.chartCom;
        return chart.xAxis && (chart.xAxis.find(x => x.options.id !== 'navigator-x-axis') || chart.xAxis[0]);
    }
}


