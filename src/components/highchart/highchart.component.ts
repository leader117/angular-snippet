import { Component, Input, ViewChild, ElementRef, Output, EventEmitter, OnInit, OnDestroy, OnChanges } from '@angular/core';
import { ApiService, API_Constrants } from 'src/services/api';
import * as moment from 'moment-timezone';
import * as Highcharts from 'highcharts';
import { adjustElementRemainWindowHeight } from 'src/scripts/utilities';
import { ConfirmModalService } from '../confirm.modal/confirm.modal.service';
declare var require: any;
const Highstock = require('highcharts/highstock');
const Boost = require('highcharts/modules/boost');
const noData = require('highcharts/modules/no-data-to-display');
const dragPanes = require('highcharts/modules/drag-panes.js');
const More = require('highcharts/highcharts-more');

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);
dragPanes(Highcharts);

Boost(Highstock);
noData(Highstock);
More(Highstock);
noData(Highstock);
dragPanes(Highstock);

Highstock.setOptions({
  lang: {
    rangeSelectorFrom: '',
    rangeSelectorZoom: '',
    thousandsSep: ',',
  },
  time: {
    timezoneOffset: -moment.tz('America/New_York').utcOffset()
  }
});
function getSeriesOptions(defaultOptions) {
  const color = defaultOptions.color || Highcharts.getOptions().colors[0];
  const types = {
    area: {
      type: 'area',
      threshold: null,
      data: [],
      tooltip: {
        valueDecimals: 2
      }
    },
    line: {
      type: 'line',
      threshold: null,
      data: [],
      tooltip: {
        valueDecimals: 2
      }
    }
  };
  let opt = {
    ...types[defaultOptions.type || 'line'],
  };
  if (!defaultOptions.negativeColor) {
    opt.fillColor = {
      linearGradient: {
        x1: 0,
        y1: 0,
        x2: 0,
        y2: 1
      },
      stops: [
        [0, color],
        // tslint:disable-next-line: no-string-literal
        [1, (Highcharts['Color'] as any)(color).setOpacity(0).get('rgba')]
      ]
    };
  }
  opt = {
    ...opt,
    name: `series_${moment().valueOf()}`,
    ...defaultOptions,
  };
  return opt;
}


function getChartOptions(isStock = true) {

  const options = {
    title: {
      text: ''
    },
    subtitle:
    {
      text: ''
    },
    chart: {
      // marginRight: 30,
      zoomType: 'x'
    },
    navigator: {
      enabled: isStock,
      height: 25
    },
    legend: {
      enabled: false,
      align: 'center',
      verticalAlign: 'bottom',
      x: 10,
      y: -50,
      floating: true
    },
    rangeSelector: {
      enabled: isStock,
      buttons: [{
        type: 'month',
        count: 1,
        text: '1m'
      }, {
        type: 'month',
        count: 3,
        text: '3m'
      }, /* {
        type: 'month',
        count: 6,
        text: '6m'
      }, */ {
        type: 'ytd',
        text: 'YTD'
      }, {
        type: 'year',
        count: 1,
        text: '1y'
      }, /* {
        type: 'year',
        count: 3,
        text: '3y'
      }, */{
        type: 'year',
        count: 5,
        text: '5y'
      }, {
        type: 'year',
        count: 10,
        text: '10y'
      }, {
        type: 'all',
        text: 'Max'
      }],
      selected: 8,
      // x: -30,
    },
    credits: {
      text: '\u00A9 Equeum.com',
      href: '/',
      position: {
        align: 'right',
        // y: -380
      },
      style: {
        fontSize: '9pt',
        color: '#aaa'
      }
    },
    xAxis: {
      // type: 'datetime',
    },
    plotOptions: {
      series: {
        compare: ''
      }
    },
    yAxis: [{
      id: 'yAxis_main',
      opposite: true,
      labels: {
        align: 'left',
        // x: 20,
        // y: 20
      }
    }],
    tooltip: {
      shared: true,
      crosshairs: true,
      dateTimeLabelFormats: {
        hour: '%m-%e-%Y',
        day: '%m-%e-%Y',
        month: '%Y-%m',
      },
    },
    series: [],
    responsive: {
      rules: [{
        condition: {
          maxWidth: 530
        },
        chartOptions: {
          chart: {
            // height: '100%'
          },
          subtitle: {
            text: null
          },
          navigator: {
            height: 5
          },
          rangeSelector: {
            inputEnabled: false,
          },
        }
      }]
    }

  };
  return options;
}
/**
 * @title App main search box
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'highchart',
  templateUrl: 'highchart.component.html',
  styleUrls: ['highchart.component.css'],
})
export class HighchartComponent implements OnInit, OnDestroy, OnChanges {
  @ViewChild('container', { static: true }) container: ElementRef;
  @Input() chartSeries: any;
  @Input() chartOptions: any;
  options: any;
  chart: any;
  seriesResolvers: Array<(startDate: number, endDate: number) => Promise<any[]>> = [];
  isLoading = false;
  @Input() isStock = true;
  onAfterSetExtremes = new EventEmitter<any>();
  private isInsideMouseOver = false;
  private oldExtremes: { min: number, max: number };
  constructor(private confirmModal: ConfirmModalService) {
    this.resetChart();
  }
  ngOnInit() {
  }
  ngOnDestroy() {
    this.resetChart();
  }
  ngOnChanges(params) {
    if (params.chartSeries && this.chartSeries) {
      this.resetChart();
      const arr = Array.isArray(this.chartSeries) ? this.chartSeries : [this.chartSeries];
      arr.forEach(r => this.addSeries(r));
      this.drawChart();
    }
    if (params.chartOptions && this.chartOptions) {
      this.resetChart();
      const arr = Array.isArray(this.chartSeries) ? this.chartSeries : [this.chartSeries];
      arr.forEach(r => this.addSeries(r));
      this.drawChart();
    }
  }
  resetChart() {
    if (this.chart) {
      this.chart.destroy();
      this.chart = null;
    }
    // this.onAfterSetExtremes.unsubscribe();
    this.options = Highcharts.merge(getChartOptions(this.isStock), this.chartOptions);
    Highcharts.merge(true, this.options, {
      xAxis: {
        events: {
          afterSetExtremes: e => this.onAfterSetExtremes.emit(e)
        },
      }
    });
    this.seriesResolvers = [];
  }
  setCompare(compare: string) {
    this.options.plotOptions.series.compare = compare;
  }
  setXDateFormat(format: string) {
    if (this.chart) {
      this.chart.series[0].update({
        tooltip: {
          xDateFormat: format,
        }
      });
    } else this.options.tooltip.xDateFormat = format;
  }
  /*
  * Add a new yAxis to the chart's options
  *
  * @Param {object} yAxisOptions : The yAxis Options (see highstock docs)
  */
  addYAxis(yAxisOptions: any, index: number = null) {
    const instance = this;
    if (index === null) index = instance.options.yAxis.length;
    if (!yAxisOptions.id) yAxisOptions.id = 'yAxis_' + (new Date()).valueOf();
    instance.options.yAxis.splice(index, 0, yAxisOptions);
    if (!yAxisOptions.title) yAxisOptions.title = { text: '' };
    yAxisOptions.opposite = true;
    // Highcharts.merge(yAxisOptions, instance.options.yAxis[0], { id: yAxisOptions.id });
    return yAxisOptions;
  }
  /*
  * Add a new yAxis to the chart's options
  *
  * @Param {object} yAxisOptions : The yAxis Options (see highstock docs)
  */
  addYAxisToChart(yAxisOptions: any, index: number = null) {
    this.addYAxis(yAxisOptions, index);
    return this.chart.addAxis(yAxisOptions);
  }
  /*
   * If the yAxis exists then return it, if not, then Add a new yAxis to the chart's options
   *
   * @Param {string} yAxisTitle : The yAxis title (see highstock docs)
   */
  getOrAddYAxis(yAxisTitle: string) {
    const instance = this;
    const yAxis = instance.options.yAxis.filter(r => r.title && r.title.text === yAxisTitle)[0];
    return yAxis || instance.addYAxis({ title: { text: yAxisTitle } });
  }
  clearYAxis() {
    this.options.yAxis = [];
  }
  addSeries(hsOptions: any = {}) {
    const series = !this.isStock ? hsOptions : getSeriesOptions(hsOptions);
    this.options.series.push(series);
    return series;
  }
  initLazyLoading(seriesArray: any[]) {
    const instance = this;
    seriesArray = seriesArray.map(s => instance.addSeries({ ...s, dataGrouping: { enabled: s.data.length > 1000 } }));
    this.onAfterSetExtremes.subscribe(e => this.loadLazyLoadingData(e));
    Highcharts.merge(true, instance.options,
      {
        xAxis: { minRange: 1000 * 60 * 60 * 24 },
        scrollbar: { liveRedraw: false },
        navigator: {
          adaptToUpdatedData: false,
          series: {
            data: seriesArray[0].data,
          }
        }
      });
    instance.selectRangeButton('Max');
  }
  private async loadLazyLoadingData(e) {
    if (!e.userMax) return;
    this.oldExtremes = e;
    this.loadResolvers(this.seriesResolvers);
  }
  async loadResolvers(seriesResolvers: Array<(startDate: number, endDate: number) => Promise<any[]>> = []) {
    const { chart, isLoading } = this;
    const { min, max } = this.oldExtremes || { min: undefined, max: undefined };
    if (!chart || isLoading) return;
    this.isLoading = true;
    chart.showLoading('Loading data from server...');
    try {
      const arr = await Promise.all(seriesResolvers.map(r => r(min, max)));
      arr.forEach(seriesArray => {
        seriesArray.forEach(s1 => {
          const chartSeries = chart.series.find(s2 => s1.id === s2.options.id);
          chartSeries.setData(s1.data);
          chartSeries.setName(s1.name);
        });
      });
    } catch (err) {
      this.confirmModal.showMessage(err);
    }
    this.isLoading = false;
    chart.hideLoading();
  }
  getSelectedRange(): { type: string, count: number } {
    const { chart } = this;
    const selectedIndex = chart.rangeSelector.selected;
    const selectedButton = chart.rangeSelector.buttonOptions[selectedIndex];
    if (selectedButton) {
      chart.lastSelectedRange = {
        type: selectedButton.type,
        count: selectedButton.count
      };
    }
    return chart.lastSelectedRange;
  }
  selectRangeButton(buttonText: string) {
    const index = this.options.rangeSelector.buttons.findIndex(r => r.text === buttonText);
    if (index < 0) return;
    this.options.rangeSelector.selected = index;
    if (this.chart) this.chart.rangeSelector.clickButton(index);
  }
  // Checks if the points has a not null values
  hasValuesInRange(series, startDate, endDate, valuesCount) {
    const { points } = series;
    const length = points.length;
    for (let i = 0; i < length; i++) {
      if (points[i].x <= startDate) continue;
      if (points[i].x >= endDate) break;
      if (Number.isFinite(points[i].y))
        valuesCount--;
      if (valuesCount <= 0)
        break;
    }
    return valuesCount <= 0;
  }
  syncZoomingWith(chart: HighchartComponent) {
    const instance = this;
    this.onAfterSetExtremes.subscribe(event => {
      const xMin = event.min;
      const xMax = event.max;
      const ex1 = chart.chart.xAxis[0].getExtremes();
      if (ex1.min !== xMin || ex1.max !== xMax) chart.chart.xAxis[0].setExtremes(xMin, xMax, true, false);
    });
    instance.syncTooltipsWith(chart);
  }
  syncTooltipsWith(chart: HighchartComponent) {
    const instance = this;
    Highcharts.merge(true, instance.options,
      {
        plotOptions: {
          series: {
            point: {
              events: {
                mouseOver: function () {
                  if (chart.isInsideMouseOver) return;
                  chart.isInsideMouseOver = true;
                  const { x } = this;
                  const series = chart.chart.series[0];
                  if (!series) return;
                  const p = series.points.find(p => p.x >= x);
                  if (p) p.onMouseOver();
                  chart.isInsideMouseOver = false;
                }
              }
            }
          }
        }
      }
    );
  }
  fireOnAfterSetExtremes() {
    const ex = this.chart.xAxis[0].getExtremes();
    ex.userMax = ex.max;
    this.onAfterSetExtremes.emit(ex);
  }
  drawChart() {
    const fun = this.isStock ? Highstock.stockChart : Highstock.chart;
    this.chart = fun(this.container.nativeElement, this.options);
    this.reflow();
  }
  adjustRemainWindowHeight() {
    const g: any = document.getElementsByClassName('chart-container')[0];
    adjustElementRemainWindowHeight(g, 300, () => {
      if (this.chart) this.reflow();
    });
  }
  reflow() {
    setTimeout(() => {
      if (this.chart) this.chart.reflow();
    }, 100);
  }
  showLoading(value: boolean) {
    if (!this.chart) return;
    if (value) this.chart.showLoading('Loading...');
    else this.chart.hideLoading();
    this.reflow();
  }
  getChartSeries() {
    const series = this.chart.series.filter(s => !s.name.includes('Navigator'));
    return series;
  }
  moveSeriesToDifferentAxis(singleAxis: boolean, compare = 'percent', seriesFilter?: (s: any) => boolean) {
    // console.log(this); return;
    let series = this.getChartSeries();
    const height = Math.floor(100 / series.length);
    if (seriesFilter) series = series.filter(seriesFilter);
    if (singleAxis) {
      const axis = this.addYAxisToChart({
        labels: {
          formatter: compare === 'value' ? formatterValue : formatterPercent
        },
        height: `${height * series.length}%`,
      });
      series.forEach(s => s.update({ yAxis: axis.options.id }), false);
      axis.setCompare(compare);
    } else {
      series.forEach((s, i) => {
        const axisId = this.addYAxisToChart({
          height: `${height}%`,
          top: `${i * height + 5}%`,
          resize: { enabled: false }
        }).options.id;
        s.update({ yAxis: axisId }, false);
      });
    }
    setTimeout(() => {
      this.chart.yAxis.filter(a => !a.series.length).forEach(s => s.remove());
      this.reflow();
    }, 100);
  }
}

export function formatterValue() {
  return (this.value > 0 ? ' + ' : '') + this.value;
}
export function formatterPercent() {
  return (this.value > 0 ? ' + ' : '') + this.value + '%';
}
