import { NgModule } from '@angular/core';
import { HighchartComponent } from './highchart.component';
import { ConfirmModalService } from '../confirm.modal/confirm.modal.service';

@NgModule({
  declarations: [
    HighchartComponent
  ],
  imports: [],
  providers: [ConfirmModalService],
  bootstrap: [],
  exports: [HighchartComponent],
})
export class HighchartModule { }
