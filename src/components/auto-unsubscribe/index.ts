import { OnDestroy } from '@angular/core';

export class AutoUnsubscribeComponent implements OnDestroy {

  ngOnDestroy(): void {
    const that: any = this;
    for (const prop of Object.keys(that)) {
      const property = that[prop];
      if (property && (typeof property.unsubscribe === 'function')) {
        property.unsubscribe();
      }
    }
    if (that.socket && that._socketCallbacks) {
      that._socketCallbacks.forEach(name => that.socket.off(name));
      that._socketCallbacks = null;
    }
  }
}
