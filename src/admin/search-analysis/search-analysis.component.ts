import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';
import { PERIODS } from '../constants';


@Component({
  selector: 'app-search-analysis',
  templateUrl: './search-analysis.component.html',
  styleUrls: ['./search-analysis.component.less', '../admin.component.less']
})
export class SearchAnalysisComponent implements OnInit {
  loading = true;
  searchAnalysis = { total: [], charts: [] };
  searchCategories = [];
  period = PERIODS[0].value;
  selectedPage = 0;
  selectedTab = 0;

  periods = PERIODS;

  columnDefs = [
    {
      headerName: 'Search Category',
      field: 'category',
      resizable: true,
      sortable: false,
      rowGroup: true,
      hide: true,
    },
    {
      headerName: 'Search Term',
      field: 'term',
      resizable: true,
      sortable: false,
    },
    {
      headerName: 'Total Unique Searches',
      field: 'total',
      resizable: true,
      sortable: false,
    },
  ];

  constructor(private admin: AdminService) { }

  async ngOnInit() {
    this.searchAnalysis = await this.admin.loadSearchAnalysis(this.period);
    this.loading = false;
  }

  async reload() {
    this.loading = true;
    if (this.selectedPage === 0) {
      this.searchAnalysis = await this.admin.loadSearchAnalysis(this.period);
    }
    if (this.selectedPage === 1) {
      this.searchCategories = await this.admin.loadSearchCategories(this.period);
    }
    this.loading = false;
  }
}
