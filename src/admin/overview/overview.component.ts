import { Component, OnInit, ViewChild } from '@angular/core';
import { AppAgGridComponent } from 'src/components/grid/grid.component';
import { AdminService } from '../admin.service';
import { PERIODS } from '../constants';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.less', '../admin.component.less']
})
export class OverviewComponent implements OnInit {
  generalStatisticsLoading = true;
  generalStatistics = { total: [], charts: [] };
  generalStatisticsPeriod = PERIODS[0].value;
  selectedTab = 0;

  pagesStatisticsLoading = true;
  pagesStatisticsPeriod = PERIODS[0].value;

  eventsStatisticsLoading = true;
  eventsStatisticsPeriod = PERIODS[0].value;

  searchStatisticsLoading = true;
  searchStatisticsPeriod = PERIODS[0].value;

  usersStatisticsLoading = true;
  usersStatistics = [];
  usersStatisticsPeriod = PERIODS[0].value;
  usersChart = {
    chart: {
      type: 'pie',
    },
  };

  periods = PERIODS;

  @ViewChild('pagesGrid', { static: true }) pagesGrid: AppAgGridComponent;
  @ViewChild('eventsGrid', { static: true }) eventsGrid: AppAgGridComponent;
  @ViewChild('searchGrid', { static: true }) searchGrid: AppAgGridComponent;

  constructor(private admin: AdminService) { }

  async ngOnInit() {
    this.pagesGrid.columnDefs = [
      { headerName: 'Page', field: 'page', width: 350 },
      { headerName: 'Visits', field: 'visits' },
    ];

    this.eventsGrid.columnDefs = [
      { headerName: 'Event Category', field: 'category' },
      { headerName: 'Total Events', field: 'total' },
    ];

    this.searchGrid.columnDefs = [
      { headerName: 'Search Term', field: 'term' },
      { headerName: 'Total Unique Searches', field: 'total' },
    ];

    [this.generalStatistics,
    this.pagesGrid.rowData,
    this.eventsGrid.rowData,
    this.searchGrid.rowData,
    this.usersStatistics,
    ] = await Promise.all([
      this.admin.loadGeneralStatistics(this.generalStatisticsPeriod),
      this.admin.loadPagesStatistics(this.pagesStatisticsPeriod),
      this.admin.loadEventsStatistics(this.eventsStatisticsPeriod),
      this.admin.loadSearchStatistics(this.searchStatisticsPeriod),
      this.admin.loadUsersStatistics(this.usersStatisticsPeriod),
    ]);

    this.generalStatisticsLoading = false;
    this.pagesStatisticsLoading = false;
    this.eventsStatisticsLoading = false;
    this.searchStatisticsLoading = false;
    this.usersStatisticsLoading = false;
  }

  async reloadGeneralStatistics() {
    this.generalStatisticsLoading = true;
    this.generalStatistics = await this.admin.loadGeneralStatistics(this.generalStatisticsPeriod);
    this.generalStatisticsLoading = false;
  }

  async reloadPagesStatistics() {
    this.pagesStatisticsLoading = true;
    this.pagesGrid.rowData = await this.admin.loadPagesStatistics(this.pagesStatisticsPeriod);
    this.pagesStatisticsLoading = false;
  }

  async reloadEventsStatistics() {
    this.eventsStatisticsLoading = true;
    this.eventsGrid.rowData = await this.admin.loadEventsStatistics(this.eventsStatisticsPeriod);
    this.eventsStatisticsLoading = false;
  }

  async reloadSearchStatistics() {
    this.searchStatisticsLoading = true;
    this.searchGrid.rowData = await this.admin.loadSearchStatistics(this.searchStatisticsPeriod);
    this.searchStatisticsLoading = false;
  }

  async reloadUsersStatistics() {
    this.usersStatisticsLoading = true;
    this.usersStatistics = await this.admin.loadUsersStatistics(this.usersStatisticsPeriod);
    this.usersStatisticsLoading = false;
  }
}
