import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';
import { PERIODS } from '../constants';

@Component({
  selector: 'app-events-analysis',
  templateUrl: './events-analysis.component.html',
  styleUrls: ['./events-analysis.component.less', '../admin.component.less']
})
export class EventsAnalysisComponent implements OnInit {
  loading = true;
  eventsAnalysis = { total: [], charts: [] };
  eventsCategories = [];
  period = PERIODS[0].value;
  selectedPage = 0;
  selectedTab = 0;

  periods = PERIODS;

  columnDefs = [
    {
      headerName: 'Event Category',
      field: 'category',
      resizable: true,
      sortable: false,
      rowGroup: true,
      hide: true,
    },
    {
      headerName: 'Event Action',
      field: 'action',
      resizable: true,
      sortable: false,
    },
    {
      headerName: 'Total Events',
      field: 'total',
      resizable: true,
      sortable: false,
    },
    {
      headerName: 'Unique Events',
      field: 'unique',
      resizable: true,
      sortable: false,
    },
  ];

  constructor(private admin: AdminService) { }

  async ngOnInit() {
    this.eventsAnalysis = await this.admin.loadEventsAnalysis(this.period);
    this.loading = false;
  }

  async reload() {
    this.loading = true;
    if (this.selectedPage === 0) {
      this.eventsAnalysis = await this.admin.loadEventsAnalysis(this.period);
    }
    if (this.selectedPage === 1) {
      this.eventsCategories = await this.admin.loadEventsCategories(this.period);
    }
    this.loading = false;
  }
}
