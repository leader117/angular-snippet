const PERIODS = [
  {
    label: 'Last 7 days',
    value: '7daysAgo',
    prevValue: '14daysAgo',
  },
  {
    label: 'Last 14 days',
    value: '14daysAgo',
    prevValue: '28daysAgo',
  },
  {
    label: 'Last 30 days',
    value: '30daysAgo',
    prevValue: '60daysAgo',
  },
  {
    label: 'Last 90 days',
    value: '90daysAgo',
    prevValue: '180daysAgo',
  },
];

const GENERAL_STATISTIC = [
  { header: 'Users', type: 'int' },
  { header: 'Sessions', type: 'int', },
  { header: 'Bounce Rate', type: 'percent' },
  { header: 'Session Duration', type: 'time' },
];

const EVENTS_ANALYSIS = [
  { header: 'Total Events', type: 'int' },
  { header: 'Unique Events', type: 'int', },
  { header: 'Session with Event', type: 'int' },
  { header: 'Events / Session with Event', type: 'float' },
];

const SEARCH_ANALYSIS = [
  { header: 'Sessions with Search', type: 'int' },
  { header: 'Total Unique Searches', type: 'int', },
  { header: 'Results Pageviews / Search', type: 'float' },
  { header: 'Time after Search', type: 'time' },
];

const PAGE_ATTENDANCE_ANALYSIS = [
  { header: 'Pageviews', type: 'int' },
  { header: 'Unique Pageviews', type: 'int' },
  { header: 'Avg. Time on Page', type: 'time' },
  { header: 'Bounce Rate', type: 'percent' },
];

const USER_ANALYSIS = [
  { header: 'Users', type: 'int' },
  { header: 'New Users', type: 'int' },
  { header: 'Sessions', type: 'int' },
  { header: 'Number of Sessions per User', type: 'float' },
  { header: 'Pageviews', type: 'int', },
  { header: 'Pages / Session', type: 'float' },
  { header: 'Avg. Session Duration', type: 'time' },
  { header: 'Bounce Rate', type: 'percent' },
];

export {
  PERIODS,
  GENERAL_STATISTIC,
  EVENTS_ANALYSIS,
  SEARCH_ANALYSIS,
  PAGE_ATTENDANCE_ANALYSIS,
  USER_ANALYSIS,
};
