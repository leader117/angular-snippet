import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { OverviewComponent } from './overview/overview.component';
import { HighchartModule } from 'src/components/highchart/highchart.module';
import { AppAgGridModule } from 'src/components/grid/grid.module';
import { SpinnerFlagModule } from 'src/components/spinner/spinner.module';
import { EventsAnalysisComponent } from './events-analysis/events-analysis.component';
import { PageAttendanceComponent } from './page-attendance/page-attendance.component';
import { SearchAnalysisComponent } from './search-analysis/search-analysis.component';
import { UserExplorerComponent } from './user-explorer/user-explorer.component';
import { AdminService } from './admin.service';
import { GraphQlApiService } from 'src/services/graphql.api';
import { AppSharedModule } from 'src/shared-modules';
import { BsModalService, ModalOptions } from 'ngx-bootstrap/modal';

@NgModule({
  declarations: [
    AdminComponent,
    OverviewComponent,
    EventsAnalysisComponent,
    PageAttendanceComponent,
    SearchAnalysisComponent,
    UserExplorerComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    AppSharedModule,
    HighchartModule,
    AppAgGridModule,
    AdminRoutingModule,
    SpinnerFlagModule,
  ],
  providers: [AdminService, GraphQlApiService, BsModalService],
})
export class AdminModule { }
