import * as moment from 'moment-timezone';

const parseData = (type, value) => {
  switch (type) {
    case 'float':
      return parseFloat(value).toFixed(2);
    case 'int':
      return Number(value);
    case 'time': {
      const duration = moment.duration({ seconds: value });
      return `${duration.get('hours')}:${duration.get('minutes')}:${duration.get('seconds')}`;
    }
    case 'percent':
      return `${parseFloat(value).toFixed(2)}%`;
    default:
      return value;
  }
};

export { parseData };
