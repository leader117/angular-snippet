import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OverviewComponent } from './overview/overview.component';
import { AdminComponent } from './admin.component';
import { UserExplorerComponent } from './user-explorer/user-explorer.component';
import { PageAttendanceComponent } from './page-attendance/page-attendance.component';
import { SearchAnalysisComponent } from './search-analysis/search-analysis.component';
import { EventsAnalysisComponent } from './events-analysis/events-analysis.component';

const routes: Routes = [
  {
    path: '', component: AdminComponent,
    children: [
      { path: '', redirectTo: 'overview', pathMatch: 'full' },
      { path: 'overview', component: OverviewComponent },
      { path: 'user-explorer', component: UserExplorerComponent },
      { path: 'page-attendance-analysis', component: PageAttendanceComponent },
      { path: 'search-analysis', component: SearchAnalysisComponent },
      { path: 'events-analysis', component: EventsAnalysisComponent },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
