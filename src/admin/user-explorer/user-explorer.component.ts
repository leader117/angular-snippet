import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';
import { PERIODS } from '../constants';
import { parseData } from '../helpers';

@Component({
  selector: 'app-user-explorer',
  templateUrl: './user-explorer.component.html',
  styleUrls: ['./user-explorer.component.less', '../admin.component.less']
})
export class UserExplorerComponent implements OnInit {
  loading = true;
  userAnalysis = { total: [], charts: [] };
  usersList = [];
  period = PERIODS[0].value;
  selectedPage = 0;
  selectedTab = 0;

  periods = PERIODS;

  columnDefs = [
    {
      headerName: 'User',
      field: 'user',
      resizable: true,
      sortable: false,
    },
    {
      headerName: 'Sessions',
      field: 'sessions',
      resizable: true,
      sortable: false,
    },
    {
      headerName: 'Avg. Session Duration',
      field: 'avg',
      resizable: true,
      sortable: false,
      cellRenderer: ({ value }) => parseData('time', value),
    },
    {
      headerName: 'Bounce Rate',
      field: 'bounce',
      resizable: true,
      sortable: false,
      cellRenderer: ({ value }) => parseData('percent', value),
    },
  ];

  constructor(private admin: AdminService) { }

  async ngOnInit() {
    this.userAnalysis = await this.admin.loadUserAnalysis(this.period);
    this.loading = false;
  }

  async reload() {
    this.loading = true;
    if (this.selectedPage === 0) {
      this.userAnalysis = await this.admin.loadUserAnalysis(this.period);
    }
    if (this.selectedPage === 1) {
      this.usersList = await this.admin.loadUsersList(this.period);
    }
    this.loading = false;
  }
}
