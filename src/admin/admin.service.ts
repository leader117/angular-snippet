import { Injectable } from '@angular/core';
import * as moment from 'moment-timezone';

import { GraphQlApiService } from 'src/services/graphql.api';

import { GA_QUERY } from 'src/graphql/queries/ga';
import {
  GENERAL_STATISTIC,
  EVENTS_ANALYSIS,
  SEARCH_ANALYSIS,
  PAGE_ATTENDANCE_ANALYSIS,
  USER_ANALYSIS,
} from './constants';
import { parseData } from './helpers';

@Injectable()
export class AdminService {

  constructor(private graphqlApi: GraphQlApiService) { }

  async loadGeneralStatistics(period) {
    const { googleAnalytics: [[total, charts]] } = await this.graphqlApi.post(GA_QUERY, {
      groups: [{
        reports: [
          { metrics: 'ga:users, ga:sessions, ga:bounceRate, ga:avgSessionDuration', startDate: period, },
          { metrics: 'ga:users, ga:sessions, ga:bounceRate, ga:avgSessionDuration', startDate: period, dimensions: 'ga:date' },
        ],
      }],
    });
    return {
      total: total.rows[0].map((value, index) => ({
        header: GENERAL_STATISTIC[index].header,
        value: parseData(GENERAL_STATISTIC[index].type, value),
      })),
      charts: total.rows[0].map((_, index) => ({
        name: GENERAL_STATISTIC[index].header,
        data: charts.rows.map(item => [moment(item[0]).unix() * 1000, +item[index + 1]]),
      })),
    };
  }

  async loadPagesStatistics(period) {
    const { googleAnalytics: [[{ rows = [] }]] } = await this.graphqlApi.post(GA_QUERY, {
      groups: [{
        reports: [
          { metrics: 'ga:pageviews', startDate: period, dimensions: 'ga:pagePath', sort: '-ga:pageviews' },
        ],
      }],
    });
    return rows.map(item => ({ page: item[0], visits: item[1] }));
  }

  async loadEventsStatistics(period) {
    const { googleAnalytics: [[{ rows = [] }]] } = await this.graphqlApi.post(GA_QUERY, {
      groups: [{
        reports: [
          { metrics: 'ga:totalEvents', startDate: period, dimensions: 'ga:eventCategory', sort: '-ga:totalEvents' },
        ],
      }],
    });
    return rows.map(item => ({ category: item[0], total: item[1] }));
  }

  async loadSearchStatistics(period) {
    const { googleAnalytics: [[{ rows = [] }]] } = await this.graphqlApi.post(GA_QUERY, {
      groups: [{
        reports: [
          { metrics: 'ga:searchUniques', startDate: period, dimensions: 'ga:searchKeyword', sort: '-ga:searchUniques' },
        ]
      }],
    });
    return rows.map(item => ({ term: item[0], total: item[1] }));
  }

  async loadUsersStatistics(period) {
    const { googleAnalytics: [[newUsers, returningUsers]] } = await this.graphqlApi.post(GA_QUERY, {
      groups: [{
        reports: [
          { metrics: 'ga:newUsers', startDate: period, },
          { metrics: 'ga:users', startDate: period, filters: 'ga:userType==Returning Visitor' },
        ],
      }],
    });
    return [{
      name: 'Users',
      data: [
        { name: 'New Users', y: +newUsers.rows[0][0] },
        { name: 'Returning Users', y: +returningUsers.rows[0][0] }
      ]
    }];
  }

  async loadEventsAnalysis(period) {
    const { googleAnalytics: [[total, charts]] } = await this.graphqlApi.post(GA_QUERY, {
      groups: [{
        reports: [
          { metrics: 'ga:totalEvents, ga:uniqueEvents, ga:sessionsWithEvent, ga:eventsPerSessionWithEvent', startDate: period },
          {
            metrics: 'ga:totalEvents, ga:uniqueEvents, ga:sessionsWithEvent, ga:eventsPerSessionWithEvent',
            startDate: period,
            dimensions: 'ga:date'
          },
        ],
      }],
    });
    return {
      total: total.rows[0].map((value, index) => ({
        header: EVENTS_ANALYSIS[index].header,
        value: parseData(EVENTS_ANALYSIS[index].type, value),
      })),
      charts: total.rows[0].map((_, index) => ({
        name: EVENTS_ANALYSIS[index].header,
        data: charts.rows.map(item => [moment(item[0]).unix() * 1000, +item[index + 1]]),
      })),
    };
  }

  async loadEventsCategories(period) {
    const { googleAnalytics: [[{ rows = [] }]] } = await this.graphqlApi.post(GA_QUERY, {
      groups: [{
        reports: [
          {
            metrics: 'ga:totalEvents, ga:uniqueEvents',
            dimensions: 'ga:eventCategory, ga:eventAction',
            startDate: period,
          }
        ],
      }],
    });
    return rows.map(item => ({ category: item[0], action: item[1], total: item[2], unique: item[3] }));
  }

  async loadSearchAnalysis(period) {
    const { googleAnalytics: [[total, charts]] } = await this.graphqlApi.post(GA_QUERY, {
      groups: [{
        reports: [
          { metrics: 'ga:searchSessions, ga:searchUniques, ga:avgSearchResultViews, ga:avgSearchDuration', startDate: period },
          {
            metrics: 'ga:searchSessions, ga:searchUniques, ga:avgSearchResultViews, ga:avgSearchDuration',
            startDate: period,
            dimensions: 'ga:date'
          },
        ],
      }],
    });
    return {
      total: total.rows[0].map((value, index) => ({
        header: SEARCH_ANALYSIS[index].header,
        value: parseData(SEARCH_ANALYSIS[index].type, value),
      })),
      charts: total.rows[0].map((_, index) => ({
        name: SEARCH_ANALYSIS[index].header,
        data: charts.rows.map(item => [moment(item[0]).unix() * 1000, +item[index + 1]]),
      })),
    };
  }

  async loadSearchCategories(period) {
    const { googleAnalytics: [[{ rows = [] }]] } = await this.graphqlApi.post(GA_QUERY, {
      groups: [{
        reports: [
          {
            metrics: 'ga:searchUniques',
            dimensions: 'ga:searchCategory, ga:searchKeyword',
            startDate: period,
            sort: '-ga:searchUniques',
          }
        ]
      }],
    });
    return rows.map(item => ({ category: item[0], term: item[1], total: item[2] }));
  }

  async loadPageAttendanceAnalysis(period) {
    const { googleAnalytics: [[total, charts]] } = await this.graphqlApi.post(GA_QUERY, {
      groups: [{
        reports: [
          { metrics: 'ga:pageviews, ga:uniquePageviews, ga:avgTimeOnPage, ga:bounceRate', startDate: period },
          {
            metrics: 'ga:pageviews, ga:uniquePageviews, ga:avgTimeOnPage, ga:bounceRate',
            startDate: period,
            dimensions: 'ga:date'
          },
        ],
      }],
    });
    return {
      total: total.rows[0].map((value, index) => ({
        header: PAGE_ATTENDANCE_ANALYSIS[index].header,
        value: parseData(PAGE_ATTENDANCE_ANALYSIS[index].type, value),
      })),
      charts: total.rows[0].map((_, index) => ({
        name: PAGE_ATTENDANCE_ANALYSIS[index].header,
        data: charts.rows.map(item => [moment(item[0]).unix() * 1000, +item[index + 1]]),
      })),
    };
  }

  async loadAllPages(period) {
    const { googleAnalytics: [[{ rows = [] }]] } = await this.graphqlApi.post(GA_QUERY, {
      groups: [{
        reports: [
          {
            metrics: 'ga:pageviews, ga:uniquePageviews, ga:avgTimeOnPage, ga:entrances',
            dimensions: 'ga:pagePath',
            startDate: period,
            sort: '-ga:pageviews',
          }
        ],
      }],
    });
    return rows.map(item => ({ page: item[0], pageview: item[1], unique: item[2], avg: item[3], entrances: item[4] }));
  }

  async loadUserAnalysis(period) {
    const { googleAnalytics: [[total, charts]] } = await this.graphqlApi.post(GA_QUERY, {
      groups: [{
        reports: [
          {
            metrics: 'ga:users, ga:newUsers, ga:sessions, ga:sessionsPerUser, ga:pageviews, ga:pageviewsPerSession, ga:avgSessionDuration, ga:bounceRate',
            startDate: period
          },
          {
            metrics: 'ga:users, ga:newUsers, ga:sessions, ga:sessionsPerUser, ga:pageviews, ga:pageviewsPerSession, ga:avgSessionDuration, ga:bounceRate',
            startDate: period,
            dimensions: 'ga:date'
          },
        ],
      }],
    });
    return {
      total: total.rows[0].map((value, index) => ({
        header: USER_ANALYSIS[index].header,
        value: parseData(USER_ANALYSIS[index].type, value),
      })),
      charts: total.rows[0].map((_, index) => ({
        name: USER_ANALYSIS[index].header,
        data: charts.rows.map(item => [moment(item[0]).unix() * 1000, +item[index + 1]]),
      })),
    };
  }

  async loadUsersList(period) {
    const { googleAnalytics: [[{ rows = [] }]] } = await this.graphqlApi.post(GA_QUERY, {
      groups: [{
        reports: [
          {
            metrics: 'ga:sessions, ga:avgSessionDuration, ga:bounceRate',
            dimensions: 'ga:dimension1',
            startDate: period,
            sort: '-ga:sessions',
          }
        ],
      }],
    });
    return rows.map(item => ({ user: item[0], sessions: item[1], avg: item[2], bounce: item[3] }));
  }
}
