import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';
import { PERIODS } from '../constants';
import { parseData } from '../helpers';

@Component({
  selector: 'app-page-attendance',
  templateUrl: './page-attendance.component.html',
  styleUrls: ['./page-attendance.component.less', '../admin.component.less']
})
export class PageAttendanceComponent implements OnInit {
  loading = true;
  pageAttendanceAnalysis = { total: [], charts: [] };
  allPages = [];
  period = PERIODS[0].value;
  selectedPage = 0;
  selectedTab = 0;

  periods = PERIODS;

  columnDefs = [
    {
      headerName: 'Page',
      field: 'page',
      resizable: true,
      sortable: false,
    },
    {
      headerName: 'Pageviews',
      field: 'pageview',
      resizable: true,
      sortable: false,
    },
    {
      headerName: 'Unique Pageviews',
      field: 'unique',
      resizable: true,
      sortable: false,
    },
    {
      headerName: 'Avg. Time on Page',
      field: 'avg',
      resizable: true,
      sortable: false,
      cellRenderer: ({ value }) => parseData('time', value),
    },
    {
      headerName: 'Entrances',
      field: 'entrances',
      resizable: true,
      sortable: false,
    },
  ];

  constructor(private admin: AdminService) { }

  async ngOnInit() {
    this.pageAttendanceAnalysis = await this.admin.loadPageAttendanceAnalysis(this.period);
    this.loading = false;
  }

  async reload() {
    this.loading = true;
    if (this.selectedPage === 0) {
      this.pageAttendanceAnalysis = await this.admin.loadPageAttendanceAnalysis(this.period);
    }
    if (this.selectedPage === 1) {
      this.allPages = await this.admin.loadAllPages(this.period);
    }
    this.loading = false;
  }
}
