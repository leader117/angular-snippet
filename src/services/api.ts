import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ZsGridColumn } from 'src/components/grid/grid.column';
import { ZsGridResult } from 'src/components/grid/grid.result';
import * as moment from 'moment-timezone';

export const API_Constrants = {
    DATE_FORMAT: 'YYYY-MM-DD',
    DATE_TIME_FORMAT: 'YYYY-MM-DDTHH:mm:ss',
};
const API_HEADERS = new HttpHeaders().append('utc-offset', moment().utcOffset().toString());

@Injectable({ providedIn: 'root' })
export class ApiService {

    constructor(public http: HttpClient) { }
    static cachedItems = {};
    async get(apiUrl: string, cache = false) {
        try {
            if (cache && ApiService.cachedItems[apiUrl]) return ApiService.cachedItems[apiUrl];
            const data = await this.http.get(apiUrl, { headers: API_HEADERS }).toPromise<any>();
            if (cache && !data.error) ApiService.cachedItems[apiUrl] = data;
            return data;
        } catch (err) { return { error: (err.message || err) }; }
    }
    async post(apiUrl: string, body: any, options?: any) {
        try {
            const data = await this.http.post(apiUrl, body, options).toPromise<any>();
            return data;
        } catch (err) {
            let error = err;
            if (error.error) error = error.error;
            if (error.error) error = error.error;
            if (error.message) error = error.message;
            return { error };
         }
    }
    async getWithParams(apiUrl: string, params: any) {
        const data = await this.get(this.paramsToUrlArgs(apiUrl, params));
        return data;
    }
    async getGridData(url: string, params: any = {}): Promise<ZsGridResult> {
        params = { ...params, disablePaggings: true };
        const data = await this.get(this.paramsToUrlArgs(url, params));
        if (data.error) return data;
        return this.parseGridData(data);
    }
    private paramsToUrlArgs(url: string, params: object): string {
        if (url.indexOf('?') < 0) url += '?';
        else url += '&';
        const args = Object.keys(params || {}).map((k) => {
            if (params[k] === null || params[k] === undefined) return '';
            return `${k}=${encodeURIComponent(params[k])}`;
        }).join('&');
        return url + args;
    }
    private parseGridData(data: any): ZsGridResult {
        return {
            columns: (data.columns || []).map(c => new ZsGridColumn(c)),
            rows: data.rows,
            groupBy: data.groupBy,
            totalRecords: data.totalRecords,
            totalPages: data.totalPages,
            pageSize: data.pageSize,
            pageNum: data.pageNum,
            sortField: data.order && data.order.column,
            sortDirection: data.order && data.order.order,
            supportPricesSocket: data.supportPricesSocket || false,
        };
    }
}
