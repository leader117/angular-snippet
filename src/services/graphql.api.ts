import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Apollo } from 'apollo-angular';
import { ApolloError } from 'apollo-client';
import { DocumentNode } from 'apollo-link';
import { GET_ACCOUNT } from 'src/graphql/queries/workspaces';

@Injectable()
export class GraphQlApiService {

  constructor(private auth: AuthService, private apollo: Apollo) { }
  private async getContext() {
    const token = await this.auth.getTokenSilently(false);
    const headers = new HttpHeaders().set('authorization', `Bearer ${token}`);
    return { headers };
  }
  async post(gqlan: DocumentNode, variables?: any, reTryIfFail = true, fetchPolicy?: any): Promise<any> {
    try {
      const context = await this.getContext();
      let res;
      if (gqlan.definitions[0]['operation'] === 'mutation')
        res = await this.apollo.mutate({ mutation: gqlan, variables, context }).toPromise();
      else res = await this.apollo.query({ query: gqlan, variables, context, fetchPolicy }).toPromise();
      return res.data;
    } catch (err1) {
      const err = err1 as ApolloError;
      if (reTryIfFail && this.isGraphQLUnauthorized(err)) {
        await this.auth.getTokenSilently(true);
        return this.post(gqlan, variables, false);
      }
      if (err.message) err1 = err.message;
      return { error: err1 };
    }
  }
  async postNoCache(gqlan: DocumentNode, variables?: any) {
    return this.post(gqlan, variables, true, 'no-cache');
  }
  private isGraphQLUnauthorized(err: ApolloError) {
    // GraphQL error: 401: Unauthorized
    const net = err.networkError as HttpErrorResponse;
    if (net && net.status === 401) return true;
    if (err.message && err.message.includes('401: Unauthorized')) return true;
    return false;
  }
  async getAccountInfo(): Promise<any> {
    const res = await this.post(GET_ACCOUNT);
    if (res.error) return res;
    return res.account;
  }
}
