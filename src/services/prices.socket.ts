import { Injectable } from '@angular/core';
// import io from 'socket.io-client';
const io = (url) => {};

@Injectable()
export class PricesSocketService {
    constructor() { }

    createSymbolChannel(container: ISocketContainer, symbolIds: string): any {
        return;
        const name = 'prices';
        container.socket = io(`/prices?symbolIds=${encodeURIComponent(symbolIds)}`);
        container.socket.on('connect', () => console.log('Connected'));
        container.socket.on('reconnect', () => {
            container.onSocketReConnected();
            this.resumeContainer(container);
        });
        if (!container._socketCallbacks) container._socketCallbacks = [];
        if (!container._socketCallbacks.includes(name)) container._socketCallbacks.push(name);
        this.resumeContainer(container);
    }
    suspendContainer(container: ISocketContainer): any {
        const name = 'prices';
        container.socket.off(name);
    }
    resumeContainer(container: ISocketContainer): any {
        if (!container._socketCallbacks) return;
        const name = 'prices';
        container.socket.off(name);
        container.socket.on(name, msg => container.onSocketMessage(name, msg));
    }
}

export interface ISocketContainer {
    socket?: any;
    _socketCallbacks?: Array<string>;
    onSocketMessage(name: string, msg: any): void;
    onSocketReConnected(): void;
}
