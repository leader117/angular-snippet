import { Injectable } from '@angular/core';
import { UpdateObj, ArrayRemoveItem } from 'src/scripts/utilities';
import { GaService } from 'src/services/ga.service';
import { GraphQlApiService } from 'src/services/graphql.api';
import { WorkspaceEntity } from 'src/graphql/types/WorkspaceEntity';
import { WindowEntity } from 'src/graphql/types/WindowEntity';
import { DELETE_WORKSPACE, ADD_WORKSPACE, EDIT_WORKSPACE, ADD_WINDOW, EDIT_WINDOW, DELETE_WINDOW } from 'src/graphql/mutations/workspaces';
import { ResourcesApiService } from 'src/services/resources.api';

@Injectable()
export class WorkspaceApiService {
    static workspaces: Array<WorkspaceEntity>;
    constructor(
      private graphQl: GraphQlApiService,
      private resAPIs: ResourcesApiService,
      private ga: GaService,
    ) { }

    async loadUserWorkspaces() {
        if (WorkspaceApiService.workspaces) return WorkspaceApiService.workspaces;
        WorkspaceApiService.workspaces = [];
        const { workspaces } = await this.graphQl.getAccountInfo();
        for (const ws of workspaces) {
            WorkspaceApiService.workspaces.push(WorkspaceEntity.fromGraphQL(ws));
        }
        return WorkspaceApiService.workspaces;
    }
    async getWorkspaceTools() {
        const resources = await this.resAPIs.getToolsResources();
        return resources.map(WindowEntity.fromResource);
    }
    async getWorkspaceTool(toolId: string): Promise<WindowEntity> {
        const tool: any = await this.resAPIs.getResource(toolId);
        if (tool.error) return tool;
        return WindowEntity.fromResource(tool);
    }
    getWorkspace(wsId: string) {
        return WorkspaceApiService.workspaces.find(ws => ws.id === wsId);
    }
    async createWorkspace(workspace: WorkspaceEntity) {
        const res = await this.graphQl.post(ADD_WORKSPACE, { name: workspace.name, description: workspace.description });
        if (res.error) return res;
        const ws = await WorkspaceEntity.fromGraphQL(res.addWorkspace);
        if (ws.error) return ws;
        this.ga.trackEvent('create', 'workspace');
        return WorkspaceEntity.fromGraphQL(ws);
    }
    async saveWorkspace(workspace: WorkspaceEntity) {
        const settings = { ...workspace.settings };
        const vars = { id: workspace.id, name: workspace.name, description: workspace.description, settings };
        const res = await this.graphQl.post(EDIT_WORKSPACE, vars);
        if (res.error) return res;
        return workspace;
    }
    async deleteWorkspace(workspace: WorkspaceEntity) {
        const res = await this.graphQl.post(DELETE_WORKSPACE, { id: workspace.id });
        if (res.error) return res;
        return res.deleteWorkspace;
    }
    async addWindow(ws: WorkspaceEntity, tab: WindowEntity) {
        const vars = { workspaceId: ws.id, resourceId: tab.id, title: tab.title, settings: tab.settings };
        const res = await this.graphQl.post(ADD_WINDOW, vars);
        if (res.error) return res;
        const data = WindowEntity.fromGraphQL(res.addWindow);
        UpdateObj(tab, WindowEntity.fromGraphQL(data));
        ws.windows.push(tab);
        this.ga.trackEvent('add window', 'workspace');
        return tab;
    }
    async saveWorkspaceTab(ws: WorkspaceEntity, tab: WindowEntity, newValues: WindowEntity): Promise<any> {
        const res = await this.graphQl.post(EDIT_WINDOW, { id: newValues.id, title: newValues.title, settings: newValues.settings });
        if (res.error) return res;
        const data = res.editWindow;
        if (data.error) return data;
        UpdateObj(tab, newValues);
        return tab;
    }
    async deleteWorkspaceTab(ws: WorkspaceEntity, tab: WindowEntity): Promise<any> {
        const res = await this.graphQl.post(DELETE_WINDOW, { id: tab.id });
        if (res.error) return res;
        ArrayRemoveItem(ws.windows, tab);
        return tab;
    }
}
