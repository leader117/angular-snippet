import { Injectable } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import uuidv4 from 'uuid/v4';

declare let gtag: Function;

// ID from google analytics panel
const GA_MEASUREMENT_ID = 'UA-136015077-1';

@Injectable({
  providedIn: 'root'
})
export class GaService {
  userId: string;

  constructor(public router: Router) { }

  // TODO: save guest to LS if no logged
  init(userId: string = `guest-${uuidv4()}`) {
    this.userId = userId;

    gtag('config', GA_MEASUREMENT_ID, { user_id: this.userId });
    gtag('set', {
      dimension1: this.userId,
      // TODO: change after getting new GA account (spam filter)
      dimension3: '777',
    });

    // Track navigation from router
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const lastPart = event.urlAfterRedirects.split('/').pop() || 'home';
        const title = `${lastPart[0].toUpperCase()}${lastPart.slice(1, lastPart.length)}`;

        gtag('set', { page: event.urlAfterRedirects });
        gtag('config', GA_MEASUREMENT_ID, {
          user_id: this.userId,
          page_title: title,
          page_path: event.urlAfterRedirects,
        });
      }
    });
  }

  trackEvent(action: string, category: string) {
    gtag('event', action, { event_category: category });
  }

  trackPage(path: string, title: string) {
    gtag('set', { page: path });
    gtag('config', GA_MEASUREMENT_ID, {
      user_id: this.userId,
      page_title: title,
      page_path: path,
    });
  }

  trackSearch(query: string, category: string) {
    gtag('set', { page: `/search/?q=${query}&cat=${category}` });
    gtag('config', GA_MEASUREMENT_ID, {
      user_id: this.userId,
      page_path: `/search/?q=${query}&cat=${category}`,
    });
  }
}
