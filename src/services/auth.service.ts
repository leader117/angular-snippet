import { Injectable } from '@angular/core';
import createAuth0Client from '@auth0/auth0-spa-js';
import Auth0Client from '@auth0/auth0-spa-js/dist/typings/Auth0Client';
import { from, of, Observable, BehaviorSubject, combineLatest, throwError } from 'rxjs';
import { tap, catchError, concatMap, shareReplay } from 'rxjs/operators';
import { Router } from '@angular/router';
import { getRandomString } from 'src/scripts/utilities';
import { CookieService } from 'ngx-cookie-service';
import * as CryptoJS from 'crypto-js';

const config = {
    clientId: 'VcPCQBR693boi9pCrnBWWO946DY1Mzp2',
    domain: 'equeumco.auth0.com',
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // Create an observable of Auth0 instance of client
  auth0Client$ = (from(
    createAuth0Client({
      domain: config.domain,
      client_id: config.clientId,
      redirect_uri: `${window.location.origin}/callback`,
      audience: 'http://api.equeum.com'
    })
  ) as Observable<Auth0Client>).pipe(
    shareReplay(1), // Every subscription receives the same shared value
    catchError(err => throwError(err))
  );
  // Define observables for SDK methods that return promises by default
  // For each Auth0 SDK method, first ensure the client instance is ready
  // concatMap: Using the client instance, call SDK method; SDK returns a promise
  // from: Convert that resulting promise into an observable
  isAuthenticated$ = this.auth0Client$.pipe(
    concatMap((client: Auth0Client) => from(client.isAuthenticated()))
  );
  handleRedirectCallback$ = this.auth0Client$.pipe(
    concatMap((client: Auth0Client) => from(client.handleRedirectCallback()))
  );
  // Create subject and public observable of user profile data
  private userProfileSubject$ = new BehaviorSubject<any>(null);
  userProfile$ = this.userProfileSubject$.asObservable();
  // Create a local property for login status
  loggedIn: boolean = null;

  constructor(private router: Router, private cookieService: CookieService) { }

  // getUser$() is a method because options can be passed if desired
  // https://auth0.github.io/auth0-spa-js/classes/auth0client.html#getuser
  getUser$(options?): Observable<any> {
    return this.auth0Client$.pipe(
      concatMap((client: Auth0Client) => from(client.getUser(options)))
    );
  }

  localAuthSetup() {
    // This should only be called on app initialization
    // Set up local authentication streams
    const checkAuth$ = this.isAuthenticated$.pipe(
      concatMap((loggedIn: boolean) => {
        if (loggedIn) {
          // If authenticated, get user data
          return this.getUser$();
        }
        // If not authenticated, return stream that emits 'false'
        return of(loggedIn);
      })
    );
    const checkAuthSub = checkAuth$.subscribe((response: { [key: string]: any } | boolean) => {
      // If authenticated, response will be user object
      // If not authenticated, response will be 'false'
      // Set subjects appropriately
      if (response) {
        const user = response;
        this.storeUserProfile(user);
        this.userProfileSubject$.next(user);
      }
      this.loggedIn = !!response;
      // Clean up subscription
      checkAuthSub.unsubscribe();
    });
  }

  login(redirectPath: string = '/', loginHint = '', initalScreen = 'signIn') {
    // A desired redirect path can be passed to login method
    // (e.g., from a route guard)
    // Ensure Auth0 client instance exists
    this.auth0Client$.subscribe((client: Auth0Client) => {
      // Call method to log in
      client.loginWithRedirect({
        redirect_uri: `${window.location.origin}/callback`,
        login_hint: loginHint,
        mode: initalScreen,
        appState: { target: redirectPath },
      });
    });
  }

  handleAuthCallback() {
    // Only the callback component should call this method
    // Call when app reloads after user logs in with Auth0
    let targetRoute: string; // Path to redirect to after login processsed
    // Ensure Auth0 client instance exists
    const authComplete$ = this.auth0Client$.pipe(
      // Have client, now call method to handle auth callback redirect
      concatMap(() => this.handleRedirectCallback$),
      tap(cbRes => {
        // Get and set target redirect route from callback results
        targetRoute = cbRes.appState && cbRes.appState.target ? cbRes.appState.target : '/';
      }),
      concatMap(() => {
        // Redirect callback complete; create stream
        // returning user data and authentication status
        this.getTokenSilently(true);
        return combineLatest([this.getUser$(), this.isAuthenticated$]);
      })
    );
    // Subscribe to authentication completion observable
    // Response will be an array of user and login status
    authComplete$.subscribe(([user, loggedIn]) => {
      // Update subjects and loggedIn property
      this.storeUserProfile(user);
      this.userProfileSubject$.next(user);
      this.loggedIn = loggedIn;
      // Redirect to target route after callback processing
      this.router.navigate([targetRoute]);
    });
  }

  logout() {
    // Ensure Auth0 client instance exists
    this.auth0Client$.subscribe((client: Auth0Client) => {
      // Call method to log out
      client.logout({
        client_id: config.clientId,
        returnTo: window.location.origin
      });
    });
  }

  async getTokenSilently(force?: boolean): Promise<string> {
    let token = this.getCachedToken();
    if (force || !token) {
      const client = await this.auth0Client$.toPromise();
      token = await client.getTokenSilently();
      this.setLocalStorageItem('access_token', token);
      console.log('New access token issued');
    }
    return token;
  }

  getCachedToken() {
    return this.getLocalStorageItem('access_token');
  }
  async getUserProfile(fromCache = true): Promise<any> {
    let user = this.getLocalStorageItem('user_profile');
    if (!fromCache || !user) {
      user = await this.userProfile$.toPromise();
      this.storeUserProfile(user);
    } else user = JSON.parse(user);
    return user;
  }
  private storeUserProfile(user: any) {
    if (typeof user !== 'object') return;
    this.setLocalStorageItem('user_profile', JSON.stringify(user));
  }
  getLocalStorageItem(key: string) {
    let value = localStorage.getItem(key);
    try {
      const bytes = CryptoJS.AES.decrypt(value, this.getEncrptionKey());
      value = bytes.toString(CryptoJS.enc.Utf8);
      return value;
    } catch (e) {
      if (value) console.error('Getting from local storage fail', e);
      return undefined;
    }
  }
  setLocalStorageItem(key: string, value: string) {
    try {
      const encrypted = CryptoJS.AES.encrypt(value, this.getEncrptionKey()).toString();
      localStorage.setItem(key, encrypted);
    } catch (e) {
      console.error('Storing to local storage fail', e);
    }
  }
  private getEncrptionKey() {
    const cName = '_ek_v1__';
    let key = this.cookieService.get(cName);
    if (!key) {
      key = getRandomString(50);
      this.cookieService.set(cName, key, Number.MAX_VALUE, '/');
    }
    return `SNKNSKA!!@#$_${key}_dnaskndas&**`;
  }
}
