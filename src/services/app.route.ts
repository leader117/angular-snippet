import { Injectable } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class AppRouteService {

    constructor(public route: ActivatedRoute, private router: Router) { }
    getParam(paramName: string): string {
        return this.getParams()[paramName];
    }
    getParams(): any {
        const p = this.route.snapshot.paramMap;
        return {
            ...this.route.snapshot.queryParams,
            ...p.keys.map(k => ({ [k]: p.get(k) })),
        };
    }
    getState(): any {
        return history.state || {};
    }
    navigate(screen: string, newParams = {}, keepCurrentQueryParams = false, state?: { [k: string]: any }) {
        const queryParams = {
            ...(keepCurrentQueryParams ? this.route.snapshot.queryParams : {}),
            ...newParams,
        };
        this.router.navigate(screen ? [screen] : [], { queryParams, state });
    }
    navigateByUrl(url: string) {
        this.router.navigateByUrl(url);
    }
    subscribe(callback: (params: any) => void): Subscription {
        return this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                callback(this.getParams());
            }
        });
    }
    navigateSymbolApp(tab: string = 'summary', symbolId: number = 68, state?: { [k: string]: any }) {
        this.navigate('/tools/symbol', { tab, symbolId }, false, state);
    }
    navigateMarketApp(tab: string = 'indices', title = '', view = 'main_view', state?: { [k: string]: any }) {
        this.navigate('/tools/markets', { tab, title, view }, false, state);
    }
    getUrl() {
        return location.pathname;
    }
    getUrlPaths() {
        return this.getUrl().split('/').filter(p => p);
    }
    isWorkspacePath() {
        return this.getUrlPaths()[0] === 'workspace';
    }
    isToolsPath(toolCode?: string) {
        const p = this.getUrlPaths();
        if (p[0] !== 'tools') return false;
        return !toolCode || p[1] === toolCode;
    }
    navigateWorkspaceApp(workspaceId) {
        const url = `/workspace/${workspaceId}`;
        this.navigateByUrl(url);
    }
    navigateToolsApp(toolId: string, args?: any, state?: { [k: string]: any }) {
        const url = `/tools${toolId ? '/' + toolId : ''}`;
        this.navigate(url, args, true, state);
    }
}
