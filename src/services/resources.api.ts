import { Injectable } from '@angular/core';
import { GraphQlApiService } from './graphql.api';
import { GET_RESOURCE, GET_RESOURCES } from 'src/graphql/queries/resource';
import { ResourceEntity } from 'src/graphql/types/ResourceEntity';
import { CategoryEntity } from 'src/graphql/types/CategoryEntity';
import { GET_CATEGORIES } from 'src/graphql/queries/categories';

@Injectable()
export class ResourcesApiService {

  constructor(
    private graphQl: GraphQlApiService) { }

    async getResource(resourceId: string): Promise<ResourceEntity> {
      const res = await this.graphQl.post(GET_RESOURCE, { id: resourceId });
      if (res.error) return res;
      return ResourceEntity.fromGraphQL(res.resource);
    }
    async getResources(params): Promise<ResourceEntity[]> {
      const vars = {
        order: { publishedAt: 'desc' },
        ...params
      };
      const res = await this.graphQl.postNoCache(GET_RESOURCES, vars);
      if (res.error) return res;
      return res.resources.map(resource => ResourceEntity.fromGraphQL(resource));
    }
    async getMyResources(params) {
      const { id } = await this.graphQl.getAccountInfo();
      return this.getResources({
        availableFor: id,
        createdBy: id,
        ...params
      });
    }
    async getMyModels(params = {}) {
      return this.getMyResources({
        typeIds: ['1f3b5c0b-2d1c-4dee-b30b-ad80ffedbf88'],
        ...params,
      });
    }
    async getToolsResources() {
      return this.getResources({
        typeIds: ['5cb4cb17-4cd1-41ac-a300-287351099787'],
      });
    }
    async getSharedResources() {
      return await this.getResources({});
    }
    async getCategories(): Promise<CategoryEntity[]> {
      const res = await this.graphQl.post(GET_CATEGORIES);
      if (res.error) return res;
      return res.categories.map(cat => CategoryEntity.fromGraphQL(cat));
    }
}
