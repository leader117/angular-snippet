import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/services/auth.service';
import { AppRouteService } from 'src/services/app.route';

@Component({
  selector: 'app-auth-callback',
  template: `
            <div class="h-100 text-center pt-3" *ngIf="!error">
              <div class="spinner-border" role="status">
                <span class="sr-only"></span>
              </div>
            </div>
            <div class="card text-center border-0" *ngIf="error">
                <div class="card-body">
                    <h5 class="card-title"> {{error}}</h5>
                    <p class="card-text"> {{errorDescription}}.</p>
                    <button class="btn btn-primary" (click)="auth.login()">Try again</button>
                </div>
            </div>
            `,
  styleUrls: []
})
export class AuthCallbackComponent implements OnInit {
  error: string;
  errorDescription: string;
  constructor(private auth: AuthService, private router: AppRouteService) { }

  ngOnInit() {
    this.error = this.router.getParam('error');
    this.errorDescription = this.router.getParam('error_description');
    if (!this.error) this.auth.handleAuthCallback();
  }

}
