import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { GraphQLModule } from 'src/app/graphql.module';
import { AuthRoutingModule } from './AuthRoutingModule';
import { CommonModule } from '@angular/common';
import { AuthCallbackComponent } from './auth.callback.component';

@NgModule({
    declarations: [
        AuthCallbackComponent,
    ],
    imports: [
        CommonModule,
        HttpClientModule,
        GraphQLModule,
        AuthRoutingModule
    ],
    providers: [],
})
export class AuthCallbackModule { }
