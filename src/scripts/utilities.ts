
/**
 * Group array by field
 */
export function ArrayGroupBy(arr: Array<any>, field: string) {
    const obj = {};

    arr.forEach((item) => {
        if (!obj[item[field]]) { obj[item[field]] = []; }
        obj[item[field]].push(item);
    });

    return obj;
}
export class ArrayGroupItem {
    public group: string;
    public rows: Array<any>;
}
/*
* Group array by field
*/
export function ArrayGroupByAsArray(arr: Array<any>, field: string): Array<ArrayGroupItem> {
    const appGroups = ArrayGroupBy(arr, field);
    return Object.keys(appGroups).map(g => ({ group: g, rows: appGroups[g] }));
}
/*
* Return all items without repeats
*/
export function ArrayNoRepeats(arr: Array<any>, field: string): Array<any> {
    if (field) {
        const unique = [];
        const map = {};
        arr.forEach((r) => {
            if (map[r[field]]) return;
            map[r[field]] = 1;
            unique.push(r);
        });
        return unique;
    } else {
        const unique = arr.filter((item, pos) => arr.indexOf(item) === pos);
        return unique;
    }
}
/*
* Remove item from the array
*/
export function ArrayRemoveItem(arr: Array<any>, item: any): Array<any> {
    const index = arr.indexOf(item, 0);
    if (index > -1) {
        arr.splice(index, 1);
    }
    return arr;
}
/*
* Return the item from the array starts from right
*/
export function ArrayItemFromRight(arr: Array<any>, index: number): any {
    return arr[arr.length - index - 1];
}
export function Sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
export function UpdateObj(target: any, newValues: any) {
    if (!target || !newValues) return;
    Object.keys(newValues).forEach(k => target[k] = newValues[k]);
    return target;
}

export function UintArrayToString(uintArray) {
    return new TextDecoder('utf-8').decode(uintArray);
    const encodedString = String.fromCharCode.apply(null, uintArray);
    const decodedString = decodeURIComponent(escape(encodedString));
    return decodedString;
}

export function TruncateText(str: string, maxLength: number) {
    return (str.length > maxLength) ? (str.slice(0, maxLength) + '..') : str;
}

export function getQueryStringParams(query) {
    return query
        ? (/^[?#]/.test(query) ? query.slice(1) : query)
            .split('&')
            .reduce((params, param) => {
                    const [key, value] = param.split('=');
                    params[key] = value ? decodeURIComponent(value.replace(/\+/g, ' ')) : '';
                    return params;
                }, {}
            )
        : {};
}

/*
* Lazy queue caller, when you expect the function can be called muliple times and you only need to request the last time.
*
*/
export class LazyQueue {
    refCount = 0;
    async wait(p: Promise<any>) {
        this.refCount += 1;
        const refCount = this.refCount;
        const data = await p;
        if (refCount !== this.refCount) {
            console.log('Multiple requests, cancel old one!!');
            return false;
        }
        return data;
    }
    // Wait ms time and return True if it's last request
    async waitTime(ms: number) {
        this.refCount += 1;
        const refCount = this.refCount;
        await Sleep(ms);
        return refCount === this.refCount;
    }
}
export function getElementPosition(elem: any) {
    const rect = elem.getBoundingClientRect();
    const win = elem.ownerDocument.defaultView;
    return {
        top: rect.top + win.pageYOffset,
        left: rect.left + win.pageXOffset
    };
}
export function adjustElementRemainWindowHeight(elm: any, minHeight, cb?: () => void) {
    if (elm && elm.nativeElement) elm = elm.nativeElement;
    if (elm) {
        if (!elm.offsetTop) return setTimeout(() => adjustElementRemainWindowHeight(elm, cb), 100);
        const height = Math.max(window.innerHeight - getElementPosition(elm).top - 10, minHeight);
        elm.style.height = `${height}px`;
    }
    if (cb) cb();
}
export function formatNumberTwoDigits(num: number) {
    return ('0' + num).slice(-2);
}
export function formatNumberFourDigits(num: number) {
    return ('000' + num).slice(-4);
}
/**
 * Wrap controller db call with promise.
 */
export function wrapToPromise(func: (args: any, cb: (err: any, res: any) => void) => any, args: any): Promise<any> {
    return new Promise((resolve, reject) => {
        func(args, (err, res) => {
            if (err) reject(err);
            else resolve(res);
        });
    });
}

/**
 * Get top/Left of element relative to screen.
 */
export function getDocumentOffsetPosition(el) {
    const position = {
        top: el.offsetTop,
        left: el.offsetLeft
    };
    if (el.offsetParent) {
        const parentPosition = getDocumentOffsetPosition(el.offsetParent);
        position.top += parentPosition.top;
        position.left += parentPosition.left;
    }
    return position;
}
/**
 * Parse the JWT tokens
 */
export function parseJwt(token: string) {
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    const jsonPayload = decodeURIComponent(atob(base64).split('').map((c) => {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
}

export function getRandomString(length: number) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

export function matchAll(txt: string, regex: string): {index: number, match: string }[] {
    // tslint:disable-next-line: no-string-literal
    const m = txt['matchAll'](regex);
    const arr = [];
    while (true) {
        const x = m.next();
        if (x.done) break;
        arr.push({
            index: x.value.index,
            match: x.value[0]
        });
    }
    return arr;
}


export class CodeNameGroupItem {
    code: string;
    name: string;
    group?: string;
    subTitle?: string;
    constructor(code: string, name: string, group: string, subTitle?: string) {
        this.code = code;
        this.name = name;
        this.group = group;
        this.subTitle = subTitle;
    }
}
