
import gql from 'graphql-tag';

const GOOGLE_AUTH_QUERY = gql`{
  externalAuthUrls(callbackUrl: "${location.href}/create") {
    googleOAuth2Url
  },
  hasValidExternalAuthAccessToken
 }`;

const GET_PROJECTS_QUERY = gql`{
   bigQueryProjects {
     id,
     name
   }
   datasetTemplates {
    id,
    name,
    fields {
      id,
      name,
      compatibleTypes
    }
  }
  }`;

const GET_DATASETS_QUERY = gql`
    query bigQueryDatasets($projectId: String!) {
      bigQueryDatasets(projectId: $projectId) {
        id,
        name
      }
     }`;

const GET_TABLES_QUERY = gql`
            query bigQueryTables($datasetId: String!) {
             bigQueryTables(datasetId: $datasetId) {
                id,
                name
              }
             }`;

const GET_TABLE_COLUMNS_QUERY = gql`
                    query bigQueryColumns($tableId: String!) {
                      bigQueryColumns(tableId: $tableId) {
                        id,
                        name,
                        type
                      }
                     }`;

const GET_TABLE_DATA_QUERY = gql`
                    query bigQueryRun($settings: DatasetSettings!,$pageSize: Float,$pageNumber: Float) {
                      bigQueryRun(settings: $settings,pageSize: $pageSize,pageNumber: $pageNumber) {
                        rows {
                          timestamp,
                           value
                        },
                        pageInfo {
                          pageSize,
                          pageNumber,
                          pageCount
                        }
                      }
                     }`;
export {
  GOOGLE_AUTH_QUERY,
  GET_PROJECTS_QUERY,
  GET_DATASETS_QUERY,
  GET_TABLES_QUERY,
  GET_TABLE_COLUMNS_QUERY,
  GET_TABLE_DATA_QUERY,
};
