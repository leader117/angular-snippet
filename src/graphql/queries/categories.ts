import gql from 'graphql-tag';

const GET_CATEGORIES = gql`
{
  categories {
    id
    name
    type {
      id
      name
      key
    }
  }
  }
`;
export { GET_CATEGORIES };
