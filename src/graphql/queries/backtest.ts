import gql from 'graphql-tag';

const GET_BACKTESTS = gql`
query backtests($modelId: ID!) {
    backtests(modelId: $modelId){
      id
      name
      eql
      startTime
      endTime
      barSize
      targetSymbol
      targetField
      settings
      timestamp
      model
      numTrades
      numProfitableTrades
      numUnprofitableTrades
      tradesPerDay
      totalReturns
      benchReturns
      maxUnrealizedTotalReturns
      annualReturns
      annualizedSharpe
      maxDrawdown
      annualVolatility
      downsideRisk
      sortino
      createdAt
      updatedAt
  }
}
`;

export { GET_BACKTESTS };
