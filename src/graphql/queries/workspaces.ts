import gql from 'graphql-tag';

const GET_ACCOUNT = gql`{
    account {
      id
      role
      name
      displayName
      email
      isPublic
      systemSettings
      notebookSettings
      catalogSettings
      notificationSettings
      picture
      dashboards {
        id
        name
        settings
        isPublic
        dashboardWidgets {
          settings
          id
          widget {
            id
            name
            __typename
          }
          __typename
        }
        __typename
      }
      workspaces {
        id
        name
        description
        order
        settings
        windows {
          id
          title
          settings
          resource {
            id
            name
            settings
            type {
              id
              name
              key
              __typename
            }
            image
            __typename
          }
          updatedAt
          __typename
        }
        updatedAt
        __typename
      }
      filters {
        id
        name
        __typename
      }
      purchasedResources {
        id
        __typename
      }
      watchedResources {
        id
        __typename
      }
      favoriteResources {
        id
        __typename
      }
      __typename
    }
  }`;


export { GET_ACCOUNT };
