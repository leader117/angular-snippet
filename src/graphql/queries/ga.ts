
import gql from 'graphql-tag';

const GA_QUERY = gql`
  query googleAnalytics($groups: [AnalyticGroupInput!]!) {
    googleAnalytics(groups: $groups)
  }
`;

export { GA_QUERY };
