import gql from 'graphql-tag';

const GET_RESOURCE = gql`
  query resource($id: ID!) {
    resource(id: $id) {
      id
      type {
        id
        name
        key
      }
      name
      description
      version
      versionUpgrade
      changelog
      categories {
        id
        name
        type {
          id
          name
        }
      }
      author {
        id
        name
        displayName
      }
      price
      rating
      downloads
      settings
      published
      isFavorite
      isWatched
      isPurchased
      tags {
        id
        name
      }
      reviews {
        id
      }
      createdBy
      publishedAt
      createdAt
      updatedAt
      image
    }
  }
`;

const GET_RESOURCES = gql`
    query resources(
        $query: String,
        $typeIds: [ID],
        $createdBy: ID,
        $availableFor: ID,
        $limit: Int,
        $offset: Int,
        $order: JSON,
        $where: JSON,
        $isWatched: Boolean,
        $isPurchased: Boolean) {
    resources(
        query: $query,
        typeIds: $typeIds,
        createdBy: $createdBy,
        availableFor: $availableFor,
        limit: $limit,
        offset: $offset,
        order: $order,
        where: $where,
        isWatched: $isWatched,
        isPurchased: $isPurchased) {
    id
    type {
        id
        name
        key
        __typename
    }
    name
    description
    version
    categories {
        id
        name
        type {
        id
        name
        __typename
        }
        __typename
    }
    tags {
      id
    }
    author {
        id
        name
        displayName
        __typename
    }
    price
    rating
    downloads
    settings
    published
    isFavorite
    isWatched
    isPurchased
    createdBy
    publishedAt
    createdAt
    updatedAt
    image
    __typename
    }
}`;


export { GET_RESOURCE, GET_RESOURCES };
