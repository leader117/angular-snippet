import { BaseEntity } from './BaseEntity';
import * as moment from 'moment-timezone';
import { UpdateObj } from 'src/scripts/utilities';

export class DatasetEntitySettings extends BaseEntity {
    projectId: string;
    datasetId: string;
    tableId: string;
    templateId: string;
    fieldMappings: { fieldId: string, mappedColumnId: string }[] = [];
}

export class DatasetEntity extends BaseEntity {
    static TYPE_ID = '12b03b05-be32-4e3f-a0d5-99dacf6c5801';
    id: string;
    name: string;
    description: string;
    date: Date;
    typeId = DatasetEntity.TYPE_ID;
    settings = new DatasetEntitySettings();
    categoryIds = [];
    tagIds = [];
    published = false;
    static fromGraphQL(d: any) {
        const ds1 = new DatasetEntity();
        ds1.id = d.id;
        ds1.name = d.name;
        ds1.description = d.description;
        ds1.date = moment(d.createdAt).toDate();
        ds1.settings = d.settings;
        ds1.published = d.published;
        return ds1;
    }
    clone(overrideBy: any = {}): DatasetEntity {
        const obj = new DatasetEntity();
        UpdateObj(obj, { ...JSON.parse(JSON.stringify(this)), ...overrideBy });
        return obj;
    }
}

export class GoogleAuthResult extends BaseEntity {
    isAuthorized: boolean;
    url: string;
    static fromGraphQL(res: any) {
        const obj = new GoogleAuthResult();
        obj.isAuthorized = res.hasValidExternalAuthAccessToken;
        obj.url = res.externalAuthUrls.googleOAuth2Url;
        return obj;
    }
}
export class GoogleProjectRow extends BaseEntity {
    id: string;
    name: string;
    datasets: GoogleDatasetRow[];
    static fromGraphQL(res: any) {
        const obj = new GoogleProjectRow();
        obj.id = res.id;
        obj.name = res.name;
        obj.datasets = [];
        return obj;
    }
    find(dsId: string) {
        return this.datasets.find(p => p.id === dsId);
    }
}

export class GoogleDatasetRow extends BaseEntity {
    id: string;
    name: string;
    tables: GoogleTableRow[];
    static fromGraphQL(res: any) {
        const obj = new GoogleDatasetRow();
        obj.id = res.id;
        obj.name = res.name;
        obj.tables = [];
        return obj;
    }
    find(tableId: string) {
        return this.tables.find(p => p.id === tableId);
    }
}

export class GoogleTableRow extends BaseEntity {
    id: string;
    name: string;
    fields: GoogleFieldRow[];
    static fromGraphQL(res: any) {
        const obj = new GoogleTableRow();
        obj.id = res.id;
        obj.name = res.name;
        obj.fields = [];
        return obj;
    }
}

export class GoogleFieldRow extends BaseEntity {
    id: string;
    name: string;
    type: string;
    static fromGraphQL(res: any) {
        const obj = new GoogleFieldRow();
        obj.id = res.id;
        obj.name = res.name;
        obj.type = res.type;
        return obj;
    }
}

export class DatasetTemplateField extends BaseEntity {
    id: string;
    name: string;
    compatibleTypes: string[];
    static fromGraphQL(res: any) {
        const obj = new DatasetTemplateField();
        obj.id = res.id;
        obj.name = res.name;
        obj.compatibleTypes = res.compatibleTypes;
        return obj;
    }
}

export class DatasetMappingTemplate extends BaseEntity {
    id: string;
    name: string;
    fields: DatasetTemplateField[];
    static fromGraphQL(res: any) {
        const obj = new DatasetMappingTemplate();
        obj.id = res.id;
        obj.name = res.name;
        obj.fields = res.fields.map(DatasetTemplateField.fromGraphQL);
        return obj;
    }
}

export class DatasetTableRow extends BaseEntity {
    date: Date;
    value: number;
    static fromGraphQL(res: any) {
        const obj = new DatasetTableRow();
        obj.date = moment.unix(res.timestamp).toDate();
        obj.value = Number(res.value);
        return obj;
    }
}
