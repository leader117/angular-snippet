import { UpdateObj } from 'src/scripts/utilities';
import * as moment from 'moment-timezone';
import { BaseEntity } from './BaseEntity';
import { CategoryEntity } from './CategoryEntity';
import { TypeEntity } from './TypeEntity';

export class ResourceEntity extends BaseEntity {
    author?: { id: string, name: string, displayName: string, __typename: string };
    categories?: CategoryEntity[] = [];
    changelog: any;
    createdAt: Date;
    createdBy: string;
    description: string;
    downloads: number;
    id: string;
    image: { url: string };
    isFavorite: boolean;
    isPurchased: boolean;
    isWatched: boolean;
    name: string;
    price: number;
    published: boolean;
    publishedAt: Date;
    rating: number;
    reviews: { id: string }[] = [];
    settings: any;
    tags: { id: string, name: string }[] = [];
    type: TypeEntity;
    updatedAt: Date;
    version: string;
    versionUpgrade: string;

    static fromGraphQL(item: any) {
        const obj = new ResourceEntity();
        if (item.createdAt) item.createdAt = moment(item.createdAt).toDate();
        if (item.publishedAt) item.publishedAt = moment(item.publishedAt).toDate();
        if (item.updatedAt) item.updatedAt = moment(item.updatedAt).toDate();
        UpdateObj(obj, item);
        return obj;
    }
    clone(override?: any) {
        const obj = new ResourceEntity();
        UpdateObj(obj, { ...this, ...override });
        return obj;
    }
}
