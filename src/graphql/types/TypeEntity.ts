import { UpdateObj } from 'src/scripts/utilities';
import * as moment from 'moment-timezone';
import { BaseEntity } from './BaseEntity';

export class TypeEntity extends BaseEntity {
    id: string;
    name: string;
    key: string;

    static fromGraphQL(item: any) {
        const obj = new TypeEntity();
        UpdateObj(obj, item);
        return obj;
    }
}
