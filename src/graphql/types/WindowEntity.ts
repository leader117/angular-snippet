import { UpdateObj } from 'src/scripts/utilities';
import * as moment from 'moment-timezone';
import { BaseEntity } from './BaseEntity';
import { ResourceEntity } from './ResourceEntity';
import { IToolApp } from 'src/applications/base/IToolApp';

export class WindowEntity extends BaseEntity {
    id: string;
    title: string;
    settings: any;
    resource: ResourceEntity;
    updatedAt: Date;
    uiCreated?: boolean;
    instance?: IToolApp;
    icon?: string;
    isBusy?: boolean;
    uiCode?: string;
    static fromGraphQL(item: any) {
        const obj = new WindowEntity();
        if (item.updatedAt) item.updatedAt = moment(item.updatedAt).toDate();
        UpdateObj(obj, item);
        obj.resource = ResourceEntity.fromGraphQL(item.resource);
        if (obj.resource && obj.resource.image) obj.icon = obj.resource.image.url;
        obj.fetchUiCode();
        return obj;
    }
    static fromResource(r: ResourceEntity) {
        const win = new WindowEntity();
        win.id = r.id;
        win.settings = r.settings;
        win.title = r.name;
        win.updatedAt = r.updatedAt;
        win.resource = r;
        win.fetchUiCode();
        if (r.image) win.icon = r.image.url;
        return win;
    }
    private fetchUiCode() {
        if (!this.resource) return;
        const { type } = this.resource;
        let code = this.resource.name;
        if (type && type.name === 'Dataset') code = type.name;
        if (type && type.name === 'Model') code = 'Model Builder';
        this.uiCode = code;
    }
    clone(override?: any) {
        const obj = new WindowEntity();
        UpdateObj(obj, { ...this, ...override });
        return obj;
    }
}
