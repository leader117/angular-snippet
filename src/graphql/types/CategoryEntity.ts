import { UpdateObj } from 'src/scripts/utilities';
import * as moment from 'moment-timezone';
import { BaseEntity } from './BaseEntity';
import { TypeEntity } from './TypeEntity';

export class CategoryEntity extends BaseEntity {
    id: string;
    name: string;
    type: TypeEntity;

    static fromGraphQL(item: any) {
        const obj = new CategoryEntity();
        UpdateObj(obj, item);
        return obj;
    }
}
