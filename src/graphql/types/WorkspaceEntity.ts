import { UpdateObj } from 'src/scripts/utilities';
import * as moment from 'moment-timezone';
import { BaseEntity } from './BaseEntity';
import { ResourceEntity } from './ResourceEntity';
import { WindowEntity } from './WindowEntity';

export class WorkspaceEntity extends BaseEntity {
    id: string;
    name: string;
    description: string;
    settings: any;
    order: number;
    windows: WindowEntity[] = [];
    updatedAt: Date;
    isOpened = false;
    static fromGraphQL(item: any) {
        const obj = new WorkspaceEntity();
        if (item.updatedAt) item.updatedAt = moment(item.updatedAt).toDate();
        UpdateObj(obj, item);
        obj.windows = item.windows.map(WindowEntity.fromGraphQL);
        if (!obj.settings) obj.settings = {};
        return obj;
    }
    getMaxZIndex() {
        let zIndex = 0;
        this.windows.forEach(w => zIndex = Math.max(zIndex, w.settings.zIndex || 0));
        return zIndex;
    }
    getTopWindow() {
        const zIndex = this.getMaxZIndex();
        return this.windows.find(w => w.settings.zIndex === zIndex) || this.windows[0];
    }
    setTopWindow(window: WindowEntity) {
        window.settings.zIndex = this.getMaxZIndex() + 1;
    }
    clone(override?: any) {
        const obj = new WorkspaceEntity();
        UpdateObj(obj, { ...this, ...override });
        return obj;
    }
}
