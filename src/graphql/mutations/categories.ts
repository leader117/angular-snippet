import gql from 'graphql-tag';

const ADD_RESOURCE_CATEGORY = gql`mutation addResourceCategory($categoryId: ID!, $resourceId: ID!) {
    addResourceCategory(categoryId: $categoryId, resourceId: $resourceId) {
                              id
                              }
                          }`;

export { ADD_RESOURCE_CATEGORY };
