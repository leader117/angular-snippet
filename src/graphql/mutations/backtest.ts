import gql from 'graphql-tag';

const CREATE_BACKTEST = gql`
mutation createBacktest (
  $name: String!,
  $eql: String!,
  $startTime: Float!,
  $endTime: Float!,
  $barSize: Float!,
  $targetSymbol: String!,
  $targetField: String!,
  $settings: String!,
  $timestamp: Float!,
  $model: ID,
  $numTrades: Float,
  $numProfitableTrades: Float,
  $numUnprofitableTrades: Float,
  $tradesPerDay: Float,
  $totalReturns: Float,
  $benchReturns: Float,
  $maxUnrealizedTotalReturns: Float,
  $annualReturns: Float,
  $annualizedSharpe: Float,
  $maxDrawdown: Float,
  $annualVolatility: Float,
  $downsideRisk: Float,
  $sortino: Float ) {
    createBacktest(
      name: $name,
      eql: $eql,
      startTime: $startTime,
      endTime: $endTime,
      barSize: $barSize,
      targetSymbol: $targetSymbol,
      targetField: $targetField,
      settings: $settings,
      timestamp: $timestamp,
      model: $model,
      numTrades: $numTrades,
      numProfitableTrades: $numProfitableTrades,
      numUnprofitableTrades: $numUnprofitableTrades,
      tradesPerDay: $tradesPerDay,
      totalReturns: $totalReturns,
      benchReturns: $benchReturns,
      maxUnrealizedTotalReturns: $maxUnrealizedTotalReturns,
      annualReturns: $annualReturns,
      annualizedSharpe: $annualizedSharpe,
      maxDrawdown: $maxDrawdown,
      annualVolatility: $annualVolatility,
      downsideRisk: $downsideRisk,
      sortino: $sortino) {
      id
      name
      eql
      startTime
      endTime
      barSize
      targetSymbol
      targetField
      settings
      timestamp
      model
      numTrades
      numProfitableTrades
      numUnprofitableTrades
      tradesPerDay
      totalReturns
      benchReturns
      maxUnrealizedTotalReturns
      annualReturns
      annualizedSharpe
      maxDrawdown
      annualVolatility
      downsideRisk
      sortino
      createdAt
      updatedAt
    }
  }
`;

const DELETE_BACKTEST = gql`
mutation deleteBacktest($id: ID!) {
  deleteBacktest(id: $id){
    id
    deletedAt
  }
}
`;

export { CREATE_BACKTEST, DELETE_BACKTEST };
