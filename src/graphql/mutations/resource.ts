import gql from 'graphql-tag';

const ADD_RESOURCE = gql`
  mutation addResource(
    $categoryIds: [ID!]!
    $name: String!
    $tagIds: [ID!]!
    $typeId: ID!
    $config: JSON
    $description: String
    $meta: JSON
    $settings: JSON
  ) {
    addResource(
      categoryIds: $categoryIds
      name: $name
      tagIds: $tagIds
      typeId: $typeId
      config: $config
      description: $description
      meta: $meta
      settings: $settings
    ) {
      id
      type {
        id
        name
        key
      }
      name
      description
      changelog
      version
      categories {
        id
        name
        type {
          id
          name
        }
      }
      author {
        id
        name
        displayName
      }
      price
      rating
      downloads
      settings
      published
      isFavorite
      isWatched
      isPurchased
      tags {
        id
        name
      }
      reviews {
        id
      }
      createdBy
      createdAt
      updatedAt
    }
  }
`;

const EDIT_RESOURCE = gql`
  mutation editResource(
    $id: ID!
    $categoryIds: [ID!]!
    $name: String!
    $tagIds: [ID!]!
    $typeId: ID!
    $config: JSON
    $description: String
    $changelog: String
    $versionUpgrade: VersionUpgrade
    $meta: JSON
    $settings: JSON
  ) {
    editResource(
      id: $id
      categoryIds: $categoryIds
      name: $name
      tagIds: $tagIds
      typeId: $typeId
      config: $config
      versionUpgrade: $versionUpgrade
      description: $description
      changelog: $changelog
      meta: $meta
      settings: $settings
    ) {
      id
      type {
        id
        name
        key
      }
      name
      description
      changelog
      version
      categories {
        id
        name
        type {
          id
          name
        }
      }
      author {
        id
        name
        displayName
      }
      price
      rating
      downloads
      settings
      published
      isFavorite
      isWatched
      isPurchased
      tags {
        id
        name
      }
      reviews {
        id
      }
      createdBy
      createdAt
      updatedAt
    }
  }
`;

const PUBLISH_RESOURCE = gql`
  mutation publishResource($id: ID!) {
    publishResource(id: $id) {
      id
      published
    }
  }
`;

const CREATE_RESOURCE_VERSION = gql`
  mutation createResourceVersion(
    $id: ID!
    $categoryIds: [ID]
    $name: String
    $tagIds: [ID]
    $typeId: ID
    $config: JSON
    $description: String
    $meta: JSON
    $settings: JSON
  ) {
    createResourceVersion(
      id: $id
      categoryIds: $categoryIds
      name: $name
      tagIds: $tagIds
      typeId: $typeId
      config: $config
      description: $description
      meta: $meta
      settings: $settings
    ) {
      id
      name
    }
  }
`;

const DELETE_RESOURCE = gql`
  mutation deleteModel($id: ID!) {
    deleteModel(id: $id) {
      id
    }
  }
`;

export { ADD_RESOURCE, EDIT_RESOURCE, PUBLISH_RESOURCE, CREATE_RESOURCE_VERSION, DELETE_RESOURCE };
