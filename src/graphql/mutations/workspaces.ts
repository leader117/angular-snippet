import gql from 'graphql-tag';


const ADD_WORKSPACE = gql`mutation addWorkspace($name: String!, $description: String!, $settings: JSON) {
                                addWorkspace(name: $name, description: $description, settings: $settings) {
                                id
                                name
                                description
                                order
                                settings
                                windows {
                                    id
                                    __typename
                                }
                                __typename
                                }
                            }`;

const DELETE_WORKSPACE = gql`mutation deleteWorkspace($id: ID!) {
                              deleteWorkspace(id: $id) {
                              id
                              __typename
                              }
                          }`;

const EDIT_WORKSPACE = gql`mutation editWorkspace($id: ID!, $name: String, $description: String, $order: Int, $settings: JSON) {
                                    editWorkspace(id: $id, name: $name, description: $description, order: $order, settings: $settings) {
                                    id
                                    name
                                    description
                                    order
                                    settings
                                    __typename
                                    }
                                }`;

const EDIT_WINDOW = gql`mutation editWindow($id: ID!, $title: String!, $settings: JSON) {
                                  editWindow(id: $id, title: $title, settings: $settings) {
                                    id
                                    title
                                    settings
                                    }
                                }`;
const ADD_WINDOW = gql`mutation addWindow($resourceId: ID!, $workspaceId: ID!, $title: String!, $settings: JSON) {
                                addWindow(resourceId: $resourceId, title: $title, workspaceId: $workspaceId, settings: $settings) {
                                id
                                workspaceId
                                title
                                settings
                                resource {
                                    id
                                    name
                                    settings
                                    type {
                                    id
                                    name
                                    key
                                    __typename
                                    }
                                    __typename
                                }
                                updatedAt
                                __typename
                                }
                            }`;
const DELETE_WINDOW = gql`mutation deleteWindow($id: ID!) {
                                deleteWindow(id: $id) {
                                id
                                workspaceId
                                __typename
                                }
                            }`;

export { ADD_WORKSPACE, DELETE_WORKSPACE, EDIT_WORKSPACE, ADD_WINDOW, EDIT_WINDOW, DELETE_WINDOW };