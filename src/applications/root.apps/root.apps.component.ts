import { Component, OnInit, OnDestroy } from '@angular/core';
import { AppRouteService } from 'src/services/app.route';
import { AuthService } from 'src/services/auth.service';
import { KNOWLEDGE_GRAPH_TOOL_ID } from 'src/constants';

const myApps = [
  {
    code: 'User Workspaces',
    name: 'Home',
    url: '/home',
    moreURLs: [],
    uiCreated: false,
    iconUrl: 'assets/icons/home.svg',
    iconSize: 24,
  },
  {
    code: 'Workspace Viewer',
    name: 'Workspaces',
    url: '/workspace',
    moreURLs: [],
    uiCreated: false,
    iconUrl: 'assets/icons/workspaces.svg',
    iconSize: 20,
  },
  {
    code: 'tools',
    name: 'Tools',
    url: '/tools',
    moreURLs: [],
    uiCreated: false,
    iconUrl: 'assets/icons/tools.svg',
    iconSize: 20,
    hidden: true,
  },
  {
    code: 'Models',
    name: 'Models',
    url: '/tools/models',
    moreURLs: [],
    uiCreated: false,
    iconUrl: 'assets/icons/modelBuilder.svg',
    iconSize: 20,
  },
  {
    code: 'tools',
    name: 'Knowledge Graph',
    url: `/tools/${KNOWLEDGE_GRAPH_TOOL_ID}`,
    moreURLs: [],
    uiCreated: false,
    iconUrl: 'assets/icons/knowledgeGraph.svg',
    iconSize: 20,
  },
  {
    code: 'Datasets Building',
    name: 'Datasets',
    url: '/tools/datasets',
    moreURLs: ['/tools/datasets/create', '/tools/datasets/edit', '/tools/datasets/view'],
    uiCreated: false,
    iconUrl: 'assets/icons/datasets.svg',
    iconSize: 20,
  },
];
/**
 * @title App main search box
 */
@Component({
  selector: 'app-root-apps',
  templateUrl: 'root.apps.component.html',
  styleUrls: ['root.apps.component.less'],
})
export class RootAppsAppComponent implements OnInit, OnDestroy {
  apps = [...myApps];
  expanded = false;
  selectedApp;
  isLoading = false;
  profile;
  constructor(
    public auth: AuthService,
    private appRoute: AppRouteService) { }
  async ngOnInit() {
    this.profile = await this.auth.getUserProfile();
    if (this.profile.email.endsWith('@equeum.com')) {
      this.apps.push({
        code: 'Equeum NLP',
        name: 'NLP',
        url: '/tools/nlp',
        moreURLs: [],
        uiCreated: false,
        iconUrl: 'assets/icons/brain.svg',
        iconSize: 20,
      });
    }

    this.appRoute.subscribe(() => this.openAppFromPath());
    this.openAppFromPath();
  }
  ngOnDestroy() {
  }
  openAppFromPath() {
    const app = this.appRoute.getUrlPaths()[0] || 'home';
    const loc = location.pathname;
    let selectedApp;
    if (app === 'tools') selectedApp = this.apps.find(a => a.url === loc || a.moreURLs.some( l => loc.startsWith(l)));
    else selectedApp = this.apps.find(a => loc.startsWith(a.url));
    this.openApp(selectedApp || this.apps[2]);
  }
  openApp(app, updateUrl = false) {
    if (app) {
      app.uiCreated = true;
      if (updateUrl) this.appRoute.navigateByUrl(app.code);
    }
    this.selectedApp = app;
  }
}
