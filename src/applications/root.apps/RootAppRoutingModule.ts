import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RootAppsAppComponent } from './root.apps.component';



const routes: Routes = [
  {
    path: '**',
    component: RootAppsAppComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RootAppRoutingModule { }
