import { NgModule } from '@angular/core';
import { AppSharedModule } from 'src/shared-modules';
import { AppLoaderModule } from '../base/app.loader/app.loader.module';
import { RootAppsAppComponent } from './root.apps.component';
import { RootAppRoutingModule } from './RootAppRoutingModule';

@NgModule({
  declarations: [RootAppsAppComponent],
  imports: [
    AppSharedModule,
    AppLoaderModule,
    RootAppRoutingModule
  ],
  providers: [
  ],
  exports: [RootAppsAppComponent],
  bootstrap: [RootAppsAppComponent],
})
export class RootAppsAppModule { }
