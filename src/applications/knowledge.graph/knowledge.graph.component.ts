import { Component, ElementRef, ViewChild, Input, OnInit, OnDestroy } from '@angular/core';
import { AutoUnsubscribeComponent } from 'src/components/auto-unsubscribe';
import * as moment from 'moment-timezone';
import { IToolApp } from '../base/IToolApp';
import { ConfirmModalService } from 'src/components/confirm.modal/confirm.modal.service';
import { EQLEngine } from 'src/grpc/proto-gen/executor_grpc_pb';
import { GaService } from 'src/services/ga.service';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4plugins from '@amcharts/amcharts4/plugins/forceDirected';
import * as am4themes_animated from '@amcharts/amcharts4/themes/animated';
import * as am4charts from '@amcharts/amcharts4/charts';
import { TreeData, TableData, TableColumns } from './sample.data';
import { WorkspaceEntity } from 'src/graphql/types/WorkspaceEntity';
import { WindowEntity } from 'src/graphql/types/WindowEntity';

am4core.useTheme(am4themes_animated.default);

/**
 * @title App main search box
 */
@Component({
  selector: 'app-knowledge-graph',
  templateUrl: 'knowledge.graph.component.html',
  styleUrls: ['knowledge.graph.component.css'],
})
export class KnowledgeGraphAppComponent extends AutoUnsubscribeComponent implements OnInit, OnDestroy, IToolApp {
  ws: WorkspaceEntity;
  chart: am4plugins.ForceDirectedTree;
  uiPart = EQLEngine.UIPart.UIPART_VOLUME;
  uiPartItems = [
    { code: EQLEngine.UIPart.UIPART_PROFITLOSS, name: 'Profit/Loss' },
    { code: EQLEngine.UIPart.UIPART_RETURNS, name: 'Returns' },
    { code: EQLEngine.UIPart.UIPART_SHARPE, name: 'Sharpe' },
    { code: EQLEngine.UIPart.UIPART_VOLUME, name: 'Volume' }
  ];
  isLoading = false;
  isTool = true;
  rows = TableData;
  columnDefs = TableColumns;
  one$;
  @ViewChild('chartDiv', { static: true }) chartDivElm: ElementRef;
  constructor(private confirmModal: ConfirmModalService, private ga: GaService) {
    super();
  }
  ngOnInit() {
    this.drawTree();
  }
  ngOnDestroy() {
    this.destroyChart();
  }
  onUiPartChanged(value) {
    this.uiPart = value;
  }
  getToolTitle() { return 'Knowledge Graph'; }
  getToolParams() {
    return {
    };
  }
  setToolParams(ws: WorkspaceEntity, tab: WindowEntity) {
    this.ws = ws;
    this.isTool = false;
  }
  drawTree() {
    this.destroyChart();
    this.chart = am4core.create(this.chartDivElm.nativeElement, am4plugins.ForceDirectedTree);
    const networkSeries = this.chart.series.push(new am4plugins.ForceDirectedSeries());

    this.chart.data = TreeData;

    networkSeries.dataFields.value = 'value';
    networkSeries.dataFields.name = 'name';
    networkSeries.dataFields.children = 'children';
    networkSeries.nodes.template.tooltipText = '{name}:{value}';
    networkSeries.nodes.template.fillOpacity = 1;
    networkSeries.manyBodyStrength = -20;
    networkSeries.links.template.strength = 0.8;
    networkSeries.minRadius = am4core.percent(2);

    networkSeries.nodes.template.label.text = '{name}';
    networkSeries.fontSize = 10;

    networkSeries.nodes.template.events.on('hit', e => this.onClickNode(e.target.dataItem.dataContext));
  }
  onClickNode(node: any) {
    TreeData[0].name = node.name;
    this.isLoading = true;
    setTimeout(() => {
      this.drawTree();
      this.isLoading = false;
    }, 100);
  }
  destroyChart() {
    if (this.chart) this.chart.dispose();
  }
}
