export const TreeData = [
    {
        name: 'Core',
        children: [
            {
                name: 'First',
                children: [
                    { name: 'A1', value: 100 },
                    { name: 'A2', value: 60 }
                ]
            },
            {
                name: 'Second',
                children: [
                    { name: 'B1', value: 135 },
                    { name: 'B2', value: 98 }
                ]
            },
            {
                name: 'Third',
                children: [
                    {
                        name: 'C1',
                        children: [
                            { name: 'EE1', value: 130 },
                            { name: 'EE2', value: 87 },
                            { name: 'EE3', value: 55 }
                        ]
                    },
                    { name: 'C2', value: 148 },
                    {
                        name: 'C3', children: [
                            { name: 'CC1', value: 53 },
                            { name: 'CC2', value: 30 }
                        ]
                    },
                    { name: 'C4', value: 26 }
                ]
            },
            {
                name: 'Fourth',
                children: [
                    { name: 'D1', value: 415 },
                    { name: 'D2', value: 148 },
                    { name: 'D3', value: 89 }
                ]
            },
            {
                name: 'Fifth',
                children: [
                    {
                        name: 'E1',
                        children: [
                            { name: 'EE1', value: 33 },
                            { name: 'EE2', value: 40 },
                            { name: 'EE3', value: 89 }
                        ]
                    },
                    {
                        name: 'E2',
                        value: 148
                    }
                ]
            }

        ]
    }
];
export const TableData = [
    {
        name: '15555_System',
        total_assests: '180 M',
        ytd: 15,
        avg_volume: 500,
    },
    {
        name: '888895_System',
        total_assests: '190 M',
        ytd: 20,
        avg_volume: 600,
    },
    {
        name: '7777777_System',
        total_assests: '120 M',
        ytd: 7,
        avg_volume: 60,
    },
    {
        name: '2222222_System',
        total_assests: '18 K',
        ytd: 170,
        avg_volume: 300,
    }
];

export const TableColumns = [
    {
        headerName: 'Name',
        field: 'name',
        resizable: true,
        sortable: true,
    },
    {
        headerName: 'Total Assests',
        field: 'total_assests',
        resizable: true,
        sortable: true,
    },
    {
        headerName: 'YTD',
        field: 'ytd',
        resizable: true,
        sortable: true,
    },
    {
        headerName: 'AVG Volume',
        field: 'avg_volume',
        resizable: true,
        sortable: true,
    },
];
