import { NgModule } from '@angular/core';
import { AppSharedModule } from 'src/shared-modules';
import { KnowledgeGraphAppComponent } from './knowledge.graph.component';
import { AppAgGridModule } from 'src/components/grid/grid.module';

@NgModule({
  declarations: [
    KnowledgeGraphAppComponent,
  ],
  imports: [
    AppSharedModule,
    AppAgGridModule,
  ],
  providers: [],
  bootstrap: [KnowledgeGraphAppComponent],
  exports: [KnowledgeGraphAppComponent],
})
export class KnowledgeGraphAppModule { }
