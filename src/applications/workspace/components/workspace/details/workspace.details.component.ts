import { Component, Input, Output, EventEmitter } from '@angular/core';
import { WorkspaceApiService } from 'src/services/workspace.apis.service';
import { UpdateObj } from 'src/scripts/utilities';
import { ConfirmModalService } from 'src/components/confirm.modal/confirm.modal.service';
import { WorkspaceEntity } from 'src/graphql/types/WorkspaceEntity';

/**
 * @title App main search box
 */
@Component({
  selector: 'app-workspace-details',
  templateUrl: 'workspace.details.component.html',
  styleUrls: ['workspace.details.component.css'],
})
export class WorkspaceDetailsComponent {
  @Output() confirmed = new EventEmitter<WorkspaceEntity>();
  @Output() cancelled = new EventEmitter();
  @Input() workspace;
  isBusy = false;
  constructor(private wsApi: WorkspaceApiService, private confirmModal: ConfirmModalService) {
  }
  async confirm() {
    this.isBusy = true;
    const result = await this.wsApi.saveWorkspace(this.workspace);
    this.isBusy = false;
    if (result.error) return this.confirmModal.showMessage(result.error);
    UpdateObj(this.workspace, result);
    this.confirmed.emit(this.workspace);
  }
  decline(): void {
    this.cancelled.emit();
  }
}
