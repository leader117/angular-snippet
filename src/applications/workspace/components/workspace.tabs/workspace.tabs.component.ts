import { Component, OnInit, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { ArrayRemoveItem, UpdateObj } from 'src/scripts/utilities';
import { ConfirmModalService } from 'src/components/confirm.modal/confirm.modal.service';
import { WorkspaceApiService } from 'src/services/workspace.apis.service';
import { AppRouteService } from 'src/services/app.route';
import { InputModalService } from 'src/components/input.modal/input.modal.service';
import { IToolApp } from 'src/applications/base/IToolApp';
import { GaService } from 'src/services/ga.service';
import { WorkspaceEntity } from 'src/graphql/types/WorkspaceEntity';
import { WindowEntity } from 'src/graphql/types/WindowEntity';



/**
 * @title App main search box
 */
@Component({
  selector: 'app-workspace-tabs',
  templateUrl: 'workspace.tabs.component.html',
  styleUrls: ['workspace.tabs.component.less'],
})
export class WorkspaceTabsAppComponent implements OnInit, OnDestroy, IToolApp {
  workspaces: WorkspaceEntity[] = [];
  selectedWorkspace: WorkspaceEntity;
  editedWorkspace: WorkspaceEntity;
  activeView = 'ws_tabs';
  private dashboardWidth: number;
  private dashboardHeight: number;
  private defaultWidgetWidth: number;
  private defaultWidgetHeight: number;

  constructor(
    private wsApi: WorkspaceApiService,
    private confirmModal: ConfirmModalService,
    private appRoute: AppRouteService,
    private inputModal: InputModalService,
    private ga: GaService,
    private dashboardEl: ElementRef,
  ) {}
  async ngOnInit() {
    this.dashboardHeight = this.dashboardEl.nativeElement.clientHeight;
    this.dashboardWidth = this.dashboardEl.nativeElement.clientWidth;
    this.defaultWidgetWidth = this.dashboardWidth / 4;
    this.defaultWidgetHeight = this.dashboardHeight / 4;

    this.workspaces = await this.wsApi.loadUserWorkspaces();
    this.openWorkspace(this.selectedWorkspace || this.getWorkspaceFromPath() || this.workspaces[0]);
    this.appRoute.subscribe(() => {
      if (this.activeView !== 'ws_tabs') return;
      const ws = this.getWorkspaceFromPath();
      if (!ws) return;
      this.openWorkspace(ws);
      this.activeView = this.appRoute.getParam('activeView') || 'ws_tabs';
      if (this.dashboardEl.nativeElement.clientHeight && this.dashboardEl.nativeElement.clientWidth) {
        this.dashboardHeight = this.dashboardEl.nativeElement.clientHeight;
        this.dashboardWidth = this.dashboardEl.nativeElement.clientWidth;
        this.defaultWidgetWidth = this.dashboardWidth / 4;
        this.defaultWidgetHeight = this.dashboardHeight / 4;
      }
    });
  }
  ngOnDestroy() {
  }
  getWorkspaceFromPath() {
    if (!this.appRoute.isWorkspacePath()) return;
    const wsId = this.appRoute.getUrlPaths()[1];
    return this.workspaces.find(ws => ws.id === wsId);
  }
  openWorkspace(workspace: WorkspaceEntity) {
    if (!workspace) return;
    this.selectedWorkspace = workspace;
    const maxZindex = Math.max(this.selectedWorkspace.getMaxZIndex(), 100);

    this.selectedWorkspace.windows
      // only news elements
      .filter(w => !w.settings.zIndex || !w.settings.alreadyMoved || w.settings.justAdded)
      .forEach((tab, ind) => {
        if (!tab.settings.zIndex) {
          tab.settings.zIndex = maxZindex + ind + 1;
        }

        const widgetHeight = tab.settings.height ? tab.settings.height : tab.resource.settings && tab.resource.settings.height
          ? tab.resource.settings.height : this.defaultWidgetHeight;
        const widgetWidth = tab.settings.width ? tab.settings.width : tab.resource.settings && tab.resource.settings.width
          ? tab.resource.settings.width : this.defaultWidgetWidth;

        tab.settings = {
          ...tab.settings,
          width: widgetWidth,
          height: widgetHeight,
          x: tab.settings.x ? tab.settings.x : (this.dashboardWidth / 2) - (widgetWidth / 2) - 25 + 25 * ind,
          y: tab.settings.y ? tab.settings.y : (this.dashboardHeight / 2) - (widgetHeight / 2) - 25 + 25 * ind,
          zIndex: tab.settings.zIndex,
        };

        if (tab.settings.justAdded) {
          delete tab.settings.justAdded;
          console.log('saved');
          this.updateTab(tab);
        }
    });
    workspace.isOpened = true;
    this.appRoute.navigateWorkspaceApp(workspace.id);
  }
  getWorkspaceOfTab(tab) {
    const workspace = this.workspaces.find(w => w.windows.map(t => t.id).includes(tab.id));
    return workspace;
  }
  removeTab(tab) {
    if (!this.selectedWorkspace) return;
    if (this.selectedWorkspace.windows.length < 2) {
      this.confirmModal.showMessage('Workspace should have at least one tab');
      return;
    }
    this.confirmModal.show({}, async () => {
      const ws = this.getWorkspaceOfTab(tab);
      const draftTab = ws.windows.find(w => w.id === tab.id);
      draftTab.isBusy = true;
      const res = await this.wsApi.deleteWorkspaceTab(ws, draftTab);
      ArrayRemoveItem(this.selectedWorkspace.windows, draftTab);
      draftTab.isBusy = false;
      if (res.error) return this.confirmModal.showMessage(res.error);
    });
  }
  renameTab(tab) {
    const workspace = this.getWorkspaceOfTab(tab);
    if (!workspace) return;
    this.inputModal.show('Rename tool', tab.title, async (newName) => {
      tab.isBusy = true;
      const res = await this.wsApi.saveWorkspaceTab(workspace, tab, { ...tab, title: newName });
      tab.isBusy = false;
      if (res.error) return this.confirmModal.showMessage(res.error);
    });
  }
  async updateTab(tab: WindowEntity) {
    const workspace = this.getWorkspaceOfTab(tab);
    if (!workspace) return;
    tab.isBusy = true;
    const res = await this.wsApi.saveWorkspaceTab(workspace, tab, tab.clone());
    tab.isBusy = false;
    if (res.error) return this.confirmModal.showMessage(res.error);
  }
  openTab(tab: WindowEntity) {
    this.selectedWorkspace.setTopWindow(tab);
    tab.settings.isClosed = false;
    this.updateTab(tab);
  }
  openInBrowserTab(tab: WindowEntity) {
    window.open(`/tab/${tab.id}`, '_blank');
  }
  removeWorkspace(ws) {
    if (this.workspaces.length < 2) {
      this.confirmModal.showMessage('You should have at least one workspace');
      return;
    }
    this.confirmModal.show({}, async () => {
      const res = await this.wsApi.deleteWorkspace(ws);
      if (res.error) return this.confirmModal.showMessage(res.error);
      ArrayRemoveItem(this.workspaces, ws);
      if (this.selectedWorkspace === ws) this.openWorkspace(this.workspaces[0]);
      this.ga.trackEvent('delete', 'workspace');
    });
  }
  showWorkspaceDetails(workspace: WorkspaceEntity) {
    this.editedWorkspace = workspace.clone();
    this.setActiveView('ws_details');
  }
  showWorkspaceTools() {
    const ws = this.selectedWorkspace;
    this.appRoute.navigateToolsApp(undefined, { workspace: ws && ws.id });
  }
  setActiveView(view: string) {
    this.activeView = view;
    this.appRoute.navigate(undefined, {activeView: view }, true);
  }
  onAppCreated({ event: instance, tab }: { event: any, tab: WindowEntity }) {
    if (!tab.settings) return;
    if (tab.settings) instance.setToolParams(this.selectedWorkspace, tab);
  }
  afterSaveWorkspace(ws: WorkspaceEntity) {
    UpdateObj(this.selectedWorkspace, ws);
    this.setActiveView('ws_tabs');
  }
  getToolTitle() { return 'Tabs'; }
  getToolParams() {
    return {};
  }
  setToolParams(ws: WorkspaceEntity, tab: WindowEntity) { }
}
