import { Component, EventEmitter, Input, Output, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'workspace-expanded-panel',
  templateUrl: 'workspace.expanded-panel.component.html',
  styleUrls: ['workspace.expanded-panel.component.less'],
})
export class WorkspaceExpandedPanelComponent {
  @ViewChild('details', { static: true }) details: ElementRef;
  @Output() clickOpen = new EventEmitter();
  isOpen = false;
  @Input() ws;

  openWorkspace() {
    this.clickOpen.emit();
  }

  toggleOpen() {
    this.isOpen = !this.isOpen;
    if (this.isOpen) {
      setTimeout(() => { this.details.nativeElement.style.overflow = 'auto'; }, 400);
    } else {
      this.details.nativeElement.style.overflow = 'hidden';
    }
  }
}
