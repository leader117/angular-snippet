import { Component, OnInit, OnDestroy, OnChanges, ViewChild } from '@angular/core';
import { WorkspaceApiService } from 'src/services/workspace.apis.service';
import { AppRouteService } from 'src/services/app.route';
import { IToolApp } from 'src/applications/base/IToolApp';
import { WindowEntity } from 'src/graphql/types/WindowEntity';
import { WorkspaceEntity } from 'src/graphql/types/WorkspaceEntity';
import { WorkspaceModalService } from '../../WorkspaceModals';
import { AppLoaderComponent } from 'src/applications/base/app.loader/app.loader.component';
import { AutoUnsubscribeComponent } from 'src/components/auto-unsubscribe';
import { GaService } from 'src/services/ga.service';


/**
 * @title App main search box
 */
@Component({
  selector: 'app-workspace-tools',
  templateUrl: 'workspace.tools.component.html',
  styleUrls: ['workspace.tools.component.less'],
})
export class WorkspaceToolsAppComponent extends AutoUnsubscribeComponent implements OnInit, OnDestroy, IToolApp, OnChanges {
  view = '';
  selectedTool: WindowEntity;
  tools: Array<WindowEntity>;
  workspace: WorkspaceEntity;
  isLoading = false;
  @ViewChild('appLoader', { static: true }) appLoader: AppLoaderComponent;
  $1;
  constructor(
    private wsApi: WorkspaceApiService,
    private appRoute: AppRouteService,
    private ga: GaService,
    private wsModal: WorkspaceModalService) {
      super();
  }
  ngOnChanges(params) {
  }
  async ngOnInit() {
    this.isLoading = true;
    this.wsApi.loadUserWorkspaces();
    this.tools = await this.wsApi.getWorkspaceTools();
    this.isLoading = false;
    this.workspace = this.wsApi.getWorkspace(this.appRoute.getParam('workspace'));
    this.$1 = this.appRoute.subscribe(() => this.onPathChange());
    this.onPathChange();
  }
  ngOnDestroy() {
  }
  async onPathChange() {
    if (this.appRoute.isToolsPath()) {
      const toolId = this.appRoute.getUrlPaths()[1];
      const tool = toolId && await this.wsApi.getWorkspaceTool(toolId);
      if (tool && tool.error) return;
      if (tool) this.selectTool(tool);
      else this.view = 'selectTool';
    } else this.view = 'selectTool';
  }
  async addWidget(tab: WindowEntity, changeUrl = false) {
    const draftTool = tab.clone();
    draftTool.settings.justAdded = true;
    const resourceId = this.appRoute.getParam('resourceId');
    if (resourceId) draftTool.id = resourceId;
    if (draftTool.instance) {
      draftTool.settings = draftTool.instance.getToolParams();
      draftTool.title = draftTool.instance.getToolTitle() || draftTool.title;
    }

    tab.isBusy = true;
    const workspace = this.wsApi.getWorkspace(this.appRoute.getParam('workspace'));
    const result: any = await this.wsApi.addWindow(workspace, draftTool);
    tab.isBusy = false;
    if (changeUrl) this.appRoute.navigateWorkspaceApp(workspace.id);
  }
  selectTool(tab: WindowEntity, changeUrl = false) {
    this.selectedTool = tab.clone();
    this.view = 'viewTool';
    if (changeUrl) this.appRoute.navigateToolsApp(this.selectedTool.id);
    this.appLoader.createComponent(tab.uiCode);
  }

  onAppCreated(instance: IToolApp) {
    const tool = this.selectedTool;
    tool.instance = instance;
    if (tool.settings) instance.setToolParams(undefined, tool);
  }
  showAddToWorkspaceModal() {
    const tab = this.selectedTool.clone();
    const resourceId = this.appRoute.getParam('resourceId');
    if (resourceId) tab.id = resourceId;
    if (tab.instance) {
      tab.settings = tab.instance.getToolParams();
      tab.title = tab.instance.getToolTitle() || tab.title;
    }
    this.wsModal.showAddToolToWorkspaceModal(tab, (res) => {
      this.view = 'selectTool';
      res.ws.setTopWindow(res.tab);
      this.wsApi.saveWorkspaceTab(res.ws, res.tab, res.tab);
      this.appRoute.navigateWorkspaceApp(res.ws.id);
      this.ga.trackEvent(tab.title, 'Add to Workspace');
    });
  }
  back() {
    this.view = 'selectTool';
    window.history.back();
  }
  getToolTitle() { return 'Tools'; }
  getToolParams() {
    return {};
  }
  setToolParams(ws: WorkspaceEntity, tab: WindowEntity) { }
}
