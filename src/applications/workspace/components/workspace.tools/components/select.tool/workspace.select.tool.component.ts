import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { ArrayGroupByAsArray } from 'src/scripts/utilities';
import { WindowEntity } from 'src/graphql/types/WindowEntity';



/**
 * @title App main search box
 */
@Component({
  selector: 'app-workspace-select-tool',
  templateUrl: 'workspace.select.tool.component.html',
  styleUrls: ['workspace.select.tool.component.less'],
})
export class WorkspaceSelectToolAppComponent implements OnInit, OnChanges {
  groups = [];
  @Input() tools: Array<WindowEntity> = [];
  @Output() select = new EventEmitter<WindowEntity>();
  constructor() {
  }
  async ngOnInit() {
    this.groups = ArrayGroupByAsArray(this.tools || [], 'group');
  }
  ngOnChanges(params) {
    if (params.tools) this.groups = ArrayGroupByAsArray(this.tools || [], 'group');
  }
  selectTool(app: WindowEntity) {
    this.select.emit(app);
  }
}
