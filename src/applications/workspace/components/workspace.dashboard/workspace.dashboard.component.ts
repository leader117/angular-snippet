import { Component, Input, Output, EventEmitter, OnChanges, SimpleChanges, OnInit, ViewChild, ElementRef } from '@angular/core';
import { WindowEntity } from 'src/graphql/types/WindowEntity';
import { UpdateObj } from 'src/scripts/utilities';

@Component({
  selector: 'app-workspace-dashboard',
  templateUrl: './workspace.dashboard.component.html',
  styleUrls: ['./workspace.dashboard.component.less'],
})
export class WorkspaceDashboardComponent implements OnInit, OnChanges {
  @Input()
  public readonly tabs: WindowEntity[];

  @Output()
  public appCreated = new EventEmitter<any>();

  @Output()
  public updateTab = new EventEmitter<WindowEntity>();

  public positions = [];


  private get _activeZindex(): number {
    return Math.max(...this.tabs.map(t => t.settings.zIndex || 100));
  }

  public ngOnChanges(changes: SimpleChanges) {
    if (!('tabs' in changes) || !changes.tabs.currentValue) {
      return;
    }

    this.tabs.forEach((t, i) => {
      t.settings.zIndex = t.settings.zIndex || 100 + i;
      t.settings.height = t.settings.isMinimized ? 26 : t.settings.height || t.settings.beforeMinimizationHeight;
      t.settings.x = t.settings.x || 0;
      t.settings.y = t.settings.y || 0;
    });

    this.positions = this.tabs.map(t => ({ x: t.settings.x, y: t.settings.y, }));
  }

  public ngOnInit() { }

  public onMoveEnd({ x, y }, tab, tabIndex: number) {
    if (tab.settings.x === x && tab.settings.y === y) {
      return;
    }
    UpdateObj(tab.settings, {
      alreadyMoved: true,
      x,
      y,
    });
    this.updateTab.emit(tab);
  }

  public closeTab(tab) {
    tab.settings.isClosed = true;
    this.updateTab.emit(tab);
  }

  public onResizeStop(tab, element: HTMLElement) {
    UpdateObj(tab.settings, {
      width: element.clientWidth,
      height: element.clientHeight,
    });
    this.updateTab.emit(tab);
  }

  public selectWidget(tab, element: HTMLElement) {
    if (tab.settings.zIndex && tab.settings.zIndex === this._activeZindex) {
      return;
    }
    tab.settings.zIndex = (this._activeZindex ? this._activeZindex : 100) + 2;
    this.updateTab.emit(tab);
  }

  public minimizeItem(tab, element: HTMLElement) {
    tab.settings.isMinimized = !tab.settings.isMinimized;

    if (tab.settings.isMinimized) {
      tab.settings.width = tab.settings.width ? tab.settings.width : element.clientWidth;
      tab.settings.beforeMinimizationHeight = element.clientHeight;
    }

    tab.settings.height = tab.settings.isMinimized ? 26 : tab.settings.beforeMinimizationHeight || tab.resource.settings.height;
    this.updateTab.emit(tab);
  }

  public showSettings() { }

  public onCreateApp(event, tab) {
    this.appCreated.emit({ event, tab });
  }
}
