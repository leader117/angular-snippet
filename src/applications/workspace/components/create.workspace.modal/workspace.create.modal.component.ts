import { Component, Input, Output, EventEmitter } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { WorkspaceEntity } from 'src/graphql/types/WorkspaceEntity';

/**
 * @title App main search box
 */
@Component({
  selector: 'app-workspace-create-modal',
  templateUrl: 'workspace.create.modal.component.html',
  styleUrls: ['workspace.create.modal.component.css'],
})
export class WorkspaceCreateModalComponent {
  @Output() confirmed = new EventEmitter<WorkspaceCreateModalComponent>();
  @Output() cancelled = new EventEmitter();
  @Input() workspace = new WorkspaceEntity();
  errorMessage: string;
  isBusy = false;
  invalidName = false;
  invalidDescription = false;
  constructor(public bsModalRef: BsModalRef) {
  }
  confirm(): void {
    this.errorMessage = undefined;
    this.invalidName = !this.workspace.name;
    this.invalidDescription = !this.workspace.description;
    if (this.invalidName || this.invalidDescription) return;
    this.confirmed.emit(this);
  }
  decline(): void {
    this.cancelled.emit();
    this.bsModalRef.hide();
  }
  showError(err: string) {
    this.errorMessage = err;
  }
}
