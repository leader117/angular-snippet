import { Component, Input, Output, EventEmitter, OnInit, OnChanges } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { WorkspaceEntity } from 'src/graphql/types/WorkspaceEntity';
import { WindowEntity } from 'src/graphql/types/WindowEntity';

/**
 * @title App main search box
 */
@Component({
  selector: 'app-workspace-add-tool-modal',
  templateUrl: 'workspace.add.tool.modal.component.html',
  styleUrls: ['workspace.add.tool.modal.component.css'],
})
export class WorkspaceAddToolModalComponent implements OnChanges, OnInit {
  @Output() confirmed = new EventEmitter<WorkspaceAddToolModalComponent>();
  @Output() cancelled = new EventEmitter();
  @Output() newWorkspaceDialog = new EventEmitter<WorkspaceAddToolModalComponent>();
  @Input() tool: WindowEntity;
  @Input() selectedWorkspace: WorkspaceEntity;
  @Input() workspaces: Array<WorkspaceEntity> = [];
  errorMessage: string;
  isBusy = false;
  constructor(public bsModalRef: BsModalRef) {
  }
  async ngOnInit() {
    if (!this.selectedWorkspace) this.selectedWorkspace = this.workspaces[0];
  }
  async ngOnChanges(params) {
    if (params.workspaces && this.workspaces) {
      if (!this.selectedWorkspace) this.selectedWorkspace = this.workspaces[0];
    }
  }
  confirm(): void {
    this.errorMessage = undefined;
    this.confirmed.emit(this);
  }
  decline(): void {
    this.cancelled.emit();
    this.bsModalRef.hide();
  }
  showNewWorkspaceDialog(): void {
    this.newWorkspaceDialog.emit(this);
  }
  showError(err: string) {
    this.errorMessage = err;
  }
}
