import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { WorkspaceApiService } from 'src/services/workspace.apis.service';
import { AppRouteService } from 'src/services/app.route';
import { IToolApp } from 'src/applications/base/IToolApp';
import { WorkspaceEntity } from 'src/graphql/types/WorkspaceEntity';
import { WindowEntity } from 'src/graphql/types/WindowEntity';
import { WorkspaceModalService } from '../../WorkspaceModals';

/**
 * @title App main search box
 */
@Component({
  selector: 'app-workspace-home',
  templateUrl: 'workspace.home.component.html',
  styleUrls: ['workspace.home.component.less'],
})
export class WorkspaceHomeAppComponent implements OnInit, OnDestroy, IToolApp {
  workspaces: WorkspaceEntity[] = [];
  isLoading = false;
  constructor(
    private wsApi: WorkspaceApiService,
    private wsModal: WorkspaceModalService,
    private appRoute: AppRouteService) {
  }
  async ngOnInit() {
    this.isLoading = true;
    this.workspaces = await this.wsApi.loadUserWorkspaces();
    this.isLoading = false;
  }
  ngOnDestroy() {
  }
  async openWorkspace(ws: WorkspaceEntity, tab?: WindowEntity) {
    if (tab) {
      ws.setTopWindow(tab);
      await this.wsApi.saveWorkspaceTab(ws, tab, tab);
    }
    this.appRoute.navigateWorkspaceApp(ws.id);
  }
  showWorkspaceCreateModal() {
    this.wsModal.showCreateModal(new WorkspaceEntity(), (ws) => this.openWorkspace(ws));
  }
  getToolTitle() { return 'Workspace Home'; }
  getToolParams() {
    return {};
  }
  setToolParams(ws: WorkspaceEntity, tab: WindowEntity) { }
}
