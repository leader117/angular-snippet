import { NgModule } from '@angular/core';
import { AngularDraggableModule } from 'angular2-draggable';

import { AppSharedModule } from 'src/shared-modules';
import { WorkspaceTabsAppComponent } from './components/workspace.tabs/workspace.tabs.component';
import { WorkspaceHomeAppComponent } from './components/workspace.home/workspace.home.component';
import { WorkspaceDetailsComponent } from './components/workspace/details/workspace.details.component';
import { WorkspaceToolsAppComponent } from './components/workspace.tools/workspace.tools.component';
import { WorkspaceSelectToolAppComponent } from './components/workspace.tools/components/select.tool/workspace.select.tool.component';
import {WorkspaceExpandedPanelComponent} from './components/workspace.expanded-panel/workspace.expanded-panel.component';
import { AppLoaderModule } from '../base/app.loader/app.loader.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { WorkspaceModalService } from './WorkspaceModals';
import { ConfirmModalService } from 'src/components/confirm.modal/confirm.modal.service';
import { WorkspaceApiService } from 'src/services/workspace.apis.service';
import { GraphQlApiService } from 'src/services/graphql.api';
import { ResourcesApiService } from 'src/services/resources.api';
import { WorkspaceCreateModalComponent } from './components/create.workspace.modal/workspace.create.modal.component';
import { WorkspaceAddToolModalComponent } from './components/add.tool.modal/workspace.add.tool.modal.component';
import { WorkspaceDashboardComponent } from './components/workspace.dashboard/workspace.dashboard.component';
import { ModelBuilderIndexModule } from '../model.builder/model.builder.module';
import { KnowledgeGraphAppModule } from '../knowledge.graph/knowledge.graph.module';

@NgModule({
  declarations: [
    WorkspaceTabsAppComponent,
    WorkspaceDetailsComponent,
    WorkspaceHomeAppComponent,
    WorkspaceToolsAppComponent,
    WorkspaceSelectToolAppComponent,
    WorkspaceExpandedPanelComponent,
    WorkspaceCreateModalComponent,
    WorkspaceAddToolModalComponent,
    WorkspaceDashboardComponent,
  ],
  imports: [
    AppSharedModule,
    AppLoaderModule,
    ModalModule.forRoot(),
    AngularDraggableModule,

    ModelBuilderIndexModule,
    KnowledgeGraphAppModule,
  ],
  providers: [
    ConfirmModalService,
    WorkspaceModalService,
    GraphQlApiService,
    ResourcesApiService,
    WorkspaceApiService,
  ],
  exports: [
    WorkspaceTabsAppComponent,
    WorkspaceHomeAppComponent,
    WorkspaceDetailsComponent,
    WorkspaceToolsAppComponent,
    WorkspaceSelectToolAppComponent,
    WorkspaceAddToolModalComponent,
    WorkspaceDashboardComponent,
  ],
  entryComponents: [WorkspaceCreateModalComponent, WorkspaceAddToolModalComponent]
})
export class WorkspaceIndexModule { }

@NgModule({
  imports: [WorkspaceIndexModule],
  bootstrap: [WorkspaceHomeAppComponent],
})
export class WorkspaceHomeAppModule { }
@NgModule({
  imports: [WorkspaceIndexModule],
  bootstrap: [WorkspaceTabsAppComponent],
})
export class WorkspaceTabsAppModule { }

@NgModule({
  imports: [WorkspaceIndexModule],
  bootstrap: [WorkspaceToolsAppComponent],
})
export class WorkspaceToolsAppModule { }
