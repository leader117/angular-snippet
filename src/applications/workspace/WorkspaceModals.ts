import { WorkspaceApiService } from 'src/services/workspace.apis.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { WorkspaceEntity } from 'src/graphql/types/WorkspaceEntity';
import { WindowEntity } from 'src/graphql/types/WindowEntity';
import { AppRouteService } from 'src/services/app.route';
import { Injectable } from '@angular/core';
import { WorkspaceCreateModalComponent } from './components/create.workspace.modal/workspace.create.modal.component';
import { WorkspaceAddToolModalComponent } from './components/add.tool.modal/workspace.add.tool.modal.component';

@Injectable()
export class WorkspaceModalService {
    constructor(
        private wsApi: WorkspaceApiService,
        private modalService: BsModalService,
        private appRoute: AppRouteService) {

    }

    showCreateModal(workspace: WorkspaceEntity, onConfirmed?: (ws: WorkspaceEntity) => void) {
        const bsModalRef = this.modalService.show(WorkspaceCreateModalComponent, { initialState: { workspace } });
        const $1 = bsModalRef.content.confirmed.subscribe(async (instance: WorkspaceCreateModalComponent) => {
            instance.isBusy = true;
            const result = await this.wsApi.createWorkspace(instance.workspace);
            instance.isBusy = false;
            if (result.error) return instance.showError(result.error);
            WorkspaceApiService.workspaces.push(result);
            if (onConfirmed) onConfirmed(result);
            $1.unsubscribe();
            instance.bsModalRef.hide();
        });
    }
    async showAddToolToWorkspaceModal(tool: WindowEntity, onConfirmed?: (res: { ws: WorkspaceEntity, tab: WindowEntity }) => void) {
        const workspaces = await this.wsApi.loadUserWorkspaces();
        const selectedWorkspace = this.wsApi.getWorkspace(this.appRoute.getParam('workspace'));
        const initialState = { workspaces, selectedWorkspace, tool };
        const bsModalRef = this.modalService.show(WorkspaceAddToolModalComponent, { initialState });
        const $1 = bsModalRef.content.confirmed.subscribe(async (instance: WorkspaceAddToolModalComponent) => {
            instance.isBusy = true;
            const result: any = await this.wsApi.addWindow(instance.selectedWorkspace, instance.tool);
            instance.isBusy = false;
            if (result.error) return instance.showError(result.error);
            if (onConfirmed) onConfirmed({ ws: instance.selectedWorkspace, tab: result });
            $1.unsubscribe();
            instance.bsModalRef.hide();
        });
        const $2 = bsModalRef.content.newWorkspaceDialog.subscribe(async (instance: WorkspaceAddToolModalComponent) => {
            this.showCreateModal(new WorkspaceEntity(), (ws) => {
                instance.selectedWorkspace = ws;
                $2.unsubscribe();
            });
        });
    }
}
