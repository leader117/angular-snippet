import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { WorkspaceApiService } from 'src/services/workspace.apis.service';
import { AppRouteService } from 'src/services/app.route';
import { IToolApp } from 'src/applications/base/IToolApp';
import { WindowEntity } from 'src/graphql/types/WindowEntity';
import { WorkspaceEntity } from 'src/graphql/types/WorkspaceEntity';



/**
 * @title
 */
@Component({
  selector: 'app-workspace-browser-tab',
  templateUrl: 'workspace.browser.tab.component.html',
  styleUrls: ['workspace.browser.tab.component.less'],
})
export class WorkspaceBrowserTabAppComponent implements OnInit, OnDestroy {
  workspace: WorkspaceEntity;
  window: WindowEntity;
  isInvalid = false;
  constructor(
    private wsApi: WorkspaceApiService,
    private appRoute: AppRouteService,
  ) {}
  async ngOnInit() {
    const tabId = this.appRoute.getUrlPaths()[1];
    const workspaces = await this.wsApi.loadUserWorkspaces();
    this.workspace = workspaces.find(ws => !!ws.windows.find(w => w.id === tabId));
    this.window = this.workspace && this.workspace.windows.find(w => w.id === tabId);
    this.isInvalid = !this.window;
  }
  ngOnDestroy() {
  }
  onAppCreated(tab: WindowEntity, instance: IToolApp) {
    if (!tab.settings) return;
    if (tab.settings) instance.setToolParams(this.workspace, tab);
  }
}
