import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorkspaceBrowserTabAppComponent } from './workspace.browser.tab.component';



const routes: Routes = [
  {
    path: '**',
    component: WorkspaceBrowserTabAppComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkspaceBrowserTabRoutingModule { }
