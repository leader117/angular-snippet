import { NgModule } from '@angular/core';
import { AppSharedModule } from 'src/shared-modules';
import { AppLoaderModule } from '../base/app.loader/app.loader.module';
import { WorkspaceBrowserTabAppComponent } from './workspace.browser.tab.component';
import { WorkspaceBrowserTabRoutingModule } from './WorkspaceBrowserTabRoutingModule';
import { ModalModule } from 'ngx-bootstrap/modal';
import { WorkspaceApiService } from 'src/services/workspace.apis.service';
import { GraphQlApiService } from 'src/services/graphql.api';
import { ResourcesApiService } from 'src/services/resources.api';

@NgModule({
  declarations: [
    WorkspaceBrowserTabAppComponent
  ],
  imports: [
    AppSharedModule,
    AppLoaderModule,
    ModalModule.forRoot(),
    WorkspaceBrowserTabRoutingModule,
  ],
  providers: [WorkspaceApiService, GraphQlApiService, ResourcesApiService],
  bootstrap: [],
  exports: [
    WorkspaceBrowserTabAppComponent
  ],
})
export class WorkspaceBrowserTabAppModule { }
