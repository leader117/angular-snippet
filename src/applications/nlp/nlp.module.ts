import { NgModule } from '@angular/core';
import { DatePickerModule } from 'src/components/date.picker/date.picker.module';
import { NLPComponent } from './nlp.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ConfirmModalService } from 'src/components/confirm.modal/confirm.modal.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { HighchartModule } from 'src/components/highchart/highchart.module';
import { SpinnerFlagModule } from 'src/components/spinner/spinner.module';
import { GRPCService } from 'src/grpc/grpc.service';
import { ModelBuilderRequestProgressModule } from 'src/components/model.builder.request.progress/model.builder.request.progress.module';

@NgModule({
  declarations: [
    NLPComponent,
  ],
  imports: [
   CommonModule, FormsModule, DatePickerModule, SpinnerFlagModule, HighchartModule, ModelBuilderRequestProgressModule
  ],
  providers: [ConfirmModalService, BsModalService, GRPCService],
  bootstrap: [NLPComponent],
  exports: [NLPComponent],
})
export class NLPAppModule { }
