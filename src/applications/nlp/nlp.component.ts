import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { AutoUnsubscribeComponent } from 'src/components/auto-unsubscribe';
import { IToolApp } from '../base/IToolApp';
import { ConfirmModalService } from 'src/components/confirm.modal/confirm.modal.service';
import { GaService } from 'src/services/ga.service';
import { WorkspaceEntity } from 'src/graphql/types/WorkspaceEntity';
import { WindowEntity } from 'src/graphql/types/WindowEntity';
import { ActionMetadata } from 'src/grpc/proto-gen/eqltree_pb';
import { NPLSettings } from './NLPSettings';
import { HighchartComponent } from 'src/components/highchart/highchart.component';
import { GRPCService, SubmitUIRequestWithHandle } from 'src/grpc/grpc.service';
import { EqlSettings } from '../model.builder/entries/EqlSettings';
import { EQLEngine } from 'src/grpc/proto-gen/executor_grpc_pb';
import * as moment from 'moment-timezone';
import { drawHistogramChart, drawTimeSeriesChart } from '../model.builder/model.builder.utilities';
import { AppRouteService } from 'src/services/app.route';
import { MODEL_BUILDER_TOOL_ID } from 'src/constants';

/**
 * @title App main search box
 */
@Component({
  selector: 'app-nlp',
  templateUrl: 'nlp.component.html',
  styleUrls: ['nlp.component.less'],
})
export class NLPComponent extends AutoUnsubscribeComponent implements OnInit, OnDestroy, IToolApp {
  showInstructions = true;
  showPlottingSettings = true;
  barSizeItems = [
    { code: ActionMetadata.BarSize.BS_1MIN, name: '1 Minute' },
    { code: ActionMetadata.BarSize.BS_3MIN, name: '3 Minutes' },
    { code: ActionMetadata.BarSize.BS_5MIN, name: '5 Minutes' },
    { code: ActionMetadata.BarSize.BS_10MIN, name: '10 Minutes' },
    { code: ActionMetadata.BarSize.BS_15MIN, name: '15 Minutes' },
    { code: ActionMetadata.BarSize.BS_30MIN, name: '30 Minutes' },
    { code: ActionMetadata.BarSize.BS_1HOUR, name: '1 Hour' },
    { code: ActionMetadata.BarSize.BS_1DAY, name: '1 Day' },
    { code: ActionMetadata.BarSize.BS_MONTH, name: '1 Month' },
  ];
  viewByItems = [
    { code: 'time', name: 'Time' },
    { code: 'frequency', name: 'Frequency' },
  ];
  recalculateEveryItems = [
    { code: '4d', name: '4 Days' },
    { code: '1w', name: '1 Week' },
    { code: '1mon', name: '1 Month' },
    { code: '1y', name: '1 Year' },
  ];
  groupByItems = [
    { code: 'mean', name: 'Mean' },
    { code: 'median', name: 'Median' },
    { code: 'count', name: 'Count' },
  ];
  settings = new NPLSettings();
  loading = false;
  activeReq: SubmitUIRequestWithHandle;
  @ViewChild('chart1', { static: true }) chart1: HighchartComponent;
  constructor(
    private confirmModal: ConfirmModalService,
    private ga: GaService,
    private grpc: GRPCService,
    private appRoute: AppRouteService) {
    super();
  }
  ngOnInit() {
    this.chart1.chartOptions = {
      plotOptions: {
        series: {
          compare: 'value'
        }
      }
    };
  }
  ngOnDestroy() {
  }
  onStartDateChanged(startDate: Date) {
    this.settings.startDate = startDate;
  }
  onEndDateChanged(endDate: Date) {
    this.settings.endDate = endDate;
  }
  async run() {
    this.loading = true;
    try {
      const res = await this.getUIReqResult();
      if (res.histogram && res.histogram.length) drawHistogramChart(this.chart1, res);
      if (res.series && res.series.length) drawTimeSeriesChart(this.chart1, res);
    } catch (err) {
      this.confirmModal.showMessage(err);
    }
    this.loading = false;
  }
  async cancelRun() {
    if (!this.activeReq) return;
    try { await this.grpc.cancelTask(this.activeReq.taskHandle); }
    catch (err) { }
    this.activeReq = null;
  }
  setShowPlottingSettings(value: boolean) {
    this.showPlottingSettings = value;
    this.chart1.reflow();
  }
  private async getUIReqResult() {
    const req = this.grpc.getSubmitRequest(this.getEqlCode(), EqlSettings.getDefault());
    this.activeReq = this.grpc.getSubmitUIRequest(req, [EQLEngine.UIPart.UIPART_MAIN_TIMESERIES]);
    const res = (await this.grpc.submitUIParseAsync(this.activeReq))[0];
    if (!res) throw new Error('Invalid null response from Engine');
    return res;
  }
  private getEqlCode() {
    const s = this.settings;
    const sDate = moment(s.startDate).format('YYYY.MM.DD');
    const eDate = moment(s.endDate).format('YYYY.MM.DD');

    let code = `with start_date=${sDate}, end_date=${eDate}
    with literal_proxy.v="raw_universe:michael_nlp_test:v", bar_size=BS_1MIN
    minusUni as UNIVERSE("raw_universe:michael_nlp_test")
    secl as ((minusUni.names->rebalance(${ s.recalculateEvery},default))[minusUni.v->rebalance(${s.recalculateEvery}, ${s.groupBy})->idesc()])[1:100]->tranches(${s.groupsCount},strict=0)
    ${ s.lockGroups ? '' : '//'} secl as secl->lock_seclist()
    secl->trim_tranches()->keys(proxy=v)->data()->linearize()->agg_rebalance(${ s.formatFrequencyForEngine()}, ${s.aggregation})`;
    if (s.viewBy === 'frequency') code += '-> histogram(10)';
    // console.log(code);
    return code;
  }
  sendToModelBuilder() {
    this.ga.trackEvent('Send to Models', 'NLP');
    this.appRoute.navigateToolsApp(MODEL_BUILDER_TOOL_ID, {}, { code: this.getEqlCode(), runCode: true });
  }
  getToolTitle() { return 'Equeum NLP'; }
  getToolParams() {
    return {
    };
  }
  setToolParams(ws: WorkspaceEntity, tab: WindowEntity) {
  }
}
