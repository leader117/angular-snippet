import { ActionMetadata } from 'src/grpc/proto-gen/eqltree_pb';
import * as moment from 'moment-timezone';

export class NPLSettings {
    security = 'SP500';
    frequency: ActionMetadata.BarSizeMap[keyof ActionMetadata.BarSizeMap] = ActionMetadata.BarSize.BS_5MIN;
    startDate: Date = moment('2015-01-01', 'YYYY-MM-DD').toDate();
    endDate: Date = moment('2015-02-01', 'YYYY-MM-DD').toDate();
    viewBy = 'time';
    recalculateEvery = '4d';
    groupBy = 'median';
    lockGroups = true;
    aggregation = 'median';
    groupsCount = 10;
    formatFrequencyForEngine() {
        switch (this.frequency) {
            case ActionMetadata.BarSize.BS_1MIN: return '1min';
            case ActionMetadata.BarSize.BS_3MIN: return '3min';
            case ActionMetadata.BarSize.BS_5MIN: return '5min';
            case ActionMetadata.BarSize.BS_10MIN: return '10min';
            case ActionMetadata.BarSize.BS_15MIN: return '15min';
            case ActionMetadata.BarSize.BS_30MIN: return '30min';
            case ActionMetadata.BarSize.BS_1HOUR: return '1hr';
            case ActionMetadata.BarSize.BS_1DAY: return '1d';
            case ActionMetadata.BarSize.BS_1WEEK: return '1w';
            case ActionMetadata.BarSize.BS_MONTH: return '1mon';
        }
    }
}
