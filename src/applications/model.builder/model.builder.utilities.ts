import { EQLEngine } from 'src/grpc/proto-gen/executor_grpc_pb';
import { HighchartComponent } from 'src/components/highchart/highchart.component';
import * as moment from 'moment-timezone';
import { UIPartResult } from 'src/grpc/grpc.service';
import * as Highcharts from 'highcharts';

export const defaultChart1Options = {
    chart: {
        // marginBottom: 0
    },
    title: {
        enabled: false,
        // text: 'Run your model to show chart here!'
    },
    credits: {
        enabled: false,
    },
    plotOptions: {
        series: {
            // compare: 'percent',
            // compareBase: 100,
        }
    },
    xAxis: {
        // visible: false
    },
    yAxis: [],
    navigator: {
        // enabled: false,
    },
    scrollbar: {
        // enabled: false,
    },
    rangeSelector: {
        buttons: [{
            type: 'day',
            count: 1,
            text: '1d'
        }, {
            type: 'week',
            count: 1,
            text: '1w'
        }, {
            type: 'month',
            count: 1,
            text: '1m'
        }, {
            type: 'month',
            count: 3,
            text: '3m'
        }, {
            type: 'month',
            count: 6,
            text: '6m'
        }, {
            type: 'ytd',
            text: 'YTD'
        }, {
            type: 'year',
            count: 1,
            text: '1y'
        }, /* {
        type: 'year',
        count: 3,
        text: '3y'
      }, {
        type: 'year',
        count: 5,
        text: '5y'
      }, */ {
            type: 'year',
            count: 10,
            text: '10y'
        }, {
            type: 'all',
            text: 'Max'
        }],
        selected: 8,
        // x: -30,
        inputPosition: {
            // align: 'left',
            x: -30
        },
    },
    tooltip: {
        // shape: 'square',
        // headerShape: 'callout',
        // borderWidth: 0,
        // shadow: false,
        // positioner: FixedTooltipPosition
    }
};

export const defaultTwoYAxisOptions = [{
    labels: {
        // align: 'left',
        // formatter: formatterPercent,
    },
    height: '65%',
    /* resize: {
      enabled: true
    } */
}, {
    labels: {
        // align: 'left',
    },
    top: '70%',
    height: '30%',
    offset: 0
}];
export const defaultOneYAxisOptions = [{
    labels: {},
}];
export const xDateFormats = {
    [EQLEngine.RepresentationBarSize.RBS_1DAY]: '%Y/%m/%d',
    [EQLEngine.RepresentationBarSize.RBS_15MIN]: '%Y/%m/%d %H:%M',
    [EQLEngine.RepresentationBarSize.RBS_3MIN]: '%Y/%m/%d %H:%M',
    [EQLEngine.RepresentationBarSize.RBS_1MIN]: '%Y/%m/%d %H:%M',
};
export function resolveChartData(rows, field) {
    return rows.map(r => {
        const dd = moment.tz(r.date, 'YYYY-MM-DDTHH:mm:ss', 'America/New_York').valueOf();
        if (Array.isArray(r[field])) return [dd, ...r[field]];
        else return [dd, r[field]];
    });
}
export function addChangeAxisViewButtons(chart: HighchartComponent) {
    const series = chart.getChartSeries();
    if (series.length < 2) return;
    let singleAxis = true;
    // if (singleAxis) chart.moveSeriesToDifferentAxis(singleAxis);
    const attr = {
        fill: '#e6ebf5',
        zIndex: 3,
        width: 60,
        height: 16,
        fontWeight: 'bold',
        color: '#000000',
        cursor: 'pointer',
    };
    let title = singleAxis ? 'Multi Axis' : 'Single Axis';
    const btn = chart.chart.renderer.button(title, 350, 5).attr(attr)
        .on('click', () => {
            singleAxis = !singleAxis;
            title = singleAxis ? 'Multi Axis' : 'Single Axis';
            btn.attr({ text: title });
            chart.moveSeriesToDifferentAxis(singleAxis, 'percent', s => s.type !== 'column');
        }).add();
    chart.moveSeriesToDifferentAxis(singleAxis, 'percent', s => s.type !== 'column');
}


export function drawHistogramChart(chart: HighchartComponent, res: UIPartResult) {
    const { histogram } = res;
    chart.isStock = false;
    chart.resetChart();
    const { options } = chart;
    const panelHeight = 150;
    Highcharts.merge(true, options, {
        chart: {
            type: 'column',
            height: (panelHeight + 30) * histogram.length + 50
        },
        tooltip: {
            shared: false,
            crosshairs: true,
            split: true,
            formatter: function () {
                return ['<b>' + this.x + '</b>'].concat(
                    this.points ? this.points.map(p => `${p.series.name}<br/>Range: ${p.x}<br/>Value: ${p.y}`) : []
                );
            },
        },
        xAxis: {
            categories: histogram[0].data.map(d => d.range),
            crosshair: true
        },
        yAxis: histogram.map((r, i) => {
            return {
                top: i * (panelHeight + 30) + 10,
                height: panelHeight,
                offset: 0,
                title: {
                    text: r.name,
                }
            };
        }),
        series: histogram.map((r, i) => ({ name: r.name, data: r.data.map(d => d.value), yAxis: i }))
    });
    chart.drawChart();
}
export function drawTimeSeriesChart(chart: HighchartComponent, res: UIPartResult) {
    chart.isStock = true;
    chart.resetChart();
    const fields = Object.keys(res.series[0]).filter(d => d !== 'date');
    fields.forEach((s) => {
        const data = resolveChartData(res.series, s);
        chart.addSeries({ name: s, data });
    });
    chart.drawChart();
}