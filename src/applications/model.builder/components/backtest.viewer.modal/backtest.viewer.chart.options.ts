import { formatterPercent } from 'src/components/highchart/highchart.component';

export const BACKTEST_VIEWER_CHART_OPTIONS = {
    chart: {
        // marginBottom: 0
    },
    title: {
        enabled: false,
        // text: 'Run your model to show chart here!'
    },
    credits: {
        enabled: false,
    },
    navigator: {
        // enabled: false,
    },
    scrollbar: {
        // enabled: false,
    },
    plotOptions: {
        series: {
            compare: 'percent'
        }
    },
    yAxis: [{
        labels: {
            // align: 'left',
            formatter: formatterPercent,
        },
    }],
    rangeSelector: {
        buttons: [{
            type: 'day',
            count: 1,
            text: '1d'
        }, {
            type: 'week',
            count: 1,
            text: '1w'
        }, {
            type: 'month',
            count: 1,
            text: '1m'
        }, {
            type: 'month',
            count: 3,
            text: '3m'
        }, {
            type: 'month',
            count: 6,
            text: '6m'
        }, {
            type: 'ytd',
            text: 'YTD'
        }, {
            type: 'year',
            count: 1,
            text: '1y'
        }, /* {
        type: 'year',
        count: 3,
        text: '3y'
      }, {
        type: 'year',
        count: 5,
        text: '5y'
      }, */ {
            type: 'year',
            count: 10,
            text: '10y'
        }, {
            type: 'all',
            text: 'Max'
        }],
        selected: 8,
        // x: -30,
        inputPosition: {
            // align: 'left',
            x: -30
        },
    }
};
