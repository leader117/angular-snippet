import { Component, Input, Output, EventEmitter, OnInit, OnChanges, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { EQLEngine } from 'src/grpc/proto-gen/executor_grpc_pb';
import * as moment from 'moment-timezone';
import { BacktestRow } from '../backtest.grid/BacktestRow';
import { HighchartComponent } from 'src/components/highchart/highchart.component';
import * as Highcharts from 'highcharts';
import { BACKTEST_VIEWER_CHART_OPTIONS } from './backtest.viewer.chart.options';
import { ConfirmModalService } from 'src/components/confirm.modal/confirm.modal.service';
import { GRPCService, UIPartResult } from 'src/grpc/grpc.service';
import { ExecutionMode } from 'src/grpc/proto-gen/execution_pb';
import { resolveChartData } from '../../model.builder.utilities';
import { KeyValueRow } from 'src/components/key.value.table/key.value.table.component';
import * as numeral from 'numeral';

function formatNumber(p) {
    return numeral(p).format('0.0000');
}

/**
 * @title App main search box
 */
@Component({
  selector: 'app-backtest-viewer-modal',
  templateUrl: 'backtest.viewer.modal.component.html',
  styleUrls: ['backtest.viewer.modal.component.css'],
})
export class BacktestViewerModalComponent implements OnChanges, OnInit {
  @Input() backtest: BacktestRow;
  @ViewChild('chart1', { static: true }) chart1: HighchartComponent;
  chart1Options = Highcharts.merge({}, BACKTEST_VIEWER_CHART_OPTIONS);
  isLoading = false;
  btKeyValues1: KeyValueRow[];
  btKeyValues2: KeyValueRow[];
  constructor(
    public bsModalRef: BsModalRef,
    private confirmModal: ConfirmModalService,
    private grpc: GRPCService) {
  }
  async ngOnInit() {
    if (this.backtest) this.runModel();
  }
  async ngOnChanges(params) {
    if (params.backtest && this.backtest) this.runModel();
  }
  decline(): void {
    this.bsModalRef.hide();
  }
  private async getUIReqResult(startDate?: number, endDate?: number) {
    const eql = this.backtest.eqlSettings;
    const settings = eql.clone();
    if (startDate) settings.startDate = moment(startDate).toDate();
    if (endDate) settings.endDate = moment(endDate).toDate();
    const req = this.grpc.getSubmitRequest(eql.code, settings);
    req.getContext().setExecutionmode(ExecutionMode.SIMPLIFIED_EXECUTION_MODE);
    const parts = [EQLEngine.UIPart.UIPART_MAIN_TIMESERIES, EQLEngine.UIPart.UIPART_TARGET];
    const uiReq = this.grpc.getSubmitUIRequest(req, parts);
    const arr = await this.grpc.submitUIParseAsync(uiReq);
    return arr;
  }
  private getUISeriesResolver() {
    return async (startDate?: number, endDate?: number) => {
      const arr = await this.getUIReqResult(startDate, endDate);
      const list = [];
      arr.forEach(a => {
        if (!a.series || !a.series.length) return;
        const series = Object.keys(a.series[0]).filter(d => d !== 'date');
        series.forEach((s) => {
          const data = resolveChartData(a.series, s);
          list.push({ data, ...this.getSeriesOptions(a) });
        });
      });
      return list;
    };
  }
  async runModel() {
    this.isLoading = true;
    try {
      const tsResolve = this.getUISeriesResolver();
      const ts = await tsResolve();
      this.chart1.chartOptions = this.chart1Options;
      this.chart1.resetChart();
      this.chart1.seriesResolvers.push(tsResolve);
      this.chart1.initLazyLoading(ts);

      this.chart1.drawChart();
      this.setKeyValues();
    } catch (err) {
      this.confirmModal.showMessage(err);
    }
    this.isLoading = false;
  }
  private setKeyValues() {
    const v = this.backtest.values;
    this.btKeyValues1 = [
      { key: 'Annual Returns', value: formatNumber(v.annualReturns) },
      { key: 'Annual Volatility', value: formatNumber(v.annualVolatility) },
      { key: 'Annualized Sharpe', value: formatNumber(v.annualizedSharpe) },
      { key: 'Bench Returns', value: formatNumber(v.benchReturns) },
      { key: 'Downside Risk', value: formatNumber(v.downsideRisk) },
      { key: 'Max Drawdown', value: formatNumber(v.maxDrawdown) },
      { key: 'Total Returns', value: formatNumber(v.totalReturns) },
    ];
    this.btKeyValues2 = [
      { key: 'Max Unrealized Total Returns', value: formatNumber(v.maxUnrealizedTotalReturns) },
      { key: 'Number of Trades', value: v.numTrades.toString() },
      { key: 'Number of Profitable Trades', value: v.numProfitableTrades.toString() },
      { key: 'Number of Unprofitable Trades', value: v.numUnprofitableTrades.toString() },
      { key: 'Trades Per Day', value: formatNumber(v.tradesPerDay) },
      { key: 'Sortino', value: formatNumber(v.sortino) },
    ];
  }
  getSeriesOptions(part: UIPartResult) {
    if (part.uiPart === EQLEngine.UIPart.UIPART_MAIN_TIMESERIES) return { id: `series_eql`, name: this.backtest.name };
    if (part.uiPart === EQLEngine.UIPart.UIPART_TARGET) return { id: `series_target`, name: this.backtest.eqlSettings.targetSymbol };
  }
}
