import { Component, Input, ViewChild, Output, EventEmitter, OnChanges, OnInit } from '@angular/core';
import { AppAgGridComponent } from 'src/components/grid/grid.component';
import { ResourcesApiService } from 'src/services/resources.api';
import { AutoUnsubscribeComponent } from 'src/components/auto-unsubscribe';
import { UIPartResult, GRPCService } from 'src/grpc/grpc.service';
import * as moment from 'moment-timezone';
import { SecListRowModel } from './SecListRowModel';

@Component({
  selector: 'app-sec-list-grid',
  templateUrl: 'secList.grid.component.html',
  styleUrls: ['secList.grid.component.less'],
})
export class SecListGridComponent extends AutoUnsubscribeComponent implements OnChanges, OnInit {
  @Input() isLoading = false;
  @Input() hideHeader = false;
  @Input() domLayout = 'normal';
  @ViewChild('secListGrid', { static: true }) secListGrid: AppAgGridComponent;
  @Input() uiPartResults: UIPartResult[] = [];
  selectedItem: UIPartResult;
  constructor(private grpc: GRPCService) {
    super();
  }
  ngOnInit() {
    this.refreshGrid();
  }

  ngOnChanges(params) {
    if (params.uiPartResults && this.getSecLists().length) {
      this.selectedItem = this.getSecLists()[0];
      this.refreshGrid();
    }
    if (params.selectedItem && this.selectedItem) {
      this.refreshGrid();
    }
  }
  getSecLists(): UIPartResult[] {
    if (!this.uiPartResults) return [];
    return this.uiPartResults.filter(r => r.secLists && r.secLists.data.length);
  }

  private refreshGrid() {
    if (!this.secListGrid.agGrid) return setTimeout(() => this.refreshGrid(), 100);
    if (!this.selectedItem) this.selectedItem = this.getSecLists()[0];
    if (!this.selectedItem) return;
    const secList = this.selectedItem.secLists;
    const gridRows = secList.data;
    const columns = [
      {
        headerName: 'Tranche Name',
        field: 'name',
        resizable: true,
        sortable: false,
        rowGroup: true,
        hide: true,
      },
      ...secList.dates.map(c => ({
        headerName: moment(c).format('MM/DD/YY'),
        field: c,
        resizable: true,
        sortable: false,
        width: 110,
      }))
    ];
    this.secListGrid.agGrid.setColumnDefs(columns);
    const model = new SecListRowModel(this.selectedItem.req, this.grpc, this.secListGrid);
    model.rows = gridRows;
    this.secListGrid.gridOptions.getChildCount = data => {
      if (!data) return 0;
      return data.width;
    };
    this.secListGrid.agGrid.setServerSideDatasource(model);
  }
}
