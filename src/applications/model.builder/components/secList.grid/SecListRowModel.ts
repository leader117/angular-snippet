import { AutoUnsubscribeComponent } from 'src/components/auto-unsubscribe';
import { AppAgGridComponent } from 'src/components/grid/grid.component';
import { ArrayNoRepeats } from 'src/scripts/utilities';
import { EQLEngine } from 'src/grpc/proto-gen/executor_grpc_pb';
import { GRPCService, SecListTrancheData } from 'src/grpc/grpc.service';

export class SecListRowModel extends AutoUnsubscribeComponent {
    rowModelType = 'serverSide';
    rows: SecListTrancheData[];
    sortField: string;
    sortDirection: string;
    constructor(private req: EQLEngine.SubmitRequest, private grpc: GRPCService, private grid: AppAgGridComponent) {
        super();
    }
    async getRows(params) {
        const req = params.request;
        const { groupKeys } = req;
        if (!groupKeys.length) {
            return params.successCallback(this.rows, this.rows.length);
        }
        const tranch = this.rows.find(r => r.name === groupKeys[0]);
        const tranchIndex = this.rows.findIndex(r => r.name === groupKeys[0]);
        if (req.startRow === 0) return params.successCallback(tranch.rows, tranch.width);
        const repres = this.req.getResultrepresentation();
        repres.setSeclistrangestart(req.startRow);
        repres.setSeclistrangeend(req.endRow);
        repres.setSecliststarttranche(tranchIndex);
        repres.setSeclistendtranche(tranchIndex);
        const uiReq = this.grpc.getSubmitUIRequest(this.req, [EQLEngine.UIPart.UIPART_MAIN_TIMESERIES]);
        const arr = (await this.grpc.submitUIParseAsync(uiReq)).filter(r => r.secLists);
        if (!arr.length) params.successCallback([], tranch.width);
        else {
            const newTranche = arr[0].secLists.data[0];
            params.successCallback(newTranche.rows, newTranche.width);
        }
    }
}
