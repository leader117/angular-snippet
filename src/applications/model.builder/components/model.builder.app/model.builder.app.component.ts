import { Component, ElementRef, ViewChild, Input, OnInit, OnDestroy } from '@angular/core';
import { AutoUnsubscribeComponent } from 'src/components/auto-unsubscribe';
import * as moment from 'moment-timezone';
import { HighchartComponent } from 'src/components/highchart/highchart.component';
import { ConfirmModalService } from 'src/components/confirm.modal/confirm.modal.service';
import { GRPCService, UIPartResult, SecListResult, SubmitUIRequestWithHandle } from 'src/grpc/grpc.service';
import { EQLEngine } from 'src/grpc/proto-gen/executor_grpc_pb';
import { GaService } from 'src/services/ga.service';
import { HighstockLeftRightExtremeButtons } from 'src/components/highchart/left.right.extreme.buttons';
import * as Highcharts from 'highcharts';
import { WorkspaceApiService } from 'src/services/workspace.apis.service';
import { BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import {
  defaultChart1Options,
  xDateFormats,
  resolveChartData,
  addChangeAxisViewButtons,
  defaultTwoYAxisOptions,
  defaultOneYAxisOptions,
  drawHistogramChart
} from '../../model.builder.utilities';
import { BacktestGridComponent } from '../backtest.grid/backtest.grid.component';
import { ResourcesPanelComponent } from '../resources.panel/resources.panel.component';
import { WorkspaceEntity } from 'src/graphql/types/WorkspaceEntity';
import { WindowEntity } from 'src/graphql/types/WindowEntity';
import { ResourceEntity } from 'src/graphql/types/ResourceEntity';
import { BacktestsApiService } from 'src/applications/model.builder/backtests.api.service';
import { AppRouteService } from 'src/services/app.route';
import { IToolApp } from 'src/applications/base/IToolApp';
import { EqlSettings } from '../../entries/EqlSettings';
import { ModelBuilderService } from '../../model-builder.service';
import { ArrayRemoveItem, UpdateObj } from 'src/scripts/utilities';
import { BacktestRow } from '../backtest.grid/BacktestRow';
import { EqlSettingsModalComponent } from '../eql.settings.modal/eql.settings.modal.component';
import { SaveModalComponent } from '../save-modal/save-modal.component';
import { EqlEditorComponent } from 'src/components/eql.editor/eql.editor.component';
import { ExtendEqlEditorModel } from 'src/components/eql.editor/eql.editor.model.extended';
import { FloatWindowEntity } from 'src/components/float.window/FloatWindowEntity';
declare var $: any;


/**
 * @title App main search box
 */
@Component({
  selector: 'app-model-builder-app',
  templateUrl: 'model.builder.app.component.html',
  styleUrls: ['model.builder.app.component.less'],
})
export class ModelBuilderAppComponent extends AutoUnsubscribeComponent implements OnInit, OnDestroy, IToolApp {
  @ViewChild('chart1', { static: true }) chart1: HighchartComponent;
  leftRightExtreme: HighstockLeftRightExtremeButtons;
  chart2UiPart = EQLEngine.UIPart.UIPART_VOLUME;
  chart2UiPartItems = [
    { code: EQLEngine.UIPart.UIPART_PROFITLOSS, name: 'Profit/Loss' },
    { code: EQLEngine.UIPart.UIPART_RETURNS, name: 'Returns' },
    { code: EQLEngine.UIPart.UIPART_SHARPE, name: 'Sharpe' },
    { code: EQLEngine.UIPart.UIPART_VOLUME, name: 'Volume' }
  ];
  @ViewChild('containerElm', { static: true }) containerElm: ElementRef;
  isLoading = false;
  // bt: any;
  one$;
  isVisibleChart = true;
  ws: WorkspaceEntity;
  tab: WindowEntity;
  eqlSettings: EqlSettings = EqlSettings.getDefault();
  model = new ResourceEntity();
  isResourcesOpened = false;
  uiPartResults: UIPartResult[] = [];
  isBacktestOpened = false;
  isBacktesting = false;
  @ViewChild(EqlEditorComponent, { static: true }) editor?: EqlEditorComponent;
  @ViewChild(BacktestGridComponent, { static: true }) btGrid?: BacktestGridComponent;
  @ViewChild(ResourcesPanelComponent, { static: true }) resourcesPanel: ResourcesPanelComponent;
  uiReqs: SubmitUIRequestWithHandle[] = [];
  hasMetricChart = false;
  varWindows: FloatWindowEntity[] = [];
  constructor(
    private confirmModal: ConfirmModalService,
    private grpc: GRPCService,
    private ga: GaService,
    private modelBuilderService: ModelBuilderService,
    private workspaceService: WorkspaceApiService,
    private modalService: BsModalService,
    private backtestsAPIs: BacktestsApiService,
    private appRoute: AppRouteService
  ) {
    super();
  }
  async ngOnInit() {
    const state = this.appRoute.getState();
    if (state.code) this.eqlSettings.code = state.code;
    this.leftRightExtreme = new HighstockLeftRightExtremeButtons(this.chart1);
    this.one$ = this.chart1.onAfterSetExtremes.subscribe(e => {
      const barSize = this.grpc.getRepresentationBarSize(e.min, e.max);
      this.chart1.setXDateFormat(xDateFormats[barSize]);
    });
    this.adjustHeight();
    this.openResourceId(true);
    if (state.runCode) this.runModel();
  }
  ngOnDestroy() {
    super.ngOnDestroy();
    this.cancelModel();
  }
  onUiPartChanged(value) {
    this.chart2UiPart = value;
    this.runChart2UiPart();
  }
  // Adjust the height of the model builder
  private adjustHeight() {
    const container = document.getElementsByClassName('app-content-container')[0];
    const target = this.containerElm.nativeElement;
    if (!container) return;
    if (container.clientHeight > 950) target.style.height = `${container.clientHeight - 150}px`;
  }
  // tslint:disable-next-line: max-line-length
  private async getUIReqResult(code: string, parts: EQLEngine.UIPartMap[keyof EQLEngine.UIPartMap][], startDate?: number, endDate?: number) {
    const settings = this.eqlSettings.clone();
    if (startDate) settings.startDate = moment(startDate).toDate();
    if (endDate) settings.endDate = moment(endDate).toDate();
    const req = this.grpc.getSubmitRequest(code, settings);
    if (parts[0] === EQLEngine.UIPart.UIPART_BACKTEST) req.clearResultrepresentation();
    const uiReq = this.grpc.getSubmitUIRequest(req, parts);
    this.uiReqs.push(uiReq);
    try {
      const arr = await this.grpc.submitUIParseAsync(uiReq);
      if (uiReq.cancelSignalRequested) throw new Error('Task canceled');
      return arr;
    } finally { ArrayRemoveItem(this.uiReqs, uiReq); }
  }
  private getUISeriesResolver(
    eqlCode: string,
    seriesOptionsGetter: (part: UIPartResult, field: string) => any,
    partsGetter: () => EQLEngine.UIPartMap[keyof EQLEngine.UIPartMap][]) {
    return async (startDate?: number, endDate?: number) => {
      const code = eqlCode || this.eqlSettings.code;
      const arr = await this.getUIReqResult(code, partsGetter(), startDate, endDate);
      const list = [];
      arr.forEach(a => {
        if (!a.series || !a.series.length) return;
        const series = Object.keys(a.series[0]).filter(d => d !== 'date');
        const tsName = this.grpc.getUIPartName(a.uiPart);
        series.forEach((s) => {
          const name = `${tsName}(${s})`;
          const data = resolveChartData(a.series, s);
          list.push({ name, data, ...seriesOptionsGetter(a, s), uiPart: a.uiPart });
        });
      });
      const secLists = arr.filter(r => r.secLists && r.secLists.data.length).map(r => r.secLists);
      if (secLists.length) this.isResourcesOpened = true;
      this.uiPartResults = arr;
      return list;
    };
  }
  async runModel(eqlCode?: string) {
    this.isLoading = true;
    try {
      eqlCode = eqlCode || this.eqlSettings.code;
      const series = (p, field) => this.getSeriesOptions(p, field);
      const parts = () => {
        const myParts: any[] = [
          EQLEngine.UIPart.UIPART_MAIN_TIMESERIES,
          EQLEngine.UIPart.UIPART_SECLIST_AVERAGES,
          this.chart2UiPart
        ];
        if (this.eqlSettings.showTargetSymbol) myParts.push(EQLEngine.UIPart.UIPART_TARGET);
        return myParts;
      };
      const tsResolve = await this.getUISeriesResolver(eqlCode, series, parts);
      const ts = await tsResolve();
      this.hasMetricChart = ts.some(r => r.uiPart === this.chart2UiPart);
      this.chart1.chartOptions = Highcharts.merge({}, defaultChart1Options);
      this.chart1.resetChart();
      const histogram = this.uiPartResults.find(r => r.histogram && r.histogram.length);
      if (histogram) drawHistogramChart(this.chart1, histogram);
      else {
        if (ts.length) {
          this.chart1.options.yAxis = this.hasMetricChart ? defaultTwoYAxisOptions : defaultOneYAxisOptions;
          this.chart1.seriesResolvers.push(tsResolve);
          this.chart1.initLazyLoading(ts);
        }
        // this.chart1.options.title.text = 'Normalized Performance Chart';
        this.chart1.drawChart();
        const hasMultiAxis = ts.filter(e => !e.yAxis).length > 1;
        if (hasMultiAxis) {
          this.chart1.chart.yAxis[0].setCompare('percent');
          addChangeAxisViewButtons(this.chart1);
        }
        this.leftRightExtreme.apply();
      }
      // this.bt = bt[0].series;
    } catch (err) {
      this.showErrorMessage(err);
    }
    this.isLoading = false;
    ExtendEqlEditorModel(this.editor.model).highlightAllVariables();
    this.ga.trackEvent('run model', 'model builder');
  }
  runChart2UiPart() {
    if (!this.chart1.seriesResolvers.length) return;
    this.chart1.loadResolvers(this.chart1.seriesResolvers);
  }
  async cancelModel() {
    try {
      this.uiReqs.forEach(u => u.cancelSignalRequested = true);
      await Promise.all(this.uiReqs.map(u => this.grpc.cancelTask(u.taskHandle)));
    } catch (err) { console.error(err); }
    this.isLoading = false;
  }
  enableCancelTaskButton() {
    return this.uiReqs.some(u => !!u.taskHandle);
  }
  getSeriesOptions(part: UIPartResult, field: string) {
    const t = part.series[0] || {};
    const isOHLC = Array.isArray(t[field]);
    const index = Object.keys(t).indexOf(field);
    const type = isOHLC ? 'candlestick' : 'line';
    // /* name: this.tab ? this.tab.title : 'EQL', */
    if (part.uiPart === EQLEngine.UIPart.UIPART_MAIN_TIMESERIES) return { id: `series_eql_${index}`, type };
    if (part.uiPart === EQLEngine.UIPart.UIPART_TARGET) return { id: `series_target_${index}`, type: 'line' };
    if (part.uiPart === this.chart2UiPart) return { id: `series_ui_${index}`, type: 'column', color: 'gray', yAxis: 1 };
  }
  afterResize() {
    if (this.chart1) this.chart1.reflow();
  }
  onContentChange(content: string) {
    this.eqlSettings.code = content;
  }
  onChangeCursor(e: any) {
  }
  async toggleBacktestPanel(value = !this.isBacktestOpened) {
    if (value) {
      this.toggleResourcesPanel(false);
    }
    if (value && !this.btGrid.rowsLoaded && this.model.id) {
      this.isBacktesting = true;
      try {
        this.btGrid.setBacktests(await this.backtestsAPIs.getBacktests(this.model.id));
      } catch (err) {
        this.showErrorMessage(err);
      }
      this.isBacktesting = false;
    }
    this.isBacktestOpened = value;
  }
  async clickDecoration(editor: EqlEditorComponent, args) {
    if (args.classNames.includes('eqlVariableDecoration')) {
      const varName = editor.model.getWordAtPosition(args.position).word;
      const item = this.uiPartResults.find(r => !!r.outputContext);
      if (!item) return this.confirmModal.showMessage('No context found, did you run the code?');
      const req = this.grpc.getSubmitRequestForContext(varName, item.outputContext);
      const uiReq = this.grpc.getSubmitUIRequest(req, [EQLEngine.UIPart.UIPART_MAIN_TIMESERIES]);

      const win = new FloatWindowEntity();
      const { clientWidth, clientHeight } = this.containerElm.nativeElement;
      win.title = `Variable "${varName}"`;
      win.adjustPosition(clientWidth, clientHeight);
      this.varWindows.push(win);
      win.data.isLoading = true;
      try {
        const arr = await this.grpc.submitUIParseAsync(uiReq);
        win.data.hasTS = arr.find(s => s.series && s.series.length);
        win.data.hasSecList = arr.find(s => s.secLists && s.secLists.data.length);
        win.data.uiPartResults = arr;
       }
      catch (err) { win.data.error = err; }
      win.data.isLoading = false;
    }
  }
  onVarWindowChange(win: FloatWindowEntity, { action }) {
    if (action === 'close') ArrayRemoveItem(this.varWindows, win);
  }
  // Run the code to current line
  async runModelCurrentLine(editor: EqlEditorComponent, args) {
    if (!args.classNames.includes('loupeGlyphDecoration')) return;
    await this.toggleBacktestPanel(true);
    this.isBacktesting = true;
    const model = ExtendEqlEditorModel(editor.model);
    try {
      const code = model.getCodeInRange(1, args.line + 1);
      const bt = (await this.getUIReqResult(code, [EQLEngine.UIPart.UIPART_BACKTEST]))[0].series;
      const row = new BacktestRow();
      row.name = `Backtest ${moment().format('HH:mm:ss')}`;
      row.modelId = this.model.id;
      row.eqlSettings = this.eqlSettings.clone({ code });
      row.values.setBacktestValues(bt);
      const xx = await this.backtestsAPIs.createBacktest(row);
      this.btGrid.addBacktest(xx);
      this.btGrid.showBacktestDialog(xx);
    } catch (err) {
      this.showErrorMessage(err);
    }
    this.isBacktesting = false;
  }
  private showErrorMessage(err: string | Error) {
    if (typeof (err) === 'object') err = err.message;
    if ((err && err.toLocaleLowerCase()) === 'task not found') return;
    this.confirmModal.showMessage(err);
  }
  getToolTitle() { return 'Model Builder'; }
  getToolParams() {
    return {
      uiPart: this.chart2UiPart,
      eqlSettings: this.eqlSettings,
    };
  }
  setToolParams(ws: WorkspaceEntity, tab: WindowEntity) {
    this.ws = ws;
    this.tab = tab;
    const params = this.tab.settings;
    if (params.uiPart) this.chart2UiPart = params.uiPart;
    if (params.eqlSettings) this.eqlSettings = EqlSettings.fromJSON(params.eqlSettings);
  }

  toggleVisibleChart() {
    this.isVisibleChart = !this.isVisibleChart;
  }
  showSettingsDialog() {
    const config: ModalOptions = {
      backdrop: true,
      ignoreBackdropClick: true,
      keyboard: false,
      class: 'modal-dialog modal-lg',
      initialState: { settings: this.eqlSettings.clone() }
    };
    const bsModalRef = this.modalService.show(EqlSettingsModalComponent, config);
    const $1 = bsModalRef.content.confirmed.subscribe((instance: EqlSettingsModalComponent) => {
      instance.isBusy = true;
      instance.isBusy = false;
      UpdateObj(this.eqlSettings, instance.settings);
      // if (true) return instance.showError('Not ready yet');
      $1.unsubscribe();
      instance.bsModalRef.hide();
      this.runModel();
    });
  }
  toggleResourcesPanel(value = !this.isResourcesOpened) {
    if (value) {
      this.toggleBacktestPanel(false);
    }
    this.isResourcesOpened = value;
    this.afterResize();
  }
  openResourceModel(model: ResourceEntity, runModel = true) {
    if (!model.settings) this.confirmModal.showMessage('Invalid model');
    this.model = model;
    this.eqlSettings = EqlSettings.fromJSON(model.settings);
    if (this.ws && this.tab) {
      const clone = this.tab.clone({ title: model.name, settings: { ...this.tab.settings, resourceId: model.id } });
      this.workspaceService.saveWorkspaceTab(this.ws, this.tab, clone);
    }
    if (runModel) this.runModel();
  }
  async openResourceId(runModel = true) {
    if (!this.tab) return;
    const { resource } = this.tab;
    let resourceId;
    if (resource.type.name === 'Model') resourceId = resource.id;
    else resourceId = this.tab.settings.resourceId;
    if (resourceId) {
      const model = await this.modelBuilderService.getModel(resourceId);
      this.openResourceModel(model, runModel);
    }
  }
  saveModel() {
    const config: ModalOptions = {
      backdrop: true,
      ignoreBackdropClick: true,
      keyboard: false,
      class: 'modal-dialog modal-md',
      initialState: {
        workspaceId: this.ws && this.ws.id,
        model: this.model.clone({ settings: this.eqlSettings }),
      },
    };
    const modalRef = this.modalService.show(SaveModalComponent, config);
    const subscription = modalRef.content.closed.subscribe((instance: SaveModalComponent) => {
      this.model = instance.model;
      if (!instance.model.settings) {
        this.eqlSettings = EqlSettings.getDefault();
      }
      instance.modalRef.hide();
      subscription.unsubscribe();
      this.openResourceModel(this.model);
    });
  }
  async quickSaveModel() {
    const clone = this.model.clone({
      settings: this.eqlSettings,
      categoryIds: this.model.categories.map(category => category.id),
      tagIds: this.model.tags.map(tag => tag.id),
      typeId: this.model.type.id,
    });
    await this.modelBuilderService.editModel(clone);
  }
}
