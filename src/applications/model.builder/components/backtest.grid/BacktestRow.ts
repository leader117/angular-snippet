import { EqlSettings } from '../../entries/EqlSettings';
import * as moment from 'moment-timezone';

export class BacktestValues {
    numTrades: number;
    numProfitableTrades: number;
    numUnprofitableTrades: number;
    tradesPerDay: number;
    totalReturns: number;
    benchReturns: number;
    annualReturns: number;
    maxUnrealizedTotalReturns: number;
    annualizedSharpe: number;
    maxDrawdown: number;
    annualVolatility: number;
    downsideRisk: number;
    sortino: number;
    setBacktestValues(bt: any) {
        const row = this;
        row.numProfitableTrades = bt.numProfitableTrades;
        row.numUnprofitableTrades = bt.numUnprofitableTrades;
        row.annualReturns = bt.annualReturns;
        row.annualizedSharpe = bt.annualizedSharpe;
        row.numTrades = bt.numTrades;
        row.totalReturns = bt.totalReturns;
        row.benchReturns = bt.benchReturns;
        row.maxUnrealizedTotalReturns = bt.maxUnrealizedTotalReturns;
        row.tradesPerDay = bt.tradesPerDay;
        row.maxDrawdown = bt.maxDrawdown;
        row.annualVolatility = bt.annualVolatility;
        row.downsideRisk = bt.downsideRisk;
        row.sortino = bt.sortino;
        return row;
    }
}

export class BacktestRow {
    id: string;
    modelId: string;
    time: Date;
    name: string;
    eqlSettings: EqlSettings;
    values = new BacktestValues();
    static fromGraghql(bt: any) {
        const row = new BacktestRow();
        row.id = bt.id;
        row.modelId = bt.model;
        row.time = bt.createdAt;
        row.name = bt.name;
        if (bt.startTime) bt.startDate = bt.startTime * 1000;
        if (bt.endTime) bt.endDate = bt.endTime * 1000;
        row.eqlSettings = EqlSettings.fromJSON(bt);
        row.values.setBacktestValues(bt);
        return row;
    }
}
