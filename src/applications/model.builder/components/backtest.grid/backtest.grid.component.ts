import { Component, EventEmitter, Input, Output, ViewChild, ElementRef, OnInit, OnChanges } from '@angular/core';
import { BacktestRow } from './BacktestRow';
import { ConfirmModalService } from 'src/components/confirm.modal/confirm.modal.service';
import { AppAgGridComponent } from 'src/components/grid/grid.component';
import { BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { BacktestViewerModalComponent } from '../backtest.viewer.modal/backtest.viewer.modal.component';
import { BACKTEST_GRID_COLUMNS } from './backtest.grid.columns';
import { BacktestsApiService } from 'src/applications/model.builder/backtests.api.service';

@Component({
  selector: 'app-backtest-grid',
  templateUrl: 'backtest.grid.component.html',
  styleUrls: ['backtest.grid.component.less'],
})
export class BacktestGridComponent {
  @Input() isLoading = false;
  columnDefs = BACKTEST_GRID_COLUMNS;
  @ViewChild(AppAgGridComponent, { static: true }) grid?: AppAgGridComponent;
  rowsLoaded = false;
  constructor(
    private backtestsAPIs: BacktestsApiService,
    private confirmModal: ConfirmModalService,
    private modalService: BsModalService, ) {
  }
  async onCellClicked(params) {
    const { colDef, data } = params;
    if (colDef.field === 'delete') {
      const message = 'Backtest will be deleted, process?';
      this.confirmModal.show({ message }, async () => {

        const deleted = await this.backtestsAPIs.deleteBacktest(data.id);
        if (deleted.error) return this.confirmModal.showMessage(deleted.error);
        this.grid.agGrid.updateRowData({ remove: [data] });

      });
    }
    else if (colDef.field === 'name') {
      this.showBacktestDialog(data);
    }
  }
  addBacktest(bt: BacktestRow) {
    this.grid.agGrid.updateRowData({ add: [bt], addIndex: 0 });
  }
  setBacktests(bts: Array<BacktestRow>) {
    this.rowsLoaded = true;
    this.grid.agGrid.setRowData(bts);
  }
  showBacktestDialog(bt: BacktestRow) {
    const config: ModalOptions = {
      backdrop: true,
      ignoreBackdropClick: false,
      keyboard: true,
      class: 'modal-dialog modal-lg',
      initialState: { backtest: bt },
    };
    this.modalService.show(BacktestViewerModalComponent, config);
  }
}
