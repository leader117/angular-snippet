import * as numeral from 'numeral';
import * as moment from 'moment-timezone';

function formatNumber(p) {
    return numeral(p.value).format('0.0000');
}

export const BACKTEST_GRID_COLUMNS = [{
    headerName: 'Backtest',
    field: 'name',
    width: 140,
    resizable: true,
    sortable: true,
    cellRenderer: p => `<a title="View backtest..." href="javascript:;">${p.value}</a>`,
}, {
    headerName: 'Time',
    field: 'time',
    tooltipField: 'time',
    width: 110,
    resizable: true,
    sortable: true,
    sort: 'desc',
    cellRenderer: p => moment(p.data.time).format('HH:mm:ss'),
}, {
    headerName: 'Target',
    field: 'eqlSettings.targetSymbol',
    width: 80,
    resizable: true,
    sortable: true,
}, {
    headerName: 'Total Trades',
    field: 'values.numTrades',
    width: 120,
    resizable: true,
    sortable: true,
}, {
    headerName: 'Trades Per Day',
    field: 'values.tradesPerDay',
    width: 130,
    resizable: true,
    sortable: true,
    cellRenderer: formatNumber,
}, {
    headerName: 'Total Returns',
    field: 'values.totalReturns',
    width: 120,
    resizable: true,
    sortable: true,
    cellRenderer: formatNumber,
}, {
    headerName: 'Annual Returns',
    field: 'values.annualReturns',
    width: 135,
    resizable: true,
    sortable: true,
    cellRenderer: formatNumber,
}, {
    headerName: 'Annualized Sharpe',
    field: 'values.annualizedSharpe',
    width: 150,
    resizable: true,
    sortable: true,
    cellRenderer: formatNumber,
}, {
    headerName: '',
    field: 'delete',
    width: 90,
    resizable: false,
    sortable: false,
    cellRenderer: p => '<a class="" href="javascript:;">Delete</a>',
}];
