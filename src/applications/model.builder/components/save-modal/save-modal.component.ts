import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ModelBuilderService } from '../../model-builder.service';
import { ConfirmModalService } from 'src/components/confirm.modal/confirm.modal.service';
import { ResourceEntity } from 'src/graphql/types/ResourceEntity';

@Component({
  selector: 'app-save-modal',
  templateUrl: './save-modal.component.html',
  styleUrls: ['./save-modal.component.less']
})
export class SaveModalComponent implements OnInit {
  @Input() model = new ResourceEntity();
  @Output() closed = new EventEmitter();
  defaultModel: ResourceEntity;
  event: string;
  isLoading = false;

  constructor(
    private modelBuilderService: ModelBuilderService,
    public modalRef: BsModalRef,
    private confirmModal: ConfirmModalService,
  ) { }

  async ngOnInit() {
    this.isLoading = true;
    if (this.model.id) {
      this.model = this.model.clone({
        categoryIds: this.model.categories.map(category => category.id),
        tagIds: this.model.tags.map(tag => tag.id),
        typeId: this.model.type.id,
      });
    } else {
      this.model = this.model.clone({
        categoryIds: [],
        tagIds: [],
        typeId: '1f3b5c0b-2d1c-4dee-b30b-ad80ffedbf88',
      });
    }
    this.defaultModel = this.model.clone();
    this.isLoading = false;
  }

  async onSubmit() {
    this.isLoading = true;
    if (this.event === 'save') {
      const { name } = this.model;
      if (this.model.id) {
        if (this.model.published) {
          const createdModel = await this.modelBuilderService.createModelVersion(this.model);
          this.model = createdModel.clone();
        } else {
          const editedModel = await this.modelBuilderService.editModel(this.model);
          this.model = editedModel.clone();
        }
      } else {
        const createdModel = await this.modelBuilderService.addModel(this.model);
        this.model = createdModel.clone();
      }
      this.closed.emit(this);
    }
    if (this.event === 'publish') {
      await this.modelBuilderService.publishModel(this.model.id);
      this.model.published = true;
    }
    if (this.event === 'delete') {
      this.confirmModal.show({}, async () => {
        await this.modelBuilderService.deleteModel(this.model.id);
        this.model = new ResourceEntity();
        this.closed.emit(this);
      });
    }
    this.isLoading = false;
  }

  onReset() {
    this.model = this.defaultModel.clone();
  }
}
