import { Component, Input, Output, EventEmitter, OnInit, OnChanges } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { EqlSettings } from '../../entries/EqlSettings';
import * as moment from 'moment-timezone';
import { ActionMetadata } from 'src/grpc/proto-gen/eqltree_pb';

/**
 * @title App main search box
 */
@Component({
  selector: 'app-eql-settings-modal',
  templateUrl: 'eql.settings.modal.component.html',
  styleUrls: ['eql.settings.modal.component.css'],
})
export class EqlSettingsModalComponent implements OnChanges, OnInit {
  @Input() settings: EqlSettings = EqlSettings.getDefault();
  @Output() confirmed = new EventEmitter<EqlSettingsModalComponent>();
  @Output() cancelled = new EventEmitter();
  errorMessage: string;
  isBusy = false;
  priceProxyItems = [
    { code: 'evwpc', name: 'Equeum-volume-weighted-percent-change'},
    { code: 'open', name: 'Open'},
    { code: 'high', name: 'High'},
    { code: 'low', name: 'Low'},
    { code: 'close', name: 'Close'},
  ];
  targetFieldsItems = [
    { code: 'o', name: 'Open'},
    { code: 'h', name: 'High'},
    { code: 'l', name: 'Low'},
    { code: 'c', name: 'Close'},
  ];
  orderItems = [
    { code: 'high', name: 'High'},
    { code: 'low', name: 'Low'},
  ];
  barSizeItems = [
    { code: ActionMetadata.BarSize.BS_1MIN, name: '1 Minute'},
    { code: ActionMetadata.BarSize.BS_3MIN, name: '3 Minutes'},
    { code: ActionMetadata.BarSize.BS_5MIN, name: '5 Minutes'},
    { code: ActionMetadata.BarSize.BS_10MIN, name: '10 Minutes'},
    { code: ActionMetadata.BarSize.BS_15MIN, name: '15 Minutes'},
    { code: ActionMetadata.BarSize.BS_30MIN, name: '30 Minutes'},
    { code: ActionMetadata.BarSize.BS_1HOUR, name: '1 Hour'},
    { code: ActionMetadata.BarSize.BS_1DAY, name: '1 Day'},
    { code: ActionMetadata.BarSize.BS_MONTH, name: '1 Month'},
  ];
  constructor(public bsModalRef: BsModalRef) {
  }
  async ngOnInit() {
  }
  async ngOnChanges(params) {
  }
  onStartDateChanged(startDate: Date) {
    this.settings.startDate = startDate;
  }
  onEndDateChanged(endDate: Date) {
    this.settings.endDate = endDate;
  }
  saveAsDefault() {
    this.settings.saveAsDefault();
  }
  confirm(): void {
    this.errorMessage = undefined;
    this.confirmed.emit(this);
  }
  decline(): void {
    this.cancelled.emit();
    this.bsModalRef.hide();
  }
  showError(err: string) {
    this.errorMessage = err;
  }
}
