import { Component, EventEmitter, Input, Output, ViewChild, ElementRef } from '@angular/core';
import { ResourceEntity } from 'src/graphql/types/ResourceEntity';

@Component({
  selector: 'app-models-expanded-panel',
  templateUrl: 'models.expanded-panel.component.html',
  styleUrls: ['models.expanded-panel.component.less'],
})
export class ModelsExpandedPanelComponent {
  @ViewChild('details', { static: true }) details: ElementRef;
  @Output() clickOpen = new EventEmitter();
  isOpen = false;
  @Input() model: ResourceEntity;

  openDataset() {
    this.clickOpen.emit();
  }

  toggleOpen() {
    this.isOpen = !this.isOpen;
    if (this.isOpen) {
      setTimeout(() => { this.details.nativeElement.style.overflow = 'auto'; }, 400);
    } else {
      this.details.nativeElement.style.overflow = 'hidden';
    }
  }
}
