import { Component, ElementRef, ViewChild, Input, OnInit, OnDestroy } from '@angular/core';
import { AutoUnsubscribeComponent } from 'src/components/auto-unsubscribe';
import { ResourceEntity } from 'src/graphql/types/ResourceEntity';
import { AppRouteService } from 'src/services/app.route';
import { MODEL_BUILDER_TOOL_ID } from 'src/constants';
import { ResourcesApiService } from 'src/services/resources.api';
import { ModelBuilderService } from '../../model-builder.service';
import { ConfirmModalService } from 'src/components/confirm.modal/confirm.modal.service';


/**
 * @title App main search box
 */
@Component({
  selector: 'app-model-builder-home',
  templateUrl: 'model.builder.home.component.html',
  styleUrls: ['model.builder.home.component.less'],
})
export class ModelBuilderHomeComponent extends AutoUnsubscribeComponent implements OnInit {
  models: ResourceEntity[] = [];
  isLoading = false;
  order: any;
  filterText = '';
  $1;
  $2
  constructor(
    private appRoute: AppRouteService,
    private resourcesAPIs: ResourcesApiService,
    private modelBuilderService: ModelBuilderService,
    private confirmModal: ConfirmModalService) {
    super();
    this.$1 = this.modelBuilderService.modelsChanged.subscribe(d => this.loadResources());
    this.$2 = appRoute.subscribe(d => this.loadResources());
  }
  async ngOnInit() {
    this.loadResources();
  }
  async onOpenModel(model: ResourceEntity) {
    const id = model ? model.id : MODEL_BUILDER_TOOL_ID;
    this.appRoute.navigateToolsApp(id);
  }
  deleteModel(model: ResourceEntity) {
    this.confirmModal.show({}, async () => {
      this.modelBuilderService.deleteModel(model.id);
    });
  }
  sortBy(field: string, order = 'asc') {
    this.order = { [field]: order };
    this.loadResources();
  }
  filterModels(models: ResourceEntity[], filterText: string) {
    return models.filter(m => (m.name || '').toLocaleLowerCase().includes(filterText.toLocaleLowerCase()));
  }
  async loadResources() {
    this.isLoading = true;
    try {
      const params: any = {};
      if (this.order) params.order = this.order;
      this.models = await this.resourcesAPIs.getMyModels(params);
    } catch (err) { }
    this.isLoading = false;
  }
}
