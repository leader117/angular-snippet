import { Component, Input, ViewChild, Output, EventEmitter, OnChanges, OnInit } from '@angular/core';
import { AppAgGridComponent } from 'src/components/grid/grid.component';
import { AutoUnsubscribeComponent } from 'src/components/auto-unsubscribe';
import { UIPartResult } from 'src/grpc/grpc.service';
import * as moment from 'moment-timezone';

@Component({
  selector: 'app-ts-grid',
  templateUrl: 'ts.grid.component.html',
  styleUrls: ['ts.grid.component.less'],
})
export class TSGridComponent extends AutoUnsubscribeComponent implements OnChanges, OnInit {
  @Input() isLoading = false;
  @Input() hideHeader = false;
  @Input() domLayout = 'normal';
  @ViewChild('tsGrid', { static: true }) tsGrid: AppAgGridComponent;
  @Input() uiPartResults: UIPartResult[] = [];
  selectedSeries: any;
  constructor() {
    super();
  }
  ngOnInit() {
  }

  ngOnChanges(params) {
    if (params.uiPartResults && this.getSeries().length) {
      this.selectedSeries = this.getSeries()[0];
      this.refreshGrid();
    }
  }
  getSeries(): any[] {
    if (!this.uiPartResults) return [];
    return this.uiPartResults.map(s => s.series);
  }
  private refreshGrid() {
    if (!this.tsGrid.agGrid) return setTimeout(() => this.refreshGrid(), 100);
    if (!this.selectedSeries) this.selectedSeries = this.getSeries()[0];
    if (!this.selectedSeries) return;
    const columns = Object.keys(this.selectedSeries[0]).map(c => ({
      headerName: c,
      field: c,
      resizable: true,
      sortable: true,
      cellRenderer: this.getCellRender(c),
    }));
    this.tsGrid.agGrid.setColumnDefs(columns);
    this.tsGrid.agGrid.setRowData(this.selectedSeries);
  }
  private getCellRender(field: string) {
    if (field === 'date') return p => moment(p.value).format('MM/DD/YYYY HH:mm');
    const f = n => Math.round(n * 100) / 100;
    return p => {
      if (!Array.isArray(p.value)) return p.value;
      return `O: ${f(p.value[0])} , H: ${f(p.value[1])} , L: ${f(p.value[2])} , C: ${f(p.value[3])}`;
    };
  }
}
