import * as moment from 'moment-timezone';

export const MODELS_GRID_COLUMNS = [{
    headerName: 'Name',
    field: 'name',
    width: 140,
    resizable: true,
    sortable: true,
    cellRenderer: p => `<a title="Open Model" href="javascript:;">${p.value}</a>`,
}, {
    headerName: 'Description',
    field: 'description',
    resizable: true,
    sortable: false,
}, {
    headerName: 'Date',
    field: 'updatedAt',
    tooltipField: 'updatedAt',
    width: 110,
    resizable: true,
    sortable: true,
    sort: 'desc',
    cellRenderer: p => moment(p.data.time).format('MM/DD/YYYY'),
}];
