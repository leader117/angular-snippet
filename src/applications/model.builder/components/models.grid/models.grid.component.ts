import { Component, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { AppAgGridComponent } from 'src/components/grid/grid.component';
import { MODELS_GRID_COLUMNS } from './models.grid.columns';
import { ResourcesApiService } from 'src/services/resources.api';
import { ResourceEntity } from 'src/graphql/types/ResourceEntity';
import { ModelBuilderService } from '../../model-builder.service';
import { AutoUnsubscribeComponent } from 'src/components/auto-unsubscribe';

@Component({
  selector: 'app-models-grid',
  templateUrl: 'models.grid.component.html',
  styleUrls: ['models.grid.component.less'],
})
export class ModelsGridComponent extends AutoUnsubscribeComponent {
  @Input() isLoading = false;
  @Input() hideHeader = false;
  @Input() domLayout = 'normal';
  columnDefs = MODELS_GRID_COLUMNS;
  @ViewChild(AppAgGridComponent, { static: true }) grid?: AppAgGridComponent;
  rowsLoaded = false;
  models: ResourceEntity[] = [];
  @Output() openModel = new EventEmitter<ResourceEntity>();
  $1;
  constructor(
    private resourcesAPIs: ResourcesApiService,
    private modelBuilderService: ModelBuilderService) {
    super();
    this.$1 = this.modelBuilderService.modelsChanged.subscribe(d => this.loadResources());
    this.loadResources();
  }
  async onCellClicked(params) {
    const { colDef, data } = params;
    if (colDef.field === 'name') {
      this.openModel.emit(data);
    }
  }
  async loadResources() {
    this.isLoading = true;
    try {
      this.models = await this.resourcesAPIs.getMyModels();
    } catch (err) { }
    this.isLoading = false;
  }
}
