import { Component, EventEmitter, Input, Output, OnInit, OnChanges } from '@angular/core';
import { ResourceEntity } from 'src/graphql/types/ResourceEntity';
import { UIPartResult } from 'src/grpc/grpc.service';


@Component({
  selector: 'app-resources-panel',
  templateUrl: 'resources.panel.component.html',
  styleUrls: ['resources.panel.component.less'],
})
export class ResourcesPanelComponent implements OnInit, OnChanges {
  @Input() uiPartResults: UIPartResult[] = [];
  @Output() openModel = new EventEmitter<ResourceEntity>();
  tabs = [
    { name: 'Models' },
    { name: 'Datasets' },
    { name: 'Security lists' },
  ];
  selectedTab;
  isLoading = false;
  searchOpened = false;
  constructor() { }

  ngOnInit() {
    this.selectedTab = this.tabs[0];
  }
  ngOnChanges(params) {
    const hasSecList = this.uiPartResults.find(p => !!p.secLists);
    if (params.uiPartResults && hasSecList) {
      this.selectedTab = this.tabs[2];
    }
  }
  onOpenModel(model: ResourceEntity) {
    this.openModel.emit(model);
  }
}
