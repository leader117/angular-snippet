import { Injectable } from '@angular/core';
import { GraphQlApiService } from '../../services/graphql.api';
import { BacktestRow } from 'src/applications/model.builder/components/backtest.grid/BacktestRow';
import { CREATE_BACKTEST, DELETE_BACKTEST } from 'src/graphql/mutations/backtest';
import { GET_BACKTESTS } from 'src/graphql/queries/backtest';

@Injectable()
export class BacktestsApiService {

  constructor(
    private graphQl: GraphQlApiService) { }

  async createBacktest(backtest: BacktestRow): Promise<BacktestRow> {
    const vars = {
      model: backtest.modelId,
      name: backtest.name,
      eql: backtest.eqlSettings.code,
      startTime: toUnix(backtest.eqlSettings.startDate),
      endTime: toUnix(backtest.eqlSettings.endDate),
      barSize: backtest.eqlSettings.barSize,
      targetSymbol: backtest.eqlSettings.targetSymbol,
      targetField: backtest.eqlSettings.targetField,
      settings: '',
      timestamp: toUnix(new Date()),
      ...backtest.values,
    };
    const res = await this.graphQl.post(CREATE_BACKTEST, vars);
    if (res.error) return res;
    return BacktestRow.fromGraghql(res.createBacktest);
  }
  async getBacktests(modelId: string): Promise<BacktestRow[]> {
    const res = await this.graphQl.post(GET_BACKTESTS, { modelId });
    if (res.error) return res;
    return res.backtests.map(bt => BacktestRow.fromGraghql(bt));
  }
  async deleteBacktest(btId: string) {
    const res = await this.graphQl.post(DELETE_BACKTEST, { id: btId });
    if (res.error) return res;
    return res.deleteBacktest;
  }
}
function toUnix(date: Date) {
  return Math.floor(date.valueOf() / 1000);
}