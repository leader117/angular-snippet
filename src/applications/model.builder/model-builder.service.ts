import { Injectable, Output, EventEmitter } from '@angular/core';
import { GraphQlApiService } from 'src/services/graphql.api';
import { GaService } from 'src/services/ga.service';
import { GET_RESOURCE } from 'src/graphql/queries/resource';
import { ADD_RESOURCE, EDIT_RESOURCE, PUBLISH_RESOURCE, CREATE_RESOURCE_VERSION, DELETE_RESOURCE } from 'src/graphql/mutations/resource';
import { ResourceEntity } from 'src/graphql/types/ResourceEntity';

@Injectable()
export class ModelBuilderService {
  @Output() modelsChanged = new EventEmitter<{ model: ResourceEntity, action: string }>();
  constructor(private graphqlApi: GraphQlApiService, private ga: GaService) { }

  async getModel(id): Promise<ResourceEntity> {
    const res = await this.graphqlApi.post(GET_RESOURCE, { id });
    if (res.error) return res;
    return ResourceEntity.fromGraphQL(res.resource);
  }

  async addModel(model: ResourceEntity) {
    const res = await this.graphqlApi.post(ADD_RESOURCE, model);
    this.modelsChanged.emit({ model, action: 'add model' });
    this.ga.trackEvent('add model', 'model builder');
    return ResourceEntity.fromGraphQL(res.addResource);
  }

  async editModel(model: ResourceEntity) {
    const res = await this.graphqlApi.post(EDIT_RESOURCE, model);
    this.modelsChanged.emit({ model, action: 'edit model' });
    this.ga.trackEvent('edit model', 'model builder');
    return ResourceEntity.fromGraphQL(res.editResource);
  }

  async publishModel(id) {
    const res = await this.graphqlApi.post(PUBLISH_RESOURCE, { id });
    this.modelsChanged.emit({ model: res.publishResource, action: 'publish model' });
    this.ga.trackEvent('publish model', 'model builder');
    return res.publishResource;
  }

  async createModelVersion(model: ResourceEntity) {
    const res = await this.graphqlApi.post(CREATE_RESOURCE_VERSION, model);
    this.modelsChanged.emit({ model, action: 'create model model' });
    this.ga.trackEvent('create model version', 'model builder');
    return ResourceEntity.fromGraphQL(res.createResourceVersion);
  }

  async deleteModel(id) {
    const res = await this.graphqlApi.post(DELETE_RESOURCE, { id });
    this.modelsChanged.emit({ model: res.deleteResource, action: 'delete model' });
    this.ga.trackEvent('delete model', 'model builder');
    return res.deleteResource;
  }
}
