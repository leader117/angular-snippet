import { NgModule } from '@angular/core';
import { AppSharedModule } from 'src/shared-modules';
import { EqlSettingsModalComponent } from './components/eql.settings.modal/eql.settings.modal.component';
import { ResourcesPanelComponent } from './components/resources.panel/resources.panel.component';
import { SaveModalComponent } from './components/save-modal/save-modal.component';
import { BacktestGridComponent } from './components/backtest.grid/backtest.grid.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BacktestViewerModalComponent } from './components/backtest.viewer.modal/backtest.viewer.modal.component';
import { AngularSplitModule } from 'angular-split';
import { HighchartModule } from 'src/components/highchart/highchart.module';
import { AppAgGridModule } from 'src/components/grid/grid.module';
import { DatePickerModule } from 'src/components/date.picker/date.picker.module';
import { ConfirmModalService } from 'src/components/confirm.modal/confirm.modal.service';
import { GRPCService } from 'src/grpc/grpc.service';
import { GraphQlApiService } from 'src/services/graphql.api';
import { ModelBuilderService } from './model-builder.service';
import { BacktestsApiService } from './backtests.api.service';
import { ResourcesApiService } from 'src/services/resources.api';
import { WorkspaceApiService } from 'src/services/workspace.apis.service';
import { ModelBuilderRequestProgressModule } from 'src/components/model.builder.request.progress/model.builder.request.progress.module';
import { ModelsGridComponent } from './components/models.grid/models.grid.component';
import { ModelBuilderAppComponent } from './components/model.builder.app/model.builder.app.component';
import { ModelBuilderHomeComponent } from './components/model.builder.home/model.builder.home.component';
import { ModelsExpandedPanelComponent } from './components/models.expanded-panel/models.expanded-panel.component';
import { EqlEditorModule } from 'src/components/eql.editor/eql.editor.module';
import { FloatWindowModule } from 'src/components/float.window/float.window.module';
import { SecListGridComponent } from './components/secList.grid/secList.grid.component';
import { TSGridComponent } from './components/ts.grid/ts.grid.component';

@NgModule({
  declarations: [
    ModelBuilderHomeComponent,
    ModelBuilderAppComponent,
    EqlSettingsModalComponent,
    ResourcesPanelComponent,
    BacktestGridComponent,
    SaveModalComponent,
    BacktestViewerModalComponent,
    ModelsGridComponent,
    ModelsExpandedPanelComponent,
    SecListGridComponent,
    TSGridComponent
  ],
  imports: [
    AppSharedModule,
    ModalModule.forRoot(),
    AngularSplitModule,
    HighchartModule,
    AppAgGridModule,
    DatePickerModule,
    ModelBuilderRequestProgressModule,
    EqlEditorModule,
    FloatWindowModule
  ],
  providers: [
    ConfirmModalService,
    GRPCService,
    GraphQlApiService,
    ModelBuilderService,
    BacktestsApiService,
    ResourcesApiService,
    WorkspaceApiService
  ],
  exports: [
    ModelBuilderHomeComponent,
    ModelBuilderAppComponent,
    ResourcesPanelComponent,
    BacktestGridComponent,
    ModelsGridComponent,
    ModelsExpandedPanelComponent,
    SecListGridComponent,
    TSGridComponent
  ],
  entryComponents: [
    BacktestViewerModalComponent,
    SaveModalComponent,
    EqlSettingsModalComponent,
  ],
})
export class ModelBuilderIndexModule { }

@NgModule({
  imports: [ModelBuilderIndexModule],
  bootstrap: [ModelBuilderHomeComponent],
})
export class ModelBuilderHomeModule { }

@NgModule({
  imports: [ModelBuilderIndexModule],
  bootstrap: [ModelBuilderAppComponent],
})
export class ModelBuilderAppModule { }
