import * as moment from 'moment-timezone';
import { UpdateObj } from 'src/scripts/utilities';
import { ActionMetadata } from 'src/grpc/proto-gen/eqltree_pb';

export class EqlSettings {
    code = 'MSFT.close';
    barSize: ActionMetadata.BarSizeMap[keyof ActionMetadata.BarSizeMap];
    showTargetSymbol = false;
    targetSymbol = 'spy';
    targetField = 'c';
    intraday = true;
    priceProxy: string;
    order: string;
    startDate: Date;
    endDate: Date;
    lag: number;

    static fromJSON(obj: any): EqlSettings {
        const s = new EqlSettings();
        if (obj.code) s.code = obj.code;
        if (obj.eql) s.code = obj.eql;
        if (obj.barSize) s.barSize = obj.barSize;
        if (obj.showTargetSymbol) s.showTargetSymbol = obj.showTargetSymbol;
        if (obj.targetSymbol) s.targetSymbol = obj.targetSymbol;
        if (obj.targetField) s.targetField = obj.targetField;
        if (obj.intraday) s.intraday = obj.intraday;
        if (obj.priceProxy) s.priceProxy = obj.priceProxy;
        if (obj.order) s.order = obj.order;
        if (obj.startDate) s.startDate = moment(obj.startDate).toDate();
        if (obj.endDate) s.endDate = moment(obj.endDate).toDate();
        if (obj.lag) s.lag = obj.lag;
        return s;
    }
    static getDefault(): EqlSettings {
        const item = localStorage.getItem('default-eql-settings');
        if (item) return EqlSettings.fromJSON(JSON.parse(item));
        const s = new EqlSettings();
        s.code = 'MSFT.close';
        s.barSize = ActionMetadata.BarSize.BS_1MIN;
        s.targetSymbol = 'spy';
        s.targetField = 'c';
        s.intraday = true;
        s.priceProxy = 'evwpc';
        s.order = 'high';
        s.startDate = moment('2014-01-01', 'YYYY-MM-DD').toDate();
        s.endDate = moment('2015-12-31', 'YYYY-MM-DD').toDate();
        s.lag = 5;
        return s;
    }
    saveAsDefault() {
        localStorage.setItem('default-eql-settings', JSON.stringify(this));
    }
    clone(overrideBy: any = {}): EqlSettings {
        const obj = UpdateObj(EqlSettings.fromJSON(this), overrideBy);
        obj.startDate = moment(obj.startDate).toDate();
        obj.endDate = moment(obj.endDate).toDate();
        return obj;
    }
}
