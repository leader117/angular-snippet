import { NgModule } from '@angular/core';
import { AppSharedModule } from 'src/shared-modules';
import { DatasetsAppComponent } from './datasets.component';
import { DatasetExpandedPanelComponent } from './components/dataset.expanded-panel/dataset.expanded-panel.component';
import { DatasetViewerComponent } from './components/dataset.viewer/dataset.viewer.component';
import { DatasetCreateComponent } from './components/dataset.create/dataset.create.component';
import { DatasetGridComponent } from './components/dataset.grid/dataset.grid.component';
import { AppAgGridModule } from 'src/components/grid/grid.module';
import { DatasetsApiService } from 'src/applications/datasets/datasets.api';
import { ConfirmModalService } from 'src/components/confirm.modal/confirm.modal.service';
import { GraphQlApiService } from 'src/services/graphql.api';
import { ResourcesApiService } from 'src/services/resources.api';
import { BsModalService, ModalOptions, ModalModule } from 'ngx-bootstrap/modal';

@NgModule({
  declarations: [
    DatasetsAppComponent,
    DatasetExpandedPanelComponent,
    DatasetGridComponent,
    DatasetViewerComponent,
    DatasetCreateComponent
  ],
  imports: [
    AppSharedModule,
    AppAgGridModule,
    ModalModule.forRoot(),
  ],
  providers: [ConfirmModalService, GraphQlApiService, DatasetsApiService, ResourcesApiService, BsModalService],
  exports: [DatasetsAppComponent, DatasetExpandedPanelComponent, DatasetGridComponent, DatasetCreateComponent],
})
export class DatasetsIndexModule { }

@NgModule({
  imports: [DatasetsIndexModule],
  bootstrap: [DatasetsAppComponent],
})
export class DatasetsAppModule { }

@NgModule({
  imports: [DatasetsIndexModule],
  bootstrap: [DatasetViewerComponent],
})
export class DatasetViewerModule { }
