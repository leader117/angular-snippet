import { Injectable } from '@angular/core';
import * as moment from 'moment-timezone';
import { GraphQlApiService } from '../../services/graphql.api';
import { ConfirmModalService } from 'src/components/confirm.modal/confirm.modal.service';
import {
  GOOGLE_AUTH_QUERY,
  GET_PROJECTS_QUERY,
  GET_TABLES_QUERY,
  GET_DATASETS_QUERY,
  GET_TABLE_COLUMNS_QUERY,
  GET_TABLE_DATA_QUERY
} from 'src/graphql/queries/biq-query';
import { GET_RESOURCE } from 'src/graphql/queries/resource';
import { ADD_RESOURCE, EDIT_RESOURCE, PUBLISH_RESOURCE, DELETE_RESOURCE } from 'src/graphql/mutations/resource';
import { GaService } from '../../services/ga.service';
import {
  GoogleProjectRow,
  GoogleDatasetRow,
  GoogleTableRow,
  GoogleFieldRow,
  DatasetMappingTemplate,
  DatasetTemplateField,
  DatasetEntity,
  GoogleAuthResult,
  DatasetTableRow
} from 'src/graphql/types/Datasets';
import { ResourcesApiService } from '../../services/resources.api';
import { ADD_RESOURCE_CATEGORY } from 'src/graphql/mutations/categories';


@Injectable()
export class DatasetsApiService {

  constructor(
    private graphQl: GraphQlApiService,
    private resourcesAPIs: ResourcesApiService,
    private confirmModal: ConfirmModalService,
    private ga: GaService) { }

  async getGoogleAuth(): Promise<GoogleAuthResult> {
    const res = await this.graphQl.postNoCache(GOOGLE_AUTH_QUERY);
    if (res.error) return res;
    return GoogleAuthResult.fromGraphQL(res);
  }
  async getProjectsAndTemplates(): Promise<{ projects: GoogleProjectRow[], templates: DatasetMappingTemplate[] }> {
    const res = await this.graphQl.postNoCache(GET_PROJECTS_QUERY);
    if (res.error) return res;
    return {
      projects: res.bigQueryProjects.map(GoogleProjectRow.fromGraphQL),
      templates: res.datasetTemplates.map(DatasetMappingTemplate.fromGraphQL)
    };
  }
  async getProjectDatasets(project: GoogleProjectRow): Promise<GoogleDatasetRow[]> {
    const res = await this.graphQl.postNoCache(GET_DATASETS_QUERY, { projectId: project.id });
    if (res.error) return res;
    project.datasets = res.bigQueryDatasets.map(GoogleDatasetRow.fromGraphQL);
    return project.datasets;
  }
  async getDatasetTables(dataset: GoogleDatasetRow): Promise<GoogleTableRow[]> {
    const res = await this.graphQl.postNoCache(GET_TABLES_QUERY, { datasetId: dataset.id });
    if (res.error) return res;
    dataset.tables = res.bigQueryTables.map(GoogleTableRow.fromGraphQL);
    return dataset.tables;
  }
  async getTablesFields(table: GoogleTableRow): Promise<GoogleFieldRow[]> {
    const res = await this.graphQl.postNoCache(GET_TABLE_COLUMNS_QUERY, { tableId: table.id });
    if (res.error) return res;
    table.fields = res.bigQueryColumns.map(GoogleFieldRow.fromGraphQL);
    return table.fields;
  }
  async getTableData(dataset: DatasetEntity, pageSize = 100, pageNumber = 0): Promise<DatasetTableRow[]> {
    const settings = {
      sourceTableId: dataset.settings.tableId,
      fieldMappings: dataset.settings.fieldMappings
    };
    const res = await this.graphQl.postNoCache(GET_TABLE_DATA_QUERY, { settings, pageSize, pageNumber });
    if (res.error) return res;
    return res.bigQueryRun.rows.map(DatasetTableRow.fromGraphQL);
  }
  async getDatasets(): Promise<DatasetEntity[]> {
    const resources = await this.resourcesAPIs.getMyResources({ typeIds: [DatasetEntity.TYPE_ID] });
    return resources.map(DatasetEntity.fromGraphQL);
  }
  async getDataset(dsId: string): Promise<DatasetEntity> {
    // return (await this.getDatasets()).find(ds => ds.id === dsId);
    const res = await this.graphQl.postNoCache(GET_RESOURCE, { id: dsId });
    return DatasetEntity.fromGraphQL(res.resource);
  }
  showGoogleLoginModal(callback) {
    const message = `To add your first dataset you need to authenticate with Google and grant us read-only access to your BiqQuery data.?`;
    const confirmButtonText = 'Authenticate with Google';
    const cancelButtonText = undefined;
    this.confirmModal.show({ message, confirmButtonText, cancelButtonText }, callback);
  }
  async addDataset(ds: DatasetEntity) {
    const res = await this.graphQl.post(ADD_RESOURCE, ds);
    if (res.error) return res;
    this.ga.trackEvent('add dataset', 'datasets');
    const ddd = res.addResource;
    ds.date = moment(ddd.createdAt).toDate();
    return ddd;
  }
  async saveDataset(ds: DatasetEntity) {
    const res = await this.graphQl.post(EDIT_RESOURCE, ds);
    if (res.error) return res;
    this.ga.trackEvent('edit dataset', 'datasets');
    return res.editResource;
  }
  async deleteDataset(ds: DatasetEntity) {
    const res = await this.graphQl.post(DELETE_RESOURCE, { id: ds.id });
    if (res.error) return res;
    this.ga.trackEvent('delete dataset', 'datasets');
    return res.deleteModel;
  }
  async publishDataset(ds: DatasetEntity) {
    await this.graphQl.post(ADD_RESOURCE_CATEGORY, { categoryId: 'b132b33b-4737-9304-f16f-4c87fcac4f3c', resourceId: ds.id });
    const res = await this.graphQl.post(PUBLISH_RESOURCE, { id: ds.id });
    if (!res.error) ds.published = true;
    this.ga.trackEvent('publish dataset', 'datasets');
    return res.publishResource;
  }
}
