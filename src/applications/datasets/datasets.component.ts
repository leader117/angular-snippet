import { Component, OnInit, OnDestroy } from '@angular/core';
import { AppRouteService } from 'src/services/app.route';
import { IToolApp } from 'src/applications/base/IToolApp';
import { ConfirmModalService } from 'src/components/confirm.modal/confirm.modal.service';
import { DatasetsApiService } from 'src/applications/datasets/datasets.api';
import { BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { ArrayRemoveItem } from 'src/scripts/utilities';
import { WorkspaceEntity } from 'src/graphql/types/WorkspaceEntity';
import { WindowEntity } from 'src/graphql/types/WindowEntity';
import { DatasetEntity } from 'src/graphql/types/Datasets';
import { AutoUnsubscribeComponent } from 'src/components/auto-unsubscribe';

/**
 * @title App main search box
 */
@Component({
  selector: 'app-datasets',
  templateUrl: 'datasets.component.html',
  styleUrls: ['datasets.component.less'],
})
export class DatasetsAppComponent extends AutoUnsubscribeComponent implements OnInit, OnDestroy, IToolApp {
  datasets: Array<DatasetEntity> = [];
  isLoading = false;
  view = 'main';
  editedDataset = new DatasetEntity();
  one$;
  constructor(
    private dsAPIs: DatasetsApiService,
    private confirmModal: ConfirmModalService,
    private appRoute: AppRouteService,
    private modalService: BsModalService) {
      super();
  }
  async ngOnInit() {
    const { path, datasetId } = this.getNavigatePath();
    if (path === 'create') this.showDatasetCreate();
    else if (path === 'edit') {
      this.isLoading = true;
      const ds = await this.dsAPIs.getDataset(datasetId);
      this.isLoading = false;
      if (!ds.error) this.editDataset(ds);
      else this.confirmModal.showMessage(ds.error);
    } else if (path === 'view') {
      this.isLoading = true;
      const ds = await this.dsAPIs.getDataset(datasetId);
      this.isLoading = false;
      if (!ds.error) this.viewDataset(ds);
      else this.confirmModal.showMessage(ds.error);
    } else this.loadDatasets();
    this.one$ = this.appRoute.subscribe(() => {
      const r = this.appRoute.getUrl();
      if (r.endsWith('/datasets')) this.backToDatasets();
    });
  }
  ngOnDestroy() {
  }
  editDataset(ds: DatasetEntity) {
    this.editedDataset = ds.clone();
    this.view = 'edit';
    this.navigate(!ds.id ? 'create' : `edit/${ds.id}`);
  }
  viewDataset(ds: DatasetEntity) {
    this.appRoute.navigateToolsApp(ds.id); return;
    this.editedDataset = ds;
    this.view = 'view';
    this.navigate(`view/${ds.id}`);
  }
  async backToDatasets() {
    this.view = 'main';
    this.loadDatasets();
    this.navigate();
  }
  private async loadDatasets() {
    if (true || !this.datasets.length) {
      this.isLoading = true;
      this.datasets = await this.dsAPIs.getDatasets();
      this.isLoading = false;
    }
  }
  private navigate(path = '', params = {}) {
    this.appRoute.navigate('/tools/datasets/' + path, params);
  }
  private getNavigatePath() {
    const l = location.pathname.split('/').filter(a => a);
    return {
      path: l[2],
      datasetId: l[3],
    };
  }
  async showDatasetCreate() {
    const auth = await this.dsAPIs.getGoogleAuth();
    if (auth.isAuthorized) {
      this.editDataset(new DatasetEntity());
      return;
    }
    this.dsAPIs.showGoogleLoginModal(() => {
      window.location.href = auth.url;
    });
  }
  async saveDataset(ds: DatasetEntity) {
    if (!ds.name) return this.confirmModal.showMessage('Please fill Title of your dataset.');
    let res;
    if (ds.id) res = await this.dsAPIs.saveDataset(ds);
    else res = await this.dsAPIs.addDataset(ds);
    if (res.error) return this.confirmModal.showMessage(res.error);
    if (!ds.id) {
      ds.id = res.id;
      this.datasets.push(ds);
    }
    this.backToDatasets();
    return true;
  }
  async saveDatasetAndPublish(ds: DatasetEntity) {
    const done = await this.saveDataset(ds);
    if (done) await this.dsAPIs.publishDataset(ds);
  }
  deleteDataset(ds: DatasetEntity) {
    const message = 'Dataset will be deleted, process?';
    this.confirmModal.show({ message }, async () => {

      const res = await this.dsAPIs.deleteDataset(ds);
      if (res.error) return this.confirmModal.showMessage(res.error);
      else ArrayRemoveItem(this.datasets, ds);
    });
  }
  getToolTitle(): string {
    return 'Datasets';
  }
  getToolParams() {
    return {};
  }
  setToolParams(ws: WorkspaceEntity, tab: WindowEntity) {
  }
}
