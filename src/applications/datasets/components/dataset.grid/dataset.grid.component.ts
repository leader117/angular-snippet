import { Component, EventEmitter, Input, Output, ViewChild, ElementRef, OnInit, OnChanges } from '@angular/core';
import { DatasetsApiService } from 'src/applications/datasets/datasets.api';
import { ConfirmModalService } from 'src/components/confirm.modal/confirm.modal.service';
import * as moment from 'moment-timezone';
import { DatasetEntity } from 'src/graphql/types/Datasets';

@Component({
  selector: 'app-dataset-grid',
  templateUrl: 'dataset.grid.component.html',
  styleUrls: ['dataset.grid.component.less'],
})
export class DatasetGridComponent implements OnInit, OnChanges {
  @Input() dataset: DatasetEntity;
  @Input() domLayout = 'autoHeight';
  loading = false;
  rows = [];
  columns = [
    {
      headerName: 'Date',
      field: 'date',
      resizable: true,
      sortable: true,
      sort: 'desc',
      cellRenderer: p => moment(p.value).format('MM/DD/YYYY HH:mm'),
    },
    {
      headerName: 'Value',
      field: 'value',
      resizable: true,
      sortable: true,
    },
  ];

  constructor(
    private dsAPIs: DatasetsApiService,
    private confirmModal: ConfirmModalService) {

  }
  ngOnInit() {
    if (this.dataset) this.openDataset();
  }
  ngOnChanges(params) {
    if (params.dataset && this.dataset) this.openDataset();
  }
  async openDataset() {
    if (!this.dataset) return;
    const { settings } = this.dataset;
    if (!settings.fieldMappings || !settings.fieldMappings.length) return;
    if (settings.fieldMappings.some(f => !f.mappedColumnId)) return;
    this.loading = true;
    try {
      const res: any = await this.dsAPIs.getTableData(this.dataset, 100, 0);
      if (res.error) throw res.error;
      this.rows = res;
    } catch (err) {
      this.confirmModal.showMessage(err);
    }
    this.loading = false;
  }
}
