import { Component, EventEmitter, Input, Output, ViewChild, ElementRef } from '@angular/core';
import { DatasetEntity } from 'src/graphql/types/Datasets';

@Component({
  selector: 'app-dataset-expanded-panel',
  templateUrl: 'dataset.expanded-panel.component.html',
  styleUrls: ['dataset.expanded-panel.component.less'],
})
export class DatasetExpandedPanelComponent {
  @ViewChild('details', { static: true }) details: ElementRef;
  @Output() clickOpen = new EventEmitter();
  isOpen = false;
  @Input() ds: DatasetEntity;

  openDataset() {
    this.clickOpen.emit();
  }

  toggleOpen() {
    this.isOpen = !this.isOpen;
    if (this.isOpen) {
      setTimeout(() => { this.details.nativeElement.style.overflow = 'auto'; }, 400);
    } else {
      this.details.nativeElement.style.overflow = 'hidden';
    }
  }
}
