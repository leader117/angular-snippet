import { Component, EventEmitter, Input, Output, ViewChild, ElementRef, OnInit } from '@angular/core';
import { DatasetsApiService } from 'src/applications/datasets/datasets.api';
import { IToolApp } from 'src/applications/base/IToolApp';
import { AppRouteService } from 'src/services/app.route';
import { ConfirmModalService } from 'src/components/confirm.modal/confirm.modal.service';
import * as moment from 'moment-timezone';
import { WorkspaceEntity } from 'src/graphql/types/WorkspaceEntity';
import { WindowEntity } from 'src/graphql/types/WindowEntity';
import { DatasetEntity } from 'src/graphql/types/Datasets';

@Component({
  selector: 'app-dataset-viewer',
  templateUrl: 'dataset.viewer.component.html',
  styleUrls: ['dataset.viewer.component.less'],
})
export class DatasetViewerComponent implements IToolApp, OnInit {
  @Input() dataset: DatasetEntity;
  loading = false;
  ws: WorkspaceEntity;
  tab: WindowEntity;
  constructor(
    private appRoute: AppRouteService,
    private dsAPIs: DatasetsApiService,
    private confirmModal: ConfirmModalService) {

  }
  ngOnInit() {
    if (this.tab) return this.openDataset(this.tab.resource.id);
    if (this.appRoute.isToolsPath()) {
      const datasetId = this.appRoute.getUrlPaths()[1];
      this.openDataset(datasetId);
    }
  }

  async openDataset(dataset: string) {
    this.loading = true;
    try {
      this.dataset = await this.dsAPIs.getDataset(dataset);
    } catch (err) {
      this.confirmModal.showMessage(err);
    }
    this.loading = false;
  }
  getToolTitle(): string {
    if (this.dataset) return this.dataset.name;
    return 'Datasets';
  }
  getToolParams() {
    return { };
  }
  setToolParams(ws: WorkspaceEntity, tab: WindowEntity) {
    this.ws = ws;
    this.tab = tab;
  }
}
