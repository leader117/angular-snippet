import { Component, Input, Output, EventEmitter, OnInit, OnChanges, ViewChild } from '@angular/core';
import { DatasetsApiService } from 'src/applications/datasets/datasets.api';
import { ArrayRemoveItem, TruncateText } from 'src/scripts/utilities';
import {
  DatasetEntity,
  GoogleProjectRow,
  DatasetMappingTemplate,
  GoogleDatasetRow,
  GoogleTableRow,
  GoogleFieldRow
} from 'src/graphql/types/Datasets';
import { DatasetGridComponent } from '../dataset.grid/dataset.grid.component';
import { ConfirmModalService } from 'src/components/confirm.modal/confirm.modal.service';

/**
 * @title App main search box
 */
@Component({
  selector: 'app-dataset-create',
  templateUrl: 'dataset.create.component.html',
  styleUrls: ['dataset.create.component.less'],
})
export class DatasetCreateComponent implements OnChanges, OnInit {
  @Output() saveDraft = new EventEmitter<DatasetEntity>();
  @Output() cancel = new EventEmitter<DatasetEntity>();
  @Output() saveAndPublish = new EventEmitter<DatasetEntity>();
  errorMessage: string;
  isBusy = false;
  projects: GoogleProjectRow[] = [];
  templates: DatasetMappingTemplate[] = [];
  @Input() ds = new DatasetEntity();
  @ViewChild('previewGrid', { static: false }) previewGrid: DatasetGridComponent;
  constructor(private dsAPIs: DatasetsApiService, private confirmModal: ConfirmModalService) {
  }
  async ngOnInit() {
    await this.loadDataset();
  }
  async ngOnChanges(params) {
    if (params.ds && this.ds && this.ds.id) {
      await this.loadDataset();
    }
  }
  async loadDataset() {
    const { settings } = this.ds;
    if (!this.projects.length || !this.templates.length) {
      const { projects, templates } = await this.dsAPIs.getProjectsAndTemplates();
      this.projects = projects;
      this.templates = templates;
    }
    if (!this.ds) return;
    if (settings.projectId) await this.selectProject(this.getSelectedProject());
  }
  async selectProject(project: GoogleProjectRow) {
    if (!project) return;
    const { settings } = this.ds;
    const newProject = settings.projectId !== project.id;
    if (newProject || !project.datasets || !project.datasets.length) await this.dsAPIs.getProjectDatasets(project);
    if (!project.datasets.length) return this.confirmModal.showMessage('Cannot select this project.  It does not contain a dataset');
    settings.projectId = project.id;
    const pDs = project.find(settings.datasetId);
    if (pDs) this.selectDataset(pDs);
  }
  async selectDataset(ds: GoogleDatasetRow) {
    if (!ds) return;
    const { settings } = this.ds;
    const newDS = settings.datasetId !== ds.id;
    if (newDS || !ds.tables || !ds.tables.length) await this.dsAPIs.getDatasetTables(ds);
    if (!ds.tables.length) return this.confirmModal.showMessage('Cannot select this dataset.  It does not contain a table');
    ds.tables.forEach(t => t.name = t.name.slice(0, 100));
    settings.datasetId = ds.id;
    const dsTbl = ds.find(settings.tableId);
    if (dsTbl) this.selectTable(dsTbl);
  }
  async selectTable(table: GoogleTableRow) {
    if (!table) return;
    await this.dsAPIs.getTablesFields(table);
    this.ds.settings.tableId = table.id;
    this.selectTemplate(this.getSelectedTemplate() || this.templates[0]);
  }
  selectTemplate(template: DatasetMappingTemplate) {
    const { settings } = this.ds;
    if (!template) template = this.getSelectedTemplate();
    settings.templateId = template.id;
    const table = this.getSelectedTable();
    if (!template || !table) return settings.fieldMappings = [];
    const targetFields = [...table.fields];

    const fdm = settings.fieldMappings || [];
    fdm.length = template.fields.length;
    template.fields.forEach((f, idx) => {
      if (!fdm[idx]) fdm[idx] = { fieldId: f.id, mappedColumnId: ''};
      let target = targetFields.find(x => fdm[idx].mappedColumnId === x.id);
      if (!target) target = targetFields.find(x => f.compatibleTypes.includes(x.type));
      if (target) ArrayRemoveItem(targetFields, target);
      fdm[idx] = {
        fieldId: f.id,
        mappedColumnId: (target ? target.id : null),
      };
    });
    settings.fieldMappings = fdm;
    if (this.previewGrid) this.previewGrid.openDataset();
  }
  getCompatibleFields(fieldId: string): GoogleFieldRow[] {
    const template = this.getSelectedTemplate();
    const table = this.getSelectedTable();
    const field = template && template.fields.find(f => f.id === fieldId);
    if (!template || !table || !field) return [];
    return table.fields.filter(x => field.compatibleTypes.includes(x.type));
  }
  getSelectedProject() { return this.projects.find(p => p.id === this.ds.settings.projectId); }
  getSelectedDataset() { return this.getSelectedProject() && this.getSelectedProject().find(this.ds.settings.datasetId); }
  getSelectedTable() { return this.getSelectedDataset() && this.getSelectedDataset().find(this.ds.settings.tableId); }
  getSelectedTemplate() { return this.templates.find(t => t.id === this.ds.settings.templateId); }
  getTemplateField(fieldId) { return this.getSelectedTemplate().fields.find(t => t.id === fieldId); }
  hasPreviewData() { return this.ds && this.ds.settings.fieldMappings.length > 0; }

  onSaveAndPublish() {
    this.saveAndPublish.emit(this.ds);
  }
  onSaveDraft() {
    this.saveDraft.emit(this.ds);
  }
  onCancel() {
    this.cancel.emit(this.ds);
  }
  showError(err: string) {
    this.errorMessage = err;
  }
  truncateText(str: string) {
    return TruncateText(str, 15);
  }
}
