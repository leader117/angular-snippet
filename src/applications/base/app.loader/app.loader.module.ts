import { NgModule } from '@angular/core';
import { AppLoaderComponent } from './app.loader.component';

@NgModule({
  declarations: [AppLoaderComponent],
  imports: [],
  providers: [],
  bootstrap: [],
  exports: [AppLoaderComponent],
})
export class AppLoaderModule { }
