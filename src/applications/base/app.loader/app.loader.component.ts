import {
  Component,
  ViewChild,
  ViewContainerRef,
  OnInit,
  Input,
  OnChanges,
  Output,
  EventEmitter,
  NgModuleFactoryLoader,
  Injector,
  OnDestroy
} from '@angular/core';
import { AppLoaderMapper } from './AppLoaderMapper';


/**
 * @title App main search box
 */
@Component({
  selector: 'app-loader',
  templateUrl: 'app.loader.component.html',
  styleUrls: ['app.loader.component.less'],
})
export class AppLoaderComponent implements OnInit, OnChanges, OnDestroy {
  @ViewChild('tabContainer', { read: ViewContainerRef, static: true }) entry: ViewContainerRef;
  componentRef: any;
  @Input() appCode: string;
  @Output() appCreated = new EventEmitter<any>();
  constructor(
    private loader: NgModuleFactoryLoader,
    private injector: Injector) { }
  ngOnInit() {
  }
  async ngOnChanges(changes) {
    if (changes.appCode && this.appCode) this.createComponent();
  }
  createComponent(appCode?: string) {
    if (!appCode) appCode = this.appCode;
    this.destroyComponent();
    const modulePath = AppLoaderMapper.getAppModulePath(appCode);
    this.loader.load(modulePath).then((moduleFactory) => {
      const entryComponent = (moduleFactory as any)._bootstrapComponents[0];
      const moduleRef = moduleFactory.create(this.injector);
      const compFactory = moduleRef.componentFactoryResolver.resolveComponentFactory(entryComponent);
      this.entry.clear();
      this.componentRef = this.entry.createComponent(compFactory);
      this.appCreated.emit(this.componentRef.instance);
    });
  }
  destroyComponent() {
    if (this.componentRef) this.componentRef.destroy();
  }
  ngOnDestroy() {
    this.destroyComponent();
  }
}
