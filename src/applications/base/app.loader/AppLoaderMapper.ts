export class AppLoaderMapper {
    static getAppModulePath(appCode) {
        switch (appCode) {
            case 'Home': return 'src/applications/root.apps/root.apps.module.ts#RootAppsAppModule';
            case 'User Workspaces': return 'src/applications/workspace/workspace.module.ts#WorkspaceHomeAppModule';
            case 'Workspace Viewer': return 'src/applications/workspace/workspace.module.ts#WorkspaceTabsAppModule';
            case 'tools': return 'src/applications/workspace/workspace.module.ts#WorkspaceToolsAppModule';
            case 'Models': return 'src/applications/model.builder/model.builder.module.ts#ModelBuilderHomeModule';
            case 'Model Builder': return 'src/applications/model.builder/model.builder.module.ts#ModelBuilderAppModule';
            case 'Knowledge Graph': return 'src/applications/knowledge.graph/knowledge.graph.module.ts#KnowledgeGraphAppModule';
            case 'Datasets Building': return 'src/applications/datasets/datasets.module.ts#DatasetsAppModule';
            case 'Dataset': return 'src/applications/datasets/datasets.module.ts#DatasetViewerModule';
            case 'Equeum NLP': return 'src/applications/nlp/nlp.module.ts#NLPAppModule';
        }
    }
}
