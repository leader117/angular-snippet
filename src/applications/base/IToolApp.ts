import { WorkspaceEntity } from 'src/graphql/types/WorkspaceEntity';
import { WindowEntity } from 'src/graphql/types/WindowEntity';

export interface IToolApp {
    getToolTitle(): string;
    getToolParams(): any;
    setToolParams(ws: WorkspaceEntity, tab: WindowEntity);
}
