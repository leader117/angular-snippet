import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, Validator, FormControl } from '@angular/forms';

@Directive({
  selector: '[formTrim][ngModel]',
  providers: [{ provide: NG_VALIDATORS, useExisting: TrimDirective, multi: true }]
})
export class TrimDirective implements Validator {
  validate(c: FormControl) {
    const isValid = c.value && c.value.trim().length > 0;
    return isValid ? null : {
      formTrim: {
        valid: false
      }
    }
  }
}
