import { NgModule } from '@angular/core';
import { SanitizeHtmlPipe } from './sanitize.html.pipe';

@NgModule({
  declarations: [
    SanitizeHtmlPipe
  ],
  imports: [],
  providers: [],
  bootstrap: [],
  exports: [SanitizeHtmlPipe],
})
export class SanitizeHtmlModule { }
