import { AppBasicModule } from 'src/basic-modules';
import { NgModule } from '@angular/core';
import { ItemsSelectModule } from 'src/components/items.select/items.select.module';
import { SpinnerFlagModule } from 'src/components/spinner/spinner.module';
import { YearSelectModule } from 'src/components/year.select/year.select.module';
import { NumberUnitSelectModule } from 'src/components/number.unit.select/number.unit.select.module';
import { KeyValueTableModule } from 'src/components/key.value.table/key.value.table.module';
import { HttpUrlEncodingCodec, HttpClientModule } from '@angular/common/http';
import { ConfirmModalModule } from './components/confirm.modal/confirm.modal.module';
import { InputModalModule } from './components/input.modal/input.modal.module';
import { GraphQLModule } from './app/graphql.module';
import { TrimDirective } from './directives/trim.directive';

@NgModule({
  imports: [],
  declarations: [TrimDirective],
  exports: [
    AppBasicModule,
    ItemsSelectModule,
    SpinnerFlagModule,
    YearSelectModule,
    NumberUnitSelectModule,
    KeyValueTableModule,
    ConfirmModalModule,
    InputModalModule,
    GraphQLModule,
    HttpClientModule,
    TrimDirective,
  ],
  providers: [
    HttpUrlEncodingCodec,
  ]
})
export class AppSharedModule { }
