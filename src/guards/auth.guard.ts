import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  CanActivate
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { tap } from 'rxjs/operators';
import { getQueryStringParams } from 'src/scripts/utilities';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private auth: AuthService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean> | boolean {
    const token = this.auth.getCachedToken();
    const params = getQueryStringParams(location.search);
    const initalScreen = location.pathname === '/signUp' ? 'signUp' : 'signIn';
    if (!token) {
      this.auth.login(state.url, params.loginHint, initalScreen);
      return false;
    }
    this.auth.getUser$().toPromise().then(p => {
      if (!p) this.auth.login(state.url, params.loginHint, initalScreen);
      return !!p;
    });
    return true;
  }

}
