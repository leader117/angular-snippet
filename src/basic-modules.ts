import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { SanitizeHtmlModule } from './pipes/sanitize.html/sanitize.html.module';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { CommonModule } from '@angular/common';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

@NgModule({
  imports: [],
  exports: [
    CommonModule,
    RouterModule,
    FormsModule,
    HttpClientModule,
    SanitizeHtmlModule,
    AngularSvgIconModule,
    BsDropdownModule,
  ],
  providers: []
})
export class AppBasicModule { }
