// package: execution
// file: execution.proto

import * as jspb from "google-protobuf";
import * as eqltree_pb from "./eqltree_pb";

export class StrategyContext extends jspb.Message {
  hasInitialmetadata(): boolean;
  clearInitialmetadata(): void;
  getInitialmetadata(): eqltree_pb.ActionMetadata | undefined;
  setInitialmetadata(value?: eqltree_pb.ActionMetadata): void;

  getArraysize(): number;
  setArraysize(value: number): void;

  getStarttimebin(): Uint8Array | string;
  getStarttimebin_asU8(): Uint8Array;
  getStarttimebin_asB64(): string;
  setStarttimebin(value: Uint8Array | string): void;

  getEndtimebin(): Uint8Array | string;
  getEndtimebin_asU8(): Uint8Array;
  getEndtimebin_asB64(): string;
  setEndtimebin(value: Uint8Array | string): void;

  hasGroupingaction(): boolean;
  clearGroupingaction(): void;
  getGroupingaction(): eqltree_pb.TSKey | undefined;
  setGroupingaction(value?: eqltree_pb.TSKey): void;

  getParallelism(): number;
  setParallelism(value: number): void;

  getContextname(): string;
  setContextname(value: string): void;

  getFeaturesenabled(): string;
  setFeaturesenabled(value: string): void;

  getBarsadvance(): number;
  setBarsadvance(value: number): void;

  getComment(): string;
  setComment(value: string): void;

  getExecutionmode(): ExecutionModeMap[keyof ExecutionModeMap];
  setExecutionmode(value: ExecutionModeMap[keyof ExecutionModeMap]): void;

  getAveragingmode(): AveragingModeMap[keyof AveragingModeMap];
  setAveragingmode(value: AveragingModeMap[keyof AveragingModeMap]): void;

  getDataformat(): eqltree_pb.DataFormatMap[keyof eqltree_pb.DataFormatMap];
  setDataformat(value: eqltree_pb.DataFormatMap[keyof eqltree_pb.DataFormatMap]): void;

  getLanguagelevel(): LanguageLevelMap[keyof LanguageLevelMap];
  setLanguagelevel(value: LanguageLevelMap[keyof LanguageLevelMap]): void;

  clearGenericvaluesList(): void;
  getGenericvaluesList(): Array<StrategyContext.NamedValue>;
  setGenericvaluesList(value: Array<StrategyContext.NamedValue>): void;
  addGenericvalues(value?: StrategyContext.NamedValue, index?: number): StrategyContext.NamedValue;

  clearCurrentvariablesList(): void;
  getCurrentvariablesList(): Array<StrategyContext.NamedKey>;
  setCurrentvariablesList(value: Array<StrategyContext.NamedKey>): void;
  addCurrentvariables(value?: StrategyContext.NamedKey, index?: number): StrategyContext.NamedKey;

  clearSyntetickeysList(): void;
  getSyntetickeysList(): Array<StrategyContext.NamedSyntheticKey>;
  setSyntetickeysList(value: Array<StrategyContext.NamedSyntheticKey>): void;
  addSyntetickeys(value?: StrategyContext.NamedSyntheticKey, index?: number): StrategyContext.NamedSyntheticKey;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StrategyContext.AsObject;
  static toObject(includeInstance: boolean, msg: StrategyContext): StrategyContext.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StrategyContext, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StrategyContext;
  static deserializeBinaryFromReader(message: StrategyContext, reader: jspb.BinaryReader): StrategyContext;
}

export namespace StrategyContext {
  export type AsObject = {
    initialmetadata?: eqltree_pb.ActionMetadata.AsObject,
    arraysize: number,
    starttimebin: Uint8Array | string,
    endtimebin: Uint8Array | string,
    groupingaction?: eqltree_pb.TSKey.AsObject,
    parallelism: number,
    contextname: string,
    featuresenabled: string,
    barsadvance: number,
    comment: string,
    executionmode: ExecutionModeMap[keyof ExecutionModeMap],
    averagingmode: AveragingModeMap[keyof AveragingModeMap],
    dataformat: eqltree_pb.DataFormatMap[keyof eqltree_pb.DataFormatMap],
    languagelevel: LanguageLevelMap[keyof LanguageLevelMap],
    genericvaluesList: Array<StrategyContext.NamedValue.AsObject>,
    currentvariablesList: Array<StrategyContext.NamedKey.AsObject>,
    syntetickeysList: Array<StrategyContext.NamedSyntheticKey.AsObject>,
  }

  export class NamedValue extends jspb.Message {
    getName(): string;
    setName(value: string): void;

    hasValue(): boolean;
    clearValue(): void;
    getValue(): ExecutorValue | undefined;
    setValue(value?: ExecutorValue): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): NamedValue.AsObject;
    static toObject(includeInstance: boolean, msg: NamedValue): NamedValue.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: NamedValue, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): NamedValue;
    static deserializeBinaryFromReader(message: NamedValue, reader: jspb.BinaryReader): NamedValue;
  }

  export namespace NamedValue {
    export type AsObject = {
      name: string,
      value?: ExecutorValue.AsObject,
    }
  }

  export class NamedKey extends jspb.Message {
    getName(): string;
    setName(value: string): void;

    hasBinding(): boolean;
    clearBinding(): void;
    getBinding(): eqltree_pb.MultiTSKey | undefined;
    setBinding(value?: eqltree_pb.MultiTSKey): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): NamedKey.AsObject;
    static toObject(includeInstance: boolean, msg: NamedKey): NamedKey.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: NamedKey, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): NamedKey;
    static deserializeBinaryFromReader(message: NamedKey, reader: jspb.BinaryReader): NamedKey;
  }

  export namespace NamedKey {
    export type AsObject = {
      name: string,
      binding?: eqltree_pb.MultiTSKey.AsObject,
    }
  }

  export class NamedSyntheticKey extends jspb.Message {
    getName(): string;
    setName(value: string): void;

    hasBindpoint(): boolean;
    clearBindpoint(): void;
    getBindpoint(): eqltree_pb.TSKey | undefined;
    setBindpoint(value?: eqltree_pb.TSKey): void;

    hasBinding(): boolean;
    clearBinding(): void;
    getBinding(): eqltree_pb.MultiTSKey | undefined;
    setBinding(value?: eqltree_pb.MultiTSKey): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): NamedSyntheticKey.AsObject;
    static toObject(includeInstance: boolean, msg: NamedSyntheticKey): NamedSyntheticKey.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: NamedSyntheticKey, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): NamedSyntheticKey;
    static deserializeBinaryFromReader(message: NamedSyntheticKey, reader: jspb.BinaryReader): NamedSyntheticKey;
  }

  export namespace NamedSyntheticKey {
    export type AsObject = {
      name: string,
      bindpoint?: eqltree_pb.TSKey.AsObject,
      binding?: eqltree_pb.MultiTSKey.AsObject,
    }
  }
}

export class ExecutorValue extends jspb.Message {
  hasTs(): boolean;
  clearTs(): void;
  getTs(): ExecutorValue.TimeSeries | undefined;
  setTs(value?: ExecutorValue.TimeSeries): void;

  hasNil(): boolean;
  clearNil(): void;
  getNil(): boolean;
  setNil(value: boolean): void;

  hasSeclist(): boolean;
  clearSeclist(): void;
  getSeclist(): ExecutorValue.StaticSecutityList | undefined;
  setSeclist(value?: ExecutorValue.StaticSecutityList): void;

  hasProcessingerror(): boolean;
  clearProcessingerror(): void;
  getProcessingerror(): string;
  setProcessingerror(value: string): void;

  getSymbolic(): boolean;
  setSymbolic(value: boolean): void;

  clearTraceList(): void;
  getTraceList(): Array<eqltree_pb.EQLTrace>;
  setTraceList(value: Array<eqltree_pb.EQLTrace>): void;
  addTrace(value?: eqltree_pb.EQLTrace, index?: number): eqltree_pb.EQLTrace;

  getDummy(): string;
  setDummy(value: string): void;

  hasSloc(): boolean;
  clearSloc(): void;
  getSloc(): eqltree_pb.SourceLocation | undefined;
  setSloc(value?: eqltree_pb.SourceLocation): void;

  getKindCase(): ExecutorValue.KindCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ExecutorValue.AsObject;
  static toObject(includeInstance: boolean, msg: ExecutorValue): ExecutorValue.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ExecutorValue, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ExecutorValue;
  static deserializeBinaryFromReader(message: ExecutorValue, reader: jspb.BinaryReader): ExecutorValue;
}

export namespace ExecutorValue {
  export type AsObject = {
    ts?: ExecutorValue.TimeSeries.AsObject,
    nil: boolean,
    seclist?: ExecutorValue.StaticSecutityList.AsObject,
    processingerror: string,
    symbolic: boolean,
    traceList: Array<eqltree_pb.EQLTrace.AsObject>,
    dummy: string,
    sloc?: eqltree_pb.SourceLocation.AsObject,
  }

  export class TSData extends jspb.Message {
    getDataformat(): eqltree_pb.DataFormatMap[keyof eqltree_pb.DataFormatMap];
    setDataformat(value: eqltree_pb.DataFormatMap[keyof eqltree_pb.DataFormatMap]): void;

    getNullformat(): NullFormatMap[keyof NullFormatMap];
    setNullformat(value: NullFormatMap[keyof NullFormatMap]): void;

    getNumericdatanature(): NumericDataNatureMap[keyof NumericDataNatureMap];
    setNumericdatanature(value: NumericDataNatureMap[keyof NumericDataNatureMap]): void;

    getAggregationpolicy(): AggregationPolicyMap[keyof AggregationPolicyMap];
    setAggregationpolicy(value: AggregationPolicyMap[keyof AggregationPolicyMap]): void;

    getInterleaving(): InterleavingMap[keyof InterleavingMap];
    setInterleaving(value: InterleavingMap[keyof InterleavingMap]): void;

    hasMetadata(): boolean;
    clearMetadata(): void;
    getMetadata(): eqltree_pb.ActionMetadata | undefined;
    setMetadata(value?: eqltree_pb.ActionMetadata): void;

    getLocaltimestart(): Uint8Array | string;
    getLocaltimestart_asU8(): Uint8Array;
    getLocaltimestart_asB64(): string;
    setLocaltimestart(value: Uint8Array | string): void;

    getTrancheid(): string;
    setTrancheid(value: string): void;

    getTrancheoffset(): string;
    setTrancheoffset(value: string): void;

    getChunkid(): number;
    setChunkid(value: number): void;

    getNonlinear(): Uint8Array | string;
    getNonlinear_asU8(): Uint8Array;
    getNonlinear_asB64(): string;
    setNonlinear(value: Uint8Array | string): void;

    getBarsadvance(): number;
    setBarsadvance(value: number): void;

    getDatalenbytes(): number;
    setDatalenbytes(value: number): void;

    getMaxseclistwidth(): number;
    setMaxseclistwidth(value: number): void;

    getColname(): string;
    setColname(value: string): void;

    hasData(): boolean;
    clearData(): void;
    getData(): Uint8Array | string;
    getData_asU8(): Uint8Array;
    getData_asB64(): string;
    setData(value: Uint8Array | string): void;

    hasLocalreference(): boolean;
    clearLocalreference(): void;
    getLocalreference(): number;
    setLocalreference(value: number): void;

    getKindCase(): TSData.KindCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): TSData.AsObject;
    static toObject(includeInstance: boolean, msg: TSData): TSData.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: TSData, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): TSData;
    static deserializeBinaryFromReader(message: TSData, reader: jspb.BinaryReader): TSData;
  }

  export namespace TSData {
    export type AsObject = {
      dataformat: eqltree_pb.DataFormatMap[keyof eqltree_pb.DataFormatMap],
      nullformat: NullFormatMap[keyof NullFormatMap],
      numericdatanature: NumericDataNatureMap[keyof NumericDataNatureMap],
      aggregationpolicy: AggregationPolicyMap[keyof AggregationPolicyMap],
      interleaving: InterleavingMap[keyof InterleavingMap],
      metadata?: eqltree_pb.ActionMetadata.AsObject,
      localtimestart: Uint8Array | string,
      trancheid: string,
      trancheoffset: string,
      chunkid: number,
      nonlinear: Uint8Array | string,
      barsadvance: number,
      datalenbytes: number,
      maxseclistwidth: number,
      colname: string,
      data: Uint8Array | string,
      localreference: number,
    }

    export enum KindCase {
      KIND_NOT_SET = 0,
      DATA = 1,
      LOCALREFERENCE = 2,
    }
  }

  export class SingleTimeSeries extends jspb.Message {
    hasKey(): boolean;
    clearKey(): void;
    getKey(): eqltree_pb.TSKey | undefined;
    setKey(value?: eqltree_pb.TSKey): void;

    hasData(): boolean;
    clearData(): void;
    getData(): ExecutorValue.TSData | undefined;
    setData(value?: ExecutorValue.TSData): void;

    getKeystring(): string;
    setKeystring(value: string): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): SingleTimeSeries.AsObject;
    static toObject(includeInstance: boolean, msg: SingleTimeSeries): SingleTimeSeries.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: SingleTimeSeries, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): SingleTimeSeries;
    static deserializeBinaryFromReader(message: SingleTimeSeries, reader: jspb.BinaryReader): SingleTimeSeries;
  }

  export namespace SingleTimeSeries {
    export type AsObject = {
      key?: eqltree_pb.TSKey.AsObject,
      data?: ExecutorValue.TSData.AsObject,
      keystring: string,
    }
  }

  export class TimeSeries extends jspb.Message {
    clearArrayList(): void;
    getArrayList(): Array<ExecutorValue.SingleTimeSeries>;
    setArrayList(value: Array<ExecutorValue.SingleTimeSeries>): void;
    addArray(value?: ExecutorValue.SingleTimeSeries, index?: number): ExecutorValue.SingleTimeSeries;

    getTimestamparray(): Uint8Array | string;
    getTimestamparray_asU8(): Uint8Array;
    getTimestamparray_asB64(): string;
    setTimestamparray(value: Uint8Array | string): void;

    getFullsize(): number;
    setFullsize(value: number): void;

    getStarttimestamp(): Uint8Array | string;
    getStarttimestamp_asU8(): Uint8Array;
    getStarttimestamp_asB64(): string;
    setStarttimestamp(value: Uint8Array | string): void;

    getEndtimestamp(): Uint8Array | string;
    getEndtimestamp_asU8(): Uint8Array;
    getEndtimestamp_asB64(): string;
    setEndtimestamp(value: Uint8Array | string): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): TimeSeries.AsObject;
    static toObject(includeInstance: boolean, msg: TimeSeries): TimeSeries.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: TimeSeries, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): TimeSeries;
    static deserializeBinaryFromReader(message: TimeSeries, reader: jspb.BinaryReader): TimeSeries;
  }

  export namespace TimeSeries {
    export type AsObject = {
      arrayList: Array<ExecutorValue.SingleTimeSeries.AsObject>,
      timestamparray: Uint8Array | string,
      fullsize: number,
      starttimestamp: Uint8Array | string,
      endtimestamp: Uint8Array | string,
    }
  }

  export class StaticSecutityList extends jspb.Message {
    clearSecurityList(): void;
    getSecurityList(): Array<string>;
    setSecurityList(value: Array<string>): void;
    addSecurity(value: string, index?: number): string;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): StaticSecutityList.AsObject;
    static toObject(includeInstance: boolean, msg: StaticSecutityList): StaticSecutityList.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: StaticSecutityList, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): StaticSecutityList;
    static deserializeBinaryFromReader(message: StaticSecutityList, reader: jspb.BinaryReader): StaticSecutityList;
  }

  export namespace StaticSecutityList {
    export type AsObject = {
      securityList: Array<string>,
    }
  }

  export enum KindCase {
    KIND_NOT_SET = 0,
    TS = 10,
    NIL = 20,
    SECLIST = 30,
    PROCESSINGERROR = 40,
  }
}

export class ParseResult extends jspb.Message {
  clearMainkeysList(): void;
  getMainkeysList(): Array<eqltree_pb.TSKey>;
  setMainkeysList(value: Array<eqltree_pb.TSKey>): void;
  addMainkeys(value?: eqltree_pb.TSKey, index?: number): eqltree_pb.TSKey;

  clearMainkeysstringsList(): void;
  getMainkeysstringsList(): Array<string>;
  setMainkeysstringsList(value: Array<string>): void;
  addMainkeysstrings(value: string, index?: number): string;

  clearVariablesList(): void;
  getVariablesList(): Array<ParseResult.VariableSpec>;
  setVariablesList(value: Array<ParseResult.VariableSpec>): void;
  addVariables(value?: ParseResult.VariableSpec, index?: number): ParseResult.VariableSpec;

  clearErrorsList(): void;
  getErrorsList(): Array<ParseResult.ParserError>;
  setErrorsList(value: Array<ParseResult.ParserError>): void;
  addErrors(value?: ParseResult.ParserError, index?: number): ParseResult.ParserError;

  hasOutputcontext(): boolean;
  clearOutputcontext(): void;
  getOutputcontext(): StrategyContext | undefined;
  setOutputcontext(value?: StrategyContext): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ParseResult.AsObject;
  static toObject(includeInstance: boolean, msg: ParseResult): ParseResult.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ParseResult, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ParseResult;
  static deserializeBinaryFromReader(message: ParseResult, reader: jspb.BinaryReader): ParseResult;
}

export namespace ParseResult {
  export type AsObject = {
    mainkeysList: Array<eqltree_pb.TSKey.AsObject>,
    mainkeysstringsList: Array<string>,
    variablesList: Array<ParseResult.VariableSpec.AsObject>,
    errorsList: Array<ParseResult.ParserError.AsObject>,
    outputcontext?: StrategyContext.AsObject,
  }

  export class VariableSpec extends jspb.Message {
    getName(): string;
    setName(value: string): void;

    clearKeysList(): void;
    getKeysList(): Array<eqltree_pb.TSKey>;
    setKeysList(value: Array<eqltree_pb.TSKey>): void;
    addKeys(value?: eqltree_pb.TSKey, index?: number): eqltree_pb.TSKey;

    hasSloc(): boolean;
    clearSloc(): void;
    getSloc(): eqltree_pb.SourceLocation | undefined;
    setSloc(value?: eqltree_pb.SourceLocation): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): VariableSpec.AsObject;
    static toObject(includeInstance: boolean, msg: VariableSpec): VariableSpec.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: VariableSpec, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): VariableSpec;
    static deserializeBinaryFromReader(message: VariableSpec, reader: jspb.BinaryReader): VariableSpec;
  }

  export namespace VariableSpec {
    export type AsObject = {
      name: string,
      keysList: Array<eqltree_pb.TSKey.AsObject>,
      sloc?: eqltree_pb.SourceLocation.AsObject,
    }
  }

  export class ParserError extends jspb.Message {
    getMessage(): string;
    setMessage(value: string): void;

    hasSloc(): boolean;
    clearSloc(): void;
    getSloc(): eqltree_pb.SourceLocation | undefined;
    setSloc(value?: eqltree_pb.SourceLocation): void;

    hasEndsloc(): boolean;
    clearEndsloc(): void;
    getEndsloc(): eqltree_pb.SourceLocation | undefined;
    setEndsloc(value?: eqltree_pb.SourceLocation): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ParserError.AsObject;
    static toObject(includeInstance: boolean, msg: ParserError): ParserError.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ParserError, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ParserError;
    static deserializeBinaryFromReader(message: ParserError, reader: jspb.BinaryReader): ParserError;
  }

  export namespace ParserError {
    export type AsObject = {
      message: string,
      sloc?: eqltree_pb.SourceLocation.AsObject,
      endsloc?: eqltree_pb.SourceLocation.AsObject,
    }
  }
}

export class Counter extends jspb.Message {
  getKey(): string;
  setKey(value: string): void;

  getCount(): number;
  setCount(value: number): void;

  getDuration(): number;
  setDuration(value: number): void;

  getTempreadsize(): number;
  setTempreadsize(value: number): void;

  getTempwritesize(): number;
  setTempwritesize(value: number): void;

  getConsumedsize(): number;
  setConsumedsize(value: number): void;

  getProducedsize(): number;
  setProducedsize(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Counter.AsObject;
  static toObject(includeInstance: boolean, msg: Counter): Counter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Counter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Counter;
  static deserializeBinaryFromReader(message: Counter, reader: jspb.BinaryReader): Counter;
}

export namespace Counter {
  export type AsObject = {
    key: string,
    count: number,
    duration: number,
    tempreadsize: number,
    tempwritesize: number,
    consumedsize: number,
    producedsize: number,
  }
}

export class ExtF extends jspb.Message {
  hasEnumreq(): boolean;
  clearEnumreq(): void;
  getEnumreq(): ExtF.EnumReq | undefined;
  setEnumreq(value?: ExtF.EnumReq): void;

  hasEnumresp(): boolean;
  clearEnumresp(): void;
  getEnumresp(): ExtF.EnumResp | undefined;
  setEnumresp(value?: ExtF.EnumResp): void;

  hasPrepreq(): boolean;
  clearPrepreq(): void;
  getPrepreq(): ExtF.PrepReq | undefined;
  setPrepreq(value?: ExtF.PrepReq): void;

  hasPrepresp(): boolean;
  clearPrepresp(): void;
  getPrepresp(): ExtF.PrepResp | undefined;
  setPrepresp(value?: ExtF.PrepResp): void;

  hasExecreq(): boolean;
  clearExecreq(): void;
  getExecreq(): ExtF.ExecReq | undefined;
  setExecreq(value?: ExtF.ExecReq): void;

  hasExecresp(): boolean;
  clearExecresp(): void;
  getExecresp(): ExtF.ExecResp | undefined;
  setExecresp(value?: ExtF.ExecResp): void;

  getKindCase(): ExtF.KindCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ExtF.AsObject;
  static toObject(includeInstance: boolean, msg: ExtF): ExtF.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ExtF, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ExtF;
  static deserializeBinaryFromReader(message: ExtF, reader: jspb.BinaryReader): ExtF;
}

export namespace ExtF {
  export type AsObject = {
    enumreq?: ExtF.EnumReq.AsObject,
    enumresp?: ExtF.EnumResp.AsObject,
    prepreq?: ExtF.PrepReq.AsObject,
    prepresp?: ExtF.PrepResp.AsObject,
    execreq?: ExtF.ExecReq.AsObject,
    execresp?: ExtF.ExecResp.AsObject,
  }

  export class EnumReq extends jspb.Message {
    getDummy(): number;
    setDummy(value: number): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): EnumReq.AsObject;
    static toObject(includeInstance: boolean, msg: EnumReq): EnumReq.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: EnumReq, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): EnumReq;
    static deserializeBinaryFromReader(message: EnumReq, reader: jspb.BinaryReader): EnumReq;
  }

  export namespace EnumReq {
    export type AsObject = {
      dummy: number,
    }
  }

  export class Act extends jspb.Message {
    getName(): string;
    setName(value: string): void;

    clearParametersList(): void;
    getParametersList(): Array<eqltree_pb.ParameterSpec>;
    setParametersList(value: Array<eqltree_pb.ParameterSpec>): void;
    addParameters(value?: eqltree_pb.ParameterSpec, index?: number): eqltree_pb.ParameterSpec;

    getActtype(): ExtF.ActTypeMap[keyof ExtF.ActTypeMap];
    setActtype(value: ExtF.ActTypeMap[keyof ExtF.ActTypeMap]): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Act.AsObject;
    static toObject(includeInstance: boolean, msg: Act): Act.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Act, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Act;
    static deserializeBinaryFromReader(message: Act, reader: jspb.BinaryReader): Act;
  }

  export namespace Act {
    export type AsObject = {
      name: string,
      parametersList: Array<eqltree_pb.ParameterSpec.AsObject>,
      acttype: ExtF.ActTypeMap[keyof ExtF.ActTypeMap],
    }
  }

  export class EnumResp extends jspb.Message {
    clearActList(): void;
    getActList(): Array<ExtF.Act>;
    setActList(value: Array<ExtF.Act>): void;
    addAct(value?: ExtF.Act, index?: number): ExtF.Act;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): EnumResp.AsObject;
    static toObject(includeInstance: boolean, msg: EnumResp): EnumResp.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: EnumResp, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): EnumResp;
    static deserializeBinaryFromReader(message: EnumResp, reader: jspb.BinaryReader): EnumResp;
  }

  export namespace EnumResp {
    export type AsObject = {
      actList: Array<ExtF.Act.AsObject>,
    }
  }

  export class ExecReq extends jspb.Message {
    getName(): string;
    setName(value: string): void;

    hasAct(): boolean;
    clearAct(): void;
    getAct(): eqltree_pb.TSKey | undefined;
    setAct(value?: eqltree_pb.TSKey): void;

    hasInput(): boolean;
    clearInput(): void;
    getInput(): ExecutorValue | undefined;
    setInput(value?: ExecutorValue): void;

    hasContext(): boolean;
    clearContext(): void;
    getContext(): StrategyContext | undefined;
    setContext(value?: StrategyContext): void;

    getExecutionid(): string;
    setExecutionid(value: string): void;

    clearArgumentsList(): void;
    getArgumentsList(): Array<ExecutorValue>;
    setArgumentsList(value: Array<ExecutorValue>): void;
    addArguments(value?: ExecutorValue, index?: number): ExecutorValue;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ExecReq.AsObject;
    static toObject(includeInstance: boolean, msg: ExecReq): ExecReq.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ExecReq, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ExecReq;
    static deserializeBinaryFromReader(message: ExecReq, reader: jspb.BinaryReader): ExecReq;
  }

  export namespace ExecReq {
    export type AsObject = {
      name: string,
      act?: eqltree_pb.TSKey.AsObject,
      input?: ExecutorValue.AsObject,
      context?: StrategyContext.AsObject,
      executionid: string,
      argumentsList: Array<ExecutorValue.AsObject>,
    }
  }

  export class ExecResp extends jspb.Message {
    hasValue(): boolean;
    clearValue(): void;
    getValue(): ExecutorValue | undefined;
    setValue(value?: ExecutorValue): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ExecResp.AsObject;
    static toObject(includeInstance: boolean, msg: ExecResp): ExecResp.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ExecResp, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ExecResp;
    static deserializeBinaryFromReader(message: ExecResp, reader: jspb.BinaryReader): ExecResp;
  }

  export namespace ExecResp {
    export type AsObject = {
      value?: ExecutorValue.AsObject,
    }
  }

  export class PrepReq extends jspb.Message {
    getName(): string;
    setName(value: string): void;

    hasAct(): boolean;
    clearAct(): void;
    getAct(): eqltree_pb.TSKey | undefined;
    setAct(value?: eqltree_pb.TSKey): void;

    hasInput(): boolean;
    clearInput(): void;
    getInput(): eqltree_pb.TSKey | undefined;
    setInput(value?: eqltree_pb.TSKey): void;

    hasContext(): boolean;
    clearContext(): void;
    getContext(): StrategyContext | undefined;
    setContext(value?: StrategyContext): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): PrepReq.AsObject;
    static toObject(includeInstance: boolean, msg: PrepReq): PrepReq.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: PrepReq, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): PrepReq;
    static deserializeBinaryFromReader(message: PrepReq, reader: jspb.BinaryReader): PrepReq;
  }

  export namespace PrepReq {
    export type AsObject = {
      name: string,
      act?: eqltree_pb.TSKey.AsObject,
      input?: eqltree_pb.TSKey.AsObject,
      context?: StrategyContext.AsObject,
    }
  }

  export class PrepResp extends jspb.Message {
    getAdvancebarswanted(): number;
    setAdvancebarswanted(value: number): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): PrepResp.AsObject;
    static toObject(includeInstance: boolean, msg: PrepResp): PrepResp.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: PrepResp, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): PrepResp;
    static deserializeBinaryFromReader(message: PrepResp, reader: jspb.BinaryReader): PrepResp;
  }

  export namespace PrepResp {
    export type AsObject = {
      advancebarswanted: number,
    }
  }

  export interface ActTypeMap {
    INVALID_ACTTYPE: 0;
    ACTTYPE_1TO1_NUMERICAL: 1;
    ACTTYPE_NTO1_NUMERICAL: 2;
    ACTTYPE_NOTSET: 3;
  }

  export const ActType: ActTypeMap;

  export enum KindCase {
    KIND_NOT_SET = 0,
    ENUMREQ = 100,
    ENUMRESP = 110,
    PREPREQ = 120,
    PREPRESP = 130,
    EXECREQ = 140,
    EXECRESP = 150,
  }
}

export interface NullFormatMap {
  INVALID_NULLFORMAT: 0;
  KDB_NULL: 9;
  LEGACY_NULL: 10;
  NO_NULLS: 20;
}

export const NullFormat: NullFormatMap;

export interface NumericDataNatureMap {
  INVALID_DATANATURE: 0;
  PRICES: 1;
  VOLUMES: 2;
  LOST: 3;
  OSCILLATOR: 4;
  SCORES: 5;
  NOT_APPLICABLE: 6;
  INDICES: 7;
  BOOLEANS: 8;
  OTHER_INDICATOR: 9;
  ML_SENTIMENTS: 10;
  ML_OTHER: 11;
  NAMES: 12;
  OTHER_STRINGS: 13;
  OTHER_NUMBERS: 14;
  PERIODS: 15;
}

export const NumericDataNature: NumericDataNatureMap;

export interface AggregationPolicyMap {
  INVALID_AGGREGATION_POLICY: 0;
  AP_LAST: 10;
  AP_FIRST: 11;
  AP_MAX: 12;
  AP_MIN: 13;
  AP_SUM: 14;
  AP_AVG: 15;
  AP_MEDIAN: 16;
  AP_COUNT: 17;
}

export const AggregationPolicy: AggregationPolicyMap;

export interface InterleavingMap {
  INTERLEAVING_NONE: 0;
  INTERLEAVING_OHLC: 10;
  INTERLEAVING_FROM_TO_COUNT: 20;
}

export const Interleaving: InterleavingMap;

export interface ExecutionModeMap {
  STANDARD_EXECUTION_MODE: 0;
  SIMPLIFIED_EXECUTION_MODE: 1;
}

export const ExecutionMode: ExecutionModeMap;

export interface LanguageLevelMap {
  LL_LOW_LEVEL: 0;
  LL_REVISION_SUMMER_2019: 1;
}

export const LanguageLevel: LanguageLevelMap;

export interface AveragingModeMap {
  AVERAGING_MODE: 0;
  NO_AVERAGING_MODE: 1;
}

export const AveragingMode: AveragingModeMap;

