// package: grpcfacade
// file: executor_grpc.proto

import * as executor_grpc_pb from "./executor_grpc_pb";
import * as eqltree_pb from "./eqltree_pb";
import {grpc} from "@improbable-eng/grpc-web";

type ExecutorServiceSubmitStrategy = {
  readonly methodName: string;
  readonly service: typeof ExecutorService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof executor_grpc_pb.EQLEngine.SubmitRequest;
  readonly responseType: typeof executor_grpc_pb.EQLEngine.SubmitResponse;
};

type ExecutorServiceSubmitStrategyN = {
  readonly methodName: string;
  readonly service: typeof ExecutorService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof executor_grpc_pb.EQLEngine.SubmitNRequest;
  readonly responseType: typeof executor_grpc_pb.EQLEngine.SubmitNResponse;
};

type ExecutorServiceSubmitUI = {
  readonly methodName: string;
  readonly service: typeof ExecutorService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof executor_grpc_pb.EQLEngine.SubmitUIRequest;
  readonly responseType: typeof executor_grpc_pb.EQLEngine.SubmitNResponse;
};

type ExecutorServiceRetrieveResult = {
  readonly methodName: string;
  readonly service: typeof ExecutorService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof executor_grpc_pb.EQLEngine.TaskRequest;
  readonly responseType: typeof executor_grpc_pb.EQLEngine.StatusResponse;
};

type ExecutorServiceRetrieveResultN = {
  readonly methodName: string;
  readonly service: typeof ExecutorService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof executor_grpc_pb.EQLEngine.TaskRequest;
  readonly responseType: typeof executor_grpc_pb.EQLEngine.StatusNResponse;
};

type ExecutorServiceCancelTask = {
  readonly methodName: string;
  readonly service: typeof ExecutorService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof executor_grpc_pb.EQLEngine.TaskRequest;
  readonly responseType: typeof executor_grpc_pb.EQLEngine.StatusResponse;
};

type ExecutorServiceGetLanguageInfo = {
  readonly methodName: string;
  readonly service: typeof ExecutorService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof executor_grpc_pb.EQLEngine.LanguageInfoRequest;
  readonly responseType: typeof eqltree_pb.LanguageInfo;
};

export class ExecutorService {
  static readonly serviceName: string;
  static readonly SubmitStrategy: ExecutorServiceSubmitStrategy;
  static readonly SubmitStrategyN: ExecutorServiceSubmitStrategyN;
  static readonly SubmitUI: ExecutorServiceSubmitUI;
  static readonly RetrieveResult: ExecutorServiceRetrieveResult;
  static readonly RetrieveResultN: ExecutorServiceRetrieveResultN;
  static readonly CancelTask: ExecutorServiceCancelTask;
  static readonly GetLanguageInfo: ExecutorServiceGetLanguageInfo;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class ExecutorServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  submitStrategy(
    requestMessage: executor_grpc_pb.EQLEngine.SubmitRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: executor_grpc_pb.EQLEngine.SubmitResponse|null) => void
  ): UnaryResponse;
  submitStrategy(
    requestMessage: executor_grpc_pb.EQLEngine.SubmitRequest,
    callback: (error: ServiceError|null, responseMessage: executor_grpc_pb.EQLEngine.SubmitResponse|null) => void
  ): UnaryResponse;
  submitStrategyN(
    requestMessage: executor_grpc_pb.EQLEngine.SubmitNRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: executor_grpc_pb.EQLEngine.SubmitNResponse|null) => void
  ): UnaryResponse;
  submitStrategyN(
    requestMessage: executor_grpc_pb.EQLEngine.SubmitNRequest,
    callback: (error: ServiceError|null, responseMessage: executor_grpc_pb.EQLEngine.SubmitNResponse|null) => void
  ): UnaryResponse;
  submitUI(
    requestMessage: executor_grpc_pb.EQLEngine.SubmitUIRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: executor_grpc_pb.EQLEngine.SubmitNResponse|null) => void
  ): UnaryResponse;
  submitUI(
    requestMessage: executor_grpc_pb.EQLEngine.SubmitUIRequest,
    callback: (error: ServiceError|null, responseMessage: executor_grpc_pb.EQLEngine.SubmitNResponse|null) => void
  ): UnaryResponse;
  retrieveResult(
    requestMessage: executor_grpc_pb.EQLEngine.TaskRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: executor_grpc_pb.EQLEngine.StatusResponse|null) => void
  ): UnaryResponse;
  retrieveResult(
    requestMessage: executor_grpc_pb.EQLEngine.TaskRequest,
    callback: (error: ServiceError|null, responseMessage: executor_grpc_pb.EQLEngine.StatusResponse|null) => void
  ): UnaryResponse;
  retrieveResultN(
    requestMessage: executor_grpc_pb.EQLEngine.TaskRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: executor_grpc_pb.EQLEngine.StatusNResponse|null) => void
  ): UnaryResponse;
  retrieveResultN(
    requestMessage: executor_grpc_pb.EQLEngine.TaskRequest,
    callback: (error: ServiceError|null, responseMessage: executor_grpc_pb.EQLEngine.StatusNResponse|null) => void
  ): UnaryResponse;
  cancelTask(
    requestMessage: executor_grpc_pb.EQLEngine.TaskRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: executor_grpc_pb.EQLEngine.StatusResponse|null) => void
  ): UnaryResponse;
  cancelTask(
    requestMessage: executor_grpc_pb.EQLEngine.TaskRequest,
    callback: (error: ServiceError|null, responseMessage: executor_grpc_pb.EQLEngine.StatusResponse|null) => void
  ): UnaryResponse;
  getLanguageInfo(
    requestMessage: executor_grpc_pb.EQLEngine.LanguageInfoRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: eqltree_pb.LanguageInfo|null) => void
  ): UnaryResponse;
  getLanguageInfo(
    requestMessage: executor_grpc_pb.EQLEngine.LanguageInfoRequest,
    callback: (error: ServiceError|null, responseMessage: eqltree_pb.LanguageInfo|null) => void
  ): UnaryResponse;
}

