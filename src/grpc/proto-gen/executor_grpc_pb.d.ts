// package: grpcfacade
// file: executor_grpc.proto

import * as jspb from "google-protobuf";
import * as eqltree_pb from "./eqltree_pb";
import * as execution_pb from "./execution_pb";

export class EQLEngine extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): EQLEngine.AsObject;
  static toObject(includeInstance: boolean, msg: EQLEngine): EQLEngine.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: EQLEngine, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): EQLEngine;
  static deserializeBinaryFromReader(message: EQLEngine, reader: jspb.BinaryReader): EQLEngine;
}

export namespace EQLEngine {
  export type AsObject = {
  }

  export class SecurityContext extends jspb.Message {
    getPermissionservice(): string;
    setPermissionservice(value: string): void;

    getEffectiveuserid(): string;
    setEffectiveuserid(value: string): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): SecurityContext.AsObject;
    static toObject(includeInstance: boolean, msg: SecurityContext): SecurityContext.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: SecurityContext, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): SecurityContext;
    static deserializeBinaryFromReader(message: SecurityContext, reader: jspb.BinaryReader): SecurityContext;
  }

  export namespace SecurityContext {
    export type AsObject = {
      permissionservice: string,
      effectiveuserid: string,
    }
  }

  export class ResultRepresentation extends jspb.Message {
    getDesiredfullresultsize(): number;
    setDesiredfullresultsize(value: number): void;

    getOffset(): number;
    setOffset(value: number): void;

    getLength(): number;
    setLength(value: number): void;

    getRangestart(): Uint8Array | string;
    getRangestart_asU8(): Uint8Array;
    getRangestart_asB64(): string;
    setRangestart(value: Uint8Array | string): void;

    getRangeend(): Uint8Array | string;
    getRangeend_asU8(): Uint8Array;
    getRangeend_asB64(): string;
    setRangeend(value: Uint8Array | string): void;

    getDesiredrangeresultsize(): number;
    setDesiredrangeresultsize(value: number): void;

    getBarsize(): EQLEngine.RepresentationBarSizeMap[keyof EQLEngine.RepresentationBarSizeMap];
    setBarsize(value: EQLEngine.RepresentationBarSizeMap[keyof EQLEngine.RepresentationBarSizeMap]): void;

    getSecliststarttranche(): number;
    setSecliststarttranche(value: number): void;

    getSeclistendtranche(): number;
    setSeclistendtranche(value: number): void;

    getSeclistrangestart(): number;
    setSeclistrangestart(value: number): void;

    getSeclistrangeend(): number;
    setSeclistrangeend(value: number): void;

    getKeepprotobufparts(): boolean;
    setKeepprotobufparts(value: boolean): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ResultRepresentation.AsObject;
    static toObject(includeInstance: boolean, msg: ResultRepresentation): ResultRepresentation.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ResultRepresentation, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ResultRepresentation;
    static deserializeBinaryFromReader(message: ResultRepresentation, reader: jspb.BinaryReader): ResultRepresentation;
  }

  export namespace ResultRepresentation {
    export type AsObject = {
      desiredfullresultsize: number,
      offset: number,
      length: number,
      rangestart: Uint8Array | string,
      rangeend: Uint8Array | string,
      desiredrangeresultsize: number,
      barsize: EQLEngine.RepresentationBarSizeMap[keyof EQLEngine.RepresentationBarSizeMap],
      secliststarttranche: number,
      seclistendtranche: number,
      seclistrangestart: number,
      seclistrangeend: number,
      keepprotobufparts: boolean,
    }
  }

  export class SubmitRequest extends jspb.Message {
    getEql(): string;
    setEql(value: string): void;

    getTarget(): string;
    setTarget(value: string): void;

    hasResultrepresentation(): boolean;
    clearResultrepresentation(): void;
    getResultrepresentation(): EQLEngine.ResultRepresentation | undefined;
    setResultrepresentation(value?: EQLEngine.ResultRepresentation): void;

    hasContext(): boolean;
    clearContext(): void;
    getContext(): execution_pb.StrategyContext | undefined;
    setContext(value?: execution_pb.StrategyContext): void;

    hasSecuritycontext(): boolean;
    clearSecuritycontext(): void;
    getSecuritycontext(): EQLEngine.SecurityContext | undefined;
    setSecuritycontext(value?: EQLEngine.SecurityContext): void;

    getParseonly(): boolean;
    setParseonly(value: boolean): void;

    getLinenumbers(): string;
    setLinenumbers(value: string): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): SubmitRequest.AsObject;
    static toObject(includeInstance: boolean, msg: SubmitRequest): SubmitRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: SubmitRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): SubmitRequest;
    static deserializeBinaryFromReader(message: SubmitRequest, reader: jspb.BinaryReader): SubmitRequest;
  }

  export namespace SubmitRequest {
    export type AsObject = {
      eql: string,
      target: string,
      resultrepresentation?: EQLEngine.ResultRepresentation.AsObject,
      context?: execution_pb.StrategyContext.AsObject,
      securitycontext?: EQLEngine.SecurityContext.AsObject,
      parseonly: boolean,
      linenumbers: string,
    }
  }

  export class UIPartOption extends jspb.Message {
    getPart(): EQLEngine.UIPartMap[keyof EQLEngine.UIPartMap];
    setPart(value: EQLEngine.UIPartMap[keyof EQLEngine.UIPartMap]): void;

    getOptions(): string;
    setOptions(value: string): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): UIPartOption.AsObject;
    static toObject(includeInstance: boolean, msg: UIPartOption): UIPartOption.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: UIPartOption, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): UIPartOption;
    static deserializeBinaryFromReader(message: UIPartOption, reader: jspb.BinaryReader): UIPartOption;
  }

  export namespace UIPartOption {
    export type AsObject = {
      part: EQLEngine.UIPartMap[keyof EQLEngine.UIPartMap],
      options: string,
    }
  }

  export class SubmitUIRequest extends jspb.Message {
    hasEqlrequest(): boolean;
    clearEqlrequest(): void;
    getEqlrequest(): EQLEngine.SubmitRequest | undefined;
    setEqlrequest(value?: EQLEngine.SubmitRequest): void;

    clearPartsList(): void;
    getPartsList(): Array<EQLEngine.UIPartOption>;
    setPartsList(value: Array<EQLEngine.UIPartOption>): void;
    addParts(value?: EQLEngine.UIPartOption, index?: number): EQLEngine.UIPartOption;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): SubmitUIRequest.AsObject;
    static toObject(includeInstance: boolean, msg: SubmitUIRequest): SubmitUIRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: SubmitUIRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): SubmitUIRequest;
    static deserializeBinaryFromReader(message: SubmitUIRequest, reader: jspb.BinaryReader): SubmitUIRequest;
  }

  export namespace SubmitUIRequest {
    export type AsObject = {
      eqlrequest?: EQLEngine.SubmitRequest.AsObject,
      partsList: Array<EQLEngine.UIPartOption.AsObject>,
    }
  }

  export class SubmitNRequest extends jspb.Message {
    hasContext(): boolean;
    clearContext(): void;
    getContext(): execution_pb.StrategyContext | undefined;
    setContext(value?: execution_pb.StrategyContext): void;

    hasSecuritycontext(): boolean;
    clearSecuritycontext(): void;
    getSecuritycontext(): EQLEngine.SecurityContext | undefined;
    setSecuritycontext(value?: EQLEngine.SecurityContext): void;

    clearPartsList(): void;
    getPartsList(): Array<EQLEngine.SubmitNRequest.Part>;
    setPartsList(value: Array<EQLEngine.SubmitNRequest.Part>): void;
    addParts(value?: EQLEngine.SubmitNRequest.Part, index?: number): EQLEngine.SubmitNRequest.Part;

    getParseonly(): boolean;
    setParseonly(value: boolean): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): SubmitNRequest.AsObject;
    static toObject(includeInstance: boolean, msg: SubmitNRequest): SubmitNRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: SubmitNRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): SubmitNRequest;
    static deserializeBinaryFromReader(message: SubmitNRequest, reader: jspb.BinaryReader): SubmitNRequest;
  }

  export namespace SubmitNRequest {
    export type AsObject = {
      context?: execution_pb.StrategyContext.AsObject,
      securitycontext?: EQLEngine.SecurityContext.AsObject,
      partsList: Array<EQLEngine.SubmitNRequest.Part.AsObject>,
      parseonly: boolean,
    }

    export class Part extends jspb.Message {
      getEql(): string;
      setEql(value: string): void;

      hasResultrepresentation(): boolean;
      clearResultrepresentation(): void;
      getResultrepresentation(): EQLEngine.ResultRepresentation | undefined;
      setResultrepresentation(value?: EQLEngine.ResultRepresentation): void;

      serializeBinary(): Uint8Array;
      toObject(includeInstance?: boolean): Part.AsObject;
      static toObject(includeInstance: boolean, msg: Part): Part.AsObject;
      static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
      static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
      static serializeBinaryToWriter(message: Part, writer: jspb.BinaryWriter): void;
      static deserializeBinary(bytes: Uint8Array): Part;
      static deserializeBinaryFromReader(message: Part, reader: jspb.BinaryReader): Part;
    }

    export namespace Part {
      export type AsObject = {
        eql: string,
        resultrepresentation?: EQLEngine.ResultRepresentation.AsObject,
      }
    }
  }

  export class ParseError extends jspb.Message {
    getError(): string;
    setError(value: string): void;

    hasSloc(): boolean;
    clearSloc(): void;
    getSloc(): eqltree_pb.SourceLocation | undefined;
    setSloc(value?: eqltree_pb.SourceLocation): void;

    getEqlindex(): number;
    setEqlindex(value: number): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ParseError.AsObject;
    static toObject(includeInstance: boolean, msg: ParseError): ParseError.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ParseError, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ParseError;
    static deserializeBinaryFromReader(message: ParseError, reader: jspb.BinaryReader): ParseError;
  }

  export namespace ParseError {
    export type AsObject = {
      error: string,
      sloc?: eqltree_pb.SourceLocation.AsObject,
      eqlindex: number,
    }
  }

  export class SubmitResponse extends jspb.Message {
    hasParseerror(): boolean;
    clearParseerror(): void;
    getParseerror(): EQLEngine.ParseError | undefined;
    setParseerror(value?: EQLEngine.ParseError): void;

    hasTaskhandle(): boolean;
    clearTaskhandle(): void;
    getTaskhandle(): string;
    setTaskhandle(value: string): void;

    hasPreparedresult(): boolean;
    clearPreparedresult(): void;
    getPreparedresult(): execution_pb.ExecutorValue | undefined;
    setPreparedresult(value?: execution_pb.ExecutorValue): void;

    getKindCase(): SubmitResponse.KindCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): SubmitResponse.AsObject;
    static toObject(includeInstance: boolean, msg: SubmitResponse): SubmitResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: SubmitResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): SubmitResponse;
    static deserializeBinaryFromReader(message: SubmitResponse, reader: jspb.BinaryReader): SubmitResponse;
  }

  export namespace SubmitResponse {
    export type AsObject = {
      parseerror?: EQLEngine.ParseError.AsObject,
      taskhandle: string,
      preparedresult?: execution_pb.ExecutorValue.AsObject,
    }

    export enum KindCase {
      KIND_NOT_SET = 0,
      PARSEERROR = 10,
      TASKHANDLE = 20,
      PREPAREDRESULT = 31,
    }
  }

  export class SubmitNResponse extends jspb.Message {
    hasParseerror(): boolean;
    clearParseerror(): void;
    getParseerror(): EQLEngine.ParseError | undefined;
    setParseerror(value?: EQLEngine.ParseError): void;

    hasTaskhandle(): boolean;
    clearTaskhandle(): void;
    getTaskhandle(): string;
    setTaskhandle(value: string): void;

    clearParseresultsList(): void;
    getParseresultsList(): Array<execution_pb.ParseResult>;
    setParseresultsList(value: Array<execution_pb.ParseResult>): void;
    addParseresults(value?: execution_pb.ParseResult, index?: number): execution_pb.ParseResult;

    getKindCase(): SubmitNResponse.KindCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): SubmitNResponse.AsObject;
    static toObject(includeInstance: boolean, msg: SubmitNResponse): SubmitNResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: SubmitNResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): SubmitNResponse;
    static deserializeBinaryFromReader(message: SubmitNResponse, reader: jspb.BinaryReader): SubmitNResponse;
  }

  export namespace SubmitNResponse {
    export type AsObject = {
      parseerror?: EQLEngine.ParseError.AsObject,
      taskhandle: string,
      parseresultsList: Array<execution_pb.ParseResult.AsObject>,
    }

    export enum KindCase {
      KIND_NOT_SET = 0,
      PARSEERROR = 10,
      TASKHANDLE = 20,
    }
  }

  export class TaskRequest extends jspb.Message {
    getTaskhandle(): string;
    setTaskhandle(value: string): void;

    getReturnbytes(): boolean;
    setReturnbytes(value: boolean): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): TaskRequest.AsObject;
    static toObject(includeInstance: boolean, msg: TaskRequest): TaskRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: TaskRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): TaskRequest;
    static deserializeBinaryFromReader(message: TaskRequest, reader: jspb.BinaryReader): TaskRequest;
  }

  export namespace TaskRequest {
    export type AsObject = {
      taskhandle: string,
      returnbytes: boolean,
    }
  }

  export class CompletionResult extends jspb.Message {
    getCompleted(): boolean;
    setCompleted(value: boolean): void;

    getDummy(): boolean;
    setDummy(value: boolean): void;

    hasResult(): boolean;
    clearResult(): void;
    getResult(): execution_pb.ExecutorValue | undefined;
    setResult(value?: execution_pb.ExecutorValue): void;

    getResultbytes(): Uint8Array | string;
    getResultbytes_asU8(): Uint8Array;
    getResultbytes_asB64(): string;
    setResultbytes(value: Uint8Array | string): void;

    getParsetime(): number;
    setParsetime(value: number): void;

    getExectime(): number;
    setExectime(value: number): void;

    getPostprocesstime(): number;
    setPostprocesstime(value: number): void;

    getResultbucket(): EQLEngine.UIPartMap[keyof EQLEngine.UIPartMap];
    setResultbucket(value: EQLEngine.UIPartMap[keyof EQLEngine.UIPartMap]): void;

    getBarsize(): EQLEngine.RepresentationBarSizeMap[keyof EQLEngine.RepresentationBarSizeMap];
    setBarsize(value: EQLEngine.RepresentationBarSizeMap[keyof EQLEngine.RepresentationBarSizeMap]): void;

    hasOutputcontext(): boolean;
    clearOutputcontext(): void;
    getOutputcontext(): execution_pb.StrategyContext | undefined;
    setOutputcontext(value?: execution_pb.StrategyContext): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): CompletionResult.AsObject;
    static toObject(includeInstance: boolean, msg: CompletionResult): CompletionResult.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: CompletionResult, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): CompletionResult;
    static deserializeBinaryFromReader(message: CompletionResult, reader: jspb.BinaryReader): CompletionResult;
  }

  export namespace CompletionResult {
    export type AsObject = {
      completed: boolean,
      dummy: boolean,
      result?: execution_pb.ExecutorValue.AsObject,
      resultbytes: Uint8Array | string,
      parsetime: number,
      exectime: number,
      postprocesstime: number,
      resultbucket: EQLEngine.UIPartMap[keyof EQLEngine.UIPartMap],
      barsize: EQLEngine.RepresentationBarSizeMap[keyof EQLEngine.RepresentationBarSizeMap],
      outputcontext?: execution_pb.StrategyContext.AsObject,
    }
  }

  export class StatusResponse extends jspb.Message {
    hasCompletionresult(): boolean;
    clearCompletionresult(): void;
    getCompletionresult(): EQLEngine.CompletionResult | undefined;
    setCompletionresult(value?: EQLEngine.CompletionResult): void;

    clearCountersList(): void;
    getCountersList(): Array<execution_pb.Counter>;
    setCountersList(value: Array<execution_pb.Counter>): void;
    addCounters(value?: execution_pb.Counter, index?: number): execution_pb.Counter;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): StatusResponse.AsObject;
    static toObject(includeInstance: boolean, msg: StatusResponse): StatusResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: StatusResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): StatusResponse;
    static deserializeBinaryFromReader(message: StatusResponse, reader: jspb.BinaryReader): StatusResponse;
  }

  export namespace StatusResponse {
    export type AsObject = {
      completionresult?: EQLEngine.CompletionResult.AsObject,
      countersList: Array<execution_pb.Counter.AsObject>,
    }
  }

  export class StatusNResponse extends jspb.Message {
    clearCompletionresultList(): void;
    getCompletionresultList(): Array<EQLEngine.CompletionResult>;
    setCompletionresultList(value: Array<EQLEngine.CompletionResult>): void;
    addCompletionresult(value?: EQLEngine.CompletionResult, index?: number): EQLEngine.CompletionResult;

    clearCountersList(): void;
    getCountersList(): Array<execution_pb.Counter>;
    setCountersList(value: Array<execution_pb.Counter>): void;
    addCounters(value?: execution_pb.Counter, index?: number): execution_pb.Counter;

    getProgresstexttechnical(): string;
    setProgresstexttechnical(value: string): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): StatusNResponse.AsObject;
    static toObject(includeInstance: boolean, msg: StatusNResponse): StatusNResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: StatusNResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): StatusNResponse;
    static deserializeBinaryFromReader(message: StatusNResponse, reader: jspb.BinaryReader): StatusNResponse;
  }

  export namespace StatusNResponse {
    export type AsObject = {
      completionresultList: Array<EQLEngine.CompletionResult.AsObject>,
      countersList: Array<execution_pb.Counter.AsObject>,
      progresstexttechnical: string,
    }
  }

  export class LanguageInfoRequest extends jspb.Message {
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): LanguageInfoRequest.AsObject;
    static toObject(includeInstance: boolean, msg: LanguageInfoRequest): LanguageInfoRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: LanguageInfoRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): LanguageInfoRequest;
    static deserializeBinaryFromReader(message: LanguageInfoRequest, reader: jspb.BinaryReader): LanguageInfoRequest;
  }

  export namespace LanguageInfoRequest {
    export type AsObject = {
    }
  }

  export interface RepresentationBarSizeMap {
    RBS_NOT_SPECIFIED: 0;
    RBS_1MIN: 5;
    RBS_3MIN: 10;
    RBS_15MIN: 20;
    RBS_1DAY: 40;
    RBS_1WEEK: 50;
    RBS_1MONTH: 60;
  }

  export const RepresentationBarSize: RepresentationBarSizeMap;

  export interface UIPartMap {
    UIPART_INVALID: 0;
    UIPART_MAIN_TIMESERIES: 10;
    UIPART_PROFITLOSS: 20;
    UIPART_VOLUME: 30;
    UIPART_SHARPE: 40;
    UIPART_RETURNS: 50;
    UIPART_BACKTEST: 60;
    UIPART_TARGET: 70;
    UIPART_SECLIST_AVERAGES: 80;
  }

  export const UIPart: UIPartMap;
}

