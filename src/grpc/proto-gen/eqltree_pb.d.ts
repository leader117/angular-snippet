// package: equeum
// file: eqltree.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_descriptor_pb from "google-protobuf/google/protobuf/descriptor_pb";

export class SerializationHints extends jspb.Message {
  getIsexcluded(): boolean;
  setIsexcluded(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SerializationHints.AsObject;
  static toObject(includeInstance: boolean, msg: SerializationHints): SerializationHints.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SerializationHints, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SerializationHints;
  static deserializeBinaryFromReader(message: SerializationHints, reader: jspb.BinaryReader): SerializationHints;
}

export namespace SerializationHints {
  export type AsObject = {
    isexcluded: boolean,
  }
}

export class Timestamp extends jspb.Message {
  getSeconds(): number;
  setSeconds(value: number): void;

  getNanos(): number;
  setNanos(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Timestamp.AsObject;
  static toObject(includeInstance: boolean, msg: Timestamp): Timestamp.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Timestamp, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Timestamp;
  static deserializeBinaryFromReader(message: Timestamp, reader: jspb.BinaryReader): Timestamp;
}

export namespace Timestamp {
  export type AsObject = {
    seconds: number,
    nanos: number,
  }
}

export class SourceLocation extends jspb.Message {
  getLine(): number;
  setLine(value: number): void;

  getPos(): number;
  setPos(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SourceLocation.AsObject;
  static toObject(includeInstance: boolean, msg: SourceLocation): SourceLocation.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SourceLocation, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SourceLocation;
  static deserializeBinaryFromReader(message: SourceLocation, reader: jspb.BinaryReader): SourceLocation;
}

export namespace SourceLocation {
  export type AsObject = {
    line: number,
    pos: number,
  }
}

export class LocationAndError extends jspb.Message {
  hasSloc(): boolean;
  clearSloc(): void;
  getSloc(): SourceLocation | undefined;
  setSloc(value?: SourceLocation): void;

  getError(): string;
  setError(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LocationAndError.AsObject;
  static toObject(includeInstance: boolean, msg: LocationAndError): LocationAndError.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: LocationAndError, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LocationAndError;
  static deserializeBinaryFromReader(message: LocationAndError, reader: jspb.BinaryReader): LocationAndError;
}

export namespace LocationAndError {
  export type AsObject = {
    sloc?: SourceLocation.AsObject,
    error: string,
  }
}

export class NameReference extends jspb.Message {
  getName(): string;
  setName(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): NameReference.AsObject;
  static toObject(includeInstance: boolean, msg: NameReference): NameReference.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: NameReference, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): NameReference;
  static deserializeBinaryFromReader(message: NameReference, reader: jspb.BinaryReader): NameReference;
}

export namespace NameReference {
  export type AsObject = {
    name: string,
  }
}

export class RowRangeReference extends jspb.Message {
  getStartrow(): number;
  setStartrow(value: number): void;

  getEndrow(): number;
  setEndrow(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RowRangeReference.AsObject;
  static toObject(includeInstance: boolean, msg: RowRangeReference): RowRangeReference.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RowRangeReference, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RowRangeReference;
  static deserializeBinaryFromReader(message: RowRangeReference, reader: jspb.BinaryReader): RowRangeReference;
}

export namespace RowRangeReference {
  export type AsObject = {
    startrow: number,
    endrow: number,
  }
}

export class RowsReference extends jspb.Message {
  clearRowsList(): void;
  getRowsList(): Array<RowRangeReference>;
  setRowsList(value: Array<RowRangeReference>): void;
  addRows(value?: RowRangeReference, index?: number): RowRangeReference;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RowsReference.AsObject;
  static toObject(includeInstance: boolean, msg: RowsReference): RowsReference.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RowsReference, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RowsReference;
  static deserializeBinaryFromReader(message: RowsReference, reader: jspb.BinaryReader): RowsReference;
}

export namespace RowsReference {
  export type AsObject = {
    rowsList: Array<RowRangeReference.AsObject>,
  }
}

export class StrongType extends jspb.Message {
  hasBasetype(): boolean;
  clearBasetype(): void;
  getBasetype(): PrimaryTypeMap[keyof PrimaryTypeMap];
  setBasetype(value: PrimaryTypeMap[keyof PrimaryTypeMap]): void;

  hasArraytype(): boolean;
  clearArraytype(): void;
  getArraytype(): StrongType.StrongArrayType | undefined;
  setArraytype(value?: StrongType.StrongArrayType): void;

  hasVariablearraytype(): boolean;
  clearVariablearraytype(): void;
  getVariablearraytype(): StrongType.VariableArrayType | undefined;
  setVariablearraytype(value?: StrongType.VariableArrayType): void;

  getSeclistwidth(): number;
  setSeclistwidth(value: number): void;

  hasSymbolicbacking(): boolean;
  clearSymbolicbacking(): void;
  getSymbolicbacking(): StrongType | undefined;
  setSymbolicbacking(value?: StrongType): void;

  getColname(): string;
  setColname(value: string): void;

  getNonlinear(): boolean;
  setNonlinear(value: boolean): void;

  hasActionmetadata(): boolean;
  clearActionmetadata(): void;
  getActionmetadata(): ActionMetadata | undefined;
  setActionmetadata(value?: ActionMetadata): void;

  getSealed(): boolean;
  setSealed(value: boolean): void;

  getKindCase(): StrongType.KindCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StrongType.AsObject;
  static toObject(includeInstance: boolean, msg: StrongType): StrongType.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StrongType, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StrongType;
  static deserializeBinaryFromReader(message: StrongType, reader: jspb.BinaryReader): StrongType;
}

export namespace StrongType {
  export type AsObject = {
    basetype: PrimaryTypeMap[keyof PrimaryTypeMap],
    arraytype?: StrongType.StrongArrayType.AsObject,
    variablearraytype?: StrongType.VariableArrayType.AsObject,
    seclistwidth: number,
    symbolicbacking?: StrongType.AsObject,
    colname: string,
    nonlinear: boolean,
    actionmetadata?: ActionMetadata.AsObject,
    sealed: boolean,
  }

  export class StrongArrayType extends jspb.Message {
    hasElementtype(): boolean;
    clearElementtype(): void;
    getElementtype(): StrongType | undefined;
    setElementtype(value?: StrongType): void;

    getDimensionlength(): number;
    setDimensionlength(value: number): void;

    getChunkscount(): number;
    setChunkscount(value: number): void;

    clearColnamesList(): void;
    getColnamesList(): Array<string>;
    setColnamesList(value: Array<string>): void;
    addColnames(value: string, index?: number): string;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): StrongArrayType.AsObject;
    static toObject(includeInstance: boolean, msg: StrongArrayType): StrongArrayType.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: StrongArrayType, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): StrongArrayType;
    static deserializeBinaryFromReader(message: StrongArrayType, reader: jspb.BinaryReader): StrongArrayType;
  }

  export namespace StrongArrayType {
    export type AsObject = {
      elementtype?: StrongType.AsObject,
      dimensionlength: number,
      chunkscount: number,
      colnamesList: Array<string>,
    }
  }

  export class VariableArrayType extends jspb.Message {
    clearElementtypeList(): void;
    getElementtypeList(): Array<StrongType>;
    setElementtypeList(value: Array<StrongType>): void;
    addElementtype(value?: StrongType, index?: number): StrongType;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): VariableArrayType.AsObject;
    static toObject(includeInstance: boolean, msg: VariableArrayType): VariableArrayType.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: VariableArrayType, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): VariableArrayType;
    static deserializeBinaryFromReader(message: VariableArrayType, reader: jspb.BinaryReader): VariableArrayType;
  }

  export namespace VariableArrayType {
    export type AsObject = {
      elementtypeList: Array<StrongType.AsObject>,
    }
  }

  export enum KindCase {
    KIND_NOT_SET = 0,
    BASETYPE = 10,
    ARRAYTYPE = 20,
    VARIABLEARRAYTYPE = 25,
  }
}

export class ParameterSpec extends jspb.Message {
  getName(): string;
  setName(value: string): void;

  hasValue(): boolean;
  clearValue(): void;
  getValue(): Expression | undefined;
  setValue(value?: Expression): void;

  hasSloc(): boolean;
  clearSloc(): void;
  getSloc(): SourceLocation | undefined;
  setSloc(value?: SourceLocation): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ParameterSpec.AsObject;
  static toObject(includeInstance: boolean, msg: ParameterSpec): ParameterSpec.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ParameterSpec, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ParameterSpec;
  static deserializeBinaryFromReader(message: ParameterSpec, reader: jspb.BinaryReader): ParameterSpec;
}

export namespace ParameterSpec {
  export type AsObject = {
    name: string,
    value?: Expression.AsObject,
    sloc?: SourceLocation.AsObject,
  }
}

export class Expression extends jspb.Message {
  hasLiteral(): boolean;
  clearLiteral(): void;
  getLiteral(): Expression.Literal | undefined;
  setLiteral(value?: Expression.Literal): void;

  hasBinaryop(): boolean;
  clearBinaryop(): void;
  getBinaryop(): Expression.BinaryOp | undefined;
  setBinaryop(value?: Expression.BinaryOp): void;

  hasSloc(): boolean;
  clearSloc(): void;
  getSloc(): SourceLocation | undefined;
  setSloc(value?: SourceLocation): void;

  getKindCase(): Expression.KindCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Expression.AsObject;
  static toObject(includeInstance: boolean, msg: Expression): Expression.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Expression, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Expression;
  static deserializeBinaryFromReader(message: Expression, reader: jspb.BinaryReader): Expression;
}

export namespace Expression {
  export type AsObject = {
    literal?: Expression.Literal.AsObject,
    binaryop?: Expression.BinaryOp.AsObject,
    sloc?: SourceLocation.AsObject,
  }

  export class ExprList extends jspb.Message {
    clearItemList(): void;
    getItemList(): Array<Expression>;
    setItemList(value: Array<Expression>): void;
    addItem(value?: Expression, index?: number): Expression;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ExprList.AsObject;
    static toObject(includeInstance: boolean, msg: ExprList): ExprList.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ExprList, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ExprList;
    static deserializeBinaryFromReader(message: ExprList, reader: jspb.BinaryReader): ExprList;
  }

  export namespace ExprList {
    export type AsObject = {
      itemList: Array<Expression.AsObject>,
    }
  }

  export class Literal extends jspb.Message {
    hasStringvalue(): boolean;
    clearStringvalue(): void;
    getStringvalue(): string;
    setStringvalue(value: string): void;

    hasDoublevalue(): boolean;
    clearDoublevalue(): void;
    getDoublevalue(): number;
    setDoublevalue(value: number): void;

    hasIntvalue(): boolean;
    clearIntvalue(): void;
    getIntvalue(): number;
    setIntvalue(value: number): void;

    hasIdvalue(): boolean;
    clearIdvalue(): void;
    getIdvalue(): string;
    setIdvalue(value: string): void;

    hasVarreference(): boolean;
    clearVarreference(): void;
    getVarreference(): string;
    setVarreference(value: string): void;

    hasTimespan(): boolean;
    clearTimespan(): void;
    getTimespan(): string;
    setTimespan(value: string): void;

    hasBuiltin(): boolean;
    clearBuiltin(): void;
    getBuiltin(): BuiltinMap[keyof BuiltinMap];
    setBuiltin(value: BuiltinMap[keyof BuiltinMap]): void;

    hasKeyoraction(): boolean;
    clearKeyoraction(): void;
    getKeyoraction(): TSKey | undefined;
    setKeyoraction(value?: TSKey): void;

    hasKeys(): boolean;
    clearKeys(): void;
    getKeys(): MultiTSKey | undefined;
    setKeys(value?: MultiTSKey): void;

    hasList(): boolean;
    clearList(): void;
    getList(): Expression.ExprList | undefined;
    setList(value?: Expression.ExprList): void;

    hasRange3(): boolean;
    clearRange3(): void;
    getRange3(): Expression.ExprList | undefined;
    setRange3(value?: Expression.ExprList): void;

    hasExecutorvalue(): boolean;
    clearExecutorvalue(): void;
    getExecutorvalue(): Uint8Array | string;
    getExecutorvalue_asU8(): Uint8Array;
    getExecutorvalue_asB64(): string;
    setExecutorvalue(value: Uint8Array | string): void;

    getPercentvalue(): boolean;
    setPercentvalue(value: boolean): void;

    getKindCase(): Literal.KindCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Literal.AsObject;
    static toObject(includeInstance: boolean, msg: Literal): Literal.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Literal, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Literal;
    static deserializeBinaryFromReader(message: Literal, reader: jspb.BinaryReader): Literal;
  }

  export namespace Literal {
    export type AsObject = {
      stringvalue: string,
      doublevalue: number,
      intvalue: number,
      idvalue: string,
      varreference: string,
      timespan: string,
      builtin: BuiltinMap[keyof BuiltinMap],
      keyoraction?: TSKey.AsObject,
      keys?: MultiTSKey.AsObject,
      list?: Expression.ExprList.AsObject,
      range3?: Expression.ExprList.AsObject,
      executorvalue: Uint8Array | string,
      percentvalue: boolean,
    }

    export enum KindCase {
      KIND_NOT_SET = 0,
      STRINGVALUE = 10,
      DOUBLEVALUE = 20,
      INTVALUE = 21,
      IDVALUE = 40,
      VARREFERENCE = 41,
      TIMESPAN = 42,
      BUILTIN = 50,
      KEYORACTION = 60,
      KEYS = 63,
      LIST = 64,
      RANGE3 = 43,
      EXECUTORVALUE = 66,
    }
  }

  export class BinaryOp extends jspb.Message {
    getOp(): Expression.OpTypeMap[keyof Expression.OpTypeMap];
    setOp(value: Expression.OpTypeMap[keyof Expression.OpTypeMap]): void;

    hasLeft(): boolean;
    clearLeft(): void;
    getLeft(): Expression | undefined;
    setLeft(value?: Expression): void;

    hasRight(): boolean;
    clearRight(): void;
    getRight(): Expression | undefined;
    setRight(value?: Expression): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): BinaryOp.AsObject;
    static toObject(includeInstance: boolean, msg: BinaryOp): BinaryOp.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: BinaryOp, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): BinaryOp;
    static deserializeBinaryFromReader(message: BinaryOp, reader: jspb.BinaryReader): BinaryOp;
  }

  export namespace BinaryOp {
    export type AsObject = {
      op: Expression.OpTypeMap[keyof Expression.OpTypeMap],
      left?: Expression.AsObject,
      right?: Expression.AsObject,
    }
  }

  export interface OpTypeMap {
    INVALID_OPTYPE: 0;
    MINUS: 1;
    MULTIPLY: 2;
    DIVIDE: 3;
    PLUS: 4;
  }

  export const OpType: OpTypeMap;

  export enum KindCase {
    KIND_NOT_SET = 0,
    LITERAL = 10,
    BINARYOP = 20,
  }
}

export class DomainSpec extends jspb.Message {
  hasPlainreference(): boolean;
  clearPlainreference(): void;
  getPlainreference(): string;
  setPlainreference(value: string): void;

  hasVarreference(): boolean;
  clearVarreference(): void;
  getVarreference(): NameReference | undefined;
  setVarreference(value?: NameReference): void;

  hasSeclistreference(): boolean;
  clearSeclistreference(): void;
  getSeclistreference(): NameReference | undefined;
  setSeclistreference(value?: NameReference): void;

  hasStrategyreference(): boolean;
  clearStrategyreference(): void;
  getStrategyreference(): NameReference | undefined;
  setStrategyreference(value?: NameReference): void;

  hasSymbolic(): boolean;
  clearSymbolic(): void;
  getSymbolic(): number;
  setSymbolic(value: number): void;

  hasRowsreference(): boolean;
  clearRowsreference(): void;
  getRowsreference(): RowsReference | undefined;
  setRowsreference(value?: RowsReference): void;

  hasTsref(): boolean;
  clearTsref(): void;
  getTsref(): Expression | undefined;
  setTsref(value?: Expression): void;

  hasBuiltin(): boolean;
  clearBuiltin(): void;
  getBuiltin(): BuiltinMap[keyof BuiltinMap];
  setBuiltin(value: BuiltinMap[keyof BuiltinMap]): void;

  clearParamsList(): void;
  getParamsList(): Array<ParameterSpec>;
  setParamsList(value: Array<ParameterSpec>): void;
  addParams(value?: ParameterSpec, index?: number): ParameterSpec;

  getDataformat(): DataFormatMap[keyof DataFormatMap];
  setDataformat(value: DataFormatMap[keyof DataFormatMap]): void;

  hasSloc(): boolean;
  clearSloc(): void;
  getSloc(): SourceLocation | undefined;
  setSloc(value?: SourceLocation): void;

  getKindCase(): DomainSpec.KindCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DomainSpec.AsObject;
  static toObject(includeInstance: boolean, msg: DomainSpec): DomainSpec.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DomainSpec, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DomainSpec;
  static deserializeBinaryFromReader(message: DomainSpec, reader: jspb.BinaryReader): DomainSpec;
}

export namespace DomainSpec {
  export type AsObject = {
    plainreference: string,
    varreference?: NameReference.AsObject,
    seclistreference?: NameReference.AsObject,
    strategyreference?: NameReference.AsObject,
    symbolic: number,
    rowsreference?: RowsReference.AsObject,
    tsref?: Expression.AsObject,
    builtin: BuiltinMap[keyof BuiltinMap],
    paramsList: Array<ParameterSpec.AsObject>,
    dataformat: DataFormatMap[keyof DataFormatMap],
    sloc?: SourceLocation.AsObject,
  }

  export enum KindCase {
    KIND_NOT_SET = 0,
    PLAINREFERENCE = 10,
    VARREFERENCE = 20,
    SECLISTREFERENCE = 22,
    STRATEGYREFERENCE = 25,
    SYMBOLIC = 26,
    ROWSREFERENCE = 30,
    TSREF = 28,
    BUILTIN = 39,
  }
}

export class ActionMetadata extends jspb.Message {
  getBarsize(): ActionMetadata.BarSizeMap[keyof ActionMetadata.BarSizeMap];
  setBarsize(value: ActionMetadata.BarSizeMap[keyof ActionMetadata.BarSizeMap]): void;

  getRegion(): ActionMetadata.RegionMap[keyof ActionMetadata.RegionMap];
  setRegion(value: ActionMetadata.RegionMap[keyof ActionMetadata.RegionMap]): void;

  getSchedule(): ActionMetadata.ScheduleMap[keyof ActionMetadata.ScheduleMap];
  setSchedule(value: ActionMetadata.ScheduleMap[keyof ActionMetadata.ScheduleMap]): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ActionMetadata.AsObject;
  static toObject(includeInstance: boolean, msg: ActionMetadata): ActionMetadata.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ActionMetadata, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ActionMetadata;
  static deserializeBinaryFromReader(message: ActionMetadata, reader: jspb.BinaryReader): ActionMetadata;
}

export namespace ActionMetadata {
  export type AsObject = {
    barsize: ActionMetadata.BarSizeMap[keyof ActionMetadata.BarSizeMap],
    region: ActionMetadata.RegionMap[keyof ActionMetadata.RegionMap],
    schedule: ActionMetadata.ScheduleMap[keyof ActionMetadata.ScheduleMap],
  }

  export interface BarSizeMap {
    INVALID_BARSIZE: 0;
    BS_1MIN: 1;
    BS_3MIN: 3;
    BS_5MIN: 5;
    BS_10MIN: 10;
    BS_15MIN: 15;
    BS_30MIN: 30;
    BS_1HOUR: 60;
    BS_1DAY: 100;
    BS_1WEEK: 105;
    BS_MONTH: 110;
    BS_IRREGULAR: 112;
    BS_NON_TIMESERIES: 113;
  }

  export const BarSize: BarSizeMap;

  export interface RegionMap {
    INVALID_REGION: 0;
    US: 1;
    EUROPE: 2;
    ASIA: 3;
    SOUTH_AMERICA: 4;
  }

  export const Region: RegionMap;

  export interface ScheduleMap {
    INVALID_SCHEDULE: 0;
    US_EAST: 1;
    SCHEDULE_24H: 2;
  }

  export const Schedule: ScheduleMap;
}

export class TSKey extends jspb.Message {
  hasPlainreference(): boolean;
  clearPlainreference(): void;
  getPlainreference(): string;
  setPlainreference(value: string): void;

  hasBuiltin(): boolean;
  clearBuiltin(): void;
  getBuiltin(): BuiltinMap[keyof BuiltinMap];
  setBuiltin(value: BuiltinMap[keyof BuiltinMap]): void;

  getDataformat(): DataFormatMap[keyof DataFormatMap];
  setDataformat(value: DataFormatMap[keyof DataFormatMap]): void;

  clearParamsList(): void;
  getParamsList(): Array<ParameterSpec>;
  setParamsList(value: Array<ParameterSpec>): void;
  addParams(value?: ParameterSpec, index?: number): ParameterSpec;

  hasSloc(): boolean;
  clearSloc(): void;
  getSloc(): SourceLocation | undefined;
  setSloc(value?: SourceLocation): void;

  hasStrongtype(): boolean;
  clearStrongtype(): void;
  getStrongtype(): StrongType | undefined;
  setStrongtype(value?: StrongType): void;

  hasPrev(): boolean;
  clearPrev(): void;
  getPrev(): TSKey | undefined;
  setPrev(value?: TSKey): void;

  getHashbin(): Uint8Array | string;
  getHashbin_asU8(): Uint8Array;
  getHashbin_asB64(): string;
  setHashbin(value: Uint8Array | string): void;

  getHashedcontent(): Uint8Array | string;
  getHashedcontent_asU8(): Uint8Array;
  getHashedcontent_asB64(): string;
  setHashedcontent(value: Uint8Array | string): void;

  getSeq(): number;
  setSeq(value: number): void;

  getKindCase(): TSKey.KindCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TSKey.AsObject;
  static toObject(includeInstance: boolean, msg: TSKey): TSKey.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: TSKey, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TSKey;
  static deserializeBinaryFromReader(message: TSKey, reader: jspb.BinaryReader): TSKey;
}

export namespace TSKey {
  export type AsObject = {
    plainreference: string,
    builtin: BuiltinMap[keyof BuiltinMap],
    dataformat: DataFormatMap[keyof DataFormatMap],
    paramsList: Array<ParameterSpec.AsObject>,
    sloc?: SourceLocation.AsObject,
    strongtype?: StrongType.AsObject,
    prev?: TSKey.AsObject,
    hashbin: Uint8Array | string,
    hashedcontent: Uint8Array | string,
    seq: number,
  }

  export enum KindCase {
    KIND_NOT_SET = 0,
    PLAINREFERENCE = 10,
    BUILTIN = 25,
  }
}

export class DestVariable extends jspb.Message {
  getName(): string;
  setName(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DestVariable.AsObject;
  static toObject(includeInstance: boolean, msg: DestVariable): DestVariable.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DestVariable, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DestVariable;
  static deserializeBinaryFromReader(message: DestVariable, reader: jspb.BinaryReader): DestVariable;
}

export namespace DestVariable {
  export type AsObject = {
    name: string,
  }
}

export class RowParamSpec extends jspb.Message {
  getName(): string;
  setName(value: string): void;

  hasValue(): boolean;
  clearValue(): void;
  getValue(): Expression | undefined;
  setValue(value?: Expression): void;

  clearNamedList(): void;
  getNamedList(): Array<ParameterSpec>;
  setNamedList(value: Array<ParameterSpec>): void;
  addNamed(value?: ParameterSpec, index?: number): ParameterSpec;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RowParamSpec.AsObject;
  static toObject(includeInstance: boolean, msg: RowParamSpec): RowParamSpec.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RowParamSpec, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RowParamSpec;
  static deserializeBinaryFromReader(message: RowParamSpec, reader: jspb.BinaryReader): RowParamSpec;
}

export namespace RowParamSpec {
  export type AsObject = {
    name: string,
    value?: Expression.AsObject,
    namedList: Array<ParameterSpec.AsObject>,
  }
}

export class StrategyLine extends jspb.Message {
  getLineno(): number;
  setLineno(value: number): void;

  hasVariable(): boolean;
  clearVariable(): void;
  getVariable(): DestVariable | undefined;
  setVariable(value?: DestVariable): void;

  clearDomainspecList(): void;
  getDomainspecList(): Array<DomainSpec>;
  setDomainspecList(value: Array<DomainSpec>): void;
  addDomainspec(value?: DomainSpec, index?: number): DomainSpec;

  clearActionsList(): void;
  getActionsList(): Array<TSKey>;
  setActionsList(value: Array<TSKey>): void;
  addActions(value?: TSKey, index?: number): TSKey;

  clearRowparamsList(): void;
  getRowparamsList(): Array<RowParamSpec>;
  setRowparamsList(value: Array<RowParamSpec>): void;
  addRowparams(value?: RowParamSpec, index?: number): RowParamSpec;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StrategyLine.AsObject;
  static toObject(includeInstance: boolean, msg: StrategyLine): StrategyLine.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StrategyLine, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StrategyLine;
  static deserializeBinaryFromReader(message: StrategyLine, reader: jspb.BinaryReader): StrategyLine;
}

export namespace StrategyLine {
  export type AsObject = {
    lineno: number,
    variable?: DestVariable.AsObject,
    domainspecList: Array<DomainSpec.AsObject>,
    actionsList: Array<TSKey.AsObject>,
    rowparamsList: Array<RowParamSpec.AsObject>,
  }
}

export class Strategy extends jspb.Message {
  clearLinesList(): void;
  getLinesList(): Array<StrategyLine>;
  setLinesList(value: Array<StrategyLine>): void;
  addLines(value?: StrategyLine, index?: number): StrategyLine;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Strategy.AsObject;
  static toObject(includeInstance: boolean, msg: Strategy): Strategy.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Strategy, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Strategy;
  static deserializeBinaryFromReader(message: Strategy, reader: jspb.BinaryReader): Strategy;
}

export namespace Strategy {
  export type AsObject = {
    linesList: Array<StrategyLine.AsObject>,
  }
}

export class MultiTSKey extends jspb.Message {
  clearKeyList(): void;
  getKeyList(): Array<TSKey>;
  setKeyList(value: Array<TSKey>): void;
  addKey(value?: TSKey, index?: number): TSKey;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): MultiTSKey.AsObject;
  static toObject(includeInstance: boolean, msg: MultiTSKey): MultiTSKey.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: MultiTSKey, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): MultiTSKey;
  static deserializeBinaryFromReader(message: MultiTSKey, reader: jspb.BinaryReader): MultiTSKey;
}

export namespace MultiTSKey {
  export type AsObject = {
    keyList: Array<TSKey.AsObject>,
  }
}

export class EQLTrace extends jspb.Message {
  hasTimestamp(): boolean;
  clearTimestamp(): void;
  getTimestamp(): Timestamp | undefined;
  setTimestamp(value?: Timestamp): void;

  getNode(): string;
  setNode(value: string): void;

  getModule(): string;
  setModule(value: string): void;

  getWhat(): string;
  setWhat(value: string): void;

  getDuration(): number;
  setDuration(value: number): void;

  getCount(): number;
  setCount(value: number): void;

  getDataSize(): number;
  setDataSize(value: number): void;

  hasCurrentkey(): boolean;
  clearCurrentkey(): void;
  getCurrentkey(): TSKey | undefined;
  setCurrentkey(value?: TSKey): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): EQLTrace.AsObject;
  static toObject(includeInstance: boolean, msg: EQLTrace): EQLTrace.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: EQLTrace, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): EQLTrace;
  static deserializeBinaryFromReader(message: EQLTrace, reader: jspb.BinaryReader): EQLTrace;
}

export namespace EQLTrace {
  export type AsObject = {
    timestamp?: Timestamp.AsObject,
    node: string,
    module: string,
    what: string,
    duration: number,
    count: number,
    dataSize: number,
    currentkey?: TSKey.AsObject,
  }
}

export class ActionDescription extends jspb.Message {
  getName(): string;
  setName(value: string): void;

  getHelp(): string;
  setHelp(value: string): void;

  clearInputmodesList(): void;
  getInputmodesList(): Array<ActionDescription.ModeDescrption>;
  setInputmodesList(value: Array<ActionDescription.ModeDescrption>): void;
  addInputmodes(value?: ActionDescription.ModeDescrption, index?: number): ActionDescription.ModeDescrption;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ActionDescription.AsObject;
  static toObject(includeInstance: boolean, msg: ActionDescription): ActionDescription.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ActionDescription, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ActionDescription;
  static deserializeBinaryFromReader(message: ActionDescription, reader: jspb.BinaryReader): ActionDescription;
}

export namespace ActionDescription {
  export type AsObject = {
    name: string,
    help: string,
    inputmodesList: Array<ActionDescription.ModeDescrption.AsObject>,
  }

  export class ParameterDescrption extends jspb.Message {
    getOptionalname(): string;
    setOptionalname(value: string): void;

    getTyp(): ActionParameterTypeDescriptionMap[keyof ActionParameterTypeDescriptionMap];
    setTyp(value: ActionParameterTypeDescriptionMap[keyof ActionParameterTypeDescriptionMap]): void;

    getEnablesymbolic(): boolean;
    setEnablesymbolic(value: boolean): void;

    getSymbolicinputtype(): PrimaryTypeMap[keyof PrimaryTypeMap];
    setSymbolicinputtype(value: PrimaryTypeMap[keyof PrimaryTypeMap]): void;

    getRaw(): boolean;
    setRaw(value: boolean): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ParameterDescrption.AsObject;
    static toObject(includeInstance: boolean, msg: ParameterDescrption): ParameterDescrption.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ParameterDescrption, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ParameterDescrption;
    static deserializeBinaryFromReader(message: ParameterDescrption, reader: jspb.BinaryReader): ParameterDescrption;
  }

  export namespace ParameterDescrption {
    export type AsObject = {
      optionalname: string,
      typ: ActionParameterTypeDescriptionMap[keyof ActionParameterTypeDescriptionMap],
      enablesymbolic: boolean,
      symbolicinputtype: PrimaryTypeMap[keyof PrimaryTypeMap],
      raw: boolean,
    }
  }

  export class ModeDescrption extends jspb.Message {
    getInputtype(): PrimaryTypeMap[keyof PrimaryTypeMap];
    setInputtype(value: PrimaryTypeMap[keyof PrimaryTypeMap]): void;

    clearParametersList(): void;
    getParametersList(): Array<ActionDescription.ParameterDescrption>;
    setParametersList(value: Array<ActionDescription.ParameterDescrption>): void;
    addParameters(value?: ActionDescription.ParameterDescrption, index?: number): ActionDescription.ParameterDescrption;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ModeDescrption.AsObject;
    static toObject(includeInstance: boolean, msg: ModeDescrption): ModeDescrption.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ModeDescrption, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ModeDescrption;
    static deserializeBinaryFromReader(message: ModeDescrption, reader: jspb.BinaryReader): ModeDescrption;
  }

  export namespace ModeDescrption {
    export type AsObject = {
      inputtype: PrimaryTypeMap[keyof PrimaryTypeMap],
      parametersList: Array<ActionDescription.ParameterDescrption.AsObject>,
    }
  }
}

export class LanguageInfo extends jspb.Message {
  clearActiondescriptionList(): void;
  getActiondescriptionList(): Array<ActionDescription>;
  setActiondescriptionList(value: Array<ActionDescription>): void;
  addActiondescription(value?: ActionDescription, index?: number): ActionDescription;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LanguageInfo.AsObject;
  static toObject(includeInstance: boolean, msg: LanguageInfo): LanguageInfo.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: LanguageInfo, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LanguageInfo;
  static deserializeBinaryFromReader(message: LanguageInfo, reader: jspb.BinaryReader): LanguageInfo;
}

export namespace LanguageInfo {
  export type AsObject = {
    actiondescriptionList: Array<ActionDescription.AsObject>,
  }
}

  export const serialization: jspb.ExtensionFieldInfo<SerializationHints>;

export interface BuiltinMap {
  BUILTIN_INVALID: 0;
  BUILTIN_EQUEUM: 1;
  BUILTIN_SYMBOLIC: 2;
  BUILTIN_GETTS: 10;
  BUILTIN_GETSET: 11;
  BUILTIN_MULTIKEY: 12;
  BUILTIN_GETSECLIST: 13;
  BUILTIN_SUMCOUNT: 14;
  BUILTIN_SUMCOUNT2: 15;
  BUILTIN_AVG2: 16;
  BUILTIN_GETSECNAMES: 17;
  BUILTIN_INDEX: 18;
  BUILTIN_SYOP: 19;
  BUILTIN_NOOP: 20;
  BUILTIN_SYMBOL_LITERAL: 21;
  BUILTIN_STRING_LITERAL: 22;
  BUILTIN_NUMERIC_LITERAL: 23;
  BUILTIN_DISPLAY_VARREF: 24;
  BUILTIN_AVG_GROUPLISTS: 25;
  BUILTIN_FUNCTION_LITERAL: 26;
  BUILTIN_SECLIST_LITERAL: 27;
  PRI: 50;
}

export const Builtin: BuiltinMap;

export interface PrimaryTypeMap {
  INVALID_TYPE: 0;
  TIMESERIES_NUMERIC: 1;
  TIMESERIES_SECLIST: 5;
  SYMBOLIC: 7;
  TIMESERIES_KEYLIST: 8;
  SCALAR_NUMBER: 10;
  SCALAR_STRING: 11;
  SCALAR_NUMBER_RANGE: 12;
  SCALAR_DATE: 13;
  SCALAR_ARRAY: 18;
  NIL: 20;
  SCALAR_INSTRUMENT: 22;
  SCALAR_UNIVERSE: 23;
  SCALAR_TIME_PERIOD: 24;
  SCALAR_FUNCTION: 25;
}

export const PrimaryType: PrimaryTypeMap;

export interface DataFormatMap {
  INVALID_DATAFORMAT: 0;
  DOUBLES: 9;
  FLOATS: 10;
  INTS32: 11;
  INTS64: 12;
  BYTES: 13;
  BITS: 14;
  INTS16: 15;
  DATES: 16;
  NORMBYTE: 20;
  COMPBITS12: 30;
  COMPBITS16: 31;
  COMPBITS8: 32;
  SECLISTS: 35;
  KEYS_ONLY: 36;
  KEYSLISTS: 37;
  TSKEYS_STRINGS: 38;
  NILFORMAT: 39;
  LISTLIST: 40;
  GROUPLISTS: 41;
}

export const DataFormat: DataFormatMap;

export interface ActionParameterTypeDescriptionMap {
  APTYPE_INVALID: 0;
  APTYPE_STRING: 10;
  APTYPE_DOUBLE: 20;
  APTYPE_INT: 30;
  APTYPE_ID: 40;
  APTYPE_TIMESPAN: 50;
  APTYPE_TIMESERIES: 60;
  APTYPE_RANGE: 70;
  APTYPE_LIST: 80;
  APTYPE_OTHER: 120;
}

export const ActionParameterTypeDescription: ActionParameterTypeDescriptionMap;

