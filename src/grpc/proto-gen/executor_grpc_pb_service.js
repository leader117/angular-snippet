// package: grpcfacade
// file: executor_grpc.proto

var executor_grpc_pb = require("./executor_grpc_pb");
var eqltree_pb = require("./eqltree_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var ExecutorService = (function () {
  function ExecutorService() {}
  ExecutorService.serviceName = "grpcfacade.ExecutorService";
  return ExecutorService;
}());

ExecutorService.SubmitStrategy = {
  methodName: "SubmitStrategy",
  service: ExecutorService,
  requestStream: false,
  responseStream: false,
  requestType: executor_grpc_pb.EQLEngine.SubmitRequest,
  responseType: executor_grpc_pb.EQLEngine.SubmitResponse
};

ExecutorService.SubmitStrategyN = {
  methodName: "SubmitStrategyN",
  service: ExecutorService,
  requestStream: false,
  responseStream: false,
  requestType: executor_grpc_pb.EQLEngine.SubmitNRequest,
  responseType: executor_grpc_pb.EQLEngine.SubmitNResponse
};

ExecutorService.SubmitUI = {
  methodName: "SubmitUI",
  service: ExecutorService,
  requestStream: false,
  responseStream: false,
  requestType: executor_grpc_pb.EQLEngine.SubmitUIRequest,
  responseType: executor_grpc_pb.EQLEngine.SubmitNResponse
};

ExecutorService.RetrieveResult = {
  methodName: "RetrieveResult",
  service: ExecutorService,
  requestStream: false,
  responseStream: false,
  requestType: executor_grpc_pb.EQLEngine.TaskRequest,
  responseType: executor_grpc_pb.EQLEngine.StatusResponse
};

ExecutorService.RetrieveResultN = {
  methodName: "RetrieveResultN",
  service: ExecutorService,
  requestStream: false,
  responseStream: false,
  requestType: executor_grpc_pb.EQLEngine.TaskRequest,
  responseType: executor_grpc_pb.EQLEngine.StatusNResponse
};

ExecutorService.CancelTask = {
  methodName: "CancelTask",
  service: ExecutorService,
  requestStream: false,
  responseStream: false,
  requestType: executor_grpc_pb.EQLEngine.TaskRequest,
  responseType: executor_grpc_pb.EQLEngine.StatusResponse
};

ExecutorService.GetLanguageInfo = {
  methodName: "GetLanguageInfo",
  service: ExecutorService,
  requestStream: false,
  responseStream: false,
  requestType: executor_grpc_pb.EQLEngine.LanguageInfoRequest,
  responseType: eqltree_pb.LanguageInfo
};

exports.ExecutorService = ExecutorService;

function ExecutorServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

ExecutorServiceClient.prototype.submitStrategy = function submitStrategy(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(ExecutorService.SubmitStrategy, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

ExecutorServiceClient.prototype.submitStrategyN = function submitStrategyN(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(ExecutorService.SubmitStrategyN, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

ExecutorServiceClient.prototype.submitUI = function submitUI(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(ExecutorService.SubmitUI, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

ExecutorServiceClient.prototype.retrieveResult = function retrieveResult(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(ExecutorService.RetrieveResult, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

ExecutorServiceClient.prototype.retrieveResultN = function retrieveResultN(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(ExecutorService.RetrieveResultN, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

ExecutorServiceClient.prototype.cancelTask = function cancelTask(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(ExecutorService.CancelTask, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

ExecutorServiceClient.prototype.getLanguageInfo = function getLanguageInfo(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(ExecutorService.GetLanguageInfo, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.ExecutorServiceClient = ExecutorServiceClient;

