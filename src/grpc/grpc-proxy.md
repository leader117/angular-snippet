1. To compile the proto files:
   ./node_modules/protoc/protoc/bin/protoc --plugin=protoc-gen-ts=.\\node_modules\\.bin\\protoc-gen-ts.cmd --js_out=import_style=commonjs,binary:src/grpc/proto-gen --ts_out="service=true:src/grpc/proto-gen" -I ./src/grpc/proto ./src/grpc/proto/*.proto
2. To install GO (version 1.9) https://medium.com/@patdhlk/how-to-install-go-1-8-on-ubuntu-16-04-710967aa53c9
3. To setup grpc proxy https://github.com/improbable-eng/grpc-web/tree/master/go/grpcwebproxy
4. Run gRPC proxy on port 8080
    add the ~/go/bin to Path
    grpcwebproxy --backend_addr=35.237.182.77:8058 --run_tls_server=false --allow_all_origins=true
	nohup grpcwebproxy --backend_addr=35.237.182.77:8058 --run_tls_server=true --allow_all_origins=true --server_tls_cert_file=/etc/ssl/certs/equeum.pem --server_tls_key_file=/etc/ssl/certs/equeum.key --backend_tls_noverify &
