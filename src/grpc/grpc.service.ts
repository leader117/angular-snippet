import { Injectable } from '@angular/core';
import { ExecutorServiceClient, ServiceError } from './proto-gen/executor_grpc_pb_service';
import { DataFormat, ActionMetadata, PrimaryType } from './proto-gen/eqltree_pb';
import { EQLEngine } from './proto-gen/executor_grpc_pb';
import { ExecutorValue, StrategyContext, Interleaving, ExecutionMode, LanguageLevel } from './proto-gen/execution_pb';
import { encodeMomentToBuffer, parseTimestampArray, parseDoublesArray, parseBacktestFields, parseHistogramArray } from './helpers/buffer-parser';
import * as moment from 'moment-timezone';
import { wrapToPromise, UintArrayToString, Sleep, ArrayNoRepeats } from 'src/scripts/utilities';
import { EqlSettings } from 'src/applications/model.builder/entries/EqlSettings';

export interface SecListTrancheData {
    name: string;
    width: number;
    rows: any[];
}
export interface SecListResult {
    code: string;
    name: string;
    data: Array<SecListTrancheData>;
    dates: Array<string>;
}
export interface UIPartResult {
    uiPart: EQLEngine.UIPartMap[keyof EQLEngine.UIPartMap];
    req?: EQLEngine.SubmitRequest;
    value: ExecutorValue;
    series?: Array<any>;
    histogram?: Array<{ name: string, data: { range: string, value: number }[] }>;
    secLists?: SecListResult;
    barSize: EQLEngine.RepresentationBarSizeMap[keyof EQLEngine.RepresentationBarSizeMap];
    outputContext: StrategyContext;
}
export class SubmitUIRequestWithHandle extends EQLEngine.SubmitUIRequest {
    taskHandle: string;
    cancelSignalRequested?: boolean;
    onProgress: (p: string) => void;
}

export interface EngineFunctionInfo {
    name: string;
    help: string;
    parameters: { name: string, help: string }[];
}
@Injectable()
export class GRPCService {
    client: ExecutorServiceClient;
    constructor() {
        this.client = new ExecutorServiceClient('');
    }
    async getLanguageInfo(): Promise<EngineFunctionInfo[]> {
        const task = (lReq, cb) => this.client.getLanguageInfo(lReq, (err, res) => {
            if (err) return cb(err);
            const funcs = res.getActiondescriptionList().map(c => {
                return {
                    name: c.getName(),
                    help: c.getHelp(),
                    parameters: ArrayNoRepeats(c.getInputmodesList()[0].getParametersList().map(p => ({
                        name: p.getOptionalname(),
                        // tslint:disable-next-line: max-line-length
                        help: p.getSymbolicinputtype() ? `${p.getOptionalname()}: ${this.getPrimaryTypeName(p.getSymbolicinputtype())}` : p.getOptionalname(),
                    })), 'name')
                };
            });
            cb(undefined, funcs);
        });
        return wrapToPromise(task, new EQLEngine.LanguageInfoRequest());
    }
    getErrorOfExecutorValue(res: ExecutorValue): string {
        if (res.getProcessingerror()) return res.getProcessingerror();
    }
    getErrorOfStatusResponse(res: EQLEngine.StatusResponse): string {
        const result = res.getCompletionresult().getResult();
        if (result && result.getProcessingerror()) return result.getProcessingerror();
    }
    getErrorOfSubmitResponse(res: EQLEngine.SubmitResponse): string {
        if (res.getParseerror()) return res.getParseerror().getError();
    }
    retrieveResult(taskHandler: string, callback: (err: string, result?: ExecutorValue) => void) {
        const req = new EQLEngine.TaskRequest();
        req.setTaskhandle(taskHandler);
        this.client.retrieveResult(req, (err, result) => {
            if (err) return callback(this.getErrorMessage(err));
            if (this.getErrorOfStatusResponse(result)) return callback(this.getErrorOfStatusResponse(result));
            const comp = result.getCompletionresult();
            if (comp.getCompleted()) return callback(undefined, comp.getResult());
            setTimeout(() => this.retrieveResult(taskHandler, callback), 100);
        });
    }
    retreiveResultsCallback(callback: (err: string, result?: ExecutorValue) => void) {
        return (err: ServiceError, result: EQLEngine.SubmitResponse) => {
            if (err) return callback(this.getErrorMessage(err));
            if (this.getErrorOfSubmitResponse(result)) return callback(this.getErrorOfSubmitResponse(result));
            this.retrieveResult(result.getTaskhandle(), callback);
        };
    }
    submitStrategy(req: EQLEngine.SubmitRequest, callback: (err: string, result?: ExecutorValue) => void) {
        this.client.submitStrategy(req, this.retreiveResultsCallback(callback));
    }
    getSubmitRequest(eqlCode: string, settings: EqlSettings): EQLEngine.SubmitRequest {
        const start = encodeMomentToBuffer(moment(settings.startDate).startOf('month'));
        const end = encodeMomentToBuffer(moment(settings.endDate).endOf('month').add(1, 'day').startOf('days'));

        const context = new StrategyContext();
        context.setExecutionmode(ExecutionMode.STANDARD_EXECUTION_MODE);
        context.setStarttimebin(start);
        context.setEndtimebin(end);
        context.setDataformat(DataFormat.DOUBLES);
        context.setArraysize(-1);
        context.setContextname('!');
        context.setLanguagelevel(LanguageLevel.LL_REVISION_SUMMER_2019);

        const metaData = new ActionMetadata();
        metaData.setBarsize(settings.barSize || ActionMetadata.BarSize.BS_1MIN);
        metaData.setRegion(ActionMetadata.Region.US);
        metaData.setSchedule(ActionMetadata.Schedule.US_EAST);
        context.setInitialmetadata(metaData);

        const req = this.getSubmitRequestForContext(eqlCode, context);
        req.setTarget(`${settings.targetSymbol}(${settings.targetField || 'c'})`);
        return req;
    }
    getSubmitRequestForContext(eqlCode: string, context: StrategyContext): EQLEngine.SubmitRequest {
        const req = new EQLEngine.SubmitRequest();
        req.setEql(eqlCode);
        req.setContext(context);

        const rep = new EQLEngine.ResultRepresentation();
        rep.setDesiredfullresultsize(100);
        req.setResultrepresentation(rep);
        rep.setSeclistrangestart(0);
        rep.setSeclistrangeend(100);
        rep.setSecliststarttranche(0);
        rep.setSeclistendtranche(100);

        const secContext = new EQLEngine.SecurityContext();
        secContext.setEffectiveuserid('');
        req.setSecuritycontext(secContext);
        return req;
    }
    async cancelTask(taskHandle: string) {
        const rr = new EQLEngine.TaskRequest();
        rr.setTaskhandle(taskHandle);
        const task = (r, cb) => this.client.cancelTask(rr, cb);
        return wrapToPromise(task, taskHandle);
    }
    getRepresentationBarSize(startDate: string | number | Date, endDate: string | number | Date) {
        const daysDiff = moment(endDate).diff(moment(startDate), 'days');
        // console.log(moment(startDate).format(), moment(endDate).format(), daysDiff);
        // if (daysDiff <= 1) return EQLEngine.RepresentationBarSize.RBS_1MIN;
        if (daysDiff <= 5) return EQLEngine.RepresentationBarSize.RBS_3MIN;
        if (daysDiff <= 31) return EQLEngine.RepresentationBarSize.RBS_15MIN;
        if (daysDiff <= 365) return EQLEngine.RepresentationBarSize.RBS_1DAY;
        if (daysDiff <= 7 * 365) return EQLEngine.RepresentationBarSize.RBS_1WEEK;
        return EQLEngine.RepresentationBarSize.RBS_1MONTH;
    }
    executorValueParse(data: ExecutorValue) {
        const ts = data.getTs();
        const dates = parseTimestampArray(ts.getTimestamparray_asU8());
        const names = ts.getArrayList().map((s, i) => (s.getData().getColname() || 'ts') + `:${i}`);
        const timeSeries = [];
        const histogramSeries = [];
        ts.getArrayList().map((arr, i) => {
            const chart = arr.getData();
            const dataFormat = chart.getDataformat();
            const isOHLC = chart.getInterleaving() === Interleaving.INTERLEAVING_OHLC;
            const isHistogram = chart.getInterleaving() === Interleaving.INTERLEAVING_FROM_TO_COUNT;
            if (isHistogram) {
                const histogramName = chart.getColname() || `Group ${i + 1}`;
                histogramSeries.push({ name: histogramName, data: parseHistogramArray(chart.getData_asB64()) });
            }
            else if (dataFormat === DataFormat.DOUBLES) {
                timeSeries.push(parseDoublesArray(chart.getData_asB64(), isOHLC));
            }
        });
        const result = dates.map((d, i) => {
            const obj = { date: d };
            names.filter((n, j) => timeSeries[j]).forEach((n, j) => (obj[n] = timeSeries[j][i]));
            return obj;
        }).filter(d => moment(d.date).isValid());
        return {
            histogramSeries,
            timeSeries: result,
        };
    }
    executorValueParseSecLists(data: ExecutorValue, index: number): SecListResult {
        const ts = data.getTs();
        const items = ts.getArrayList().filter(arr => arr.getData().getDataformat() === DataFormat.SECLISTS);
        let maxWidth;
        let dates;
        const gridRows = items.map((arr, i) => {
            maxWidth = arr.getData().getMaxseclistwidth();
            dates = parseTimestampArray(arr.getData().getLocaltimestart_asU8());
            const rows = UintArrayToString(arr.getData().getData_asU8()).split('\n').filter(r => r).map(r => r.split('\t'));
            dates.length = rows.length;
            const obj = {};
            dates.forEach((d, j) => (obj[d] = rows[j]));

            const tranche = { name: `Tranche ${i + 1}`, width: maxWidth, rows: [] };
            let symbolCount = 0;
            rows.forEach(r => symbolCount = Math.max(symbolCount, r.length));
            for (let j = 0; j < symbolCount; j++) {
                const row = {};
                dates.forEach(c => row[c] = obj[c][j]);
                tranche.rows.push(row);
            }
            return tranche;
        });
        return {
            code: `Sec List ${index}`,
            name: `Sec List ${index}`,
            data: gridRows,
            dates,
        };
    }
    uIPartResultParse(data: UIPartResult): any {
        const ts = data.value.getTs().getArrayList();
        if (data.uiPart === EQLEngine.UIPart.UIPART_BACKTEST) return parseBacktestFields(ts);
        return this.executorValueParse(data.value);
    }
    /* Next for N Strategy functions */
    getErrorOfStatusNResponse(res: EQLEngine.StatusNResponse): string {
        const error = res.getCompletionresultList().map(c => {
            const result = c.getResult();
            if (result && result.getProcessingerror()) return result.getProcessingerror();
        }).filter(s => s).join(';\n');
        return error;
    }
    getErrorOfSubmitNResponse(res: EQLEngine.SubmitNResponse): string {
        if (res.getParseerror()) return res.getParseerror().getError();
    }
    retrieveResultN(uiReq: SubmitUIRequestWithHandle, callback: (err: string, result?: UIPartResult[]) => void) {
        if (uiReq.cancelSignalRequested) return callback('Task cancelled');
        const req = new EQLEngine.TaskRequest();
        req.setTaskhandle(uiReq.taskHandle);
        this.client.retrieveResultN(req, (err, result) => {
            if (err) return callback(this.getErrorMessage(err));
            if (this.getErrorOfStatusNResponse(result)) return callback(this.getErrorOfStatusNResponse(result));
            const comp = result.getCompletionresultList();
            if (comp.every(c => c.getCompleted())) {
                const list = comp.map(c => ({
                    uiPart: c.getResultbucket(),
                    value: c.getResult(),
                    barSize: c.getBarsize(),
                    outputContext: c.getOutputcontext(),
                }));
                return callback(undefined, list);
            }
            if (uiReq.onProgress) uiReq.onProgress(result.getProgresstexttechnical());
            setTimeout(() => this.retrieveResultN(uiReq, callback), 100);
        });
    }
    retreiveResultNCallback(uiReq: SubmitUIRequestWithHandle, callback: (err: string, result?: UIPartResult[]) => void) {
        return (err: ServiceError, result: EQLEngine.SubmitNResponse) => {
            if (err) return callback(this.getErrorMessage(err));
            if (this.getErrorOfSubmitNResponse(result)) return callback(this.getErrorOfSubmitNResponse(result));
            uiReq.taskHandle = result.getTaskhandle();
            this.retrieveResultN(uiReq, callback);
        };
    }
    getUIPartName(part: EQLEngine.UIPartMap[keyof EQLEngine.UIPartMap]): string {
        switch (part) {
            case EQLEngine.UIPart.UIPART_BACKTEST: return 'Backtest';
            case EQLEngine.UIPart.UIPART_INVALID: return 'Invalid';
            case EQLEngine.UIPart.UIPART_MAIN_TIMESERIES: return 'Main Timeseries';
            case EQLEngine.UIPart.UIPART_PROFITLOSS: return 'Profit/Loss';
            case EQLEngine.UIPart.UIPART_RETURNS: return 'Returns';
            case EQLEngine.UIPart.UIPART_SHARPE: return 'Sharpe';
            case EQLEngine.UIPart.UIPART_VOLUME: return 'Volume';
            case EQLEngine.UIPart.UIPART_TARGET: return 'Target';
            case EQLEngine.UIPart.UIPART_SECLIST_AVERAGES: return 'Sec List';
            default: throw new Error('Unknown UI Part');
        }
    }
    getSubmitUIRequest(req: EQLEngine.SubmitRequest, parts: EQLEngine.UIPartMap[keyof EQLEngine.UIPartMap][]): SubmitUIRequestWithHandle {
        const uiReq = new SubmitUIRequestWithHandle();
        uiReq.setEqlrequest(req);
        parts.map(p => {
            const part1 = new EQLEngine.UIPartOption();
            part1.setPart(p);
            uiReq.addParts(part1);
        });
        return uiReq;
    }
    submitUI(uiReq: SubmitUIRequestWithHandle, callback: (err: string, result?: UIPartResult[]) => void) {
        this.client.submitUI(uiReq, this.retreiveResultNCallback(uiReq, callback));
    }
    submitUIParse(req: SubmitUIRequestWithHandle, callback) {
        this.submitUI(req, (err, data) => {
            if (err) return callback(err);
            const badItems = data.filter(d => !d.value || !d.uiPart);
            if (badItems.length) console.error('Engine return wrong TS', badItems);
            data = data.filter(d => d.value && d.uiPart);
            data.forEach((d, i) => d.req = req.getEqlrequest());
            data.forEach(d => {
                const { histogramSeries, timeSeries } = this.uIPartResultParse(d);
                d.histogram = histogramSeries;
                d.series = timeSeries;
            });
            data.forEach((d, i) => d.secLists = this.executorValueParseSecLists(d.value, i));
            callback(undefined, data);
        });
    }
    submitUIParseAsync(req: SubmitUIRequestWithHandle): Promise<Array<UIPartResult>> {
        const task = (r, cb) => this.submitUIParse(r, cb);
        return wrapToPromise(task, req);
    }
    private getErrorMessage(err: any) {
        if (!err) return null;
        if (typeof err === 'string') return err;
        if (err.message) return err.message;
        return 'Unknown Error';
    }
    private getPrimaryTypeName(num) {
        return Object.keys(PrimaryType).find(k => PrimaryType[k] === num);
    }
}
