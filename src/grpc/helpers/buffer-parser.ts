import { formatNumberTwoDigits, formatNumberFourDigits } from 'src/scripts/utilities';
import * as moment from 'moment-timezone';
import { Buffer } from 'buffer';
import { ExecutorValue } from '../proto-gen/execution_pb';

export function parseTimestampArray(int8Arr: Uint8Array) {
    const arr = [];
    for (let i = 0; i < int8Arr.length; i += 8) {
        const year = formatNumberFourDigits(int8Arr[i + 1] * 256 + int8Arr[i]);
        const month = formatNumberTwoDigits(int8Arr[i + 2]);
        const day = formatNumberTwoDigits(int8Arr[i + 3]);
        const hour = formatNumberTwoDigits(int8Arr[i + 4]);
        const minute = formatNumberTwoDigits(int8Arr[i + 5]);
        const second = formatNumberTwoDigits(int8Arr[i + 6]);
        arr.push(`${year}-${month}-${day}T${hour}:${minute}:${second}`);
    }
    return arr;
}

export function parseDoublesArray(b64: string, isOHLC = false) {
    let arr = [];
    const buffer = Buffer.from(b64, 'base64');
    for (let i = 0; i < buffer.length; i += 8) arr.push(buffer.readDoubleLE(i));
    arr = arr.map(r => Number.isNaN(r) ? null : r);
    // console.log('arr', arr);
    if (isOHLC) {
        const ohlc = [];
        for (let i = 0; i < arr.length; i += 4){
            ohlc.push([arr[i + 0], arr[i + 1], arr[i + 2], arr[i + 3]]);
        }
        return ohlc;
    } else return arr;
}

export function parseHistogramArray(b64: string): Array<{ range: string, value: number }> {
    let arr = [];
    const buffer = Buffer.from(b64, 'base64');
    for (let i = 0; i < buffer.length; i += 8) arr.push(buffer.readDoubleLE(i));
    arr = arr.map(r => Number.isNaN(r) ? null : r);
    const histogram = [];
    for (let i = 0; i < arr.length; i += 3) {
        histogram.push({ range: `${_round2(arr[i])} to ${_round2(arr[i + 1])}`, value: _round2(arr[i + 2])});
    }
    return histogram;
}
function _round2(n: number) {
    return Math.round(n * 100) / 100;
}
export function encodeMomentToBuffer(momentTime: moment.Moment): Uint8Array {
    // return Uint8Array.from(momentTime.toArray());
    const buffer = Buffer.alloc(8);
    buffer.writeInt16LE(momentTime.year(), 0);
    buffer.writeInt8(momentTime.month() + 1, 2);
    buffer.writeInt8(momentTime.date(), 3);
    buffer.writeInt8(momentTime.hour(), 4);
    buffer.writeInt8(momentTime.minute(), 5);
    buffer.writeInt8(momentTime.second(), 6);
    return Uint8Array.from(buffer);
}

const backtestFields = [
    'numTrades', // Show in table
    'numProfitableTrades',
    'numUnprofitableTrades',
    'tradesPerDay', // Show in table
    'totalReturns', // Show in table
    'benchReturns',
    'maxUnrealizedTotalReturns',
    'annualReturns', // Show in table
    'annualizedSharpe', // Show in table
    'maxDrawdown',
    'annualVolatility',
    'downsideRisk',
    'sortino',
];

export function parseBacktestFields(tsArray: Array<ExecutorValue.SingleTimeSeries>) {
    const obj = {};
    tsArray.forEach((arr, i) => {
        const chart = arr.getData();
        const name = backtestFields[i];
        if (!name) return;
        obj[name] = parseDoublesArray(chart.getData_asB64())[0];
    });
    return obj;
}
